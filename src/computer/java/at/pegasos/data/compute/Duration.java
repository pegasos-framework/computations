package at.pegasos.data.compute;

import java.util.Collection;

import at.pegasos.computer.Computer;
import at.pegasos.computer.SessionComputation;
import at.pegasos.computer.ValueComputation;
import at.pegasos.data.Data;
import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;

@SessionComputation
public class Duration implements ValueComputation {

  private long duration;

  public String getName()
  {
    return "DURATION";
  }

  @Override
  public void compute(Computer computer)
  {
    Data data= computer.getData();
    duration= data.getEnd() - data.getBegin();
    switch( data.getTimeUnit() )
    {
      case MicroSecond:
        duration/= 1000;
        break;
      case MilliSecond:
        break;
      case Second:
        duration*= 1000;
        break;
      default:
        break;
    }
  }

  @Override
  public Value getResult()
  {
    return new NumberValue(getName(), duration);
  }

  /**
   * @returns null as there are no dependencies
   */
  @Override
  public Collection<String> getDependencies()
  {
    return null;
  }
}
