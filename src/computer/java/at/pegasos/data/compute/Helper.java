package at.pegasos.data.compute;

public class Helper {
  
  /**
   * Return object as double. If obj is null, 0 will be returned.
   * 
   * @param obj
   *          object to be converted
   * @return double value of object
   */
  public static Double asDouble(Object obj)
  {
    if( obj == null )
      return 0d;
    else if( obj instanceof Double )
      return (Double) obj;
    else if( obj instanceof Integer )
      return ((Integer) obj).doubleValue();
    
    return null;
  }
  
  /**
   * Save cast of object to double. If object is null return null
   * 
   * @param obj
   *          Object to be cased
   * @return object casted to double or null if obj is null
   */
  public static Double asDoubleOrNull(Object obj)
  {
    if( obj == null )
      return null;
    else if( obj instanceof Double )
      return (Double) obj;
    else if( obj instanceof Integer )
      return ((Integer) obj).doubleValue();
    
    return null;
  }
}
