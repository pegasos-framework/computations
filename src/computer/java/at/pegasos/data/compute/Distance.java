package at.pegasos.data.compute;

import java.util.Collection;

import at.pegasos.computer.Computer;
import at.pegasos.computer.ValueComputation;
import at.pegasos.data.Data;
import at.pegasos.data.Data.ColumnListIterator;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;

/**
 * Compute the distance in metres
 * @outputs DISTANCE
 */
public class Distance implements ValueComputation {
  
  private Double dist;
  
  public String getName()
  {
    return "DISTANCE";
  }
  
  @Override
  public void compute(Computer computer)
  {
    String col= null;
    boolean gps= false;
    
    Data data= computer.getData();
    if( data.hasColumn("bike_distance_m") )
      col= "bike_distance_m";
    else if( data.hasColumn("foot_pod_distance_m") )
      col= "foot_pod_distance_m";
    else if( data.hasColumn("distance_m") )
      col= "distance_m";
    else if( data.hasColumn("gps_lat") && data.hasColumn("gps_lon") )
    {
      int latIdx= data.getColumnIndex("gps_lat");
      int lonIdx= data.getColumnIndex("gps_lon");
      double lastLat= 0, lastLon= 0;
      double dist2= 0;
      TableIterator tid= data.iterateTable();
      while( tid.hasNext() )
      {
        Object[] row= tid.next();
        if( row[latIdx] != null && row[lonIdx] != null )
        {
          double lat= Helper.asDouble(row[latIdx]);
          double lon= Helper.asDouble(row[lonIdx]);
          
          if( lastLat != 0 )
          {
            dist2+= distance(lat, lon, lastLat, lastLon);
          }
          lastLat= lat;
          lastLon= lon;
        }
      }
      System.out.println("dist2= " + dist2);
      dist= dist2;
      gps= true;
    }
    else
    {
      // data.debug();
      System.err.println("No distance column " + java.util.Arrays.toString(data.getColumnNames()));
      return;
    }
    
    // TODO: segmented data
    
    if( !gps )
    {
      ColumnListIterator lit= data.iterateTableColumnList(col, true);
      dist= 0d;
      while( lit.hasPrevious() )
      {
        Object o= lit.previous();
        Double d= Helper.asDoubleOrNull(o);
        if( d != null )
        {
          dist= d.doubleValue();
          break;
        }
      }
    }
    System.out.println("dist= " + dist);
  }
  
  @Override
  public Value getResult()
  {
    if( dist != null )
      return new NumberValue(getName(), dist);
    else
      return null;
  }

  /**
   * @returns null as there are no dependencies
   */
  @Override
  public Collection<String> getDependencies()
  {
    return null;
  }
  
  /**
   * Calculates the distance between two positions.
   *
   * @param lat1
   *          Latitude of the first position
   * @param lon1
   *          Longitude of the first position
   * @param lat2
   *          Latitude of the second position
   * @param lon2
   *          Longitude of the second position
   * @return Distance between both positions in metres
   */
  public static double distance(double lat1, double lon1, double lat2, double lon2)
  {
    double dLat= Math.toRadians(lat2 - lat1);
    double dLon= Math.toRadians(lon2 - lon1);
    lat1= Math.toRadians(lat1);
    lat2= Math.toRadians(lat2);
    
    double a= Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
    double c= 2 * Math.asin(Math.sqrt(a));
    
    return EQUATORIAL_EARTH_RADIUS * c * 1000.0;
  }
  
  private static final double EQUATORIAL_EARTH_RADIUS= 6371;
}
