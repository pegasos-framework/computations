package at.pegasos.data.compute;

import java.util.ArrayList;
import java.util.List;

/**
 * Compute a sum for a timed window
 */
public class TimedMovingSum {
  
  /**
   * Moving size
   */
  private final long duration;
  
  List<Double> sums;
  List<Long> times;
  
  /**
   * Current begin of the moving average field
   */
  private long from;
  
  /**
   * Sum of the values in the field
   */
  double sum= 0;
  
  long lastTime;
  
  /**
   * Create a new timed moving sum
   * 
   * @param duration
   *          window size
   * @param begin
   *          begin of the first window.
   */
  public TimedMovingSum(long duration, long begin)
  {
    this.duration= duration;
    sums= new ArrayList<Double>();
    times= new ArrayList<Long>();
    lastTime= from= begin;
  }
  
  /**
   * Add value to the sum. It is important that values are added in time ascending order
   * 
   * @param currTime
   *          time of the value
   * @param value
   *          value
   */
  public void addValue(long currTime, double value)
  {
    // TODO: for times other than 1s this would need interpolation as we could remove more than necessary
    while( currTime - from > duration && sums.size() > 0 )
    {
      sum-= sums.remove(0);
      from= times.remove(0);
    }
    
    sum+= value;
    
    lastTime= currTime;
    
    sums.add(value);
    times.add(currTime);
    
    // ma= (sum / duration);
  }
  
  /**
   * Get sum of current window
   * 
   * @return sum
   */
  public double getMovingSum()
  {
    return sum;
  }
}