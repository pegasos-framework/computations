package at.pegasos.data.compute;

import java.util.ArrayList;
import java.util.List;

/**
 * Compute a time weighted moving average of a value
 */
public class TimedMovingAverage {
  
  /**
   * Moving size
   */
  private final long duration;
  
  List<Double> sums;
  List<Long> times;
  
  /**
   * Current begin of the moving average field
   */
  private long from;
  
  /**
   * Sum of the values in the field
   */
  double sum= 0;
  
  long lastTime;
  
  double ma;
  
  /**
   * Create a new moving average
   * 
   * @param duration
   *          window size in [ms]
   * @param begin
   *          start of the time series (what is zero). Miss specification should only have an affect
   *          on the first window
   */
  public TimedMovingAverage(long duration, long begin)
  {
    this.duration= duration;
    sums= new ArrayList<Double>();
    times= new ArrayList<Long>();
    lastTime= from= begin;
  }
  
  /**
   * Add a value to the computation. It is important that values are added in time ascending order
   * 
   * @param currTime
   *          time of the value
   * @param value
   *          value
   */
  public void addValue(long currTime, double value)
  {
    // TODO: for times other than 1s this would need interpolation as we could remove more than necessary
    while( currTime - from > duration && sums.size() > 0 )
    {
      sum-= sums.remove(0);
      from= times.remove(0);
    }
    
    double csum= value * (currTime - lastTime);
    sum+= csum;
    
    lastTime= currTime;
    
    sums.add(csum);
    times.add(currTime);
    
    ma= (sum / duration);
  }
  
  /**
   * Get the current moving average
   * @return moving average
   */
  public double getMovingAverage()
  {
    return ma;
  }
}