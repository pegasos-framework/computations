package at.pegasos.data;

public class TimedValue extends TimedObject<Value> {
  public TimedValue(long ts, Value v)
  {
    super(ts, v);
  }

  public TimedValue(long start, long end, Value value)
  {
    super(start, end, value);
  }
}
