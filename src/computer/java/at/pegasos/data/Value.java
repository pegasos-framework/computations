package at.pegasos.data;

import java.util.Map;

public abstract class Value {
  
  private final String name;
  
  public Value(String name)
  {
    this.name= name;
  }
  
  /**
   * Get the name of the value
   * @return name
   */
  public String getName()
  {
    return name;
  }
  
  /**
   * Get the value as integer
   * @return int representing the value
   */
  public abstract Integer getIntValue();
  
  /**
   * Get the value as double
   * @return double representing the value
   */
  public abstract Double getDoubleValue();
  
  /**
   * Get the value as text
   * @return string representing the value
   */
  public abstract String getStringValue();
  
  
  public abstract void setValue(Integer value);
  
  public abstract void setValue(Double value);
  
  public abstract void setValue(String value);
  
  /**
   * Get a sub value of this value.
   * 
   * @param name
   * @return the sub value or null if it does not exist
   */
  public abstract Value getSubValue(String name);

  /**
   * Check if this value has sub values
   * 
   * @return true if values are available
   */
  public abstract boolean hasSubValues();

  /**
   * Set sub values for this value
   * 
   * @param values
   */
  public abstract void setSubValues(Map<String, Value> values);

  /**
   * Get all sub values
   * 
   * @return
   */
  public abstract Map<String, Value> getSubValues();
  
  /**
   * Get the value of this value for a segment. Null if not available
   * 
   * @param number
   *          number of the segment (1 based)
   * @return
   */
  public abstract Value getSegmentValue(int number);

  /**
   * Get all segment values
   * 
   * @return
   */
  public abstract Value[] getSegmentValues();

  /**
   * Check if this value has specific values for segments
   * 
   * @return true if values are available
   */
  public abstract boolean hasSegmentValues();

  /**
   * Set the value of this value for a segment
   * 
   * @param number
   *          number of the segment (1 based)
   * @param value
   */
  public abstract void setSegmentValue(int number, Value value);

  /**
   * Batch add values for segments. The values will be added in consecutive order
   * 
   * @param values
   *          values to be added
   */
  public abstract void setSegmentValues(Value[] values);
}
