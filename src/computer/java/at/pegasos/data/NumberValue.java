package at.pegasos.data;

import java.util.Map;

public class NumberValue extends Value {

  private Value[] segmentValues= new Value[0];
  
  private Map<String, Value> subValues= null;

  @Override
  public String toString()
  {
    return "NumberValue [name=" + getName() + ", value=" + internal + "]";
  }

  protected Double internal;
  
  public NumberValue(String name)
  {
    super(name);
    internal= -1d;
  }
  
  public NumberValue(String name, int value)
  {
    super(name);
    internal= Double.valueOf(value);
  }
  
  public NumberValue(String name, double value)
  {
    super(name);
    internal= value;
  }
  
  @Override
  public Integer getIntValue()
  {
    return (int) Math.round(internal.doubleValue());
  }
  
  @Override
  public Double getDoubleValue()
  {
    return internal.doubleValue();
  }
  
  @Override
  public String getStringValue()
  {
    return internal.toString();
  }
  
  @Override
  public void setValue(Integer value)
  {
    internal= value.doubleValue();
  }
  
  @Override
  public void setValue(Double value)
  {
    internal= value;
  }
  
  @Override
  public void setValue(String value)
  {
    internal= Double.parseDouble(value);
  }

  public boolean equals(Object o)
  {
    if( !(o instanceof NumberValue) )
      return false;

    NumberValue other= (NumberValue) o;

    return getName().equals(other.getName()) && internal.equals(other.internal);
  }

  @Override
  public Value getSegmentValue(int number)
  {
    if( number > 0 && number <= segmentValues.length )
    {
      return segmentValues[number-1];
    }

    return null;
  }

  @Override
  public boolean hasSegmentValues()
  {
    return segmentValues.length > 0;
  }

  @Override
  public void setSegmentValue(int number, Value value)
  {
    if( number > 0 && number <= segmentValues.length )
    {
      segmentValues[number-1]= value;
    }
    else if( number > segmentValues.length )
    {
      Value[] values= new Value[number];
      System.arraycopy(segmentValues, 0, values, 0, segmentValues.length);
      segmentValues= values;
      segmentValues[number-1]= value;
    }
  }

  @Override
  public void setSegmentValues(Value[] values)
  {
    this.segmentValues= values;
  }

  @Override
  public Value[] getSegmentValues()
  {
    return segmentValues;
  }

  @Override
  public Value getSubValue(String name)
  {
    if( subValues == null )
      return null;
    else
      return subValues.get(name);
  }

  @Override
  public boolean hasSubValues()
  {
    return subValues != null && subValues.size() > 0;
  }

  @Override
  public void setSubValues(Map<String, Value> values)
  {
    this.subValues= values;
  }

  @Override
  public Map<String, Value> getSubValues()
  {
    return this.subValues;
  }
}

