package at.pegasos.computer;

import at.pegasos.computer.Computation;
import at.pegasos.data.Data.Column;

public interface ColumnComputation extends Computation {
  public Column getResult();
}
