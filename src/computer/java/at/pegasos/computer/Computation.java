package at.pegasos.computer;

import java.util.Collection;

public interface Computation {
  /**
   * Get the dependencies of this computation. If these objects are not present the computation
   * should not be performed. If a computation has no dependencies it should return null. A
   * dependency can be a user value, a session value/metric or a data column
   * 
   * @return collection of dependencies or null
   */
  public Collection<String> getDependencies();
  
  /**
   * Name of the computation
   * 
   * @return name
   */
  public String getName();
  
  /**
   * Perform the actual computation
   * 
   * @param computer
   *          reference to the computer
   */
  public void compute(Computer computer);
}
