package at.pegasos.computer;

import at.pegasos.computer.Computation;
import at.pegasos.data.Value;

/**
 * A simple computation to compute a metric of a session
 */
public interface ValueComputation extends Computation {
  public Value getResult();
}
