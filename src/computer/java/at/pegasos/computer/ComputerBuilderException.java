package at.pegasos.computer;

public class ComputerBuilderException extends Exception {
  private static final long serialVersionUID= -6603245040134973496L;

  public ComputerBuilderException(String message)
  {
    super(message);
  }

  public ComputerBuilderException(Exception e)
  {
    super(e);
  }
}
