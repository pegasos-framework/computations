package at.pegasos.computer;

import at.pegasos.computer.Computation;
import at.pegasos.data.TimedValue;

public interface UserValueComputation extends Computation {
  /**
   * Get the result of the computation. The time of the value should indicate the validity of the
   * value. It can be either a time span (fixed start and end) or be valid from a given start point
   * (only start fixed but end = null)
   * 
   * @return
   */
  public TimedValue getResult();
}
