package at.pegasos.computer.providers;

import java.io.IOException;
import java.nio.file.*;
import java.sql.Types;
import java.util.*;
import java.util.stream.Collectors;

import at.pegasos.data.*;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.SingleRowHeader;

public class FileUserValuesProvider implements UserValueProvider {
  
  private static FileUserValuesProvider instance;
  
  public static void main(String[] args)
  {
    FileUserValuesProvider p= FileUserValuesProvider.getInstance(Paths.get("."), "1");
    System.out.println(p.getValues(Calendar.getInstance()));
  }

  private Data values;
  private String userId;
  
  private FileUserValuesProvider(Path dir, String userId)
  {
    this.userId= userId;
    String fn= "values_" + userId + ".csv";
    try
    {
      // We are manually determining the params as doing so returns null if an IOException was raised
      CSVLoader.Parameter p= CSVLoader.determineParams(fn);
      if( p == null )
      {
        throw new IOException();
      }
      else
      {
        values= CSVLoader.create()
          .setParams(p)
          .setHeader(new SingleRowHeader(1))
          .Load()
          .getData();

        values= values.Col2Data();
        if( values == null )
        {
          values= emptyFrame().Col2Data();
        }
      }
    }
    catch( IOException e )
    {
      // e.printStackTrace();
      System.out.println("Loading user file " + fn + " failed: " + e.getLocalizedMessage());
      values= emptyFrame();
    }
    
    values= values.Col2Data();
    // values.debug();
  }

  private static Data emptyFrame()
  {
    Data ret= new Data();
    Column c= new Column();
    c.name= "data";
    c.names= new String[] {"value_name", "date_end", "value"};
    c.types= new int[] {Types.VARCHAR, Types.DATE, Types.DOUBLE};
    c.timestamps= new ArrayList<Long>(0);
    c.values= new ArrayList<Object[]>(0);
    c.orig= new ArrayList<boolean[]>(0);
    ret.addColumn(c);
    return ret;
  }

  private FileUserValuesProvider()
  {
    values= new Data();
    Column c= new Column();
    c.name= "data";
    c.names= new String[] {"value_name", "date_end", "value"};
    c.types= new int[] {Types.VARCHAR, Types.DATE, Types.DOUBLE};
    c.timestamps= new ArrayList<Long>(0);
    c.values= new ArrayList<Object[]>(0);
    c.orig= new ArrayList<boolean[]>(0);
    values.addColumn(c);

    values= values.Col2Data();
  }

  public static synchronized FileUserValuesProvider getInstance(Path location, String userId)
  {
    if( instance == null )
    {
      instance= new FileUserValuesProvider(location, userId);
    }

    return instance;
  }

  public static synchronized FileUserValuesProvider getEmptyInstance()
  {
    instance= new FileUserValuesProvider();

    return instance;
  }

  public Collection<Value> getValues(Calendar date)
  {
    Map<String, TimedValue> ret= new HashMap<String, TimedValue>();
    long dateMs= date.getTimeInMillis();
    TableIterator it= values.iterateTable();
    while( it.hasNext() )
    {
      it.next();
      if( it.getTimestamp() <= dateMs )
      {
        if( it.get("date_end") == null || dateMs <= ((Long) it.get("date_end")) )
        {
          // System.out.println("Matching " + it.getTimestamp() + " " + it.get("date_end") + " " + it.get("value_name"));
          String name= (String) it.get("value_name");
          // have we seen this value before?
          if( ret.containsKey(name) )
          {
            TimedValue tv= ret.get(name);
            // is our value newer?
            if( it.getTimestamp() > tv.getStart() )
            {
              // we are newer --> update
              ret.put(name, new TimedValue(it.getTimestamp(), new NumberValue(name, (Double) it.get("value"))));
            }
          }
          else
          {
            ret.put(name, new TimedValue(it.getTimestamp(), new NumberValue(name, (Double) it.get("value"))));
          }
        }
      }
    }
    return ret.values().stream().map(tv -> tv.getValue()).collect(Collectors.toList());
  }

  @Override
  public List<TimedValue> fetchValues(String valueName, long from, long to)
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TimedValue fetchValue(String valueName, Calendar date)
  {
    /*
	  System.out.print("Fetching: '" + valueName + "' ");
      printDate(date);
      System.out.println("/" + date.getTimeInMillis());
	*/
    TableIterator it= values.iterateTable();
    while(it.hasNext())
    {
      it.next();
      // System.out.print(it.get("value_name") + " " + it.getTimestamp() + " ");
      // printDate(date);
      // System.out.println("/" + date.getTimeInMillis());
      Object end= it.get("date_end");
      if( it.get("value_name").equals(valueName) && (end == null && Utils.dayAfter(it.getTimestamp(), date)
          || end != null && Utils.dateIn(it.getTimestamp(), end, date)) )
      {
        return new TimedValue(it.getTimestamp(), new NumberValue(valueName, (Double) it.get("value")));
      }
    }
    
    /*System.out.print(valueName + " ");
    printDate(date);
    System.out.println(" not found");*/
    
    return null;
  }
  
  private void printDate(Calendar date)
  {
    System.out.print("Computing daily values " + date.get(Calendar.YEAR) + "-" + (date.get(Calendar.MONTH)+1) + "-" + date.get(Calendar.DAY_OF_MONTH));
  }
  
  @Override
  public void setValues(Calendar date, Collection<Value> computedValues)
  {
    Set<String> names= computedValues.stream().map(v -> v.getName()).collect(Collectors.toSet());
    
    TableIterator it= values.iterateTable();
    while(it.hasNext())
    {
      it.next();
      if( names.contains(it.get("value_name")) && it.get("date_end") == null )
      {
        it.set("date_end", date.getTimeInMillis());
      }
    }
    
    for(Value value : computedValues)
    {
      values.addRow(date.getTimeInMillis(), new Object[] {value.getName(), null, value.getDoubleValue()});
    }
    
    // values.printTable();
  }

  @Override
  public void setUser(String userId)
  {
    this.userId= userId;
  }

  @Override
  public String getUser()
  {
    return userId;
  }

  public void persist() throws IOException
  {
    values.writeFileCSV("values_" + userId + ".new.csv");
  }

  @Override
  public void setValue(TimedValue value)
  {
    String name= value.getValue().getName();
    boolean replaced= false;
    boolean closingNec= false;
    boolean inserted= false;

    // System.out.print("Setting value " + value);
    int oldsize= values.getRowCount();

    // First check whether we need to replace an old value
    TableIterator it= values.iterateTable();
    while(it.hasNext())
    {
      it.next();
      if( value.getEnd() != 0 )
      {
        if( name.equals(it.get("value_name")) )
        {
          if( it.getTimestamp() == value.getStart() )
          {
            // also the end date matches
            Object end= it.get("date_end");
            if( end == null || end.equals(value.getEnd()) )
            {
              it.set("date_end", value.getEnd());
              it.set("value", value.getValue().getDoubleValue());
              replaced= true;
            }
          }
        }
      }
      else
      { // TODO check this case
        if( name.equals(it.get("value_name")) )
        {
          if( it.getTimestamp() == value.getStart() )
          {
            it.set("value", value.getValue().getDoubleValue());
            replaced= true;
          }
          else
            closingNec= true;
        }
      }
    }

    if( !replaced )
    {
      if( closingNec )
      {
        // System.out.print(" closing");
        it= values.iterateTable();
        while(it.hasNext())
        {
          it.next();
          if( name.equals(it.get("value_name")) && it.get("date_end") == null )
          {
            it.set("date_end", value.getStart() - 1);
            break;
          }
        }
      }
      inserted= true;
      values.addRow(value.getStart(), new Object[] {
          value.getValue().getName(), 
          (value.getEnd() == 0) ? null : value.getEnd(), 
          value.getValue().getDoubleValue()
      });
    }
    else
    {
    	// System.out.println(" replaced");
    }
    // System.out.println("Setting value " + value.getValue().getName() + " end?" + (value.getEnd() != 0) + " replaced:" + replaced + " closingNec:" + closingNec);
    
    /*final SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    System.out.println("Setting value " + value.getValue().getName() + 
    		" start:" + sdf.format(new Date(value.getStart())) + 
    		" end:" + (value.getEnd() != 0 ? sdf.format(new Date(value.getEnd())) : "") +
    		" r:" + replaced + " c:" + closingNec + " i:" + inserted + " " + oldsize + "/" + values.getRowCount());*/
  }

  public int getSize()
  {
    return values.getRowCount();
  }

  public Value getLast()
  {
    Object[] row= values.getRow(values.getRowCount() - 1);
    return new NumberValue((String) row[values.getColumnIndex("value_name")],
        (Double) row[values.getColumnIndex("value")]);
  }
}

