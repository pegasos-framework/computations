package at.pegasos.computer.providers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import at.pegasos.data.Value;

/**
 * 
 */
public class SimpleDateSessionValuesProvider implements SessionValuesProvider {

  private Map<Long, Collection<Value>> vals;

  public SimpleDateSessionValuesProvider()
  {
    // vals= new ArrayList<TimedValue>(10000);
    vals= new HashMap<Long, Collection<Value>>();
  }

  @Override
  public Collection<Value> getValuesDay(String name, Calendar date)
  {
    long day= getDay(date);
    
    if( this.vals.containsKey(day) )
    {
      return vals.get(day)
          .stream()
          .filter(v -> v.getName().equals(name))
          .collect(Collectors.toList());
    }
    else
    {
      return Arrays.asList(new Value[0]);
    }
  }
  
  @Override
  public void addSessionMetrics(Calendar date, Collection<Value> computedValues)
  {
    long day= getDay(date);
    
    Collection<Value> vals;
    if( !this.vals.containsKey(day) )
    {
      vals= new ArrayList<Value>(200);
      this.vals.put(day, vals);
    }
    else
    {
      vals= this.vals.get(day);
    }

    vals.addAll(computedValues);
  }

  @Override
  public void setUserId(String userId)
  {
    // ignore this operation
  }

  @Override
  public void setSessionId(String sessionId)
  {
    // ignore this operation
  }
  
  private static long getDay(Calendar date)
  {
    long day;
    Calendar d= Calendar.getInstance();
    d.clear();
    d.set(Calendar.YEAR, date.get(Calendar.YEAR));
    d.set(Calendar.DAY_OF_YEAR, date.get(Calendar.DAY_OF_YEAR));
    /*
    d.setTimeInMillis(date.getTimeInMillis());
    d.set(Calendar.HOUR_OF_DAY, 0);
    d.set(Calendar.MINUTE, 0);
    d.set(Calendar.SECOND, 0);
    */
    day= d.getTimeInMillis();
    // System.out.println(BaseSimpleExperiment.DateFormat.format(date.getTime()) + " to " + BaseSimpleExperiment.DateFormat.format(d.getTime()));
    return day;
  }
  
  public int getSize()
  {
    return vals.size();
  }

  public void debug()
  {
    System.out.println("size: " + vals.size());
  }

  public void debug2()
  {
    System.out.println("size: " + vals.size() + " " + vals.keySet().toString());
    for(Value v : vals.get(1262300400000L))
    {
      System.out.println(" " + v);
    }
  }
}
