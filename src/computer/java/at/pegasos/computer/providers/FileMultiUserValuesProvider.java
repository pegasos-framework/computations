package at.pegasos.computer.providers;

import java.io.IOException;
import java.sql.Types;
import java.util.*;
import java.util.stream.Collectors;

import at.pegasos.data.*;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.loader.*;

public class FileMultiUserValuesProvider implements UserValueProvider {
  // private final SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  
  // private static FileMultiUserValuesProvider instance;
  
  private Data values;
  private String personId;
  
  public FileMultiUserValuesProvider()
  {
    tryLoad();
  }

  public FileMultiUserValuesProvider(boolean empty)
  {
    if( empty )
    {
      values= new Data();
      Column c= new Column();
      c.name= "data";
      c.names= new String[] {"person", "value_name", "date_end", "value"};
      c.types= new int[] {Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.DOUBLE};
      c.timestamps= new ArrayList<Long>(0);
      c.values= new ArrayList<Object[]>(0);
      c.orig= new ArrayList<boolean[]>(0);
      values.addColumn(c);

      values= values.Col2Data();
    }
    else
    {
      tryLoad();
    }
  }
  
  private void tryLoad()
  {
    String fn= "values.csv";
    try
    {
      // values= CSVLoaderLegacy.LoadFile(fn, "values", new int[] {Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.DOUBLE}, ";", "date_start", false).getData();
      // We are manually determining the params as doing so returns null if an IOException was raised
      CSVLoader.Parameter p= CSVLoader.determineParams(fn);
      if( p == null )
        throw new IOException();
      values= CSVLoader.create()
          .setParams(p)
          .setHeader(new SingleRowHeader(1))
          .Load()
          .getData();
    }
    catch( IOException e )
    {
      // e.printStackTrace();
      System.out.println("Loading user file " + fn + " failed: " + e.getLocalizedMessage());
      values= new Data();
      Column c= new Column();
      c.name= "data";
      c.names= new String[] {"person", "value_name", "date_end", "value"};
      c.types= new int[] {Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.DOUBLE};
      c.timestamps= new ArrayList<Long>(0);
      c.values= new ArrayList<Object[]>(0);
      c.orig= new ArrayList<boolean[]>(0);
      values.addColumn(c);
    }

    values= values.Col2Data();
    // values.debug();
  }

  /*public static synchronized FileMultiUserValuesProvider getInstance(String userId)
  {
    if( instance == null )
    {
      instance= new FileMultiUserValuesProvider(userId);
    }
    
    return instance;
  }*/

  public Collection<Value> getValues(Calendar date)
  {
    if( personId == null )
      throw new IllegalStateException("No person set for " + getClass().getName());

    Map<String, TimedValue> ret= new HashMap<String, TimedValue>();
    long dateMs= date.getTimeInMillis();
    TableIterator it= values.iterateTable();
    while( it.hasNext() )
    {
      it.next();
      if( it.getTimestamp() <= dateMs )
      {
        if( it.get("date_end") == null || dateMs <= ((Long) it.get("date_end")) )
        {
          // System.out.println("Matching " + it.getTimestamp() + " " + it.get("date_end") + " " + it.get("value_name"));
          String name= (String) it.get("value_name");
          // have we seen this value before?
          if( ret.containsKey(name) )
          {
            TimedValue tv= ret.get(name);
            // is our value newer?
            if( it.getTimestamp() > tv.getStart() )
            {
              // we are newer --> update
              ret.put(name, new TimedValue(it.getTimestamp(), new NumberValue(name, (Double) it.get("value"))));
            }
          }
          else
          {
            ret.put(name, new TimedValue(it.getTimestamp(), new NumberValue(name, (Double) it.get("value"))));
          }
        }
      }
    }
    return ret.values().stream().map(tv -> tv.getValue()).collect(Collectors.toList());
  }

  @Override
  public List<TimedValue> fetchValues(String valueName, long from, long to)
  {
    if( personId == null )
      throw new IllegalStateException("No person set for " + getClass().getName());

    List<TimedValue> ret= new ArrayList<TimedValue>();

    TableIterator it= values.iterateTable();
    while(it.hasNext())
    {
      it.next();
      Object end= it.get("date_end");
      if( personId.equals(it.get("person")) && it.get("value_name").equals(valueName)
          && it.getTimestamp() >= from && it.getTimestamp() <= to )
      {
        if( end != null )
          ret.add(new TimedValue(it.getTimestamp(), Utils.safeDateToTimestamp(end),
              new NumberValue(valueName, (Double) it.get("value"))));
        else
          ret.add(new TimedValue(it.getTimestamp(), new NumberValue(valueName, (Double) it.get("value"))));
      }
    }

    return ret;
  }

  @Override
  public TimedValue fetchValue(String valueName, Calendar date)
  {
    if( personId == null )
      throw new IllegalStateException("No person set for " + getClass().getName());

    /*
	  System.out.print("Fetching: '" + valueName + "' ");
      printDate(date);
      System.out.println("/" + date.getTimeInMillis());
	*/
    TableIterator it= values.iterateTable();
    while(it.hasNext())
    {
      it.next();
      // System.out.print(it.get("value_name") + " " + it.getTimestamp() + " ");
      // printDate(date);
      // System.out.println("/" + date.getTimeInMillis());
      Object end= it.get("date_end");
      if( personId.equals(it.get("person")) && it.get("value_name").equals(valueName)
          && (end == null && Utils.dayAfter(it.getTimestamp(), date)
              || end != null && Utils.dateIn(it.getTimestamp(), end, date)) )
      {
        return new TimedValue(it.getTimestamp(), new NumberValue(valueName, (Double) it.get("value")));
      }
    }
    
    /*System.out.print(valueName + " ");
    printDate(date);
    System.out.println(" not found");*/
    
    return null;
  }

  @Override
  public void setValues(Calendar date, Collection<Value> computedValues)
  {
    if( personId == null )
      throw new IllegalStateException("No person set for " + getClass().getName());

    Set<String> names= computedValues.stream().map(v -> v.getName()).collect(Collectors.toSet());

    TableIterator it= values.iterateTable();
    while( it.hasNext() )
    {
      it.next();
      if( personId.equals(it.get("person")) && names.contains(it.get("value_name")) && it.get("date_end") == null )
      {
        // System.out.println("closing " + it.get("value_name"));
        it.set("date_end", date.getTimeInMillis());
      }
    }

    for(Value value : computedValues)
    {
      values.addRow(date.getTimeInMillis(), new Object[] {personId, value.getName(), null, value.getDoubleValue()});
    }

    // values.printTable();
  }

  @Override
  public void setUser(String userId)
  {
    this.personId= userId;
  }

  @Override
  public String getUser()
  {
    return personId;
  }

  public void persist() throws IOException
  {
    values.writeFileCSV("values.new.csv");
  }

  @Override
  public void setValue(TimedValue value)
  {
    if( personId == null )
      throw new IllegalStateException("No person set for " + getClass().getName());

    String name= value.getValue().getName();
    boolean replaced= false;
    boolean closingNec= false;
    // boolean inserted= false;

    // System.out.println("Setting " + name + " " + value.getStart() + " " + value.getEnd());

    // First check whether we need to replace an old value
    TableIterator it= values.iterateTable();
    while( it.hasNext() )
    {
      it.next();
      // Value has an end date
      if( value.getEnd() != 0 )
      {
        // this might be the value we want to replace
        if( personId.equals(it.get("person")) && name.equals(it.get("value_name")) )
        {
          // it has the same starting date
          if( it.getTimestamp() == value.getStart() )
          {
            // also the end date matches
            Object end= it.get("date_end");
            if( end == null || end.equals(value.getEnd()) )
            {
              it.set("date_end", value.getEnd());
              it.set("value", value.getValue().getDoubleValue());
              replaced= true;
            }
          }
        }
      }
      else
      {
        if( personId.equals(it.get("person")) && name.equals(it.get("value_name")) )
        {
          if( it.getTimestamp() == value.getStart() )
          {
            it.set("value", value.getValue().getDoubleValue());
            replaced= true;
          }
          else
            closingNec= true;
        }
      }
    }

    // we haven't replaced a value --> need to insert a new one
    if( !replaced )
    {
      // It is necessary to close (set end date) of a previous value
      if( closingNec )
      {
        it= values.iterateTable();
        while( it.hasNext() )
        {
          it.next();
          if( personId.equals(it.get("person")) && name.equals(it.get("value_name")) && it.get("date_end") == null )
          {
            it.set("date_end", value.getStart() - 1);
            break;
          }
        }
      }
      // inserted= true;
      values.addRow(value.getStart(), new Object[] {personId, value.getValue().getName(),
          (value.getEnd() == 0) ? null : value.getEnd(), value.getValue().getDoubleValue()});
    }
    // System.out.println("Setting value " + value.getValue().getName() + " end?" + (value.getEnd() != 0) + " replaced:" + replaced + " closingNec:" + closingNec);
    
    /*
    System.out.println("Setting value " + value.getValue().getName() + 
    		" start:" + sdf.format(new Date(value.getStart())) + 
    		" end:" + (value.getEnd() != 0 ? sdf.format(new Date(value.getEnd())) : "") +
    		" r:" + replaced + " c:" + closingNec + " i:" + inserted + " " + oldsize + "/" + values.getRowCount());*/
  }
}

