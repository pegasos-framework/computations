package at.pegasos.computer.providers;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import at.pegasos.data.TimedValue;
import at.pegasos.data.Value;

public interface UserValueProvider {
  
  /**
   * Set the user for which all subsequent operations should be performed
   * 
   * @param userId
   *          ID of the user
   */
  void setUser(String userId);

  /**
   * Get the current user information. May return null if this UVP does not support user information
   * 
   * @return user info or null
   */
  String getUser();
  
  /**
   * Get a collection of all values which are set/active for the user on the given date
   * 
   * @param date
   * 
   * @return collection of values
   */
  Collection<Value> getValues(Calendar date);
  
  /**
   * Get a list of all value changes for the user in the time span from to to
   * 
   * @param valueName
   *          name of the value
   * @param from
   *          start of the time span (i.e. oldest value >= from)
   * @param to
   *          end of the time span (i.e. younge value <= to)
   * @return chronologically DESC ordered list of values
   */
  List<TimedValue> fetchValues(String valueName, long from, long to);
  
  /**
   * Get a value for a certain day (if it exists . If more than one value exists for this day the
   * behaviour of this method is undefined.
   * 
   * @param valueName
   *          name of the value to get
   * @param date
   *          date for which the value should be retrieved
   * @return value or null if it does not exist
   */
  TimedValue fetchValue(String valueName, Calendar date);
  
  /**
   * Set/add values for the user on a given date
   * 
   * @param date
   *          date
   * @param computedValues
   *          values to be set
   */
  void setValues(Calendar date, Collection<Value> computedValues);
  
  /**
   * Set/add value for the user on a given date, or for a given time span
   * 
   * @param value
   *          Value to be set
   */
  void setValue(TimedValue value);

  /**
   * Persist all changes
   * 
   * @throws IOException
   */
  void persist() throws IOException;
}
