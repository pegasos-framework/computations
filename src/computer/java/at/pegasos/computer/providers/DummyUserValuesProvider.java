package at.pegasos.computer.providers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import at.pegasos.data.TimedValue;
import at.pegasos.data.Value;

public class DummyUserValuesProvider implements UserValueProvider {

  private final static Collection<Value> novalues= Arrays.asList(new Value[0]);
  private final static List<TimedValue> notimedvalues= Arrays.asList(new TimedValue[0]);

  @Override
  public void setUser(String userId)
  {
  }

  @Override
  public String getUser()
  {
    return null;
  }

  @Override
  public Collection<Value> getValues(Calendar date)
  {
    return novalues;
  }

  @Override
  public List<TimedValue> fetchValues(String valueName, long from, long to)
  {
    return notimedvalues;
  }

  @Override
  public TimedValue fetchValue(String valueName, Calendar date)
  {
    // per specification we return null if it does not exist
    return null;
  }

  @Override
  public void setValues(Calendar date, Collection<Value> computedValues)
  {
    // nothing to do here
  }

  @Override
  public void setValue(TimedValue value)
  {
    // nothing to do here
  }

  @Override
  public void persist() throws IOException
  {
    // nothing to do here
  }

}
