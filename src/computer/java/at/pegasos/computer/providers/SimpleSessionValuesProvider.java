package at.pegasos.computer.providers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.stream.Collectors;

import at.pegasos.data.TimedValue;
import at.pegasos.data.Value;

/**
 * This provider is a straight forward implementation of a SessionValuesProvider. It does not
 * respect user or session information. The add operation works in O(1). However, any retrieve is in
 * O(n) with n the number of added values. Consequently, it is advised to use this provider only for
 * small experiments where performance does not matter
 */
public class SimpleSessionValuesProvider implements SessionValuesProvider {

  private static SimpleSessionValuesProvider instance;

  private Collection<TimedValue> vals;

  private SimpleSessionValuesProvider()
  {
    vals= new ArrayList<TimedValue>(10000);
  }

  public static synchronized SimpleSessionValuesProvider getInstance()
  {
    if( instance == null )
    {
      instance= new SimpleSessionValuesProvider();
    }

    return instance;
  }

  @Override
  public Collection<Value> getValuesDay(String name, Calendar date)
  {
    return vals.stream().filter(v -> v.getValue().getName().equals(name) && Utils.dateMatch(v.getTimestamp(), date))
        .map(v -> v.getValue()).collect(Collectors.toList());
  }

  @Override
  public void addSessionMetrics(Calendar date, Collection<Value> computedValues)
  {
    this.vals.addAll(
        computedValues.stream().map(v -> new TimedValue(date.getTimeInMillis(), v)).collect(Collectors.toList()));
  }

  @Override
  public void setUserId(String userId)
  {
    // ignore this operation
  }

  @Override
  public void setSessionId(String sessionId)
  {
    // ignore this operation
  }
}
