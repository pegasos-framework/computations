package at.pegasos.computer.providers;

import java.util.Calendar;
import java.util.Collection;

import at.pegasos.data.Value;

public interface SessionValuesProvider {
  
  /**
   * Get metric results for all session on a specific day
   * 
   * @param name
   *          name of the metric
   * @param date
   *          date
   * @return collection of metric results (empty if no session or metrics exist)
   */
  Collection<Value> getValuesDay(String name, Calendar date);
  
  /**
   * add/set metrics/computed values for a session
   * 
   * @param sessionId
   *          ID of the session for which the values have been computed
   * @param date
   *          Date of the session
   * @param computedValues
   *          values
   */
  void addSessionMetrics(Calendar date, Collection<Value> computedValues);

  void setUserId(String userId);

  void setSessionId(String sessionId);
}
