package at.pegasos.computer.providers;

import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import at.pegasos.data.*;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.Value;
import at.pegasos.data.loader.*;

public class ConcurrentFileMultiUserValuesProvider implements UserValueProvider {
  
  // private Data values;
  // private String personId;
  
  private Map<String, Data> personValues;
  
  private Map<Long, String> threadPerson;
  
  public ConcurrentFileMultiUserValuesProvider(boolean empty)
  {
    personValues= new ConcurrentHashMap<String, Data>();
    threadPerson= new ConcurrentHashMap<Long, String>();

    if( !empty )
    {
      loadFile("values.csv");
    }
  }

  public ConcurrentFileMultiUserValuesProvider()
  {
    personValues= new ConcurrentHashMap<String, Data>(500);
    threadPerson= new ConcurrentHashMap<Long, String>();

    loadFile("values.csv");
  }

  public ConcurrentFileMultiUserValuesProvider(String fn)
  {
    personValues= new ConcurrentHashMap<String, Data>(500);
    threadPerson= new ConcurrentHashMap<Long, String>();

    loadFile(fn);
  }

  private void loadFile(String fn)
  {
    Data values;

    try
    {
      // We are manually determining the params as doing so returns null if an IOException was raised
      CSVLoader.Parameter p= CSVLoader.determineParams(fn);
      if( p == null )
        throw new IOException();
      values= CSVLoader.create()
          .setParams(p)
          .setHeader(new SingleRowHeader(1))
          .Load()
          .getData();

      values= values.Col2Data();
      if( values == null )
      {
        values= emptyFrame().Col2Data();
      }
    }
    catch( IOException e )
    {
      System.out.println("Loading user file " + fn + " failed: " + e.getLocalizedMessage());
      values= emptyFrame().Col2Data();
    }

    Data personData= null;
    String person= null;
    TableIterator it= values.iterateTable();
    while( it.hasNext() )
    {
      Object[] vals= it.next();
      if( !it.get("person").equals(person) )
      {
        person= it.get("person").toString();
        if( !personValues.containsKey(person) )
        {
          personData= emptyFrame();
          personValues.put(person, personData);
        }
      }
      personData.addRow(it.getTimestamp(), vals);
    }
  }

  private Data emptyFrame()
  {
    Data ret= new Data();
    Column c= new Column();
    c.name= "data";
    c.names= new String[] {"person", "value_name", "date_end", "value"};
    c.types= new int[] {Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.DOUBLE};
    c.timestamps= new ArrayList<Long>(1000);
    c.values= new ArrayList<Object[]>(1000);
    c.orig= new ArrayList<boolean[]>(1000);
    ret.addColumn(c);
    return ret.Col2Data();
  }

  public Collection<Value> getValues(Calendar date)
  {
    Data values= getValues();
    Map<String, TimedValue> ret= new HashMap<String, TimedValue>();
    long dateMs= date.getTimeInMillis();
    TableIterator it= values.iterateTable();
    System.out.println("getValues");
    values.printTable();
    while( it.hasNext() )
    {
      it.next();
      if( it.getTimestamp() <= dateMs )
      {
//        System.out.println("Matching " + it.getTimestamp() + " '" + it.get("date_end") + "'");
//        if( it.get("date_end") == null || dateMs <= ((Long) it.get("date_end")) )
        if( it.get("date_end") == null || Utils.smallerOrEqual(dateMs, it.get("date_end")) )
        {
//          System.out.println("Matching " + it.getTimestamp() + " " + it.get("date_end") + " " + it.get("value_name"));
          String name= (String) it.get("value_name");
          // have we seen this value before?
          if( ret.containsKey(name) )
          {
            TimedValue tv= ret.get(name);
            // is our value newer?
            if( it.getTimestamp() > tv.getStart() )
            {
              // we are newer --> update
              ret.put(name, new TimedValue(it.getTimestamp(), new NumberValue(name, (Double) it.get("value"))));
            }
          }
          else
          {
            ret.put(name, new TimedValue(it.getTimestamp(), new NumberValue(name, (Double) it.get("value"))));
          }
        }
      }
    }
    return ret.values().stream().map(tv -> tv.getValue()).collect(Collectors.toList());
  }

  @Override
  public List<TimedValue> fetchValues(String valueName, long from, long to)
  {
    // Data values= getValues();

    throw new RuntimeException("Not implemented");

    // TODO Auto-generated method stub
  }

  @Override
  public TimedValue fetchValue(String valueName, Calendar date)
  {
    Data values= getValues();

    TableIterator it= values.iterateTable();
    while(it.hasNext())
    {
      it.next();
      Object end= it.get("date_end");
      if( it.get("value_name").equals(valueName) && (end == null && Utils.dayAfter(it.getTimestamp(), date)
          || end != null && Utils.dateIn(it.getTimestamp(), end, date)) )
      {
        return new TimedValue(it.getTimestamp(), new NumberValue(valueName, (Double) it.get("value")));
      }
    }
    
    return null;
  }

  @Override
  public void setValues(Calendar date, Collection<Value> computedValues)
  {
    Data values= getValues();

    Set<String> names= computedValues.stream().map(v -> v.getName()).collect(Collectors.toSet());

    TableIterator it= values.iterateTable();
    while( it.hasNext() )
    {
      it.next();
      if( names.contains(it.get("value_name")) && it.get("date_end") == null )
      {
        // System.out.println("closing " + it.get("value_name"));
        it.set("date_end", date.getTimeInMillis());
      }
    }

    for(Value value : computedValues)
    {
      values.addRow(date.getTimeInMillis(), new Object[] {getPerson(), value.getName(), null, value.getDoubleValue()});
    }
  }

  @Override
  public void setUser(String userId)
  {
    threadPerson.put(Thread.currentThread().getId(), userId);
  }

  @Override
  public String getUser()
  {
    return threadPerson.get(Thread.currentThread().getId());
  }

  private String getPerson()
  {
    long tId= Thread.currentThread().getId();
    if( threadPerson.containsKey(tId) )
    {
      return threadPerson.get(tId);
    }
    else
      throw new IllegalStateException("No person set for " + getClass().getName() + " on thread " + tId + " " + Thread.currentThread().getName());
  }
  
  private Data getValues()
  {
    long tId= Thread.currentThread().getId();
    if( threadPerson.containsKey(tId) )
    {
      Data ret;
      if( personValues.containsKey(threadPerson.get(tId)) )
      {
        ret= personValues.get(threadPerson.get(tId));
      }
      else
      {
        ret= emptyFrame();
        personValues.put(threadPerson.get(tId), ret);
      }
      return ret;
    }
    else
      throw new IllegalStateException("No person set for " + getClass().getName() + " on thread " + tId + " " + Thread.currentThread().getName());
  }

  public void persist() throws IOException
  {
    Data values= emptyFrame();
    personValues.values().stream().forEach(vals -> {
      TableIterator it= vals.iterateTable();
      while(it.hasNext())
      {
        Object[] row= it.next();
        values.addRow(it.getTimestamp(), row);
      }
    });
    values.writeFileCSV("values.new.csv");
  }

  @Override
  public void setValue(TimedValue value)
  {
    Data values= getValues();

    String name= value.getValue().getName();
    boolean replaced= false;
    boolean closingNec= false;
    // boolean inserted= false;

    // System.out.println("Setting " + name + " " + value.getStart() + " " + value.getEnd());

    // First check whether we need to replace an old value
    TableIterator it= values.iterateTable();
    while( it.hasNext() )
    {
      it.next();
      // Value has an end date
      if( value.getEnd() != 0 )
      {
        // this might be the value we want to replace
        if( name.equals(it.get("value_name")) )
        {
          // it has the same starting date
          if( it.getTimestamp() == value.getStart() )
          {
            // also the end date matches
            Object end= it.get("date_end");
            if( end == null || end.equals(value.getEnd()) )
            {
              it.set("date_end", value.getEnd());
              it.set("value", value.getValue().getDoubleValue());
              replaced= true;
            }
          }
        }
      }
      else
      {
        if( name.equals(it.get("value_name")) )
        {
          if( it.getTimestamp() == value.getStart() )
          {
            it.set("value", value.getValue().getDoubleValue());
            replaced= true;
          }
          else
            closingNec= true;
        }
      }
    }

    // we haven't replaced a value --> need to insert a new one
    if( !replaced )
    {
      // It is necessary to close (set end date) of a previous value
      if( closingNec )
      {
        it= values.iterateTable();
        while( it.hasNext() )
        {
          it.next();
          if( name.equals(it.get("value_name")) && it.get("date_end") == null )
          {
            it.set("date_end", value.getStart() - 1);
            break;
          }
        }
      }
      // inserted= true;
      values.addRow(value.getStart(), new Object[] {getPerson(), value.getValue().getName(),
          (value.getEnd() == 0) ? null : value.getEnd(), value.getValue().getDoubleValue()});
    }
    // System.out.println("Setting value " + value.getValue().getName() + " end?" + (value.getEnd() != 0) + " replaced:" + replaced + " closingNec:" + closingNec);
    
    /*
    System.out.println("Setting value " + value.getValue().getName() + 
    		" start:" + sdf.format(new Date(value.getStart())) + 
    		" end:" + (value.getEnd() != 0 ? sdf.format(new Date(value.getEnd())) : "") +
    		" r:" + replaced + " c:" + closingNec + " i:" + inserted + " " + oldsize + "/" + values.getRowCount());*/
  }
}

