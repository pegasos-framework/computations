package at.pegasos.computer.providers;

import java.util.Calendar;

public class Utils {
  /*public static String StringJoin(String sep, String[] args)
  {
    String ret= "";
    
    boolean first= true;
    for(String x : args)
    {
      if( !first )
        ret+= sep;
      ret+= x;
      first= true;
    }
    
    return ret;
  }

  public static String StringJoin(String sep, long[] args)
  {
    String ret= "";
    
    boolean first= true;
    for(long x : args)
    {
      if( !first )
        ret+= sep;
      ret+= x;
      first= true;
    }
    
    return ret;
  }*/
  
  /**
   * Check if two dates are on the same day
   * @param timestamp
   * @param date
   * @return
   */
  public static boolean dateMatch(long timestamp, Calendar date)
  {
    Calendar c= Calendar.getInstance();
    c.setTimeInMillis(timestamp);
    
    return c.get(Calendar.YEAR) == date.get(Calendar.YEAR) &&
        c.get(Calendar.MONTH) == date.get(Calendar.MONTH) &&
        c.get(Calendar.DAY_OF_MONTH) == date.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Check if date is after the day of timestamp
   * 
   * @param timestamp
   * @param date
   * @return
   */
  public static boolean dayAfter(long timestamp, Calendar date)
  {
    Calendar c= Calendar.getInstance();
    c.setTimeInMillis(timestamp);
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);

    return date.after(c) || date.getTimeInMillis() == c.getTimeInMillis();
  }

  /**
   * Check if date is in between timestamp and end.
   * 
   * @param timestamp
   * @param end
   *          long or Calendar
   * @param date
   * @return
   */
  public static boolean dateIn(long timestamp, Object end, Calendar date)
  {
    long endts;

    if( end instanceof Long )
    {
      endts= (Long) end;
    }
    else if( end instanceof Calendar )
    {
      endts= ((Calendar) end).getTimeInMillis();
    }
    else
      throw new IllegalArgumentException("end needs to be Long or Calendar");

    Calendar start= Calendar.getInstance();
    start.setTimeInMillis(timestamp);
    start.set(Calendar.HOUR_OF_DAY, 0);
    start.set(Calendar.MINUTE, 0);
    start.set(Calendar.SECOND, 0);
    start.set(Calendar.MILLISECOND, 0);

    if( date.after(start) || date.getTimeInMillis() == start.getTimeInMillis() )
    {
      Calendar endc= Calendar.getInstance();
      endc.setTimeInMillis(endts);
      endc.set(Calendar.HOUR_OF_DAY, 0);
      endc.set(Calendar.MINUTE, 0);
      endc.set(Calendar.SECOND, 0);
      endc.set(Calendar.MILLISECOND, 0);
      endc.add(Calendar.DAY_OF_YEAR, 1);

      endts= endc.getTimeInMillis() - 1;

      return date.getTimeInMillis() < endts;
    }
    else
    {
      return false;
    }
  }

  /**
   * Extract the date from an table object
   * 
   * @param date
   * @return if long the value itself, if a calendar .getTimeInMillis()
   */
  public static long safeDateToTimestamp(Object date)
  {
    if( date instanceof Long )
    {
      return (Long) date;
    }
    else if( date instanceof Calendar )
    {
      return ((Calendar) date).getTimeInMillis();
    }
    else
      throw new IllegalArgumentException("end needs to be Long or Calendar");
  }
  
  public static boolean smallerOrEqual(long v1, Object v2)
  {
    if( v2 instanceof Long )
    {
      return v1 <= ((Long) v2);
    }
    else if( v2 instanceof Double )
    {
      return v1 <= ((Double) v2);
    }
    else
      throw new IllegalArgumentException();
  }
}
