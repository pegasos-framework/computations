package at.pegasos.computer.providers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import at.pegasos.data.TimedValue;
import at.pegasos.data.Value;

public class MultiUserSessionValuesProvider implements SessionValuesProvider {
  
  // protected Logger logger= LoggerFactory.getLogger(MultiUserSessionValuesProvider.class);

  private Collection<TimedValue> vals;
  private Map<String, Collection<TimedValue>> uservals;
  // private String user= null;

  public MultiUserSessionValuesProvider()
  {
    uservals= new HashMap<String, Collection<TimedValue>>();
  }

  @Override
  public Collection<Value> getValuesDay(String name, Calendar date)
  {
    // logger.debug("GetValues user:" + user + " " + name + " " + date);
    return vals.stream().filter(v -> v.getValue().getName().equals(name) && Utils.dateMatch(v.getTimestamp(), date))
        .map(v -> v.getValue()).collect(Collectors.toList());
  }

  @Override
  public void addSessionMetrics(Calendar date, Collection<Value> computedValues)
  {
    // for(Value val : computedValues )
    //   logger.debug("addSessionMetrics:" + user + " " + val + " " + date);
    this.vals.addAll(
        computedValues.stream().map(v -> new TimedValue(date.getTimeInMillis(), v)).collect(Collectors.toList()));
  }

  @Override
  public void setUserId(String userId)
  {
    // user= userId;
    if( uservals.containsKey(userId) )
    {
      vals= uservals.get(userId);
    }
    else
    {
      vals= new ArrayList<TimedValue>(10000);
      uservals.put(userId, vals);
    }
  }

  @Override
  public void setSessionId(String sessionId)
  {
    // ignore this operation
  }
}
