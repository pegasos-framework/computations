package at.pegasos.computer;

import java.util.Calendar;
import java.util.Collection;

import at.pegasos.data.NumberValue;
import at.pegasos.data.TimedValue;
import at.pegasos.data.Value;

/**
 * Utility class for calculating values for a user.
 * When using this class it is important that `setComputer` is called before any computation is performed
 */
public abstract class BasicUserValueComputation implements UserValueComputation {
  
  /**
   * Internal representation of the result
   */
  protected double value;
  
  protected Computer computer;
  
  protected void setComputer(Computer computer)
  {
    this.computer= computer;
  }
  
  /**
   * Compute the average of a value for a given time span starting from `start` going `days` into
   * the future
   * 
   * @param valueName
   *          name of the value to be used
   * @param start
   *          timestamp when the time span starts. This is the last day of the span. Usually this
   *          can be set to today.
   * @param days
   *          number of days
   * @return average of the value
   */
  protected double sessionMeanDays(String valueName, int start, int days, double defaultValue)
  {
    double cum= 0;
    int count= 0;
    int d= start;
    int daysdone;

    for(daysdone= 0; daysdone < days; daysdone++)
    {
      Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(valueName, getDate(d));
      if( values.size() > 0 )
      {
        for(Value v : values)
        {
          // System.out.print(d + " " + v);
          cum+= v.getDoubleValue();
          count++;
        }
      }

      d++;
    }
    
    if( count > 0 )
    {
      // System.out.println(" " + (cum / (double) count));
      return cum / (double) count;
    }
    else
    {
      // System.out.println(" no value");
      return defaultValue;
    }
  }
  
  /**
   * Get the value for a specific day or return a default value if it does not exist
   * 
   * @param name
   *          name of the value
   * @param d
   *          day offset (i.e. 0 for today, -1 for yesterday)
   * @param defaultValue
   *          value to be used if value does not exist
   * @return
   */
  protected double valueDayOr(String name, int d, double defaultValue)
  {
    TimedValue v= computer.getUserValueProvider().fetchValue(name, getDate(d));
    if( v == null )
      return defaultValue;
    else
      return v.getValue().getDoubleValue();
  }

  /**
   * Sum metric of sessions for a given day
   * 
   * @param name
   *          name of the value
   * @param d
   *          day offset (i.e. 0 for today, -1 for yesterday)
   * @param defaultValue
   *          value to be used if no sessions / metrics exist
   * @return
   */
  protected double sessionSumDay(String name, double d, double defaultValue)
  {
    Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, getDate((int) d));
    if( values.size() == 0 )
    {
      // System.out.println(name + "@" + d + "= " + defaultValue + "(default)");
      return defaultValue;
    }
    else
    {
      double r= values.stream().map(v -> v.getDoubleValue()).reduce(0d, (a, b) -> a + b);
      // System.out.println(name + "@" + d + "= " + r);
      return r;
    }
  }

  /**
   * Sum all values for the current (date of computer) week.
   *
   * @param name
   *          name of the value
   * @param offset
   *          week offset (i.e. 0 for this, -1 for last week, 1 next week)
   * @param defaultValue
   *          value to be used if no sessions / metrics exist on a given day
   * @return sum of all values/metrics
   */
  protected double sessionSumWeek(String name, int offset, double defaultValue)
  {
    /*
     * This function is a little bit weird or might not bee intuitive at first glance. First step is
     * to compute the offset (start) for Monday. Then we loop 7 times to get the values for each day.
     */
    double ret= 0;
    int start= 0;
    switch( computer.getDate().get(Calendar.DAY_OF_WEEK) )
    {
      case Calendar.MONDAY:
        start= 0;
        break;
      case Calendar.TUESDAY:
        start= -1;
        break;
      case Calendar.WEDNESDAY:
        start= -2;
        break;
      case Calendar.THURSDAY:
        start= -3;
        break;
      case Calendar.FRIDAY:
        start= -4;
        break;
      case Calendar.SATURDAY:
        start= -5;
        break;
      case Calendar.SUNDAY:
        start= -6;
        break;
    }
    start+= offset * 7;
    // System.out.println("Start: " + start);

    boolean found= false;

    for(int i= 0; i < 7; i++)
    {
      Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, getDate(start));
      if( values.size() > 0 )
      {
        // System.out.println("Values on " + start + " " + values.toString());
        found= true;
        ret+= values.stream().map(v -> v.getDoubleValue()).reduce(0d, (a, b) -> a + b);
      }
      // else
      //  System.out.println("No values on " + start);

      start++;
    }

    if( found )
      return ret;
    else
      return defaultValue;
  }

  /**
   * Sum all values of type 'name' in the current (date of computer) month.
   * 
   * @param name
   *          name of the value
   * @param offset
   *          month offset (i.e. 0 for this month, -1 for last month, 1 next month)
   * @param defaultValue
   *          value to be used if no sessions / metrics exist in the specified month
   * @return sum of all values/metrics
   */
  protected double sessionSumMonth(String name, int offset, double defaultValue)
  {
    Calendar date= Calendar.getInstance();
    date.clear();
    int month= computer.getDate().get(Calendar.MONTH);
    date.set(Calendar.YEAR, computer.getDate().get(Calendar.YEAR));
    date.set(Calendar.MONTH, month);
    date.add(Calendar.MONTH, offset);

    month+= offset;

    double ret= 0;

    boolean found= false;

    while( date.get(Calendar.MONTH) == month )
    {
      Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, date);
      if( values.size() != 0 )
      {
        found= true;
        ret+= values.parallelStream().map(v -> v.getDoubleValue()).reduce(0d, (a, b) -> (a + b));
      }
      date.add(Calendar.DAY_OF_YEAR, 1);
    }

    if( found )
      return ret;
    else
      return defaultValue;
  }

  /**
   * Sum all values of type 'name' in the current (date of computer) year.
   * 
   * @param name
   *          name of the value
   * @param offset
   *          year offset (i.e. 0 for this year, -1 for last year, 1 next year)
   * @param defaultValue
   *          value to be used if no sessions / metrics exist in the specified year
   * @return sum of all values/metrics
   */
  protected double sessionSumYear(String name, int offset, double defaultValue)
  {
    Calendar date= Calendar.getInstance();
    date.clear();
    int year= computer.getDate().get(Calendar.YEAR) + offset;
    date.set(Calendar.YEAR, year);
    date.set(Calendar.MONTH, Calendar.JANUARY);
    date.set(Calendar.DAY_OF_YEAR, 1);

    double ret= 0;

    boolean found= false;

    while( date.get(Calendar.YEAR) == year )
    {
      Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, date);
      if( values.size() != 0 )
      {
        found= true;
        ret+= values.parallelStream().map(v -> v.getDoubleValue()).reduce(0d, (a, b) -> (a + b));
      }
      date.add(Calendar.DAY_OF_YEAR, 1);
    }

    if( found )
      return ret;
    else
      return defaultValue;
  }

  /**
   * max of the metric of sessions for a given day
   * 
   * @param name
   *          name of the value
   * @param d
   *          day offset (i.e. 0 for today, -1 for yesterday)
   * @param defaultValue
   *          value to be used if no sessions / metrics exist
   * @return
   */
  protected double sessionMaxDay(String name, double d, double defaultValue)
  {
    Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, getDate((int) d));
    if( values.size() == 0 )
    {
      // System.out.println(name + "@" + d + "= " + defaultValue + "(default)");
      return defaultValue;
    }
    else
    {
      double r= values.stream().map(v -> v.getDoubleValue()).reduce(Double.MIN_VALUE, (a, b) -> Math.max(a,b));
      // System.out.println(name + "@" + d + "= " + r);
      return r;
    }
  }

  protected double sessionMaxMonth(String name, int d)
  {
    Calendar date= Calendar.getInstance();
    date.clear();
    int month= computer.getDate().get(Calendar.MONTH);
    date.set(Calendar.YEAR, computer.getDate().get(Calendar.YEAR));
    date.set(Calendar.MONTH, month);
    // date.set(Calendar.DAY_OF_MONTH, 1);
    // date.set(Calendar.HOUR_OF_DAY,0 );
    month+= d;
    date.add(Calendar.MONTH, d);

    double ret= Double.MIN_VALUE;

    while( date.get(Calendar.MONTH) == month )
    {
      Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, date);
      // System.out.println(date);
      // System.out.println("Fetching " + name + " for " + BaseSimpleExperiment.DateFormat.format(date) + " " + values.toString());
      // System.out.println("Fetching " + name + " for " + " " + values.toString());
      if( values.size() != 0 )
      {
        ret= values.stream().map(v -> v.getDoubleValue()).reduce(ret, (a, b) -> Math.max(a, b));
        // System.out.println(ret);
      }
      date.add(Calendar.DAY_OF_YEAR, 1);
    }

    return ret;
  }

  protected double sessionMaxYear(String name, int d)
  {
    Calendar date= Calendar.getInstance();
    date.clear();
    int year= computer.getDate().get(Calendar.YEAR) + d;
    date.set(Calendar.YEAR, year);

    double ret= Double.MIN_VALUE;

    while( date.get(Calendar.YEAR) == year )
    {
      Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, date);
      if( values.size() != 0 )
      {
        ret= values.stream().map(v -> v.getDoubleValue()).reduce(ret, (a, b) -> Math.max(a, b));
      }
      date.add(Calendar.DAY_OF_YEAR, 1);
    }

    return ret;
  }

  /**
   * Calculate the average of metric of sessions for a given day
   * 
   * @param name
   *          name of the value
   * @param d
   *          day offset (i.e. 0 for today, -1 for yesterday)
   * @param defaultValue
   *          value to be used if no sessions / metrics exist
   * @return
   */
  protected double sessionMeanDay(String name, double d, double defaultValue)
  {
    Collection<Value> values= computer.getSessionValuesProvider().getValuesDay(name, getDate((int) d));
    if( values.size() == 0 )
    {
      // System.out.println(name + "@" + d + "= " + defaultValue + "(default)");
      return defaultValue;
    }
    else
    {
      double r= values.stream().map(v -> v.getDoubleValue()).reduce(0d, (a, b) -> a + b);
      
      return r / (double) values.size();
    }
  }

  protected Calendar getDate(int d)
  {
    Calendar c= Calendar.getInstance();
    c.setTime(computer.getDate().getTime());
    c.add(Calendar.DATE, d);
    return c;
  }
  
  @Override
  public TimedValue getResult()
  {
    return new TimedValue(computer.getDate().getTimeInMillis(), new NumberValue(getName(), value));
  }
  
  @Override
  public Collection<String> getDependencies()
  {
    return null;
  }
  
  /**
   * Output the value
   */
  protected void debug()
  {
    System.out.println(getName() + "= " + value);
  }
}
