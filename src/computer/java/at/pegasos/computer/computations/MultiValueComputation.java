package at.pegasos.computer.computations;

import java.util.Collection;

import at.pegasos.computer.Computation;
import at.pegasos.data.Value;

public interface MultiValueComputation extends Computation {
  /**
   * Get the result of this computation. Each sub result is represented as a single value. If only
   * one value is the possible output of this computation an implementation as ValueComputation
   * should be considered.
   * 
   * @return collection of values. null or empty for no result are acceptable
   */
  public Collection<Value> getResult();

  /**
   * Get the names of the values computed by this computation
   * 
   * @return
   */
  public Collection<String> getNames();
}
