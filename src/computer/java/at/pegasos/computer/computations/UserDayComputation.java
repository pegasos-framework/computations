package at.pegasos.computer.computations;

import java.util.Calendar;

import at.pegasos.data.NumberValue;
import at.pegasos.data.TimedValue;
import at.pegasos.computer.BasicUserValueComputation;

/**
 * Helper class for creating a value valid for the current (date of computer) day
 */
public abstract class UserDayComputation extends BasicUserValueComputation {

  @Override
  public TimedValue getResult()
  {
    Calendar c= Calendar.getInstance();
    c.clear();
    c.set(Calendar.YEAR, computer.getDate().get(Calendar.YEAR));
    c.set(Calendar.DAY_OF_YEAR, computer.getDate().get(Calendar.DAY_OF_YEAR));

    Calendar end= Calendar.getInstance();
    end.setTimeInMillis(c.getTimeInMillis());
    end.add(Calendar.DAY_OF_YEAR, 1);
    end.add(Calendar.MILLISECOND, -1);

    long start= c.getTimeInMillis();

    return new TimedValue(start, end.getTimeInMillis(), new NumberValue(getName(), value));
  }
}
