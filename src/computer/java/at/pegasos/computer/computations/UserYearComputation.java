package at.pegasos.computer.computations;

import java.util.Calendar;

import at.pegasos.data.NumberValue;
import at.pegasos.data.TimedValue;
import at.pegasos.computer.BasicUserValueComputation;

/**
 * Helper class for creating a value valid for the current (date of computer) year
 */
public abstract class UserYearComputation extends BasicUserValueComputation {

  @Override
  public TimedValue getResult()
  {
    Calendar start= Calendar.getInstance();
    start.clear();

    Calendar cd= computer.getDate();
    start.set(Calendar.YEAR, cd.get(Calendar.YEAR));
    start.set(Calendar.DAY_OF_MONTH, 1);

    long startts= start.getTimeInMillis();

    start.add(Calendar.YEAR, 1);
    start.add(Calendar.MILLISECOND, -1);

    long endts= start.getTimeInMillis();

    return new TimedValue(startts, endts, new NumberValue(getName(), value));
  }
}
