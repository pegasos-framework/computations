package at.pegasos.computer.computations;

import java.util.HashMap;
import java.util.Map;

import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.computer.ValueComputation;

/**
 * Utility class for calculating a value for a session.
 */
public abstract class BasicValueComputationWithSub implements ValueComputation {
  
  /**
   * Internal representation of the result
   */
  protected Double value;

  protected Map<String, Value> subValues= new HashMap<String, Value>();
  
  protected void setEmpty()
  {
    subValues= null;
    value= null;
  }

  @Override
  public Value getResult()
  {
    if( subValues == null )
    {
      if( value != null )
        return new NumberValue(getName(), value);
      else
        return null;
    }
    else
    {
      NumberValue ret;
      if( value != null )
        ret= new NumberValue(getName(), value);
      else
        ret= new NumberValue(getName(), 0);
      ret.setSubValues(subValues);
      return ret;
    }
  }

  /**
   * Output the value
   */
  protected void debug()
  {
    System.out.println(getName() + "= " + value);
  }
}
