package at.pegasos.computer.computations;

import at.pegasos.computer.BasicUserValueComputation;
import at.pegasos.data.NumberValue;
import at.pegasos.data.TimedValue;

/**
 * Helper class for creating a value valid from now (date of computation)
 */
public abstract class UserFromNowComputation extends BasicUserValueComputation {

  @Override
  public TimedValue getResult()
  {
    return new TimedValue(computer.getDate().getTimeInMillis(), new NumberValue(getName(), value));
  }
}
