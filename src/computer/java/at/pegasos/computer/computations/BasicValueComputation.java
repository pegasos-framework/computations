package at.pegasos.computer.computations;

import at.pegasos.data.*;
import at.pegasos.computer.ValueComputation;

/**
 * Utility class for calculating a value for a session.
 */
public abstract class BasicValueComputation implements ValueComputation {

  /**
   * Internal representation of the result
   */
  protected double value;

  protected Value[] segmentValues;

  /**
   * This flag indicates that setEmpty was called. Its here as a workaround since somehow setting
   * value as Double crashes other code ...
   */
  protected boolean empty= false;

  /**
   * Set this value as not existing / empty / not applicable / not computed
   */
  protected void setEmpty()
  {
    empty= true;
  }

  @Override
  public Value getResult()
  {
    if( segmentValues == null )
    {
      if( empty )
        return null;
      return new NumberValue(getName(), value);
    }
    else
    {
      NumberValue ret;
      if( empty )
        ret= new NumberValue(getName(), 0);
      else
        ret= new NumberValue(getName(), value);
      ret.setSegmentValues(segmentValues);
      return ret;
    }
  }

  /**
   * Output the value
   */
  protected void debug()
  {
    System.out.println(getName() + "= " + value);
  }
}
