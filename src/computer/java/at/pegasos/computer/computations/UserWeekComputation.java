package at.pegasos.computer.computations;

import java.util.Calendar;

import at.pegasos.data.NumberValue;
import at.pegasos.data.TimedValue;
import at.pegasos.computer.BasicUserValueComputation;

/**
 * Helper class for creating a value valid for the current (date of computer) week
 */
public abstract class UserWeekComputation extends BasicUserValueComputation {

  @Override
  public TimedValue getResult()
  {
    Calendar start= Calendar.getInstance();
    start.clear();

    Calendar cd= computer.getDate();
    start.set(Calendar.YEAR, cd.get(Calendar.YEAR));
    // System.out.println("start:" + GoldenCheetahCsvFormat.format(start.getTime()) + " " + computer.getDate().get(Calendar.DAY_OF_WEEK));
    start.set(Calendar.DAY_OF_YEAR, cd.get(Calendar.DAY_OF_YEAR));
    // System.out.println("start:" + GoldenCheetahCsvFormat.format(start.getTime()));
    // Our weeks are supposed to start on Monday
    long startts= 0;
    int dow= cd.get(Calendar.DAY_OF_WEEK);
    switch( dow )
    {
      case Calendar.MONDAY:
        startts= start.getTimeInMillis();
        break;
      case Calendar.TUESDAY:
        startts= start.getTimeInMillis() - 24 * 60 * 60 * 1000;
        break;
      case Calendar.WEDNESDAY:
        startts= start.getTimeInMillis() - 2 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.THURSDAY:
        startts= start.getTimeInMillis() - 3 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.FRIDAY:
        startts= start.getTimeInMillis() - 4 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.SATURDAY:
        startts= start.getTimeInMillis() - 5 * 24 * 60 * 60 * 1000;
        break;
      case Calendar.SUNDAY:
        startts= start.getTimeInMillis() - 6 * 24 * 60 * 60 * 1000;
        break;
    }
    long endts= startts + 7 * 24 * 60 * 60 * 1000 - 1;
    
    return new TimedValue(startts, endts, new NumberValue(getName(), value));
  }
}
