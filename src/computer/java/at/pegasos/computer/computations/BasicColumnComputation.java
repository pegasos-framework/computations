package at.pegasos.computer.computations;

import java.util.ArrayList;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.computer.ColumnComputation;

public abstract class BasicColumnComputation implements ColumnComputation {

  protected final static boolean[] ORIG_HELP= new boolean[] {true};

  protected Column res;

  private final String name;

  private final int type;

  public BasicColumnComputation(String name, int type)
  {
    this.name= name;
    this.type= type;
  }

  /**
   * Initialise the resulting column. As we are expected to have the same rows time is not
   * initialised.
   * 
   * @param data
   */
  protected void init(Data data)
  {
    res= new Column();
    res.name= "data";
    res.names= new String[] {name};
    res.types= new int[] {type};
    int size= data.getRowCount();
    res.values= new ArrayList<Object[]>(size);
    res.orig= new ArrayList<boolean[]>(size);
  }

  public String getName()
  {
    return name;
  }

  public Column getResult()
  {
    return res;
  }
}
