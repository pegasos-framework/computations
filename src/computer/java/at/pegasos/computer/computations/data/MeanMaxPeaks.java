package at.pegasos.computer.computations.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import at.pegasos.data.Data;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;
import at.pegasos.computer.computations.MultiValueComputation;

public class MeanMaxPeaks implements MultiValueComputation {

  private final Collection<String> dep;

  private final String src_name;
  private final String dest_name;
  private final double[] durations;
  
  private double[] max;
  
  /**
   * 
   * @param dest_name
   * @param src_name
   * @param durations in seconds
   */
  public MeanMaxPeaks(String dest_name, String src_name, double... durations)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.durations= new double[durations.length];
    for(int i= 0; i < durations.length; i++)
      this.durations[i]= durations[i] * 1000;

    dep= new ArrayList<String>(1);
    dep.add(src_name);
  }
  
  public String getName()
  {
    return this.dest_name;
  }
  
  private void init(Data data)
  {
    max= new double[durations.length];
    for(int i= 0; i < durations.length; i++)
      max[i]= Double.MIN_VALUE;
  }
  
  public void compute(Computer computer)
  {
    Data data= computer.getData();
    
    init(data);
    
    final int power= data.getColumnIndex(src_name);
    TableIterator it= data.iterateTable();
    
    @SuppressWarnings("unchecked")
    List<Double> sums[]= new List[durations.length];
    @SuppressWarnings("unchecked")
    List<Long> times[]= new List[durations.length];
    
    double[] sum= new double[durations.length]; // moving average sum
    long currTime;
    long[] from= new long[durations.length]; 
    long lastTime= data.getBegin();
    
    for(int i= 0; i < durations.length; i++)
    {
      from[i]= lastTime;
      int count= (int) (durations[i] / 1000.0) + 1;
      sums[i]= new ArrayList<Double>(count);
      times[i]= new ArrayList<Long>(count);
    }
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Double p= Helper.asDoubleOrNull(row[power]);
      
      // update time
      currTime= it.getTimestamp();
      
      for(int i= 0; i < durations.length; i++)
      {
        // TODO: for times other than 1s this would need interpolation as we could remove more than necessary
        while( currTime - from[i] > durations[i] && sums[i].size() > 0 )
        {
          sum[i]-= sums[i].remove(0);
          from[i]= times[i].remove(0);
        }
      }
      
      if( it.isOriginal(power) )
      {
        if( p != null )
        {
          double pp= ((double) p) * (currTime - lastTime);
          for(int i= 0; i < durations.length; i++)
          {
            sums[i].add(pp);
            times[i].add(currTime);
            sum[i]+= pp;
          }
        }
        else
        {
          for(int i= 0; i < durations.length; i++)
          {
            sums[i].add(0d);
            times[i].add(currTime);
          }
        }
        lastTime= currTime;
      }
      
      for(int i= 0; i < durations.length; i++)
      {
        double v= (sum[i] / durations[i]);
        if( currTime - from[i] >= durations[i] && v > max[i] )
        {
          max[i]= v;
        }
      }
    }
  }
  
  public Collection<Value> getResult()
  {
    List<Value> values= new ArrayList<Value>(durations.length);
    for(int i= 0; i < durations.length; i++)
    {
      if( max[i] > Double.MIN_VALUE )
      {
        values.add(new NumberValue(dest_name + (durations[i] / 1000), max[i]));
      }
    }
    return values;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }

  @Override
  public Collection<String> getNames()
  {
    // TODO: this assumes that the value can actualy be provided.
    // However for the currenct API / implementation there is no way to assess whether this is true
    List<String> ret= new ArrayList<String>(durations.length);
    for(int i= 0; i < durations.length; i++)
    {
      ret.add(dest_name + (durations[i] / 1000));
    }
    return ret;
  }
}