package at.pegasos.computer.computations.data;

import java.util.*;
import java.util.Collection;

import at.pegasos.computer.Computer;
import at.pegasos.computer.Subset;
import at.pegasos.computer.computations.BasicValueComputation;
import at.pegasos.data.*;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.compute.*;

public class FirstGreater extends BasicValueComputation {
  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(1);
  }

  private final String src_name;
  private final String dest_name;
  private Subset sub;

  private double searchValue;

  public FirstGreater(String dest_name, String src_name, double value)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.searchValue= value;
    dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();
    value= -1d;

    if( data.getRowCount() == 0 )
      return;

    final int colIdx= data.getColumnIndex(src_name);

    if( sub != null )
    {
      TableIterator it= data.iterateTable();

      int rowIdx= 1;
      it= data.iterateTable(sub.getStart(computer));
      rowIdx= sub.getStart(computer);

      while( it.hasNext() && rowIdx <= sub.getEnd(computer) )
      {
        Object[] row= it.next();
        Object p= row[colIdx];

        if( p != null && it.isOriginal(colIdx) )
        {
          double val= Helper.asDouble(p);
          if( val > searchValue )
          {
            value= (double) rowIdx;
            break;
          }
        }
        rowIdx++;
      }
    }
    else
    {
      TableIterator it= data.iterateTable();
      int rowIdx= 1;

      while( it.hasNext() )
      {
        Object[] row= it.next();
        Object p= row[colIdx];

        if( p != null && it.isOriginal(colIdx) )
        {
          double val= Helper.asDouble(p);
          if( val > searchValue )
          {
            value= (double) rowIdx;
            break;
          }
        }
        rowIdx++;
      }
    }
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
