package at.pegasos.computer.computations.data;

import java.util.*;
import java.util.function.Predicate;

import java.sql.Types;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.computer.ColumnComputation;
import at.pegasos.computer.Computer;

public class ColumnFilter implements ColumnComputation {
  
  private final static boolean[] ORIG_HELP= new boolean[] {true};

  private final Collection<String> dep;
  
  private final String src_name;
  private final String dest_name;
  private final Predicate<Object> predicate;
  
  private Column res;
  
  public ColumnFilter(String dest_name, String src_name, Predicate<Object> predicate)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.predicate= predicate;

    dep = Collections.singletonList(src_name);
  }
  
  public String getName()
  {
    return this.dest_name;
  }
  
  private void init(Data data)
  {
    res= new Column();
    res.name= "data";
    res.names= new String[] {this.dest_name/*, this.dest_name + "_sums"*/};
    res.types= new int[] {Types.DOUBLE};
    int size= data.getRowCount();
    res.values= new ArrayList<Object[]>(size);
    res.orig= new ArrayList<boolean[]>(size);
  }
  
  public void compute(Computer computer)
  {
    Data data= computer.getData();
    
    init(data);
    
    final int valueCol= data.getColumnIndex(src_name);
    TableIterator it= data.iterateTable();
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object p= row[valueCol];
      Object[] o;
      
      if( p != null && it.isOriginal(valueCol) )
      {
        if( predicate.test(p) )
        {
          o= new Object[] {p};
        }
        else
        {
          o= new Object[] {null};
        }
      }
      else
      {
        o= new Object[] {null};
      }
      
      res.values.add(o);
      res.orig.add(ORIG_HELP);
      // System.out.println(currTime + " " + v);
    }
  }
  
  public Column getResult()
  {
    return res;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
