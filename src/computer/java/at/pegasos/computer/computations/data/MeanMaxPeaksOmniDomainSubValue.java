package at.pegasos.computer.computations.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.util.Pair;

import at.pegasos.computer.Subset;
import at.pegasos.computer.computations.BasicValueComputationWithSub;
import at.pegasos.data.Data;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.NumberValue;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;

public class MeanMaxPeaksOmniDomainSubValue extends BasicValueComputationWithSub {

  private final String src_name;
  private final String dest_name;

  private final Collection<String> dep;

  private Subset sub;

  long currTime;

  private long lastTime;

  /**
   * Total duration in [s]
   */
  int duration;

  List<Pair<Long,Double>> A;
  List<Long> times;
  private double[] finalMax;

  /**
   * 
   * @param dest_name
   * @param src_name
   */
  public MeanMaxPeaksOmniDomainSubValue(String dest_name, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;

    this.dep= new ArrayList<String>(1);
    this.dep.add(src_name);
  }
  
  /**
   * 
   * @param dest_name
   * @param src_name
   * @param durations
   *          in seconds
   */
  public MeanMaxPeaksOmniDomainSubValue(String dest_name, Subset sub, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.sub= sub;

    this.dep= new ArrayList<String>(1);
    this.dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    long dataBegin;
    long dataEnd;

    if( sub == null )
    {
      dataBegin= data.getBegin();
      dataEnd= data.getEnd();
    }
    else
    {
      dataBegin= data.getTime(sub.getStart(computer)-1);
      dataEnd= data.getTime(sub.getEnd(computer)-1);
    }
    duration= (int) (dataEnd - dataBegin);
    duration+= data.getFrequeny();
    duration*= computer.getTimeScale();
    lastTime= dataBegin - data.getFrequeny();

    final int colIdx= data.getColumnIndex(src_name);
    TableIterator it;

    // Go to the beginning of the relevant data
    int rowIdx;
    if( sub != null )
    {
      it= data.iterateTable(sub.getStart(computer));
      rowIdx= sub.getStart(computer);
    }
    else
    {
      it= data.iterateTable();
      rowIdx= 0;
    }

    A= new ArrayList<Pair<Long, Double>>(duration);
    times= new ArrayList<Long>(duration);

    // pre compute values and times
    while( it.hasNext() && (sub == null || rowIdx <= sub.getEnd(computer)) )
    {
      Object[] row= it.next();
      Double p= Helper.asDoubleOrNull(row[colIdx]);
      rowIdx++;

      // update time
      currTime= it.getTimestamp();
      
      long timeDiff= (currTime - lastTime);
      boolean isBreak= computer.isBreak(timeDiff);
      if( it.isOriginal(colIdx) )
      {
        if( p != null )
        {
          if( isBreak )
            timeDiff= data.getFrequeny();
          double x_i= Helper.asDouble(p) * timeDiff;
          A.add(new Pair<Long, Double>(currTime, x_i));
          lastTime= currTime;
        }
      }
    }

    finalMax= new double[duration];
    for(int i= 0; i < finalMax.length; i++)
    {
      finalMax[i]= Double.MIN_VALUE;
    }

    // compute all possible max sums
    for(int i= 0; i < A.size(); i++)
    {
      // Forward pass
      int j= i;
      double sum= 0;
      for(Pair<Long, Double> a : A.subList(i, A.size()))
      {
        sum+= a.getSecond();

        double durEnding= (A.get(j).getFirst() - A.get(i).getFirst() + data.getFrequeny()) * computer.getTimeScale();

        finalMax[((int) durEnding)-1]= Math.max(finalMax[((int) durEnding)-1], sum);
        j++;
      }
    }

    output();
  }

  /**
   * Create the output values
   */
  private void output()
  {
    double timeScale = 1 / 1000.0;
    for(int i= 0; i < finalMax.length; i++)
    {
      if( finalMax[i] > Double.MIN_VALUE )
      {
        subValues.put("" + (i+1), new NumberValue(dest_name + (i+1), finalMax[i] / (i+1) * timeScale));
      }
    }
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
