package at.pegasos.computer.computations.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import at.pegasos.data.Data;
import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.data.Data.ColumnIterator;
import at.pegasos.computer.Computer;
import at.pegasos.computer.computations.MultiValueComputation;

public class ZoneAnalysis implements MultiValueComputation {
  
  private class Zone {
    
    @Override
    public String toString()
    {
      return "Zone [idx=" + idx + ", val=" + val + ", time=" + time + ", sum=" + sum + ", wsum=" + wsum + "]";
    }
    
    int idx;
    double val;
    double time;
    double sum;
    double wsum;
  }
  
  private final Collection<String> names;
  
  private final String inputName;
  private final String colName;
  private final String outputName;
  private final boolean time;
  private final boolean sum;
  private final boolean wsum;
  
  Zone zones[];
  
  private Value[] values;
  
  private Data data;
  
  /**
   * Analyse data in a column based on zones. For each zone the analysis will be computed as a
   * single variable. Zones are defined using input variables. For each zone a variable specifying
   * the lower bound has to be present. Pattern of the variable names is `inputName`_`index`. For
   * each zone the following variables will be created
   * <ul>
   * <li>TIME_IN_`outputName`_`index` (accumulated time in zone)</li>
   * <li>SUM_IN_`outputName`_`index` (sum of all values for this zone)</li>
   * <li>WSUM_IN_`outputName`_`index` (time weighted sum of all values for this zone)</li>
   * </ul>
   * 
   * @param inputName
   *          variable name for the definition of zones
   * @param colName
   *          name of the column to be used for the analysis
   * @param outputName
   *          name for the output variable
   */
  public ZoneAnalysis(String outputName, String colName, String inputName) 
  {
    this.inputName= inputName;
    this.colName= colName;
    this.outputName= outputName;
    time= true;
    sum= true;
    wsum= true;
    
    names= new ArrayList<String>(30);
    for(int i= 1; i < 10; i++)
    {
      names.add("TIME_IN_" + outputName + "_" + i);
      names.add("SUM_IN_" + outputName + "_" + i);
      names.add("wSUM_IN_" + outputName + "_" + i);
    }
  }
  
  public String getName()
  {
    return colName + "_ZoneAnalysis";
  }
  
  public void compute(Computer computer)
  {
    data= computer.getData();
    
    init(computer);
    
    if( zones.length == 0)
    {
      System.out.println("No zones defined for " + inputName + "/" + colName + "/" + outputName);
      return;
    }
    else
      System.out.println(Arrays.toString(zones));
    
    ColumnIterator it= data.iterateTableColumn(colName);
    int idx= 0;
    long prev_ts= data.getBegin();
    // Estimate the first time weight
    switch(data.getTimeUnit())
    {
      case MicroSecond:
        prev_ts-= data.getFrequeny() / 1000;
        break;
      case MilliSecond:
        prev_ts-= data.getFrequeny();
        break;
      case Second:
        prev_ts-= data.getFrequeny() * 1000;
        break;
      default:
        break;
    }

    while( it.hasNext() )
    {
      Double power= asDouble(it.next());
      long ts= it.getTimestamp();

      if( power != null )
      {
        while( idx >= 0 && power < zones[idx].val )
        {
          idx--;
        }
        // If value is out of bounds continue with next
        if( idx == -1 )
        {
          idx= 0;
          continue;
        }
        while( power > zones[idx].val && (idx + 1) < zones.length && power > zones[idx + 1].val )
        {
          idx++;
        }
        
        double t= (ts - prev_ts) / 1000.0;
        if( time )
          zones[idx].time+= t;
        if( sum )
          zones[idx].sum+= power;
        if( wsum )
          zones[idx].wsum+= t * power;
      }
      
      prev_ts= ts;
    }
    
    idx= 0;
    for(Zone z : zones)
    {
      if( time )
      {
        values[idx++]= new NumberValue("TIME_IN_" + outputName + "_" + z.idx, z.time);
      }
      if( sum )
      {
        values[idx++]= new NumberValue("SUM_IN_" + outputName + "_" + z.idx, z.sum);
      }
      if( wsum )
      {
        values[idx++]= new NumberValue("WSUM_IN_" + outputName + "_" + z.idx, z.wsum);
      }
    }
    
    // System.out.println("Zones: " + Arrays.toString(zones));
  }
  
  private Double asDouble(Object obj)
  {
    if( obj instanceof Double )
      return (Double) obj;
    else if( obj instanceof Integer )
      return ((Integer) obj).doubleValue();
    
    return null;
  }
  
  private void init(Computer computer)
  {
    final Set<String> names= computer.getValueNames();
    final int inL= inputName.length() + 1;
    ArrayList<Zone> borders= new ArrayList<Zone>(names.size());
    
    for(String name : names)
    {
      if( name.startsWith(inputName) )
      {
        int idx= Integer.parseInt(name.substring(inL));
        Value v= computer.getValue(name);
        
        Zone z= new Zone();
        z.idx= idx;
        z.val= v.getDoubleValue();
        
        int i= 0;
        while( i < borders.size() && borders.get(i).idx < idx )
        {
          i++;
        }
        borders.add(i, z);
      }
    }
    
    zones= borders.toArray(new Zone[borders.size()]);
    
    values= new Value[zones.length * ((time ? 1 : 0) + (sum ? 1 : 0) + (wsum ? 1 : 0))];
  }
  
  public Collection<Value> getResult()
  {
    return Arrays.asList(values);
  }

  @Override
  public Collection<String> getDependencies()
  {
    return new ArrayList<String>(0);
  }

  @Override
  public Collection<String> getNames()
  {
    // return Arrays.asList(values).stream().map(v -> v.getName()).collect(Collectors.toList());
	// return Arrays.asList(new String[] {outputName});
    return names;
  }
}
