package at.pegasos.computer.computations.data;

import java.util.ArrayList;
import java.util.Collection;

import at.pegasos.data.Data;
import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.computer.ValueComputation;

public class Length implements ValueComputation {

  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(1);
  }

  private final String dest_name;

  private Value value;

  public Length(String dest_name, String src_name)
  {
    this.dest_name= dest_name;
    dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    value= new NumberValue(dest_name, data.getRowCount());
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }

  @Override
  public Value getResult()
  {
    return value;
  }
}
