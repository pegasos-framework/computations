package at.pegasos.computer.computations;

import java.util.ArrayList;
import java.util.Collection;

import at.pegasos.data.Data;
import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.computer.ValueComputation;

public class First implements ValueComputation {
  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(1);
  }

  private final String src_name;
  private final String dest_name;

  private Value value;

  public First(String dest_name, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    if( data.getRowCount() == 0 )
      return;

    final int colIdx= data.getColumnIndex(src_name);

    Object o= data.getRow(0)[colIdx];

    if( o instanceof Double )
    {
      value= new NumberValue(dest_name, (Double) o);
    }
    else if( o instanceof Integer )
    {
      value= new NumberValue(dest_name, (Integer) o);
    }
    else if( o instanceof Float )
    {
      value= new NumberValue(dest_name, (Float) o);
    }
    else
    {
      throw new IllegalStateException("Getting the first element of a " + o.getClass() + " column is not implemented");
    }
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }

  @Override
  public Value getResult()
  {
    return value;
  }
}
