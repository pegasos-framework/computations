package at.pegasos.computer.computations.math;

import java.util.*;

import java.sql.Types;

import at.pegasos.computer.ColumnComputation;
import at.pegasos.computer.Computer;
import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.compute.*;

public class ColumnMovingAverage implements ColumnComputation {
  
  private final static boolean[] ORIG_HELP= new boolean[] {true};
  
  private final String src_name;
  private final String dest_name;
  private final double duration;
  
  private Column res;
  
  // TODO: integrate time units
  public ColumnMovingAverage(String dest_name, String src_name, double duration)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.duration= duration * 1000;
  }
  
  public String getName()
  {
    return this.dest_name;
  }
  
  private void init(Data data)
  {
    res= new Column();
    res.name= "data";
    res.names= new String[] {this.dest_name/*, this.dest_name + "_sums"*/};
    res.types= new int[] {Types.DOUBLE};
    int size= data.getRowCount();
    res.values= new ArrayList<Object[]>(size);
    res.orig= new ArrayList<boolean[]>(size);
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    init(data);
    
    final int srcCol= data.getColumnIndex(src_name);
    TableIterator it= data.iterateTable();
    
    List<Double> sums= new ArrayList<Double>();
    List<Long> times= new ArrayList<Long>();
    
    // double totalSum= 0;
    double sum= 0; // moving average sum
    double csum= 0; // sum for the current item
    long currTime;
    long from= data.getBegin();
    long lastTime= from;
    
    // Estimate the first time weight
    switch(data.getTimeUnit())
    {
      case MicroSecond:
        lastTime-= data.getFrequeny() / 1000;
        break;
      case MilliSecond:
        lastTime-= data.getFrequeny();
        break;
      case Second:
        lastTime-= data.getFrequeny() * 1000;
        break;
      default:
        // TODO: throw exception
        break;
    }
    from= lastTime;

    // System.out.println("Moving Average " + this.src_name);

    // Save average. If the computer does not have the value, we save it
    // final boolean savg= !computer.hasValue(src_name + "_AVERAGE");
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Double p= Helper.asDouble(row[srcCol]);
      
      // update time
      currTime= it.getTimestamp();
      // TODO: for times other than 1s this would need interpolation as we could remove more than necessary
      // TODO: epsilon only 100 for 1000 time steps?
      final long EPSILON= 100;
//      System.out.printf("currTime=%d, from=%d, sums.size=%d\n", currTime, from, sums.size());
      while( (currTime - from - EPSILON) > duration && sums.size() > 0 )
      // while( currTime > from + duration )
      {
        // System.out.print(" Remove");
        sum-= sums.remove(0);
        from= times.remove(0);
//        System.out.printf("now at from=%d\n", from);
      }
//      System.out.printf("currTime=%d, from=%d, sums.size=%d, sum=%f, sums:%s\n", currTime, from, sums.size(), sum, sums.toString());
      // System.out.println(" from: " + from + " " + times.toString());
      
      if( it.isOriginal(srcCol) )
      {
        if( p != null )
        {
          csum= ((double) p) * (currTime - lastTime);
          sum+= csum;
        }
        else
        {
          csum= 0;
        }
        lastTime= currTime;
        
        sums.add(csum);
        times.add(currTime);
      }
//      System.out.printf("sums.size=%d, sum=%f, sums:%s\n", sums.size(), sum, sums.toString());
      
      // if( savg )
      //  totalSum+= sum;
      
      double v= (sum / duration);
      res.values.add(new Double[] {v});
      res.orig.add(ORIG_HELP);
      // System.out.println(currTime + " avg:" + v + " sum:" + sum + " sums:" + sums.toString());
    }
    
    // if( savg )
    // {
    //   computer.setValue(new NumberValue(src_name + "_AVERAGE", totalSum / ((data.getEnd() - data.getBegin()))));
    // }
  }
  
  public Column getResult()
  {
    return res;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return Arrays.asList(new String[] {src_name});
  }
}
