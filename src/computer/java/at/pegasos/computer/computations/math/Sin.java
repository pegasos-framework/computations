package at.pegasos.computer.computations.math;

import java.util.ArrayList;
import java.util.Collection;

import java.sql.Types;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.computer.ColumnComputation;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;

public class Sin implements ColumnComputation {

  private final Collection<String> dep;

  private final static boolean[] ORIG_HELP= new boolean[] {true};

  private final String src_name;
  private final String dest_name;

  private Column res;

  public Sin(String dest_name, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;

    dep= new ArrayList<String>(1);
    dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  private void init(Data data)
  {
    res= new Column();
    res.name= "data";
    res.names= new String[] {this.dest_name};
    res.types= new int[] {Types.DOUBLE};
    int size= data.getRowCount();
    res.values= new ArrayList<Object[]>(size);
    res.orig= new ArrayList<boolean[]>(size);
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    init(data);

    final int srcIdx= data.getColumnIndex(src_name);
    TableIterator it= data.iterateTable();

    while( it.hasNext() )
    {
      Object[] row= it.next();
      Double val= (Double) row[srcIdx];

      res.values.add(new Double[] {Math.sin(Helper.asDouble(val))});
      res.orig.add(ORIG_HELP);
    }
  }

  public Column getResult()
  {
    return res;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
