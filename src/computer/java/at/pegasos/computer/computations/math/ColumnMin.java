package at.pegasos.computer.computations.math;

import java.util.ArrayList;
import java.util.Collection;

import at.pegasos.computer.Subset;
import at.pegasos.computer.computations.BasicValueComputation;
import at.pegasos.data.Data;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;

public class ColumnMin extends BasicValueComputation {

  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(1);
  }

  private final String src_name;
  private final String dest_name;
  private final Subset sub;

  public ColumnMin(String dest_name, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.sub= null;
    dep.add(src_name);
  }

  public ColumnMin(String dest_name, Subset sub, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.sub= sub;
    dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    final int colIdx= data.getColumnIndex(src_name);

    TableIterator it;

    double min= Double.MAX_VALUE;

    if( sub != null )
    {
      if( !sub.isApplicable(computer) )
      {
        setEmpty();

        return;
      }
      it= data.iterateTable(sub.getStart(computer));
      int rowIdx= sub.getStart(computer);

      while( it.hasNext() && rowIdx <= sub.getEnd(computer) )
      {
        Object[] row= it.next();
        Object p= row[colIdx];
        rowIdx++;

        if( it.isOriginal(colIdx) )
        {
          if( p != null )
          {
            double h= Helper.asDouble(p);

            if( h < min )
            {
              min= h;
            }
          }
        }
      }
    }
    else
    {
      it= data.iterateTable();

      while( it.hasNext() )
      {
        Object[] row= it.next();
        Object p= row[colIdx];

        if( it.isOriginal(colIdx) )
        {
          if( p != null )
          {
            double h= Helper.asDouble(p);

            if( h < min )
            {
              min= h;
            }
          }
        }
      }
    }

    value= min;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
