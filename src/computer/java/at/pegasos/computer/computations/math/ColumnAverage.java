package at.pegasos.computer.computations.math;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;

import at.pegasos.computer.Subset;
import at.pegasos.computer.computations.BasicValueComputation;
import at.pegasos.data.Data;
import at.pegasos.data.NumberValue;
import at.pegasos.data.SegmentedData;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.SegmentedData.SegmentIterator;
import at.pegasos.data.SegmentedData.SegmentedTableIterator;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;

public class ColumnAverage extends BasicValueComputation {

  private final Collection<String> dep;

  private final String src_name;
  private final String dest_name;
  private final Subset sub;

  public ColumnAverage(String dest_name, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.sub= null;

    dep= new ArrayList<String>(1);
    dep.add(src_name);
  }

  public ColumnAverage(String dest_name, Subset sub, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.sub= sub;

    dep= new ArrayList<String>(1);
    dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    final int colIdx= data.getColumnIndex(src_name);

    double csum= 0; // sum for the current item
    long currTime;
    long from= data.getBegin();
    long lastTime= from;
    BigDecimal totalSum= new BigDecimal(0);
    BigDecimal dur= new BigDecimal(0);
    long elapsed;

    if( sub != null )
    {
      if( !sub.isApplicable(computer) )
      {
        setEmpty();

        return;
      }
      TableIterator it= data.iterateTable(sub.getStart(computer));
      int rowIdx= sub.getStart(computer);

      while( it.hasNext() && rowIdx <= sub.getEnd(computer) )
      {
        Object[] row= it.next();
        Object p= row[colIdx];
        rowIdx++;

        // update time
        currTime= it.getTimestamp();

        if( it.isOriginal(colIdx) )
        {
          elapsed= (currTime - lastTime);
          if( p != null )
          {
            csum= Helper.asDouble(p) * elapsed;
          }
          else
          {
            csum= 0;
          }
          lastTime= currTime;

          totalSum= totalSum.add(new BigDecimal(csum));
          dur= dur.add(new BigDecimal(elapsed));
        }
      }
    }
    else
    {
      if( data instanceof SegmentedData )
      {
        SegmentIterator it= ((SegmentedData) data).iterateSegments();
        
        int segmentCount= ((SegmentedData) data).getSegmentCount();
        segmentValues= new Value[segmentCount];
        int segmentIdx= 0;

        // Iterate over all segments
        while(it.hasNext())
        {
          SegmentedTableIterator segment= it.next();
          BigDecimal segmentDur= new BigDecimal(0);
          BigDecimal segmentSum= new BigDecimal(0);
          
          while(segment.hasNext())
          {
            Object[] row= segment.next();
            Object p= row[colIdx];

            // update time
            currTime= segment.getTimestamp();

            if( segment.isOriginal(colIdx) )
            {
              elapsed= (currTime - lastTime);
              if( p != null )
              {
                csum= Helper.asDouble(p) * elapsed;
              }
              else
              {
                csum= 0;
              }
              lastTime= currTime;

              BigDecimal val= new BigDecimal(csum);
              totalSum= totalSum.add(val);
              segmentSum= segmentSum.add(val);

              dur= dur.add(new BigDecimal(elapsed));
              segmentDur= segmentDur.add(new BigDecimal(elapsed));
            }
          }

          if( segmentDur.longValue() != 0 )
          {
            double valueSegment= segmentSum.divide(segmentDur, RoundingMode.HALF_UP).doubleValue();
            segmentValues[segmentIdx++]= new NumberValue(getName(), valueSegment);
          }
          else
            segmentValues[segmentIdx++]= new NumberValue(getName(), 0);
        }
      }
      else
      {
        TableIterator it= data.iterateTable();

        while( it.hasNext() )
        {
          Object[] row= it.next();
          Object p= row[colIdx];

          // update time
          currTime= it.getTimestamp();

          if( it.isOriginal(colIdx) )
          {
            elapsed= (currTime - lastTime);
            if( p != null )
            {
              csum= Helper.asDouble(p) * elapsed;
            }
            else
            {
              csum= 0;
            }
            lastTime= currTime;

            totalSum= totalSum.add(new BigDecimal(csum));
            dur= dur.add(new BigDecimal(elapsed));
          }
        }
      }
    }

    if( dur.longValue() != 0 )
      value= totalSum.divide(dur, RoundingMode.HALF_UP).doubleValue();
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
