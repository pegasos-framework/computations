package at.pegasos.computer.computations.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import at.pegasos.computer.Subset;
import at.pegasos.computer.computations.BasicValueComputation;
import at.pegasos.data.Data;
import at.pegasos.data.NumberValue;
import at.pegasos.data.SegmentedData;
import at.pegasos.data.Value;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.SegmentedData.SegmentIterator;
import at.pegasos.data.SegmentedData.SegmentedTableIterator;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;

public class ColumnArea extends BasicValueComputation {

  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(1);
  }

  private final String src_name;
  private final String dest_name;
  private final Subset sub;

  public ColumnArea(String dest_name, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.sub= null;
    dep.add(src_name);
  }

  public ColumnArea(String dest_name, Subset sub, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.sub= sub;
    dep.add(src_name);
  }

  public String getName()
  {
    return this.dest_name;
  }

  public void compute(Computer computer)
  {
    Data data= computer.getData();

    final int colIdx= data.getColumnIndex(src_name);

    double sum= 0;
    long currTime;
    long lastTime= data.getBegin();

    double timeScale= computer.getTimeScale();
    if( sub != null )
    {
      if( !sub.isApplicable(computer) )
      {
        setEmpty();

        return;
      }
      TableIterator it= data.iterateTable(sub.getStart(computer));
      int rowIdx= sub.getStart(computer);

      while( it.hasNext() && rowIdx <= sub.getEnd(computer) )
      {
        Object[] row= it.next();
        Object p= row[colIdx];
        rowIdx++;

        currTime= it.getTimestamp();
        long timeDiff= (currTime - lastTime);
        boolean isBreak= computer.isBreak(timeDiff);
        if( it.isOriginal(colIdx) )
        {
          if( p != null )
          {
            if( isBreak )
              timeDiff= data.getFrequeny();
            sum+= Helper.asDouble(p) * timeDiff * timeScale;
          }
          lastTime= currTime;
        }
      }
    }
    else
    {
      if( data instanceof SegmentedData )
      {
        SegmentIterator it= ((SegmentedData) data).iterateSegments();
        
        int segmentCount= ((SegmentedData) data).getSegmentCount();
        segmentValues= new Value[segmentCount];
        int segmentIdx= 0;
        
        // Iterate over all segments
        while( it.hasNext() )
        {
          SegmentedTableIterator segment= it.next();
          double segmentSum= 0;
          
          while( segment.hasNext() )
          {
            Object[] row= segment.next();
            Object p= row[colIdx];
    
            currTime= segment.getTimestamp();
            long timeDiff= (currTime - lastTime);
            boolean isBreak= computer.isBreak(timeDiff);
            if( segment.isOriginal(colIdx) )
            {
              if( p != null )
              {
                if( isBreak )
                  timeDiff= data.getFrequeny();
                double x = Helper.asDouble(p) * timeDiff * timeScale;
                sum+= x;
                segmentSum+= x;
              }
              lastTime= currTime;
            }
          }

          segmentValues[segmentIdx++]= new NumberValue(getName(), segmentSum);
        }
        System.out.println(Arrays.toString(segmentValues));
      }
      else
      {
        TableIterator it= data.iterateTable();

        while( it.hasNext() )
        {
          Object[] row= it.next();
          Object p= row[colIdx];
  
          currTime= it.getTimestamp();
          long timeDiff= (currTime - lastTime);
          boolean isBreak= computer.isBreak(timeDiff);
          if( it.isOriginal(colIdx) )
          {
            if( p != null )
            {
              if( isBreak )
                timeDiff= data.getFrequeny();
              sum+= Helper.asDouble(p) * timeDiff * timeScale;
            }
            lastTime= currTime;
          }
        }
      }
    }

    value= sum;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
