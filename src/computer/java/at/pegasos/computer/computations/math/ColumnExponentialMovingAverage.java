package at.pegasos.computer.computations.math;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.computer.ColumnComputation;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;

public class ColumnExponentialMovingAverage implements ColumnComputation {
  
  private final static boolean[] ORIG_HELP= new boolean[] {true};
  
  private final String src_name;
  private final String dest_name;
  private final double duration;
  
  private Column res;
  
  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(1);
  }
  
  public ColumnExponentialMovingAverage(String dest_name, String src_name, double duration)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
    this.duration= duration * 1000;
    dep.add(src_name);
  }
  
  public String getName()
  {
    return this.dest_name;
  }
  
  private void init(Data data)
  {
    res= new Column();
    res.name= "data";
    res.names= new String[] {this.dest_name/*, this.dest_name + "_sums"*/};
    res.types= new int[] {Types.DOUBLE};
    int size= data.getRowCount();
    res.values= new ArrayList<Object[]>(size);
    res.orig= new ArrayList<boolean[]>(size);
  }
  
  public void compute(Computer computer)
  {
    Data data= computer.getData();
    
    init(data);
    
    final int power= data.getColumnIndex(src_name);
    TableIterator it= data.iterateTable();
    
    // System.out.println("Moving Average " + this.src_name);
    
    final long EPSILON = 100;
    final double NEGLIGIBLE = 0.1;

    // TODO: this is only true for our data ...
    double secsDelta = 1000;
    double sampsPerWindow = duration / secsDelta;
    double attenuation = sampsPerWindow / (sampsPerWindow + secsDelta/1000.0);
    double sampleWeight = secsDelta/1000.0 / (sampsPerWindow + secsDelta/1000.0);
    // double attenuation = 25.0 / 26.0;
    // double sampleWeight = 1.0 / 26.0;
    // System.out.println(attenuation + "= " + 25.0 / 26.0);
    // System.out.println(sampleWeight + "= " + 1.0 / 26.0);

    double weighted= 0.0;

    long lastSecs= 0;

    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object p= row[power];

      while( (weighted > NEGLIGIBLE) && (it.getTimestamp() > lastSecs + secsDelta + EPSILON) )
      {
        weighted*= attenuation;
        lastSecs+= secsDelta;
      }

      weighted*= attenuation;
      weighted+= sampleWeight * Helper.asDouble(p);

      lastSecs= it.getTimestamp();

      res.values.add(new Double[] {weighted});
      res.orig.add(ORIG_HELP);
    }

    /*
    // TODO: this is only true for our data ...
    double secsDelta = 1000;
    double sampsPerWindow = duration / secsDelta;
    double attenuation = sampsPerWindow / (sampsPerWindow + secsDelta);
    double sampleWeight = secsDelta / (sampsPerWindow + secsDelta);

    double weighted = 0.0;

    // System.out.println(sampsPerWindow + " " + attenuation + " " + sampleWeight);

    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object p= row[power];
      
      if( it.isOriginal(power) )
      {
    	if( p != null )
    	{
          weighted*= attenuation;
          weighted+= sampleWeight * Helper.asDouble(p);
    	}
        else
        {
          weighted*= attenuation;
        }
      }
      else
      {
        weighted*= attenuation;
      }
      
      res.values.add(new Double[] {weighted});
      res.orig.add(ORIG_HELP);
    }
    */
  }
  
  public Column getResult()
  {
    return res;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
}
