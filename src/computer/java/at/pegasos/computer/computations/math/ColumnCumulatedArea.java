package at.pegasos.computer.computations.math;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import at.pegasos.computer.ColumnComputation;
import at.pegasos.computer.Computer;
import at.pegasos.data.Data;
import at.pegasos.data.Data.*;
import at.pegasos.data.compute.*;

public class ColumnCumulatedArea implements ColumnComputation {

private final static boolean[] ORIG_HELP= new boolean[] {true};
  
  private final String src_name;
  private final String dest_name;
  
  private Column res;

  private Data data;

  // TODO: integrate time units
  public ColumnCumulatedArea(String dest_name, String src_name)
  {
    this.src_name= src_name;
    this.dest_name= dest_name;
  }

  public String getName()
  {
    return this.dest_name;
  }
  
  private void init(Data data)
  {
    res= new Column();
    res.name= "data";
    res.names= new String[] {this.dest_name};
    res.types= new int[] {Types.DOUBLE};
    int size= data.getRowCount();
    res.values= new ArrayList<Object[]>(size);
    res.orig= new ArrayList<boolean[]>(size);
  }

  public void compute(Computer computer)
  {
    data= computer.getData();

    init(data);

    final int srcCol= data.getColumnIndex(src_name);
    TableIterator it= data.iterateTable();
    
    double sum= 0; // moving average sum
    long currTime;
    long from= data.getBegin();
    long lastTime= from;
    
    double timeScale= computer.getTimeScale();

    while( it.hasNext() )
    {
      Object[] row= it.next();
      Double p= Helper.asDouble(row[srcCol]);
      
      // update time
      currTime= it.getTimestamp();

      long timeDiff= (currTime - lastTime);
      boolean isBreak= computer.isBreak(timeDiff);
      if( it.isOriginal(srcCol) )
      {
        if( p != null )
        {
          if( isBreak )
            timeDiff= data.getFrequeny();
          sum+= Helper.asDouble(p) * timeDiff * timeScale;
        }
        lastTime= currTime;
      }

      res.values.add(new Double[] {sum});
      res.orig.add(ORIG_HELP);
    }
  }
  
  public Column getResult()
  {
    return res;
  }

  @Override
  public Collection<String> getDependencies()
  {
    return Arrays.asList(new String[] {src_name});
  }
}
