package at.pegasos.computer.computations.sport.coggan;

import java.util.Arrays;
import java.util.Collection;

import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.computer.SessionComputation;
import at.pegasos.computer.ValueComputation;

@SessionComputation
public class IntensityFactor implements ValueComputation {
  
  private double IF;
  
  public String getName()
  {
    return "IF";
  }
  
  @Override
  public void compute(Computer computer)
  {
    IF= computer.getValue("NP").getDoubleValue() / computer.getValue("FTP").getDoubleValue();
  }
  
  @Override
  public Value getResult()
  {
    return new NumberValue("IF", IF);
  }

  @Override
  public Collection<String> getDependencies()
  {
    return Arrays.asList("NP", "FTP");
  }
}
