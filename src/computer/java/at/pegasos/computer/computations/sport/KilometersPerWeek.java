package at.pegasos.computer.computations.sport;

import at.pegasos.computer.computations.UserWeekComputation;
import at.pegasos.computer.Computer;

public class KilometersPerWeek extends UserWeekComputation {

  @Override
  public String getName()
  {
    return "KM_PER_WEEK";
  }

  @Override
  public void compute(Computer computer)
  {
    setComputer(computer);
    value= sessionSumWeek("DISTANCE", 0, 0) / 1000;
    debug();
  }
}
