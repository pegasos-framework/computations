package at.pegasos.computer.computations.sport.coggan;

import java.util.Arrays;
import java.util.Collection;

import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.computer.SessionComputation;
import at.pegasos.computer.ValueComputation;

@SessionComputation
public class TSS implements ValueComputation {
  
  private double tss;
  
  public String getName()
  {
    return "TSS";
  }
  
  @Override
  public void compute(Computer computer)
  {
    tss= ((computer.getValue("WorkingTime").getDoubleValue() / 1000.0) * computer.getValue("NP").getDoubleValue() * computer.getValue("IF").getDoubleValue()) / 
        (computer.getValue("FTP").getDoubleValue() * 36);
  }
  
  @Override
  public Value getResult()
  {
    return new NumberValue("TSS", tss);
  }

  @Override
  public Collection<String> getDependencies()
  {
    // return Sets.newHashSet("WorkingTime", "NP", "IF", "FTP");
    return Arrays.asList("WorkingTime", "NP", "IF", "FTP");
  }
}
