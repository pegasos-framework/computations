package at.pegasos.computer.computations.sport.coggan;

import java.util.Arrays;
import java.util.Collection;

import at.pegasos.computer.Computer;
import at.pegasos.computer.SessionComputation;
import at.pegasos.computer.ValueComputation;
import at.pegasos.data.Data;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;

@SessionComputation
public class NormalizedPower implements ValueComputation {

  private double np;

  private final static Collection<String> deps;
  static {
    deps= Arrays.asList("bike_power30");
  }

  public String getName()
  {
    return "NP";
  }

  @Override
  public void compute(Computer computer)
  {
    Data data= computer.getData();

    final int power= data.getColumnIndex("bike_power30");
    TableIterator it= data.iterateTable();

    double total= 0;
    int count= 0;
    
    // double secs;
    
    // System.out.println("" + it.hasNext());
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Double p= (Double) row[power];
      
      // System.out.println(p + " " + it.isOriginal(power));
      if( p != null && it.isOriginal(power) )
      {
        total+= Math.pow((Double) p, 4); // raise rolling average to 4th power
        count++;
      }
    }
    
    if( count > 0 )
    {
      np= Math.pow(total / (count), 0.25);
      // np= Math.pow(total, 0.25);
      // secs= count;
    }
    else
    {
      np= /*secs=*/ 0;
    }
    // System.out.println(np + " " + secs);
  }
  
  @Override
  public Value getResult()
  {
    return new NumberValue("NP", np);
  }

  @Override
  public Collection<String> getDependencies()
  {
    return deps;
  }
}
