package at.pegasos.computer.computations.sport;

import java.util.*;

import at.pegasos.data.Data;
import at.pegasos.data.Value;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.NumberValue;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;
import at.pegasos.computer.computations.MultiValueComputation;
import at.pegasos.computer.SessionComputation;
import at.pegasos.data.compute.TimedMovingAverage;

@SessionComputation
public class Altitude2 implements MultiValueComputation {
  
  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(2);
    dep.add("altitude_m");
    dep.add("distance_m");
  }
  
  private final static Collection<String> names;
  static
  {
    names= new ArrayList<String>(2);
    names.add("ASCENT2");
    names.add("DESCENT2");
  }
  
  double ascent= 0;
  double descent= 0;
  
  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
  
  @Override
  public String getName()
  {
    return "ALTSTUFF2";
  }
  
  @Override
  public void compute(Computer computer)
  {
    Data data= computer.getData();
    
    final int altCol= data.getColumnIndex("altitude_m");
    final int distCol= data.getColumnIndex("distance_m");
    
    long smoothing= 200;
    
    TimedMovingAverage altma= new TimedMovingAverage(smoothing, 0);
    
    double last_alt= (int) firstAlt(data, altCol, distCol, smoothing);
    long curdist= 0;
    long last_dist= 0;
    
    TableIterator it= data.iterateTable();
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object a= row[altCol];
      Object d= row[distCol];
      
      if( d != null )
      {
        curdist= Helper.asDouble(d).intValue();
      }
      
      if( a != null )
      {
        altma.addValue(curdist, Helper.asDouble(a));
        
        if( curdist - smoothing > last_dist )
        {
          double change= altma.getMovingAverage() - last_alt;
          last_alt= altma.getMovingAverage();
          last_dist= curdist;
          
          if( change > 0 )
          {
            ascent+= change;
          }
          else
          {
            descent+= -change;
          }
        }
      }
    }
    // System.out.println(ascent + " " + descent);
  }
  
  private double firstAlt(Data data, int altCol, int distCol, long smoothing)
  {
    TableIterator it= data.iterateTable();
    
    TimedMovingAverage altma= new TimedMovingAverage(smoothing, 0);
    
    long curdist= 0;
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object a= row[altCol];
      Object d= row[distCol];
      
      if( d != null )
      {
        curdist= Helper.asDouble(d).intValue();
      }
      
      if( a != null )
      {
        double curalt= Helper.asDouble(a);
        altma.addValue(curdist, curalt);
        
        if( curdist - smoothing > 0 )
        {
          return altma.getMovingAverage();
        }
      }
    }
    
    throw new IllegalStateException("Trying to compute grade from data without altitude");
  }
  
  @Override
  public Collection<Value> getResult()
  {
    return Arrays.asList(new Value[] {new NumberValue("ASCENT2", ascent), new NumberValue("DESCENT2", descent)});
  }
  
  @Override
  public Collection<String> getNames()
  {
    return names;
  }
}
