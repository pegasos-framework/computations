package at.pegasos.computer.computations.sport.coggan;

import at.pegasos.computer.computations.UserDayComputation;
import at.pegasos.computer.Computer;

public class ATL extends UserDayComputation {

  private final String ATL;
  private final String TSS;

  public ATL()
  {
    ATL= "ATL";
    TSS= "TSS";
  }

  public ATL(String atlname, String tssname)
  {
    ATL= atlname;
    TSS= tssname;
  }

  public String getName()
  {
    return ATL;
  }
  
  @Override
  public void compute(Computer computer)
  {
    setComputer(computer);
    
    value= valueDayOr(ATL, -1, 0) * Math.exp(-1 / 7.0) + sessionSumDay(TSS, 0, 0) * (1 - Math.exp(-1 / 7.0));
    // System.out.println(getName() + "= " + value);
  }
}
