package at.pegasos.computer.computations.sport.coggan;

import at.pegasos.computer.BasicUserValueComputation;
import at.pegasos.computer.Computer;

public class TSB extends BasicUserValueComputation {
  
  public String getName()
  {
    return "TSB";
  }
  
  @Override
  public void compute(Computer computer)
  {
    setComputer(computer);
    
    value= valueDayOr("CTL", -1, 0) - valueDayOr("ATL", -1, 0);
  }
}
