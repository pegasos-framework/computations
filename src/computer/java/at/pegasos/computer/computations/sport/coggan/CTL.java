package at.pegasos.computer.computations.sport.coggan;

import at.pegasos.computer.computations.UserDayComputation;
import at.pegasos.computer.Computer;

public class CTL extends UserDayComputation {

  private final String CTL;
  private final String TSS;

  public CTL()
  {
    CTL= "CTL";
    TSS= "TSS";
  }

  public CTL(String ctlname, String tssname)
  {
    CTL= ctlname;
    TSS= tssname;
  }

  public String getName()
  {
    return CTL;
  }

  @Override
  public void compute(Computer computer)
  {
    setComputer(computer);

    value= valueDayOr(CTL, -1, 0) * Math.exp(-1 / 42.0) + sessionSumDay(TSS, 0, 0) * (1 - Math.exp(-1 / 42.0));
    // System.out.println(getName() + "= " + value + "= " + valueDayOr("CTL", -1, 0) + "  * Math.exp(-1 / 42.0) + " + sessionSumDay("TSS", 0, 0) + " * (1 - Math.exp(-1 / 42.0)");
  }
}
