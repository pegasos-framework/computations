package at.pegasos.computer.computations.sport.coggan;

import java.util.*;

import at.pegasos.data.NumberValue;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.computer.ValueComputation;

public class VI implements ValueComputation {
  
  private double iv;
  
  public String getName()
  {
    return "IV";
  }
  
  @Override
  public void compute(Computer computer)
  {
    iv= computer.getValue("NP").getDoubleValue() / computer.getValue("bike_power_filtered_AVERAGE").getDoubleValue();
  }
  
  @Override
  public Value getResult()
  {
    return new NumberValue(getName(), iv);
  }

  @Override
  public Collection<String> getDependencies()
  {
    return Collections.singleton("NP");
  }
}
