package at.pegasos.computer.computations.sport;

import java.util.*;

import at.pegasos.data.Data;
import at.pegasos.data.Value;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.data.NumberValue;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;
import at.pegasos.computer.computations.MultiValueComputation;
import at.pegasos.computer.SessionComputation;
import at.pegasos.data.compute.TimedMovingAverage;

@SessionComputation
public class Altitude implements MultiValueComputation {
  
  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(1);
    dep.add("altitude_m");
  }
  
  private final static Collection<String> names;
  static
  {
    names= new ArrayList<String>(2);
    names.add("ASCENT");
    names.add("DESCENT");
  }
  
  double ascent= 0;
  double descent= 0;
  
  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }
  
  @Override
  public String getName()
  {
    return "ALTSTUFF";
  }
  
  @Override
  public void compute(Computer computer)
  {
    Data data= computer.getData();
    
    final int altCol= data.getColumnIndex("altitude_m");
    
    long smoothing= 60000;
    long begin= data.getBegin();
    
    long last_time= begin;
    
    // TimedMovingAverage altcma= new TimedMovingAverage(smoothing, begin);
    TimedMovingAverage altma= new TimedMovingAverage(smoothing, begin);
    
    // double last_alt= (int) firstAlt(data, altCol);
    double last_alt= (int) firstAlt(data, altCol, begin, smoothing);
    // double curalt= 0;
    // double ac= 0;
    long curTime;
    
    TableIterator it= data.iterateTable();
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object a= row[altCol];
      
      if( a != null )
      {
        curTime= it.getTimestamp();
        // curalt= Helper.asDouble(a).intValue();
        altma.addValue(it.getTimestamp(), Helper.asDouble(a));
        // ac= curalt - last_alt;
        // last_alt= curalt;
        // altcma.addValue(curTime, ac);
        
        /*double change= altcma.getMovingAverage();
        
        if( change > 0 )
        {
          ascent+= change;
        }
        else
        {
          descent+= -change;
        }*/
        
        if( curTime - smoothing > last_time )
        {
          last_time= curTime;
          
          double change2= altma.getMovingAverage() - last_alt;
          last_alt= altma.getMovingAverage();
          
          if( change2 > 0 )
          {
            ascent+= change2;
          }
          else
          {
            descent+= -change2;
          }
        }
      }
    }
    
    // System.out.println(ascent + " " + descent);
    // System.out.println(ascent2 + " " + descent2);
  }
  
  /*private double firstAlt(Data data, int altCol)
  {
    TableIterator it= data.iterateTable();
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object a= row[altCol];
      
      if( a != null )
      {
        double curalt= Helper.asDouble(a).intValue();
        
        return curalt;
      }
    }
    
    throw new IllegalStateException("Trying to compute grade from data without altitude");
  }*/
  
  private double firstAlt(Data data, int altCol, long begin, long smoothing)
  {
    TableIterator it= data.iterateTable();
    
    TimedMovingAverage altma= new TimedMovingAverage(smoothing, begin);
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object a= row[altCol];
      
      if( a != null )
      {
        double curalt= Helper.asDouble(a);
        altma.addValue(it.getTimestamp(), curalt);
        
        if( it.getTimestamp() - smoothing > begin)
        {
          return altma.getMovingAverage();
        }
      }
    }
    
    throw new IllegalStateException("Trying to compute grade from data without altitude");
  }
  
  @Override
  public Collection<Value> getResult()
  {
    return Arrays.asList(new Value[] {new NumberValue("ASCENT", ascent), new NumberValue("DESCENT", descent)});
  }
  
  @Override
  public Collection<String> getNames()
  {
    return names;
  }
  
}
