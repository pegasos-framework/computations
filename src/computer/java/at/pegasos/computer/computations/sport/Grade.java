package at.pegasos.computer.computations.sport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import java.sql.Types;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.TableIterator;
import at.pegasos.computer.ColumnComputation;
import at.pegasos.computer.Computer;
import at.pegasos.data.compute.Helper;
import at.pegasos.computer.SessionComputation;
import at.pegasos.data.compute.TimedMovingSum;

@SessionComputation
public class Grade implements ColumnComputation {
  
  private final static boolean[] ORIG_HELP= new boolean[] {true};
  
  private Column res;
  
  public Grade()
  {
  }
  
  public String getName()
  {
    return "grade";
  }
  
  private void init(Data data)
  {
    res= new Column();
    res.name= "data";
    res.names= new String[] {"grade"};
    res.types= new int[] {Types.DOUBLE};
    int size= data.getRowCount();
    res.values= new ArrayList<Object[]>(size);
    res.orig= new ArrayList<boolean[]>(size);
  }
  
  public void compute(Computer computer)
  {
    Data data= computer.getData();
    
    init(data);
    
    final int altCol= data.getColumnIndex("altitude_m");
    final int distCol= data.getColumnIndex("distance_m");
    
    long smoothing= 30000;
    long begin= data.getBegin();
    
    TimedMovingSum distma= new TimedMovingSum(smoothing, begin);
    TimedMovingSum altcma= new TimedMovingSum(smoothing, begin);
    
    // can initialise this with 0 as we should always start with 0
    double last_dist= 0;
    double last_alt= (int) firstAlt(data, altCol);
    double curdist= 0;
    double curalt= 0;
    double dist= 0, ac= 0;
    
    // double last_grade;
    
    TableIterator it= data.iterateTable();
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object a= row[altCol];
      Object d= row[distCol];
      Object[] o;
      
      // We assume that it might happen that alt and dist are not present at the same time.
      // Therefore, we use the last values present to compute it
      if( d != null )
      {
        curdist= Helper.asDouble(d).intValue();
        // there might have been a problem with interpolation or data quality
        if( curdist > last_dist )
        {
          dist= curdist - last_dist;
          last_dist= curdist;
          distma.addValue(it.getTimestamp(), dist);
        }
      }
      if( a != null )
      {
        curalt= Helper.asDouble(a).intValue();
        ac= curalt - last_alt;
        last_alt= curalt;
        altcma.addValue(it.getTimestamp(), ac);
      }
      
      if( a != null || d != null )
      {
        // double grade= ac / dist * 100;
        // o= new Object[] {grade};
        if( distma.getMovingSum() > 0 )
        {
          double grade= altcma.getMovingSum() / distma.getMovingSum() * 100;
          // System.out.println(altcma.getMovingAverage() + " " + distma.getMovingAverage() + " " + grade);
          if( it.getTimestamp() - begin > smoothing )
          {
            o= new Object[] {grade};
          }
          else
          {
            o= new Object[] {0d};
          }
        }
        else
          o= new Object[] {0d};
      }
      else
      {
        o= new Object[] {null};
      }
      
      res.values.add(o);
      res.orig.add(ORIG_HELP);
      // System.out.println(currTime + " " + v);
    }
  }
  
  private double firstAlt(Data data, int altCol)
  {
    TableIterator it= data.iterateTable();
    
    while( it.hasNext() )
    {
      Object[] row= it.next();
      Object a= row[altCol];
      
      if( a != null )
      {
        double curalt= Helper.asDouble(a);
        
        return curalt;
      }
    }
    
    throw new IllegalStateException("Trying to compute grade from data without altitude");
  }

  public Column getResult()
  {
    return res;
  }

  @Override
  public Collection<String> getDependencies()
  {
    // return Sets.newHashSet("altitude_m", "distance_m");
    return Arrays.asList("altitude_m", "distance_m");
  }
}
