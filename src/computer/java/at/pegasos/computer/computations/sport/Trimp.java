package at.pegasos.computer.computations.sport;

import java.util.ArrayList;
import java.util.Collection;

import at.pegasos.computer.computations.BasicValueComputation;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;

public class Trimp extends BasicValueComputation {
  
  private final static Collection<String> dep;
  static
  {
    dep= new ArrayList<String>(3);
    dep.add("MAXHR");
    dep.add("RESTHR");
    dep.add("hr_AVERAGE");
    dep.add("DURATION");
  }
  
  @Override
  public String getName()
  {
    return "Trimp";
  }

  @Override
  public Collection<String> getDependencies()
  {
    return dep;
  }

  @Override
  public void compute(Computer computer)
  {
    double maxHr= computer.getValue("MAXHR").getDoubleValue();
    double restHr= computer.getValue("RESTHR").getDoubleValue();
    double hr= computer.getValue("hr_AVERAGE").getDoubleValue();
    // TODO: use working time / moving time instead of duration
    long duration= computer.getValue("DURATION").getDoubleValue().longValue() / 1000;
    
    double ksex= 1.92;
    Value gender= computer.getValue("GENDER");
    if( gender != null && gender.getStringValue().equals("female") )
      ksex= 1.67;
    
    // ok lets work the score out
    if( duration == 0 || hr < restHr )
      value= 0d;
    else
      value= duration / 60 * (hr - restHr) / (maxHr - restHr) * 0.64 * Math.exp(ksex * (hr - restHr) / (maxHr - restHr));
  }
}
