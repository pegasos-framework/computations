package at.pegasos.computer.computations.sport;

import at.pegasos.computer.*;
import at.pegasos.data.*;
import at.pegasos.data.Data.*;
import at.pegasos.data.compute.Helper;

import java.util.*;

@SessionComputation
public class WorkingTime implements ValueComputation {

  private Double value;

  public String getName()
  {
    return "WorkingTime";
  }

  @Override
  public void compute(Computer computer)
  {
    Data data = computer.getData();

    if( data.hasColumn("bike_power") )
    {
      final int power = data.getColumnIndex("bike_power");
      compute(data, power, 0);
    }
    else if( data.hasColumn("foot_pod_speed_mm_s") )
    {
      final int power = data.getColumnIndex("foot_pod_speed_mm_s");
      compute(data, power, 10);
    }
  }

  private void compute(Data data, final int power, final int delta)
  {
    TableIterator it = data.iterateTable();

    double total = 0;

    while( it.hasNext() )
    {
      Object[] row = it.next();
      Double p = Helper.asDouble(row[power]);

      // System.out.println(p + " " + it.isOriginal(power));
      if( p != null && it.isOriginal(power) && p > delta )
      {
        // TODO: works only for our data!
        total += 1000;
      }
      // last_time= it.getTimestamp();
    }

    value = total;

    switch( data.getTimeUnit() )
    {
      case MicroSecond:
        value *= 1000;
        break;
      case MilliSecond:
        break;
      case Second:
        value /= 1000;
        break;
      default:
        break;
    }
  }

  @Override
  public Value getResult()
  {
    if( value != null )
      return new NumberValue(getName(), value);
    else
      return null;
  }

  /**
   * @return null as there are no dependencies
   */
  @Override public Collection<String> getDependencies()
  {
    return null;
  }
}
