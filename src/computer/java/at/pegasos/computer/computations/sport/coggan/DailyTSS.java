package at.pegasos.computer.computations.sport.coggan;

import at.pegasos.computer.computations.UserDayComputation;
import at.pegasos.computer.Computer;

public class DailyTSS extends UserDayComputation {
  private final String TSS;

  public DailyTSS()
  {
    TSS= "TSS";
  }

  public DailyTSS(String tssname)
  {
    TSS= tssname;
  }

  public String getName()
  {
    return "DailyTSS";
  }

  @Override
  public void compute(Computer computer)
  {
    setComputer(computer);
    
    value= sessionSumDay(TSS, 0, 0);
  }
}
