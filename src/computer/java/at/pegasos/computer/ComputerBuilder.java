package at.pegasos.computer;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.*;
import java.util.*;

import at.pegasos.computer.providers.SessionValuesProvider;
import at.pegasos.computer.providers.UserValueProvider;
import org.reflections.Reflections;
import org.reflections.scanners.*;
import org.reflections.util.*;

import at.pegasos.data.*;

public class ComputerBuilder {
  private boolean dateSet= false;
  
  private final Computer computer;
  
  private final Set<Class<? extends Object>> allClasses;
  
  private final Set<Computation> computations;
  
  private Set<String> provided;

  private String sessionId= null;

  private String userId= null;

  /**
   * Classloader containing extra resources (directories, jar files)
   */
  private final DynamicURLClassLoader dynalLoader;

  private final List<Value> predefinedValues;

  public ComputerBuilder()
  {
    this.computer= new Computer();
    this.computations= new HashSet<Computation>();
    this.allClasses= new HashSet<Class<? extends Object>>();
    this.dynalLoader= new DynamicURLClassLoader();
    this.predefinedValues= new ArrayList<Value>(20);
  }

  public ComputerBuilder(Data data)
  {
    this.computer= new Computer();
    this.computer.setData(data);
    this.computations= new HashSet<Computation>();
    this.allClasses= new HashSet<Class<? extends Object>>();
    this.dynalLoader= new DynamicURLClassLoader();
    this.predefinedValues= new ArrayList<Value>(20);
  }

  public ComputerBuilder setUserValueProvider(UserValueProvider provider)
  {
    computer.setUserValueProvider(provider);
    return this;
  }

  public ComputerBuilder setSessionValuesProvider(SessionValuesProvider provider)
  {
    computer.setSessionValuesProvider(provider);
    return this;
  }

  /**
   * Set the date on which the computation will take place
   * 
   * @param date
   * @return builder
   */
  public ComputerBuilder setDate(Calendar date)
  {
    dateSet= true;
    computer.setDate(date);
    return this;
  }

  /**
   * Add an instantiation of a computation to the computation stack
   * @param computation
   * @return
   */
  public ComputerBuilder addComputation(Computation computation)
  {
    computations.add(computation);
    return this;
  }

  /**
   * Add instantiations of computations to the computation stack
   * 
   * @param computations
   *          computations to be added
   * @return
   */
  public ComputerBuilder addComputations(Collection<Computation> computations)
  {
    this.computations.addAll(computations);
    return this;
  }

  /**
   * Add values which will be present in the computer as session values. These values are also used
   * for identifying applicable computations.
   * 
   * @param values
   *          values to be added
   * @return
   */
  public ComputerBuilder predefineSessionValues(Collection<Value> values)
  {
    this.predefinedValues.addAll(values);
    return this;
  }
  

  public Computer getComputer() throws ComputerBuilderException
  {
    if( !dateSet )
    {
      computer.setDate(Calendar.getInstance());
    }

    processComputations();

    computer.setSessionId(sessionId);
    computer.setUserId(userId);

    return computer;
  }
  
  public Computer getComputerUser()
  {
    if( !dateSet )
    {
      computer.setDate(Calendar.getInstance());
    }
    try
    {
      loadComputations();
      for(Computation computation : computations)
        computer.addComputation(computation);
      // System.out.println(computations.toString());
    }
    catch( ComputerBuilderException e )
    {
      e.printStackTrace();
    }

    computer.setUserId(userId);

    return computer;
  }
  
  private void processComputations() throws ComputerBuilderException
  {
    provided= new HashSet<String>();
    
    // fetchComputations();
    loadComputations();
    // System.out.println(computations.toString());
    
    Data data= computer.getData();
    if( data != null )
    {
      provided.addAll(Arrays.asList(data.getColumnNames()));
    }
    computer.addPredefinedSessionValues(predefinedValues);
    provided.addAll(computer.getValueNames());
    computer.getUserValueProvider().getValues(computer.getDate()).forEach(v -> provided.add(v.getName()));

    int setSize, newSize= 0;
    do
    {
      // System.out.println("Run");
      setSize= computations.size();
      processComputationsRound();
      newSize= computations.size();
    } while (setSize > newSize );
    // System.out.println("Computations with unmet dependencies: " + computations.toString());
  }
  
  public static class DynamicURLClassLoader extends URLClassLoader {

    public DynamicURLClassLoader()
    {
      super(new URL[0]);
    }

    @Override
    public void addURL(URL url)
    {
      System.out.println("add url " + url);
      super.addURL(url);
    }
  }

  /**
   * Add a source from which packages and computations can be loaded
   * 
   * @param source
   */
  public ComputerBuilder addCompiledSource(String source)
  {
    try
    {
      dynalLoader.addURL(new File(source).toURI().toURL());
    }
    catch( MalformedURLException e )
    {
      e.printStackTrace();
    }
    return this;
  }

  /**
   * Add locations from which packages and computations can be loaded
   * 
   * @param computationResources
   *          paths to directories, jars, etc.
   */
  public ComputerBuilder addCompiledSources(List<String> computationResources)
  {
    for(String source : computationResources)
    {
      try
      {
        dynalLoader.addURL(new File(source).toURI().toURL());
      }
      catch( MalformedURLException e )
      {
        e.printStackTrace();
      }
    }
    return this;
  }
  
  /**
   * Add computations from a package contained in a jar file . All computations that should be
   * loaded in this package need to be annotated with the @SessionComputation annotation. This
   * operation can be quite slow. Consider caching your results using getAllClasses and addClasses.
   * 
   * @param jarFile
   *          file from which the computations should be loaded
   * @param pkg
   *          package to be loaded
   * @return
   * @throws ComputerBuilderException
   */
  public ComputerBuilder addComputationPackage(String jarFile, String pkg) throws ComputerBuilderException
  {
    URLClassLoader child= null;
    try
    {
      child= new URLClassLoader(new URL[] {
          new File(jarFile).toURI().toURL()},
          this.getClass().getClassLoader());
    }
    catch( MalformedURLException e )
    {
      throw new ComputerBuilderException(e);
    }

    Reflections reflections= new Reflections(new ConfigurationBuilder()
        .setScanners(
            new SubTypesScanner(false /* don't exclude Object.class */),
            new ResourcesScanner(),
            new TypeAnnotationsScanner())
        .setUrls(ClasspathHelper.forClassLoader(new ClassLoader[] {child}))
        .addClassLoaders(child)
        .filterInputsBy(
            new FilterBuilder()
              // .include(FilterBuilder.prefix(pkg))
              // .exclude(".*java")
              .includePackage(pkg)
              .exclude(".*java")
            ));

    allClasses.addAll(reflections.getTypesAnnotatedWith(SessionComputation.class));

    return this;
  }

  /**
   * Add computations from a package. All computations that should be loaded in this package need to
   * be annotated with the @SessionComputation annotation. This operation can be quite slow.
   * Consider caching your results using getAllClasses and addClasses.
   * 
   * @param pkg
   *          package to be loaded
   * @return
   */
  public ComputerBuilder addComputationPackage(String pkg)
  {
    List<ClassLoader> classLoadersList= new LinkedList<ClassLoader>();
    classLoadersList.add(dynalLoader);
    classLoadersList.add(ClasspathHelper.contextClassLoader());
    classLoadersList.add(ClasspathHelper.staticClassLoader());

    Reflections reflections2= new Reflections(new ConfigurationBuilder()
        .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner(),
            new TypeAnnotationsScanner())
        .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
        .addClassLoaders(dynalLoader).filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(pkg))));

    allClasses.addAll(reflections2.getTypesAnnotatedWith(SessionComputation.class));
    // System.out.println("allClasses: " + allClasses);

    return this;
  }

  /**
   * Add computations from packages. All computations that should be loaded in these packages need
   * to be annotated with the @SessionComputation annotation. This operation can be quite slow.
   * Consider caching your results using getAllClasses and addClasses.
   * 
   * @param computationPackages
   *          collection of packages to be loaded
   * @return
   */
  public ComputerBuilder addComputationPackages(Collection<String> computationPackages)
  {
    List<ClassLoader> classLoadersList= new LinkedList<ClassLoader>();
    classLoadersList.add(dynalLoader);
    classLoadersList.add(ClasspathHelper.contextClassLoader());
    classLoadersList.add(ClasspathHelper.staticClassLoader());

    FilterBuilder f= new FilterBuilder();
    for(String pkg : computationPackages)
    {
      f= f.include(FilterBuilder.prefix(pkg));
    }

    Reflections reflections2= new Reflections(new ConfigurationBuilder()
        .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner(),
            new TypeAnnotationsScanner())
        .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
        .addClassLoaders(dynalLoader).filterInputsBy(f));

    allClasses.addAll(reflections2.getTypesAnnotatedWith(SessionComputation.class));

    return this;
  }
  
  private void loadComputations() throws ComputerBuilderException
  {
    for(Class<? extends Object> clasz : allClasses)
    {
      if( !Computation.class.isAssignableFrom(clasz) )
      {
        System.err.println(clasz + " needs to implement " + Computation.class.getCanonicalName());
        continue;
      }
      
      try
      {
        Computation c= (Computation) clasz.getConstructor().newInstance();
        // System.out.println(c.getDependencies().toString());
        computations.add(c);
      }
      catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
          | NoSuchMethodException | SecurityException e )
      {
        throw new ComputerBuilderException(e);
      }
    }
  }
  
  private void processComputationsRound()
  {
    List<Computation> toRemove= new ArrayList<Computation>(computations.size());
    // List<Computation> toRemove= new ArrayList<Computation>(computations.size());
    for(Computation computation : computations)
    {
      Collection<String> dependencies= computation.getDependencies(); 
      if( dependencies == null || provided.containsAll(dependencies) )
      {
        toRemove.add(computation);
        // System.out.println("Add computation " + computation + " " + dependencies + " " + provided.containsAll(dependencies));
        computer.addComputation(computation);
        Collection<String> provides= Computer.provides(computation);
        if( provides != null )
          provided.addAll(provides);
      }
    }
    computations.removeAll(toRemove);
  }

  public ComputerBuilder setUserId(String userId)
  {
    this.userId= userId;
    return this;
  }

  /**
   * Get all available classes. The returned set contains all classes implementing the
   * SessionComputation interface. The set is not filtered i.e. it might contain classes not
   * implementing Computation.
   * 
   * @return set containing all classes loaded by previous operations
   */
  public Set<Class<? extends Object>> getAllClasses()
  {
    return allClasses;
  }

  /**
   * Add classes to the set of classes used for building the computer.
   * 
   * @param classes
   *          classes to be added
   * @return
   */
  public ComputerBuilder addClasses(Set<Class<? extends Object>> classes)
  {
    this.allClasses.addAll(classes);
    return this;
  }

  /**
   * Get all computations not resolved (i.e. not added to the computer). This method will only
   * return something meaningful after .getComputer() is called
   * 
   * @return unresolved computations (or garbage / null)
   */
  public Set<Computation> getUnresolvedComputations()
  {
    return computations;
  }
}
