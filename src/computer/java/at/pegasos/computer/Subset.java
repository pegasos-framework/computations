package at.pegasos.computer;

public class Subset {
  private final int start;
  private final int end;
  private final String startName;
  private final String endName;

  public Subset(int start, int end)
  {
    this.start= start;
    this.end= end;
    startName= endName= null;
  }

  public Subset(int start, String end)
  {
    this.start= start;
    this.startName= null;
    this.end= 0;
    this.endName= end;
  }

  public Subset(String start, int end)
  {
    this.start= 0;
    this.startName= start;
    this.end= end;
    this.endName= null;
  }

  public Subset(String start, String end)
  {
    this.start= 0;
    this.startName= start;
    this.end= 0;
    this.endName= end;
  }

  public int getStart(Computer computer)
  {
    if( startName == null )
      return start;
    else
      return computer.getValue(startName).getIntValue();
  }

  public int getEnd(Computer computer)
  {
    if( endName == null )
      return end;
    else
      return computer.getValue(endName).getIntValue();
  }

  public boolean isApplicable(Computer computer)
  {
    int start= getStart(computer);
    int end= getEnd(computer);

    if( start < 0 || end < 0 || start > end || start > computer.getData().getRowCount()
        || end > computer.getData().getRowCount() )
    {
      return false;
    }
    else
    {
      return true;
    }
  }
}
