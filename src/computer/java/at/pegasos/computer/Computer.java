package at.pegasos.computer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import at.pegasos.computer.providers.SessionValuesProvider;
import at.pegasos.computer.providers.UserValueProvider;
import at.pegasos.computer.computations.MultiValueComputation;
import at.pegasos.computer.computations.UserMonthComputation;
import at.pegasos.computer.computations.UserWeekComputation;
import at.pegasos.computer.computations.UserYearComputation;
import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.TimedValue;
import at.pegasos.data.Value;

public class Computer {

  /**
   * All values used / accessible for this computer
   */
  private final Map<String, Value> values;

  private final Map<String, Boolean> computed;

  private final Map<String, Boolean> user;

  private List<at.pegasos.computer.Computation> computations;

  private boolean computationStarted= false;

  private Data data;

  private UserValueProvider provider;

  /**
   * Date when the computations are happening
   */
  private Calendar date;

  private SessionValuesProvider sessionVProvider;

  private String sessionId;

  private String userId;

  public Computer()
  {
    computations= new ArrayList<at.pegasos.computer.Computation>();
    values= new HashMap<String, Value>();
    computed= new HashMap<String, Boolean>();
    user= new HashMap<String, Boolean>();
  }

  public void addComputation(at.pegasos.computer.Computation computation)
  {
    computations.add(computation);
  }
  
  public void setData(Data data)
  {
	  this.data= data;
  }
  
  public Data getData()
  {
    return data;
  }
  
  /**
   * Set a `current' value for this computation. A current value is valid without a date restriction
   * or is valid for the data of the computation i.e. its date coincides with the `date' field of
   * this Computer instance
   * 
   * @param v the value
   */
  void setValue(Value v)
  {
    if( values.containsKey(v.getName()) )
    {
      Value val= values.get(v.getName());
      if( !(val.getClass().equals(v.getClass())) )
        throw new UnsupportedOperationException("Cannot convert variable from " + val.getClass() + " to " + v.getClass());
    }
    
    values.put(v.getName(), v);
    computed.put(v.getName(), computationStarted);
  }
  
  private void setUserValue(TimedValue v)
  {
    this.provider.setValue(v);
	
    long t= getDate().getTimeInMillis();
    if( v.getStart() <= t /* && (v.getEnd() == 0 || t <= v.getEnd()) */ )
    {
      setValue(v.getValue());
      user.put(v.getValue().getName(), true);
    }
    else
    {
      System.out.println("Not setting " + v.getValue().getName() + "(= " + v.getValue().getDoubleValue() + ") for now:" + t + " start:" + v.getStart() + " end:" + v.getEnd());
    }
  }

  /**
   * Inject values into the computer
   *
   * @param values values to be injected
   */
  public void addPredefinedSessionValues(Collection<Value> values)
  {
    values.forEach(v -> {
      if( this.values.containsKey(v.getName()) )
      {
        Value val= this.values.get(v.getName());
        if( !(val.getClass().equals(v.getClass())) )
          throw new UnsupportedOperationException(
              "Cannot convert variable from " + val.getClass() + " to " + v.getClass());
      }

      this.values.put(v.getName(), v);
      computed.put(v.getName(), false);
    });
  }

  /**
   * Get a Value from the store
   * @param name
   * @return
   */
  public Value getValue(String name)
  {
    return values.get(name);
  }

  /**
   * Check if the computer has a value
   * @param name name of the value
   * @return true if the computer has this value (set or computed)
   */
  public boolean hasValue(String name)
  {
    return values.containsKey(name);
  }

  public Set<String> getValueNames()
  {
    return values.keySet();
  }

  /**
   * Perform a computation directly. The computation is performed and the result will be stored in
   * this computer instance but not as computed values (i.e. they will not show up in the results,
   * except data columns).
   *
   * <i>Caution: this method should be used with care in order to avoid unwanted side effects.</i>
   *
   * @param computation
   *          Computation to be performed
   */
  public void directcompute(at.pegasos.computer.Computation computation)
  {
    boolean tmpComputationStarted= computationStarted;
    computationStarted= false;

    computation.compute(this);
    if( computation instanceof at.pegasos.computer.ColumnComputation )
    {
      Column res= ((at.pegasos.computer.ColumnComputation) computation).getResult();
      if( res != null )
        data.addColumnDirect(res);
    }
    else if( computation instanceof at.pegasos.computer.ValueComputation )
    {
      Value v= ((at.pegasos.computer.ValueComputation) computation).getResult();
      if( v != null )
        this.setValue(v);
    }
    else if( computation instanceof MultiValueComputation )
    {
      Collection<Value> values= ((at.pegasos.computer.computations.MultiValueComputation) computation).getResult();
      if( values != null )
      {
        for(Value v : values)
        {
          this.setValue(v);
        }
      }
    }
    else if( computation instanceof at.pegasos.computer.UserValueComputation )
    {
      TimedValue v= ((at.pegasos.computer.UserValueComputation) computation).getResult();
      if( v != null )
        this.setUserValue(v);
    }

    computationStarted= tmpComputationStarted;
  }

  private void compute()
  {
    computationStarted= true;
    
    for(at.pegasos.computer.Computation computation : computations)
    {
      // System.out.println(c);
      computation.compute(this);
      if( computation instanceof at.pegasos.computer.ColumnComputation )
      {
        Column res= ((ColumnComputation) computation).getResult();
        if( res != null )
          data.addColumnDirect(res);
      }
      else if( computation instanceof at.pegasos.computer.ValueComputation )
      {
        Value v= ((at.pegasos.computer.ValueComputation) computation).getResult();
        if( v != null )
          this.setValue(v);
      }
      else if( computation instanceof at.pegasos.computer.computations.MultiValueComputation )
      {
        Collection<Value> values= ((at.pegasos.computer.computations.MultiValueComputation) computation).getResult();
        if( values != null )
        {
          for(Value v : values)
          {
            this.setValue(v);
          }
        }
      }
      else if( computation instanceof at.pegasos.computer.UserValueComputation )
      {
        TimedValue v= ((at.pegasos.computer.UserValueComputation) computation).getResult();
        if( v != null )
          this.setUserValue(v);
      }
    }
    
    computationStarted= false;
  }
  
  public void computeSessionMetrics()
  {
    sessionVProvider.setUserId(userId);
    sessionVProvider.setSessionId(sessionId);

    compute();

    sessionVProvider.addSessionMetrics(date, getComputedValues());
  }
  
  public void computeUserMetrics()
  {
    sessionVProvider.setUserId(userId);

    compute();

    // provider.setValues(date, getComputedUserValues());
  }
  
  public void computeUserMetricsNoLongRunning()
  {
    sessionVProvider.setUserId(userId);

    computationStarted= true;
    
    for(at.pegasos.computer.Computation computation : computations)
    {
      // System.out.println(c);
      computation.compute(this);
      if( computation instanceof at.pegasos.computer.ColumnComputation )
      {
        Column res= ((at.pegasos.computer.ColumnComputation) computation).getResult();
        if( res != null )
          data.addColumnDirect(res);
      }
      else if( computation instanceof ValueComputation )
      {
        Value v= ((at.pegasos.computer.ValueComputation) computation).getResult();
        if( v != null )
          this.setValue(v);
      }
      else if( computation instanceof at.pegasos.computer.computations.MultiValueComputation )
      {
        Collection<Value> values= ((at.pegasos.computer.computations.MultiValueComputation) computation).getResult();
        if( values != null )
        {
          for(Value v : values)
          {
            this.setValue(v);
          }
        }
      }
      else if( computation instanceof at.pegasos.computer.UserValueComputation )
      {
        if( !(computation instanceof UserWeekComputation) && !(computation instanceof UserMonthComputation)
            && !(computation instanceof UserYearComputation) )
        {
          TimedValue v= ((UserValueComputation) computation).getResult();
          if( v != null )
            this.setUserValue(v);
        }
      }
    }
    
    computationStarted= false;
  }
  
  public void clearComputations()
  {
    computations.clear();
  }
  
  public List<Value> getComputedValues()
  {
    List<Value> ret= new ArrayList<Value>(this.values.size());
    
    for(Map.Entry<String, Value> kv : values.entrySet())
    {
      if( computed.get(kv.getKey()) )
        ret.add(kv.getValue());
    }
    
    return ret;
  }
  
  public List<Value> getComputedUserValues()
  {
    List<Value> ret= new ArrayList<Value>(this.user.size());
    
    for(Map.Entry<String, Boolean> kv : user.entrySet())
    {
      if( kv.getValue() && computed.get(kv.getKey()) )
        ret.add(values.get(kv.getKey()));
    }
    
    return ret;
  }
  
  public void setUserValueProvider(
      UserValueProvider provider)
  {
    this.provider= provider;
  }
  
  public final UserValueProvider getUserValueProvider()
  {
    return provider;
  }
  
  public void fetchUserValues()
  {
    boolean tmpComputationStarted= computationStarted;
    computationStarted= false;
    
    if( provider == null )
      throw new IllegalStateException("UserValueProvider not set");
    
    Collection<Value> values= provider.getValues(date);
    for(Value v : values)
      setValue(v);
    
    computationStarted= tmpComputationStarted;
  }
  
  public void debugValues()
  {
    System.out.println(values.toString());
  }

  public Calendar getDate()
  {
    return date;
  }
  
  public void setDate(Calendar date)
  {
    this.date= date;
  }
  
  public Computer setSessionValuesProvider(
      at.pegasos.computer.providers.SessionValuesProvider sessionValuesProvider)
  {
    this.sessionVProvider= sessionValuesProvider;
    return this;
  }

  public at.pegasos.computer.providers.SessionValuesProvider getSessionValuesProvider()
  {
    return sessionVProvider;
  }

  public void setSessionId(String sessionId)
  {
    this.sessionId= sessionId;
  }

  public void setUserId(String userId)
  {
    this.userId= userId;
  }

  public static Collection<String> provides(Computation computation)
  {
    if( computation instanceof at.pegasos.computer.ColumnComputation )
    {
      return Arrays.asList(new String[] {computation.getName()});
    }
    else if( computation instanceof at.pegasos.computer.ValueComputation )
    {
      return Arrays.asList(new String[] {computation.getName()});
    }
    else if( computation instanceof at.pegasos.computer.computations.MultiValueComputation )
    {
      return ((at.pegasos.computer.computations.MultiValueComputation) computation).getNames();
    }
    else if( computation instanceof at.pegasos.computer.UserValueComputation )
    {
      return null;
    }
    throw new IllegalArgumentException();
  }

  public boolean isBreak(long timeDiff)
  {
    return timeDiff > (data.getFrequeny() * 2);
  }

  // TODO: document me
  public double getTimeScale()
  {
    switch(data.getTimeUnit())
    {
      case MicroSecond:
        return 1 / 1000_000.0;
      case MilliSecond:
        return 1 / 1000.0;
      case Second:
        return 1;
      default:
        // TODO: throw exception
        return 0;
    }
  }
}
