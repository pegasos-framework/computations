package at.pegasos.data.simplify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import at.pegasos.data.Data.Column;

public abstract class Strategy {
  /**
   * The output data. Handle this with care
   */
  protected Column data;
  protected List<Column> cols;
  protected Map<String, Object> params;

  public static final String PARAM_OSCUON= "OSCUON";
  public static final String PARAM_DROPC= "DROP_COLS";

  protected Set<String> toDrop;

  public Strategy(Column data, List<Column> cols)
  {
    this.data= data;
    this.cols= cols;
  }

  public abstract void simplify();

  /**
   * Reset the data store with the approximate size. Warning: this will delete all data stored in
   * it.
   * 
   * @param size
   */
  protected void setDataSize(int size)
  {
    data.timestamps= new ArrayList<Long>(size);
    data.values= new ArrayList<Object[]>(size);
    data.orig= new ArrayList<boolean[]>(size);
  }

  protected void setBasicInfo()
  {
    List<String> names= new ArrayList<String>();
    List<Integer> types= new ArrayList<Integer>();

    boolean oscuon= params.containsKey(PARAM_OSCUON) && ((Boolean) params.get(PARAM_OSCUON));

    // TODO: respect toDrop
    if( oscuon && cols.size() == 1 )
    {
      Column c= cols.get(0);
      for(String name : c.names)
        names.add(name);
      for(Integer type : c.types)
        types.add(type);
    }
    else
    {
      for(Column c : cols)
      {
        if( c.names.length == 1 && oscuon )
          names.add(c.name);
        else
        {
          for(String name : c.names)
            names.add(c.name + "_" + name);
        }
        for(Integer type : c.types)
          types.add(type);
      }
    }

    data.names= new String[names.size()];
    data.names= names.toArray(data.names);
    data.types= new int[types.size()];
    for(int i= 0; i < types.size(); i++)
      data.types[i]= types.get(i);
  }

  protected void addRow(long timestamp, Object[] values, boolean[] orig)
  {
    data.timestamps.add(timestamp);
    data.values.add(values);
    data.orig.add(orig);

    /*
     * try { Thread.sleep(1000); } catch (InterruptedException e) { // TODO Auto-generated catch
     * block e.printStackTrace(); }
     */
  }

  @SuppressWarnings("unchecked")
  public void setParams(Map<String, Object> params)
  {
    this.params= params;

    toDrop= params.get(PARAM_DROPC) != null ? (Set<String>) params.get(PARAM_DROPC) : new HashSet<String>(0);
  }

  /**
   * When either (a) a source column contains only one column then use only the name instead of
   * combining name and colname or (b) only one source column exists then only the names of the
   * columns in the source will be used (instead of combining them with the source name)
   * 
   * @param use
   * @return
   */
  public static Map<String, Object> OnSingleColUseOnlyName(Map<String, Object> params, boolean use)
  {
    params.put(PARAM_OSCUON, use);

    return params;
  }

  /**
   * Do not use a specific column in the simplification process. Column will not be present in the
   * output
   * 
   * @param params
   *          Params object
   * @param colname
   *          name of the column to be dropped
   * @return
   */
  @SuppressWarnings("unchecked")
  public static Map<String, Object> dropColumn(Map<String, Object> params, String colname)
  {
    if( !params.containsKey(PARAM_DROPC) )
    {
      params.put(PARAM_DROPC, new HashSet<String>());
    }

    ((Set<String>) params.get(PARAM_DROPC)).add(colname);

    return params;
  }

  /**
   * Create a new empty parameter object
   * 
   * @return new parameter object
   */
  public static Map<String, Object> newParams()
  {
    return new HashMap<String, Object>();
  }
}
