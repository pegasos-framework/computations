package at.pegasos.data.simplify;

import java.util.Arrays;
import java.util.List;

import at.pegasos.data.Data.Column;

/**
 * deep-copies the first column into the data frame (*). This code does not perform any checks
 * whether there are more than one column.
 * 
 * (*) duplicates/copies the whole data
 */
public class SimpleSafe extends Strategy {
  public SimpleSafe(Column data, List<Column> cols)
  {
    super(data, cols);
  }
  
  public void simplify()
  {
    setBasicInfo();
    Column col= cols.get(0);
    
    int size= col.timestamps.size();
    setDataSize(size);
    data.types= Arrays.copyOf(col.types, col.types.length);
    
    boolean[] orig= new boolean[col.names.length];
    int i;
    for(i= 0; i < col.names.length; i++)
      orig[i]= true;
    
    for(i= 0; i < size; i++)
    {
      addRow(col.timestamps.get(i), col.values.get(i), orig);
    }
  }
}
