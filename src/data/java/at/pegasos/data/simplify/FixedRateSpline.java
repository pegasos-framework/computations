package at.pegasos.data.simplify;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
// import org.apache.commons.math3.analysis.interpolation.ClampedSplineInterpolator;
// import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import at.pegasos.data.Data.Column;

/**
 * TODO: document me
 */
public class FixedRateSpline extends Strategy {

  public static final String PARAM_FREQ= "FREQ";
  public static final String PARAM_SMOOTHINGFACTOR= "SMOOTHF";
  public static final String PARAM_ROUND_ALL= "ROUND_ALL";
  public static final String PARAM_ROUND= "ROUND_COL_";
  public static final String PARAM_DROPC= "DROP_COLS";
  // public static final String PARAM_OSCUON= "OSCUON";
  public static final String PARAM_SINGLECOL= "SINGLE";

  private boolean roundall= false;

  private Set<String> round;

  private int freq= 1000;
  private int freqHalf= 500;
  private int freqQuart= 125;
  private int tStep;
  private int colcount;
  private long start;
  private long end;
  private PolynomialSplineFunction[] splines;
  List<String> names= new ArrayList<String>();
  List<Integer> types= new ArrayList<Integer>();
  private Object[] currentRow;
  private int outputIndex;
  private int[] timeIndex;
  
  public FixedRateSpline(Column data, List<Column> cols)
  {
    super(data, cols);
    round= new HashSet<String>();
  }
  
  @Override
  public void setParams(Map<String, Object> params)
  {
    super.setParams(params);
    
    if( params != null && params.containsKey(PARAM_FREQ) )
    {
      freq= Integer.parseInt("" + params.get(PARAM_FREQ));
      freqHalf= (int) (freq / 2.0);
      freqQuart= (int) (freqHalf / 2.0);
    }
    
    if( params != null && params.containsKey(PARAM_ROUND_ALL) )
    {
      roundall= Boolean.parseBoolean("" + params.get(PARAM_ROUND_ALL));
    }
    
    int factor;
    if( params != null && params.containsKey(PARAM_SMOOTHINGFACTOR) )
    {
      factor= Integer.parseInt("" + params.get(PARAM_SMOOTHINGFACTOR));
      if( factor < 0 || factor > 2 )
        throw new IllegalArgumentException("Factor can only be 0,1,2.");
    }
    else
      factor= 0;
    
    if( factor == 0 )
      tStep= freq;
    else if( factor == 1 )
      tStep= freqHalf;
    else
      tStep= freqQuart;
    
    for(Map.Entry<String, Object> kv : params.entrySet())
    {
      if( kv.getKey().startsWith(PARAM_ROUND) && ((Boolean) kv.getValue()) == true )
      {
    	round.add(kv.getKey().substring(PARAM_ROUND.length()));
      }
    }
  }
  
  public void simplify()
  {
    colcount= 0;
    start= Long.MAX_VALUE;
    end= Long.MIN_VALUE;

    parseInput();

    data.names= new String[names.size()];
    data.names= names.toArray(data.names);
    data.types= new int[types.size()];
    for(int i= 0; i < types.size(); i++)
      data.types[i]= types.get(i);

    createSplines();
    
    createOutput();
  }

  /**
   * Check which columns to include in the input and determine min / max timestamps 
   */
  private void parseInput()
  {
    @SuppressWarnings("unchecked")
    Set<String> toDrop= params.get(PARAM_DROPC) != null ? (Set<String>) params.get(PARAM_DROPC) : new HashSet<String>(0);
    boolean oscuon= params.containsKey(PARAM_OSCUON) && ((Boolean) params.get(PARAM_OSCUON));

    if( cols.size() == 1 && params.containsKey(PARAM_SINGLECOL) && ((Boolean) params.get(PARAM_SINGLECOL)) )
    {
      Column col= cols.get(0);

      int count= 0;
      for(int i= 0; i < col.names.length; i++)
      {
        if( toDrop.contains(col.name + "." + col.names[i]) )
          continue;

        names.add(col.names[i]);
        types.add(col.types[i]);

        if( col.timestamps.get(0) < start )
          start= col.timestamps.get(0);
        if( col.timestamps.get(col.timestamps.size()-1) > end )
        {
          end= col.timestamps.get(col.timestamps.size()-1);
          // System.out.println("new end " + end + " " + col.name);
        }

        count++;
      }
      colcount+= count;
    }
    else
    {
      for(Column col : cols)
      {
        int count= 0;
        for(int i= 0; i < col.names.length; i++)
        {
          if( toDrop.contains(col.name + "." + col.names[i]) )
            continue;
          
          if( col.names.length == 1 && oscuon )
            names.add(col.name);
          else
            names.add(col.name + "_" + col.names[i]);
          types.add(col.types[i]);
          
          if( col.timestamps.get(0) < start )
            start= col.timestamps.get(0);
          if( col.timestamps.get(col.timestamps.size()-1) > end )
          {
            end= col.timestamps.get(col.timestamps.size()-1);
            // System.out.println("new end " + end + " " + col.name);
          }
          
          count++;
        }
        colcount+= count;
      }
    }
  }

  private void createSplines()
  {
    int ji= 0;
    int ii= 0;
    double[] ts;
    double[] vals;

    splines= new PolynomialSplineFunction[colcount];

    for(Column col : cols)
    {
      // Loop over all columns of this column
      for(int colcol_idx= 0; colcol_idx < col.names.length; colcol_idx++)
      {
        if( toDrop.contains(col.name + "." + col.names[colcol_idx]) )
        {
          continue;
        }
        
//        System.out.println(" " + col.names[i] + " " + col.timestamps.size() + " " + col.timestamps.get(0) + " " + col.timestamps.get(col.timestamps.size()-1));
        ts= new double[col.timestamps.size()];
        vals= new double[col.timestamps.size()];
        ji= 0;
        
        // Loop over all entries
        for(int j= 0; j < col.timestamps.size(); j++)
        {
          // System.out.println(colcol_idx + " " + Arrays.toString(col.values.get(j)));
          if( !col.orig.get(j)[colcol_idx] || col.values.get(j)[colcol_idx] == null )
            continue;
          
          ts[ji]= col.timestamps.get(j);
          // check whether we have duplicate entries in the data
          if( ji > 0 && ts[ji] == ts[ji-1] )
            ji--;
          
          switch(col.types[colcol_idx])
          {
            case Types.BIGINT:
            case Types.NUMERIC:
              vals[ji]= ((Long) col.values.get(j)[colcol_idx]).doubleValue();
                break;
              case Types.BIT:
              case Types.INTEGER:
              case Types.SMALLINT:
               vals[ji]= ((Integer) col.values.get(j)[colcol_idx]).doubleValue();
                break;
              case Types.DECIMAL:
              case Types.DOUBLE:
              case Types.FLOAT:
                vals[ji]= (Double) col.values.get(j)[colcol_idx];
                break;
                
              default:
                //TODO:
                break;
          }
          
          ji++;
        }
        
        if( ji == 0 )
        {
          // System.out.println("Empty column: " + col.names[colcol_idx]);
          splines[ii]= null;
        }
        else if( ji > 1 && ji < col.orig.size() )
        {
          // System.out.println("Copy! " + ji);
          // SplineInterpolator splinei= new SplineInterpolator();
          LinearInterpolator splinei= new LinearInterpolator();
          //ClampedSplineInterpolator splinei= new ClampedSplineInterpolator();
          splines[ii]= splinei.interpolate(Arrays.copyOf(ts, ji), Arrays.copyOf(vals, ji));
        }
        else if( vals.length > 1 )
        {
          // SplineInterpolator splinei= new SplineInterpolator();
          LinearInterpolator splinei= new LinearInterpolator();
          // ClampedSplineInterpolator splinei= new ClampedSplineInterpolator();
          
          // System.out.println(Arrays.toString(ts));
          // System.out.println(Arrays.toString(vals));
          
          splines[ii]= splinei.interpolate(ts, vals);
        }
        else
        {
          System.out.println(col.names[colcol_idx] + " " + vals.length + " -> not used");
          
          splines[ii]= null;
        }
        ii++;
      }
    }
  }

  private void createOutput()
  {
    // Start output
    boolean[] orig= new boolean[colcount];
    for(int i= 0; i < colcount; i++)
    {
      orig[i]= true;
    }
    setDataSize((int) ((end - start) / ((double) freq)) + 1);
    
    timeIndex= new int[cols.size()];
    for(int c= 0; c < cols.size(); c++)
    {
      timeIndex[c]= 0;
    }

    // start from the beginning and create rows until we reach the end
    while( start <= end )
    {
      currentRow= new Object[colcount];
      outputIndex= 0;
      int c= 0;
      boolean oneAdded= false;
      for(Column col : cols)
      {
        oneAdded|= addColToOutput(col, c++);
      }
      // System.out.println(start + " " + Arrays.toString(row) + " " + Arrays.toString(orig));
      if( oneAdded )
        addRow(start, currentRow, orig);
      start+= freq;
    }
    data.sample_freq= freq;
    // System.out.println("Frequency: " + data.sample_freq);
  }

  private boolean addColToOutput(Column col, int colIdx)
  {
    long minTime= start - freq /* + freqHalf */;
    long maxTime= start + freq * 5 /*+ freqHalf */;

    // int h1= timeIndex[colIdx];

    // Move foreward to the begin of where we want to output
    while( timeIndex[colIdx] < col.timestamps.size() && col.timestamps.get(timeIndex[colIdx]) < minTime )
    {
      // System.out.println(colIdx + " now: " + timeIndex[colIdx] + "/" + col.timestamps.get(timeIndex[colIdx]));
      timeIndex[colIdx]++;
    }

    // If we are either at the end and cannot proceed or are at a break (>maxTime) --> do not use this col
    if( timeIndex[colIdx] >= col.timestamps.size() || col.timestamps.get(timeIndex[colIdx]) > maxTime )
    {
      // System.out.print("End or break for " + colIdx + " " + minTime + " " + maxTime + " before: " + h1 + "/" + col.timestamps.get(h1));
      // System.out.println(" now: " + timeIndex[colIdx] + "/" + col.timestamps.get(timeIndex[colIdx]));

      for(int colcol_idx= 0; colcol_idx < col.names.length; colcol_idx++)
      {
        if( !toDrop.contains(col.name + "." + col.names[colcol_idx]) )
        {
          outputIndex++;
        }
      }

      return false;
    }

    maxTime= start + freq;

    // System.out.println(col.name + " " + Arrays.toString(col.names) + " " + Arrays.toString(splines));
    for(int colcol_idx= 0; colcol_idx < col.names.length; colcol_idx++, outputIndex++)
    {
      if( toDrop.contains(col.name + "." + col.names[colcol_idx]) )
      {
        // Workaround as we increase outputIndex in the loop ..
        outputIndex--;
        continue;
      }

      // if( splineIndex == 3 )
      //   System.out.println(col.names[splineIndex] + " " + start + " " + splines[splineIndex].isValidPoint(start));
      if( splines[outputIndex] != null && splines[outputIndex].isValidPoint(start) )
      {
        // System.out.println(splineIndex + " " + splines[splineIndex].isValidPoint(start));
        switch( col.types[colcol_idx] )
        {
          case Types.BIGINT:
          case Types.NUMERIC:
            long x= start;
            double sum= 0;
            double count= 0;
            while( x < maxTime && splines[outputIndex].isValidPoint(x) )
            {
              sum+= splines[outputIndex].value(x);
              x+= tStep;
              count+= 1;
            }
            currentRow[outputIndex]= (Long) (long) (sum / count);
            break;
          case Types.BIT:
          case Types.INTEGER:
          case Types.SMALLINT:
            x= start;
            sum= 0;
            count= 0;
            while( x < maxTime && splines[outputIndex].isValidPoint(x) )
            {
              sum+= splines[outputIndex].value(x);
              x+= tStep;
              count+= 1;
            }
            currentRow[outputIndex]= (Integer) (int) (sum / count);
            break;
          case Types.DECIMAL:
          case Types.DOUBLE:
          case Types.FLOAT:
            x= start;
            sum= 0;
            count= 0;
            while( x < maxTime )
            {
              if( splines[outputIndex].isValidPoint(x) )
              {
                sum+= splines[outputIndex].value(x);
                count+= 1;
              }
              x+= tStep;
            }
            if( roundall || round.contains(col.names[colcol_idx]) )
              currentRow[outputIndex]= (double) ((Double) (sum / count)).intValue();
            else
              currentRow[outputIndex]= (Double) (sum / count);
            break;

          default:
            // TODO:
            break;
        }
      }
      else
      {
        currentRow[outputIndex]= null;
      }
    }

    return true;
  }

  /**
   * When a source column contains only one column then use only the name instead of combining name and colname
   * @param use
   
  public static void OnSingleColUseOnlyName(Map<String, Object> params, boolean use)
  {
    params.put(PARAM_OSCUON, use);
  }*/
  
  /**
   * Switch to single col mode. Use this when simplifying a data frame (simplifyMe)
   * @param params
   */
  public static void SingleCol(Map<String, Object> params)
  {
    params.put(PARAM_SINGLECOL, true);
  }
  
  public static void Round(Map<String, Object> params, String column)
  {
    params.put(PARAM_ROUND + column, true);
  }
  
  /**
   * Set the frequency of the output frame. The frequency is set by specifying the distance (in time) between two rows
   * @param params
   * @param distance time interval between two rows
   */
  public static void Frequency(Map<String, Object> params, int distance)
  {
    params.put(PARAM_FREQ, distance);
  }
  
  /**
   * Set the smoothing factor during the resampling <ul>
   * <li>not specified or 0: Each sample is computed using the frequency</li>
   * <li>factor = 1: Each sample is computed using the double frequency (i.e. using a distance of frequency/2 to compute the new samples)
   * <li>factor = 2: Each sample is computed using the frequency*4 (i.e. dist / 4) (</ul>
   * @param params
   * @param distance time interval between two rows
   */
  public static void Smoothing(Map<String, Object> params, int factor)
  {
    if( factor < 0 || factor > 2 )
      throw new IllegalArgumentException("Factor can only be 0,1,2.");
    params.put(PARAM_SMOOTHINGFACTOR, factor);
  }
}
