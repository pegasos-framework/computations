package at.pegasos.data.simplify;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import at.pegasos.data.Data.Column;

/**
 * Merge all data into a frame, keep values until a new one has been found
 */
public class MergeKeepNA extends Strategy {
  private static final String PARAM_NO_KEEP= "NO_KEEP";

  /**
   * Number of columns
   */
  private int ncol;
  
  /**
   * Number of elements remaining for each respective column
   */
  private int sizes[];
  
  /**
   * Number of elements in each respective column
   */
  private int col_lengths[];
  
  /**
   * Which cols should be added to the output
   */
  private boolean output[];
  
  private Object[][] keeps;
  
  private long[] last_output;
  
  int row_length;
  
  boolean running;
  
  public MergeKeepNA(Column data, List<Column> cols)
  {
    super(data, cols);
    
    ncol= cols.size(); // number of columns
  }
  
  @Override
  public void setParams(Map<String, Object> params)
  {
    super.setParams(params);
    
    if( params != null && params.containsKey(PARAM_NO_KEEP) )
    {
      // Map<String, String[]> no_keeps= (Map<String, String[]>) params.get(PARAM_NO_KEEP);
    }
  }
  
  long smallest;
  int smallest_idx;
  long largest;
  int largest_idx;
  
  public void simplify()
  {
    setBasicInfo();
    
    sizes= new int[ncol];
    col_lengths= new int[ncol];
    output= new boolean[ncol];
    keeps= new Object[ncol][];
    long cutoff= Long.MAX_VALUE;
    last_output= new long[ncol];
    
    int i= 0;
    int max_size= 0;
    for(Column col : cols)
    {
      sizes[i]= col.values.size();
      last_output[i]= Long.MIN_VALUE;
      
      row_length+= col.names.length;
      col_lengths[i]= col.names.length;
      keeps[i]= new Object[col.names.length];
      
      if( max_size < sizes[i] ) // sizes[i] equals to col.values.size()
        max_size= sizes[i];
      
      if( col.sample_freq < cutoff )
        cutoff= col.sample_freq;
      
      i++;
    }
    
    cutoff*= 0.55D;
    
    // System.out.printf("Row length: %d\n", row_length);
    
    setDataSize(max_size);
    
    updateRunning();
    while( running )
    {
      useAll();
      do
      {
        findSmallestLargest();
        if( largest - smallest > cutoff )
        {
          output[largest_idx]= false;
        }
      } while( largest - smallest > cutoff );
      
      // debug();
      outputRow(smallest);
      
      updateRunning();
    }
  }
  
  /**
   * Try to use all columns which have an element left
   */
  private void useAll()
  {
    for(int i= 0; i < ncol; i++)
      output[i]= sizes[i] > 0;
  }
  
  private void findSmallestLargest()
  {
    int idx= -1;
    
    idx= 0;
    while( !output[idx] )
      idx++;
    
    largest= smallest= cols.get(idx).timestamps.get(0);
    largest_idx= smallest_idx= idx;
    idx++;
    while( idx < ncol )
    {
      if( output[idx] )
      {
        long ts= cols.get(idx).timestamps.get(0);
        if( ts < smallest )
        {
          smallest= ts;
          smallest_idx= idx;
        }
        else if( ts > largest )
        {
          largest= ts;
          largest_idx= idx;
        }
      }
      idx++;
    }
  }
  
  int di= 0;
  
  @SuppressWarnings("unused")
  private void debug()
  {
    System.out.println(di++ + " sizes:" + Arrays.toString(sizes) + " " + Arrays.toString(output) + " " + Arrays.toString(last_output));
  }
  
  private void outputRow(long timestamp)
  {
    Object[] out= new Object[row_length];
    boolean[] orig= new boolean[row_length];
    
    int j= 0;
    for(int i= 0; i < ncol; i++)
    {
      if( output[i] )
      {
        Column col= cols.get(i);
        Object[] values= col.values.remove(0);
        for(int k= 0; k < col_lengths[i]; k++)
        {
          out[j]= values[k];
          orig[j]= true;
          j++;
          keeps[i][k]= values[k];
        }
        
        last_output[i]= col.timestamps.remove(0);
        
        sizes[i]--;
        // output[i]= false;
      }
      else
      {
        if( last_output[i] + cols.get(i).sample_freq*1.1 > timestamp )
        {
          for(int k= 0; k < col_lengths[i]; k++)
          {
            out[j]= keeps[i][k];
            orig[j]= false;
            j++;
          }
        }
        else
        {
          for(int k= 0; k < col_lengths[i]; k++)
          {
            out[j]= null;
            orig[j]= false;
            j++;
          }
        }
      }
    }
    
    // System.out.println(Arrays.toString(out) + " " + Arrays.toString(orig));
    addRow(timestamp, out, orig);
  }
  
  private void updateRunning()
  {
    for(int i= 0; i < ncol; i++)
    {
      if( sizes[i] > 0 )
      {
        running= true;
        return;
      }
    }
    running= false;
  }
}
