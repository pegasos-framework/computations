package at.pegasos.data.simplify;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import at.pegasos.data.Data.Column;

/**
 * Align values to column with the highest sample frequency.
 * Try to impute ''missing'' values by taking old ones. If not possible insert null as na 
 */
public class AlignFreqKeepNA extends Strategy {
  private int sizes[];
  private Object keeps[][];
  private long keeps_ts[];
  
  /**
   * Which cols are selected for output
   */
  private boolean output[];
  
  /**
   * Number of columns
   */
  int ncol;

  public AlignFreqKeepNA(Column data, List<Column> cols)
  {
    super(data, cols);
  }
  
  public void simplify()
  {
    setBasicInfo();
    
    boolean running = true;
    boolean values_remaining= true;
    
    int row_length= 0;
    
    ncol= cols.size(); //number of columns

    if( ncol == 1 )
    {
      col2Data();
      return;
    }

    keeps= new Object[ncol][];
    keeps_ts= new long[ncol];
    output= new boolean[ncol];
    
    sizes= new int[ncol];
    int i= 0;
    int max_size= 0;
    for(Column col : cols)
    {
      sizes[i++]= col.values.size();
      row_length+= col.names.length;
      if( max_size < sizes[i-1] )
        max_size= sizes[i-1];
    }
    
    System.out.printf("Row length: %d\n", row_length);
    
    setDataSize(max_size);
    
    // Compute time-stamps
    int times[][]= new int[cols.size()][];
    
    // Compute differences between timestamps of first entries
    for(i= 0; i < cols.size(); i++)
    {
      times[i]= new int[2]; // long[2+cols.size()];
      /*if( cols.get(i).timestamps.size() > 0 )
      {
        times[i][0]= cols.get(i).timestamps.get(0);
        times[i][1]= i;
        for(int j= 0; j < i; j++)
          times[i][2+j]= Math.abs(times[j][0] - times[i][0]);
      }
      else
      {
        times[i][0]= Long.MAX_VALUE;
        times[i][1]= i;
      }*/
      times[i][0]= cols.get(i).sample_freq;
      times[i][1]= i;
    }
    // sort entries
    Arrays.sort(times, new Comparator<int[]>() {
      @Override
      public int compare(final int[] entry1, final int[] entry2)
      {
        return (int) (entry1[0] - entry2[0]);
      }
    });
    
    /*boolean in_order= true;
    for(i= 0; i < ncol; i++)
    {
      if( times[i][0] != i )
      {
        in_order= false;
        break;
      }
    }*/
    
    int asf= (int) times[0][0]; //a sample frequency
    int asf_2= (int) (asf / (double) 2.0);
    
    while(running)
    {
      
      values_remaining= false;
      for(i= 0; i < sizes.length; i++)
      {
        if( sizes[i] > 0 )
        {
          values_remaining= true;
          break;
        }
      }
      
      System.out.println(Arrays.toString(sizes));
      
      if(!values_remaining)
        running= false;
      else
      {
        for(i= 0; i < cols.size(); i++)
        {
          System.out.printf("%d %d", times[i][0], times[i][1]);
          for(int j= 2; j < times[i].length; j++)
            System.out.printf(" %d", times[i][j]);
          System.out.println("");
        }
        
        System.out.println("Keeps:");
        for(i= 0; i < cols.size(); i++)
        {
          System.out.printf("%d ", i);
          if( keeps[i] != null )
          {
            System.out.print(Arrays.toString(keeps[i]));
          }
          System.out.println("");
        }
        
        /*
        Determine cases:
          a-col: col with the highest sample frequency
          
          case 1: 
            * a-col has the smallest TS
            * + this col has a higher sample freq (ie. lower value) than other cols (necessary?)
            * + this col has another value with a TS similar or smaller than next smallest TS
            * + there are no old values to keep
            examples
             TS  C1   C2             TS   C1  C2
             1   x     -              1   x   -
             25  x     -             25   -   y
             26  -     y             26   x   -
            --> output this value as entire row. 
          case 1 (a): treated like 1
            * only one col with a value is remaining
          case 2: 
            * one or more cols share a TS similar to that of a-col
            examples
             TS  C1   C2             TS   C1  C2
             1   x     -             1   -   y
             2   -     y             2   x   -
            --> output these values as row. Put values into keep. 
            
          case 3:
            * similar to case 2. But x is not a neighbour of 1. ie their could be holes
            * one or more cols share a TS similar to that of a-col
            examples
             TS  C1   Cx             TS   C1  Cx
             1   x     -             1   -   y
             2   -     y             2   x   -
            --> output these values as row. Put values into keep. 
         */
        
        int row_case= 0;
        
        int idx= (int) times[0][1];
        int idx_next= (int) times[1][1];
        int idx_nextt= 1;
        
        // check case 1
        if( sizes[idx] > 0 && sizes[idx_next] > 0 )
        { // there is another value
          long ats0= cols.get(idx).timestamps.get(0);
          long ats1= cols.get(idx).timestamps.size() > 1 ? cols.get(idx).timestamps.get(1) : Long.MAX_VALUE;
          long bts0= cols.get(idx_next).timestamps.get(0);
          if( ats1 < bts0 ) // example left side
          {
            row_case= 1;
          }
          else if(bts0-ats0 > ats1-bts0) //example right side
          {
            row_case= 1;
          }
        }
        else
        {
          System.out.println("check for 1a");
          int h= -1;
          for(int i2= 0; i2 < ncol; i2++)
          {
            if( sizes[i2] > 0 )
            {
              if( h != -1 ) // more than one col with values left
                break;
              
              h= i2; //maybe only one col with value left?
            }
          }
          if( h != -1 )
          {
            row_case= 1;
            idx= h;
          }
        }
        
        if( row_case == 0 )
        {
          //check case 2
          long ats0= cols.get(idx).timestamps.get(0);
          if(sizes[idx_next] > 0 )
          {
            long bts0= cols.get(idx_next).timestamps.get(0);
            
            while( bts0 - ats0 < asf_2 )
            {
              row_case= 2;
              idx_nextt++;
              if( idx_nextt == ncol )
                break;
              idx_next= (int) times[idx_nextt][1];
              bts0= cols.get(idx_next).timestamps.get(0);
            }
          }
        }
        
        if( row_case == 0 || row_case == 1 || row_case == 2 )
        {
          long ats0= cols.get(idx).timestamps.get(0);
          long bts0;
          
          System.out.println("Check for case3");
          //check case 3
          int idx_nextt2= idx_nextt+1;
          while( idx_nextt2 < ncol )
          {
            idx_next= (int) times[idx_nextt2][1];
            if(sizes[idx_next] > 0 )
            {
              bts0= cols.get(idx_next).timestamps.get(0);
              if( bts0 - ats0 < asf_2 )
              {
                if( row_case != 3 )
                {
                  row_case= 3;
                  for(int outputidx_idx= 0; outputidx_idx < idx_nextt; outputidx_idx++)
                    output[outputidx_idx]= true;
                }
                output[idx_next]= true;
              }
              else
                output[idx_next]= false;
            }
            idx_nextt2++;
          }
        }
        
        System.out.println("Rowcase: " + row_case);
        
        switch(row_case)
        {
          case 1:
            _outputSingleRow(idx, row_length);
            // TODO: update keeps
            break;
          
          case 2:
            System.out.printf("from %d. to %d\n", idx, idx_nextt);
            idx_nextt--;
            _outputRowFromTo(idx, idx_nextt, row_length);
            break;
            
          case 3:
            _outputRowOutputArray(row_length);
            break;
            
          case 0:
            break;
        }
        
        if( row_case >= 1 && row_case <= 3 )
          continue;
        
        if( row_case == 0)
          break;
        
        if( i == cols.size()-1 ) //output only one value --> no packet
        {
          Object[] row= new Object[row_length];
          boolean[] orig= new boolean[row_length];
          int k= 0;
          int j= 0;
          int x;
          
          long ts= cols.get((int) times[i][1]).timestamps.get(0);
          
          while(j < times[i][1]) //before
          {
            for(x= 0; x < cols.get(j++).names.length; x++)
            {
              orig[k]= false;
              row[k++]= null;
            }
          }
          
          for(x= 0; x < cols.get(j).names.length; x++) //me
          {
            orig[k]= true;
            row[k++]= cols.get(j).values.get(0)[x];
          }
          cols.get(j).timestamps.remove(0);
          cols.get(j).values.remove(0);
          sizes[j]--;
          
          while(j < cols.size()) //after
          {
            for(x= 0; x < cols.get(j++).names.length; x++)
            {
              orig[k]= false;
              row[k++]= null;
            }
          }
          
          System.out.println("Row: " + ts + " " + Arrays.toString(row));
          addRow(ts, row, orig);
        }
        else
        {
          Object[] row= new Object[row_length];
          int j, k, x;
          
          System.out.println("Sizes: " + Arrays.toString(sizes) + " " + times[i][1]);
          long ts= cols.get((int) times[i][1]).timestamps.get(0);
          
          Set<Integer> outp= new HashSet<Integer>();
          while(i < cols.size())
            outp.add((int) times[i++][1]);
          
          k= 0;
          for (j= 0; j < cols.size(); j++)
          {
            // if not to output this
            if(!outp.contains(j))
              for(x= 0; x < cols.get(j).names.length; x++)
                row[k++]= null;
            else
            {
              for(x= 0; x < cols.get(j).names.length; x++)
                row[k++]= cols.get(j).values.get(0)[x];
              cols.get(j).timestamps.remove(0);
              cols.get(j).values.remove(0);
              sizes[j]--;
            }
          }
          
          System.out.println("Row: " + ts + " " + Arrays.toString(row));
          addRow(ts, row, new boolean[row_length]);
        }
        
        /*
        for(; i < cols.size(); i++)
        {
          for(int k= 0; k < cols.get((int) times[i][1]).names.length; k++)
            row[j++]= cols.get((int) times[i][1]).values.get(0)[k];
          cols.get((int) times[i][1]).timestamps.remove(0);
          cols.get((int) times[i][1]).values.remove(0);
          sizes[i]--;
        }
        
        System.out.println("Row: " + ts + " " + Arrays.toString(row));
        */
        //return;
        
        /*
        if( yes )
        {
          // check distance of lowest sampling rate to next value.
          if( dist > rate + epsilon*factor )
          {
            // set dist manual
          }
          else
          {
            // set dist + epsilon
          }
          // dist is used to know how many values can be used in this packet
          
          // output whole packet of values using values from each col as 'keep'
        }
        else
        {
          // ouput a row using the lowest value
        }*/
      }
    }
  }
  
  private void col2Data()
  {
    Column col= cols.get(0);
    data.names= col.names;
    data.orig= col.orig;
    data.sample_freq= col.sample_freq;
    data.timestamps= col.timestamps;
    data.types= col.types;
    data.values= col.values;
  }

  private void _outputSingleRow(int idx, int row_length)
  {
    Object[] row= new Object[row_length];
    int k= 0;
    int j= 0;
    int x;
    
    long ts= cols.get(idx).timestamps.get(0);
    
    while(j < idx) //before
    {
      for(x= 0; x < cols.get(j).names.length; x++)
        row[k++]= null;
      j++;
    }
    
    System.out.println("k: " + k);
    
    for(x= 0; x < cols.get(j).names.length; x++) //me
      row[k++]= cols.get(j).values.get(0)[x];
    cols.get(j).timestamps.remove(0);
    cols.get(j).values.remove(0);
    sizes[j]--;
    assert(j == idx);
    j++;
    
    _updateKeeps(ts);
    
    while(j < cols.size()) //after
    {
      if( keeps[j] == null )
      {
        for(x= 0; x < cols.get(j).names.length; x++)
          row[k++]= null;
      }
      else
      {
        for(x= 0; x < keeps[j].length; x++)
          row[k++]= keeps[j][x];
      }
      j++;
    }
    
    System.out.println("Row: " + ts + " " + Arrays.toString(row));
    addRow(ts, row, new boolean[row_length]);
  }
  
  private void _outputRowFromTo(int from, int to, int row_length)
  {
    Object[] row= new Object[row_length];
    int k= 0;
    int j= 0;
    int x;
    
    long ts= cols.get(from).timestamps.get(0);
    
    while(j < from) //before
    {
      for(x= 0; x < cols.get(j).names.length; x++)
        row[k++]= null;
      j++;
    }
    
    System.out.println("k: " + k);
    
    for(; j <= to; j++)
    {
      keeps[j]= new Object[cols.get(j).names.length];
      keeps_ts[j]= cols.get(j).timestamps.get(0);
      for(x= 0; x < cols.get(j).names.length; x++) //me
      {
        row[k]= cols.get(j).values.get(0)[x];
        keeps[j][x]= row[k];
        k++;
      }
      cols.get(j).timestamps.remove(0);
      cols.get(j).values.remove(0);
      sizes[j]--;
    }
    
    _updateKeeps(ts);
    
    while(j < cols.size()) //after
    {
      if( keeps[j] == null )
      {
        for(x= 0; x < cols.get(j).names.length; x++)
          row[k++]= null;
      }
      else
      {
        for(x= 0; x < keeps[j].length; x++)
          row[k++]= keeps[j][x];
      }
      j++;
    }
    
    System.out.println("Row: " + ts + " " + Arrays.toString(row));
    addRow(ts, row, new boolean[row_length]);
  }
  
  private void _outputRowOutputArray(int row_length)
  {
    Object[] row= new Object[row_length];
    int k= 0;
    int j= 0;
    int x;
    
    while( output[j] == false ) //before first output
    {
      if( keeps[j] == null )
      {
        for(x= 0; x < cols.get(j).names.length; x++)
          row[k++]= null;
      }
      else
      {
        for(x= 0; x < keeps[j].length; x++)
          row[k++]= keeps[j][x];
      }
      j++;
    }
    
    long ts= cols.get(j).timestamps.get(0);
    
    if( _updateKeeps(ts) < j ) // Keeps have changed need to re output
    {
      j= 0;
      while( output[j] == false ) //before first output
      {
        if( keeps[j] == null )
        {
          for(x= 0; x < cols.get(j).names.length; x++)
            row[k++]= null;
        }
        else
        {
          for(x= 0; x < keeps[j].length; x++)
            row[k++]= keeps[j][x];
        }
        j++;
      }
    }
    
    while( j < cols.size() )
    {
      if( output[j] )
      {
        keeps[j]= new Object[cols.get(j).names.length];
        keeps_ts[j]= cols.get(j).timestamps.get(0);
        for(x= 0; x < cols.get(j).names.length; x++) //me
        {
          row[k]= cols.get(j).values.get(0)[x];
          keeps[j][x]= row[k];
          k++;
        }
        cols.get(j).timestamps.remove(0);
        cols.get(j).values.remove(0);
        sizes[j]--;
      }
      else
      {
        if( keeps[j] == null )
        {
          for(x= 0; x < cols.get(j).names.length; x++)
            row[k++]= null;
        }
        else
        {
          for(x= 0; x < keeps[j].length; x++)
            row[k++]= keeps[j][x];
        }
      }
      j++;
    }
    
    System.out.println("Row: " + ts + " " + Arrays.toString(row));
    addRow(ts, row, new boolean[row_length]);
  }
  
  /**
   * Remove all values which are too old to keep them
   * @param ts timestamp which serves as reference
   * @return true idx of smallest change in keeps. ncol if nothing has changed
   */
  private int _updateKeeps(long ts)
  {
    int ret= ncol;
    for(int i= 0; i < ncol; i++)
    {
      if( keeps_ts[i] + cols.get(i).sample_freq*1.5 < ts )
      {
        keeps[i]= null;
        keeps_ts[i]= 0;
        if( ret != ncol )
          ret= i;
      }
    }
    
    return ret;
  }
}
