package at.pegasos.data.simplify;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import at.pegasos.data.Data.Column;

public class TimeNormalise extends Strategy {
  public static final String PARAM_NEWN= "NEWSIZE";
  
  private int newn= 101;
  
  public TimeNormalise(Column data, List<Column> cols)
  {
    super(data, cols);
  }
  
  @Override
  public void setParams(Map<String, Object> params)
  {
    super.setParams(params);
    
    if( params != null && params.containsKey(PARAM_NEWN) )
    {
      newn= Integer.parseInt("" + params.get(PARAM_NEWN));
    }
  }
  
  public void simplify()
  {
    setBasicInfo();
    
    //  in this interpolation method we do not use the following:
    // setDataSize(newn);
    //  this is done on purpose. As the data is added from an array
    data.timestamps= new ArrayList<Long>(newn);
    data.orig= new ArrayList<boolean[]>(newn);

    int coli= 0;
    int colIdx= 0;

    Object[] maxs= new Object[data.names.length];
    Object[] mins= new Object[data.names.length];

    double xs[][]= new double[cols.size()][];

    for(Column col : cols)
    {
      xs[colIdx]= new double[col.timestamps.size()];
      double inc= col.timestamps.size() / (newn - 2.0);
      xs[colIdx][0]= 1d;
      for(int i= 1; i < xs[colIdx].length; i++)
      {
        xs[colIdx][i]= xs[colIdx][i - 1] + inc;
      }

      for(int colcol_idx= 0; colcol_idx < col.names.length; colcol_idx++)
      {
        if( toDrop.contains(col.name + "." + col.names[colcol_idx]) )
        {
          continue;
        }

        switch( col.types[colcol_idx] )
        {
          case Types.BIGINT:
          case Types.NUMERIC:
          case Types.DATE:
            maxs[coli]= Long.MIN_VALUE;
            mins[coli]= Long.MAX_VALUE;
            break;
          case Types.BIT:
          case Types.INTEGER:
          case Types.SMALLINT:
            maxs[coli]= Integer.MIN_VALUE;
            mins[coli]= Integer.MAX_VALUE;
            break;
          case Types.DECIMAL:
          case Types.DOUBLE:
          case Types.FLOAT:
            maxs[coli]= Double.MIN_VALUE;
            mins[coli]= Double.MAX_VALUE;
            break;
        }

        coli++;
      }

      colIdx++;
    }

    boolean[] orig= new boolean[data.names.length];
    for(int i= 0; i < data.names.length; i++)
    {
      orig[i]= true;
    }

    Object[][] rows= new Object[newn][];
    for(int i= 0; i < newn; i++)
    {
      rows[i]= new Object[data.names.length];
      data.timestamps.add((long) i + 1);
      data.orig.add(orig);
    }

    colIdx= 0;
    coli= 0;
    for(Column col : cols)
    {
      int ouridx= coli;
      for(int li= 0; li < newn; li++)
      {
        // Reset the index to our index
        coli= ouridx;

        int i= 0;
        int j= col.values.size() - 1;

        double v= data.timestamps.get(li);

        /* find the correct interval by bisection */
        while( i < j - 1 )
        { /* x[i] <= v <= x[j] */
          int ij= (i + j) / 2;
          /* i+1 <= ij <= j-1 */
          if( v < xs[colIdx][ij] )
            j= ij;
          else
            i= ij;
          /* still i < j */
        }

        // Loop over all columns of this column
        for(int colcol_idx= 0; colcol_idx < col.names.length; colcol_idx++)
        {
          if( toDrop.contains(col.name + "." + col.names[colcol_idx]) )
          {
            continue;
          }

          // handle out-of-domain points
          if( v < xs[colIdx][i] || v > xs[colIdx][j] )
          {
            if( v < col.timestamps.get(i) )
              rows[li][coli]= mins[coli];
            if( v > xs[colIdx][j] )
              rows[li][coli]= maxs[coli];
            coli++;
            continue;
          }

          if( v == xs[colIdx][j] )
          {
            rows[li][coli]= col.values.get(j)[colcol_idx];
          }
          else if( v == xs[colIdx][i] )
          {
            rows[li][coli]= col.values.get(i)[colcol_idx];
          }
          else
          {
            switch( col.types[colcol_idx] )
            {
              case Types.BIGINT:
              case Types.NUMERIC:
              case Types.DATE:
                rows[li][coli]= ((Long) col.values.get(i)[colcol_idx])
                    + (((Long) col.values.get(j)[colcol_idx]) - ((Long) col.values.get(i)[colcol_idx]))
                        * ((v - xs[colIdx][i]) / (xs[colIdx][j] - xs[colIdx][i]));
                break;
              case Types.BIT:
              case Types.INTEGER:
              case Types.SMALLINT:
                rows[li][coli]= ((Integer) col.values.get(i)[colcol_idx])
                    + (((Integer) col.values.get(j)[colcol_idx]) - ((Integer) col.values.get(i)[colcol_idx]))
                        * ((v - xs[colIdx][i]) / (xs[colIdx][j] - xs[colIdx][i]));
                break;
              case Types.DECIMAL:
              case Types.DOUBLE:
              case Types.FLOAT:
                rows[li][coli]= ((Double) col.values.get(i)[colcol_idx])
                    + (((Double) col.values.get(j)[colcol_idx]) - ((Double) col.values.get(i)[colcol_idx]))
                        * ((v - xs[colIdx][i]) / (xs[colIdx][j] - xs[colIdx][i]));
                break;

              case Types.VARCHAR:
                rows[li][coli]= col.values.get(j)[colcol_idx];
                break;

              default:
                throw new IllegalStateException("Unimplemented Datatype " + col.types[colcol_idx]);
            }
          }

          coli++;
        }
      }
      colIdx++;
    }

    for(int i= 0; i < data.names.length; i++)
    {
      switch( data.types[i] )
      {
        case Types.BIGINT:
        case Types.NUMERIC:
        case Types.DATE:
          rows[0][i]= 0L;
          break;
        case Types.BIT:
        case Types.INTEGER:
        case Types.SMALLINT:
          rows[0][i]= 0;
          break;
        case Types.DECIMAL:
        case Types.DOUBLE:
        case Types.FLOAT:
          rows[0][i]= 0d;
          break;
      }
    }

    coli= 0;
    for(Column col : cols)
    {
      Object[] lastrow= col.values.get(col.values.size() - 1);
      // Loop over all columns of this column
      for(int colcol_idx= 0; colcol_idx < col.names.length; colcol_idx++)
      {
        if( toDrop.contains(col.name + "." + col.names[colcol_idx]) )
        {
          continue;
        }

        rows[newn - 1][coli++]= lastrow[colcol_idx];
      }
    }

    data.values= Arrays.asList(rows);
    data.sample_freq= 1;
  }

  /**
   * Set the number of rows for the output data
   * 
   * @param newn
   *          number of rows in the output
   * @return
   */
  public static Map<String, Object> SetOutputN(Map<String, Object> params, int newn)
  {
    params.put(PARAM_NEWN, newn);

    return params;
  }
}
