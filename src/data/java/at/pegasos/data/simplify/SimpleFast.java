package at.pegasos.data.simplify;

import java.util.Arrays;
import java.util.List;

import at.pegasos.data.Data.Column;

/**
 * shallow-copies the first column into the data frame (*). This code does not perform any checks
 * whether there are more than one column. 
 * 
 * (*) only references the data of the column instead of performing a copy
 */
public class SimpleFast extends Strategy {
  /**
   * Number of columns
   */
  int ncol;

  public SimpleFast(Column data, List<Column> cols)
  {
    super(data, cols);
  }
  
  public void simplify()
  {
    data.name= "data";
    data.names= Arrays.copyOf(cols.get(0).names, cols.get(0).names.length);
    data.types= Arrays.copyOf(cols.get(0).types, cols.get(0).types.length);
    data.values= cols.get(0).values;
    data.timestamps= cols.get(0).timestamps;
    data.sample_freq= cols.get(0).sample_freq;
  }
}
