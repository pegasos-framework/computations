package at.pegasos.data;

public class DataException extends Exception {

  public DataException(Exception e)
  {
    super(e);
  }

  /**
   * Generated serial
   */
  private static final long serialVersionUID= -8767469009494330525L;
  
}
