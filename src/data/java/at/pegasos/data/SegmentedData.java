package at.pegasos.data;

import java.util.Iterator;
import java.util.Map;

import at.pegasos.data.simplify.Strategy;

public class SegmentedData extends Data
{
  public class Segment
  {
    /**
     * Timestamp of the start of the segment
     */
    private final Long start;
    private final Long end;
    private Integer startIdx;
    private Integer endIdx;
    
    protected Segment(Long start, Long end)
    {
      this.start= start;
      this.end= end;
      
      init();
    }
    
    protected Segment(Long start, Long end, int start_idx)
    {
      this.start= start;
      this.end= end;
      
      this.startIdx= start_idx;
      init();
    }
    
    private void init()
    {
      if( startIdx == null )
        startIdx= 0;
      
      // loop forward to start of the segment
      while(data.timestamps.get(startIdx) < start) startIdx++;
      
      if( data.sample_freq != 0 )
      {
        endIdx= (int) (startIdx + (1 / (double) data.sample_freq) * (end - start));
        if( endIdx > data.timestamps.size() )
        {
          endIdx= data.timestamps.size() - 1;
        }
      }
      else
        endIdx= startIdx;
      
      if( data.timestamps.get(endIdx) > end )
      {
        while(data.timestamps.get(endIdx) > end)
          endIdx--;
      }
      else
      {
        while( data.timestamps.get(endIdx) < end )
          endIdx++;
      }
    }
    
    /**
     * Get number of rows in this segment
     * @return
     */
    public int getCount()
    {
      return endIdx - startIdx;
    }
    
    public int getFirstIdx()
    {
      return startIdx;
    }
    
    public int getLastIdx()
    {
      return endIdx;
    }
    
    public long getBegin()
    {
      return start;
    }
    
    public long getEnd()
    {
      return end;
    }
    
    /**
     * Get an iterator for this segment
     * @return
     */
    public SegmentedTableIterator iterate()
    {
      return new SegmentedTableIterator(startIdx, endIdx);
    }
    
    /**
     * Get the duration in ms for this segment
     * @return
     */
    public long duration()
    {
      return data.timestamps.get(endIdx) - data.timestamps.get(startIdx);
    }
    
    public String toString()
    {
      return String.format("Segment [start:%d, end:%d. startIdx:%d, endIdx:%d]", start,end, startIdx, endIdx);
    }
  };
  
  private Segment[] segments;
  
  public SegmentedData()
  {
    super();
  }
  
  /**
   * Create a SegmentedData out of a data object. Be careful not to modify (e.g. deleting rows) the
   * original data as this can lead to unpredicted behaviour in this object.
   * 
   * @param data
   */
  public SegmentedData(Data data)
  {
    super(data);
  }
  
  @Override
  public SegmentedData copy()
  {
    Data h= super.copy();
    SegmentedData ret= new SegmentedData();
    ret.data= h.data;
    long[]ts= new long[this.segments.length];
    for(int i= 0; i < this.segments.length; i++)
      ts[i]= segments[i].start;
    ret.segment(ts);
    return ret;
  }
  
  /*public SegmentedData segment(long session_id) throws SQLException
  {
    // Is this an empty data frame?
    if( data.timestamps.size() == 0 )
    {
      segments= new Segment[0];
      return this;
    }
    
    ResultSet res= db.directSelectRO("select rec_time from marker where session_id = " + session_id + " order by rec_time ASC");
    long markers[];
    
    if( res.last() )
    {
      int rows = res.getRow();
      
      markers= new long[rows];
      
      // Move to beginning again
      res.beforeFirst();
      int i= 0;
      while(res.next())
      {
        markers[i++]= res.getLong(1);
      }

      segment(markers);
    }
    else
    {
      // There are no markers ... segment just the whole session
      // segment(new long[0]);
      segments= new Segment[1];
      long data_end= data.timestamps.get(data.timestamps.size()-1);;
      long data_start= data.timestamps.get(0);
      segments[0]= new Segment(data_start, data_end);
    }
    
    return this;
  }*/

  /**
   * Segment the data frame using time stamps where the splits should happen. If no splits are
   * provided (i.e. ls = []) then the whole frame will be created as one segment
   * 
   * @param ls
   */
  public void segment(long[] ls)
  {
    if( ls.length == 0 )
    {
      // There are no markers ... segment just the whole session
      segments= new Segment[1];
      long data_end= data.timestamps.get(data.timestamps.size()-1);;
      long data_start= data.timestamps.get(0);
      segments[0]= new Segment(data_start, data_end);
    }
    else
    {
      int idx= 0;
      int h= 0;
      long start, end;
      
      long data_end= data.timestamps.get(data.timestamps.size()-1);;
      
      segments= new Segment[ls.length+1];
      
      start= data.timestamps.get(0);
      
      while( idx < ls.length && (end= ls[idx]) < data_end )
      {
        segments[idx]= new Segment(start, end, h);
        h= segments[idx].getLastIdx();
        
        idx++;
        start= end;
      }
      
      if( start < data_end )
      {
        end= data.timestamps.get(data.timestamps.size()-1);
        segments[idx]= new Segment(start, data_end);
      }
      // System.out.println(Arrays.toString(segments));
      
      if( idx < segments.length - 1 )
      {
        Segment segmentsnew[]= new Segment[idx];
        System.arraycopy(segments, 0, segmentsnew, 0, idx);
        segments= segmentsnew;
      }
    }
  }
  
  private void recomputeSegments()
  {
    long[]ts= new long[this.segments.length];
    for(int i= 0; i < this.segments.length; i++)
      ts[i]= segments[i].start;
    segment(ts);
  }
  
  public void simplifyMe(Class<? extends Strategy> strategy, Map<String,Object> params) throws DataException
  {
    super.simplifyMe(strategy, params);
    recomputeSegments();
  }
  
  public SegmentIterator iterateSegments()
  {
    return new SegmentIterator();
  }
  
  /**
   * Return a particular segment
   * @param idx
   * @return
   */
  public Segment getSegment(int idx)
  {
    return segments[idx];
  }
  
  public int getSegmentCount()
  {
    if( segments != null )
      return segments.length;
    else
      return 0;
  }
  
  @Override
  public void debug()
  {
    super.debug();
    System.out.println("Segments: Count: " + getSegmentCount());
    
    if( segments != null )
    {
      for(int i= 0; i < segments.length; i++)
        System.out.printf("[%d %d %d]", segments[i].startIdx, segments[i].endIdx, segments[i].duration());
      System.out.println("");
    }
    else
      System.out.println("No Segment data");
  }
  
  public class SegmentedTableIterator implements Iterator<Object[]> {
    int idx;
    int startIdx;
    int endIdx;
    private long ts;
    private Object[] cur;
    private boolean[] cur_orig;
    
    private SegmentedTableIterator(Integer startIdx, Integer endIdx)
    {
      this.startIdx= startIdx;
      this.idx= startIdx - 1;
      this.endIdx= endIdx;
    }
    
    /**
     * Return next row in this segment
     */
    public Object[] next()
    {
      ts= data.timestamps.get(++idx);
      cur= data.values.get(idx);
      cur_orig= data.orig.get(idx);
      return cur;
    }
    
    @Override
    public boolean hasNext()
    {
      return idx < endIdx;
    }

    public Object get(int colidx)
    {
      return cur[colidx];
    }
    
    public boolean isOriginal(int colidx)
    {
      return cur_orig[colidx];
    }
    
    public long getTimestamp()
    {
      return ts;
    }

    public long duration()
    {
      return data.timestamps.get(endIdx) - data.timestamps.get(startIdx);
    }

    public long getBegin()
    {
      return data.timestamps.get(startIdx);
    }
    
    public long getEnd()
    {
      return data.timestamps.get(endIdx);
    }
  }
  
  public class SegmentIterator implements Iterator<SegmentedTableIterator> {
    private int idx;
    private Segment cur;
    
    private SegmentIterator(int idx)
    {
      this.idx= idx;
    }
    
    private SegmentIterator()
    {
      this.idx= -1;
    }
    
    @Override
    public boolean hasNext()
    {
      if( SegmentedData.this.segments != null )
        return idx+1 < SegmentedData.this.segments.length;
      else
        return false;
    }
    
    /**
     * Returns the value of the current colum
     * 
     * @param colname
     * @return
     
    public Object get(String colname)
    {
      for(int i= 0; i < col.names.length; i++)
        if( col.names[i].equals(colname) )
          return cur[i];
          
      return null;
    }*/

    @Override
    public SegmentedTableIterator next()
    {
      cur= SegmentedData.this.segments[++idx];
      return new SegmentedTableIterator(cur.startIdx, cur.endIdx);
    }
  }
}
