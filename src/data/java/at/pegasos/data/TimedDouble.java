package at.pegasos.data;

public class TimedDouble extends TimedObject<Double> {
  public TimedDouble(long ts, double v)
  {
    super(ts, v);
  }
}