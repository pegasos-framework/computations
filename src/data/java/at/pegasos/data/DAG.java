package at.pegasos.data;

import java.util.*;

/**
 * simple directed Graph.
 * 
 * Might be low in performance but does the job.
 *
 * @param <T>
 *          type of node content
 */
public class DAG<T> {
  private final Map<Integer, Node<T>> nodes;
  
  public DAG()
  {
    nodes= new HashMap<Integer, Node<T>>();
  }
  
  /**
   * Add node to graph. If a node with this id exists in the graph no action is taken
   * 
   * @return node in the graph (added or existing one)
   */
  public Node<T> addNode(T id)
  {
    if( nodes.containsKey(id.hashCode()) )
    {
      return nodes.get(id.hashCode());
    }
    else
    {
      Node<T> n= new Node<T>(id);
      nodes.put(id.hashCode(), n);
      return n;
    }
  }
  
  /**
   * Add an edge from a to b without checking acyclicity constraint
   * 
   * @param a starting node
   * @param b ending node
   */
  public void addEdgeNC(T a, T b)
  {
    Node<T> n= nodes.get(b.hashCode());
    if( n == null )
      throw new IllegalArgumentException("could not find " + b);
    
    // Check whether there is no edge a -> b
    Node<T> na= nodes.get(a.hashCode());
    if( !na.successors.contains(n) )
    {
      // Add new edge
      na.successors.add(n);
    }
  }
  
  public static class Node<T> {
    public Node(T id)
    {
      this.id= id;
      this.successors= new ArrayList<Node<T>>();
    }
    
    private final T id;
    protected List<Node<T>> successors;
    
    public final T getID()
    {
      return id;
    }
    
    /**
     * Returns a flat list of all child nodes. I.e. all children and their children
     * 
     * @return list of all nodes reachable from this node
     */
    public List<Node<T>> getAllChildren()
    {
      // System.out.println("Get children for " + this + " " + this.id);
      List<Node<T>> ret= new ArrayList<Node<T>>();
      
      for(Node<T> child : successors)
      {
        // System.out.println("Child: " + child);
        ret.add(child);
        ret.addAll(child.getAllChildren());
      }
      
      return ret;
    }
    
    /**
     * Returns all children of this node. I.e. all nodes with a distance of 1.
     * 
     * @return list of nodes directly connected to this node
     */
    public List<T> getChildren()
    {
      List<T> ret= new ArrayList<T>(successors.size());
      
      for(Node<T> child : successors)
      {
        ret.add(child.getID());
      }
      
      return ret;
    }
    
    /**
     * Returns all children as nodes (i.e. tree objects)
     * 
     * @return
     */
    public List<Node<T>> getChildNodes()
    {
      List<Node<T>> ret= new ArrayList<Node<T>>(successors.size());
      
      for(Node<T> child : successors)
      {
        ret.add(child);
      }
      
      return ret;
    }

    /**
     * Traverse the tree in Pre-Order.
     * 
     * @param run
     *          action to be performed when a node is visited
     * @param post
     *          action to be performed when a node and all its children have been visited i.e. the
     *          processing of this node is done
     */
    public void traverse(NodeVisitor<T> run, NodeVisitor<T> post)
    {
      run.id= this.id;
      run.node= this;
      run.run();
      
      for(Node<T> child : successors)
      {
        child.traverse(run, post);
      }
      
      post.id= this.id;
      post.node= this;
      post.run();
    }
    
    /**
     * Merge all children from other into this node
     * @param other
     */
    public void mergeNode(Node<T> other, NodeMerger<T> merger)
    {
      if( merger != null )
      {
        merger.a= this;
        merger.b= other;
        merger.run();
      }
      
      if( this.successors.size() == 0 )
      {
        this.successors.addAll(other.successors);
      }
      else
      {
        for(Node<T> otherchild : other.successors)
        {
          boolean samechild= false;
          Node<T> mergern= null;
          for(Node<T> mychild : this.successors)
          {
            if( mychild.id.hashCode() == otherchild.id.hashCode() )
            {
              samechild= true;
              mergern= mychild;
              break;
            }
          }
          if( samechild )
          {
            mergern.mergeNode(otherchild, merger);
          }
          else
          {
            this.successors.add(otherchild);
          }
        }
      }
    }
    
    public void mergeNode(Node<T> other)
    {
      mergeNode(other, null);
    }

    @Override public String toString()
    {
      return "Node{" + "id=" + id + ", successors=" + successors + '}';
    }
  }
  
  public static class ColoredNode<T> extends Node<T> {
    public ColoredNode(Node<T> n, int color)
    {
      super(n.id);
      this.successors= n.successors;
      this.color= color;
    }
    
    private int color;
    
    public int getColor()
    {
      return color;
    }

    @Override public String toString()
    {
      return "ColoredNode{" + "color=" + color + ",node=" + super.toString() + '}';
    }
  }
  
  public static abstract class NodeVisitor<T> implements Runnable {
    protected T id;
    protected Node<T> node;
  }
  
  public static abstract class NodeMerger<T> implements Runnable {
    /**
     * Node which is the output of this merge operation
     */
    public Node<T> a;
    /**
     * Node to be merged into a
     */
    public Node<T> b;
  }
  
  /**
   * Get node with specified id
   * 
   * @param id
   * @return
   */
  public Node<T> getNode(T id)
  {
    return nodes.get(id.hashCode());
  }
  
  /**
   * Color the tree
   *
   * @param start
   *          node which will be used for starting color 1
   * @param reachable
   *          if true only nodes directly reachable from the starting node will get the color (i.e. predecessors of this node will get a different color). If false all nodes in the same tree will get the same color
   * @return colored nodes
   */
  public List<ColoredNode<T>> color(T start, boolean reachable)
  {
    int color= 1;
    List<ColoredNode<T>> ret= new ArrayList<ColoredNode<T>>(nodes.size());
    boolean first= true;

    Collection<Node<T>> remain= new ArrayList<Node<T>>(this.nodes.size());
    remain.addAll(this.nodes.values());
    while( !remain.isEmpty() )
    {
      // System.out.println(remain.size());
      
      Node<T> n= null;
      if( first )
      {
        boolean found= false;
        for(Node<T> node : remain)
        {
          if( node.id.hashCode() == start.hashCode() )
          {
            found= true;
            n= node;
            break;
          }
        }
        
        if( !found )
          throw new IllegalArgumentException("could not find starting node " + start + " " + start.getClass() + " " + start.hashCode());
        
        first= false;
      }
      else
      {
        n= remain.iterator().next();
      }
      
      if( reachable )
        color = colorChildrenD(n, remain, ret, color);
      else
        color = colorChildren(n, remain, ret, color);
    }
    
    /*System.out.println("<---");
    for(ColoredNode<T> n : ret)
    {
      System.out.println(n.color + " " + n.getID());
    }
    System.out.println("--->");*/
    
    return ret;
  }
  
  private int colorChildrenD(Node<T> node, Collection<Node<T>> remaining, List<ColoredNode<T>> colored, int color)
  {
    List<Node<T>> children= node.getAllChildren();
    
    boolean found;
    
    ColoredNode<T> s= new ColoredNode<T>(node, color);
    colored.add(s);
    remaining.remove(node);
    for(Node<T> child : children)
    {
      found= false;
      for(ColoredNode<T> other : colored)
      {
        if( other.getID().hashCode() == child.getID().hashCode() )
        {
          found= true;
          break;
        }
      }
      
      if( !found )
      {
        s= new ColoredNode<T>(child, color);
        colored.add(s);
        remaining.remove(child);
      }
    }

    return color + 1;
  }

  /**
   * Color all neighboring nodes of a node (including node itself) i.e. all nodes leading to this node and all children of this node
   * @param node node to use
   * @param remaining all uncolored nodes in the tree
   * @param colored nodes already colored
   * @param color color to use
   */
  private int colorChildren(Node<T> node, Collection<Node<T>> remaining, List<ColoredNode<T>> colored, int color)
  {
    List<Node<T>> children= node.getAllChildren();
    List<Node<T>> toColor= new ArrayList<Node<T>>(children.size());
    
    boolean found;
    
    for(Node<T> child : children)
    {
      // System.out.print("Child " + child.id);
      // Is this node already colored?
      found= false;
      for(ColoredNode<T> other : colored)
      {
        if( other.getID().hashCode() == child.getID().hashCode() )
        {
          color= other.color;
          // System.out.print(" won't be colored");
          found= true;
          break;
        }
      }
      
      if( !found )
      {
        toColor.add(child);
        // System.out.print(" will be colored");
      }
      // System.out.println("");
    }
    
    ColoredNode<T> s= new ColoredNode<T>(node, color);
    colored.add(s);
    remaining.remove(node);
    for(Node<T> child : toColor)
    {
      s= new ColoredNode<T>(child, color);
      colored.add(s);
      remaining.remove(child);
    }

    return color + 1;
  }
  
  public List<T> getNodes()
  {
    List<T> ret= new ArrayList<T>(nodes.size());
    for(Node<T> n : nodes.values())
    {
      ret.add(n.id);
    }
    return ret;
  }
}
