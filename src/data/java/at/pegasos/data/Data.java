package at.pegasos.data;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import at.pegasos.data.simplify.Strategy;

public class Data {
  /**
   * Iterate over a data column but extract only one sub column
   */
  public static class ColumnIterator implements Iterator<Object> {
    
    /**
     * Index where the data is found
     */
    private int cidx;
    
    /**
     * Iterator for the data
     */
    private Iterator<Object[]> c;
    
    /**
     * Iterator for the time
     */
    private Iterator<Long> ti;
    
    /**
     * current time
     */
    private long ct;
    
    private ColumnIterator(Column c, int cidx)
    {
      this.c= c.values.iterator();
      this.cidx= cidx;
      this.ti= c.timestamps.iterator();
    }
    
    public Object next()
    {
      Object[] x= c.next();
      ct= ti.next();
      
      return x[cidx];
    }
    
    public long getTimestamp()
    {
      return ct;
    }
    
    @Override
    public boolean hasNext()
    {
      return c.hasNext();
    }
  }
  
  /**
   * Iterate over the values of a data column (i.e. no information about time is included in this iterator)
   */
  public static class ColumnValueIterator implements Iterator<Object> {
    
    /**
     * Index where the data is found
     */
    private int cidx;
    
    /**
     * Iterator for the data
     */
    private Iterator<Object[]> c;
    
    private ColumnValueIterator(Column c, int cidx)
    {
      this.c= c.values.iterator();
      this.cidx= cidx;
    }
    
    public Object next()
    {
      Object[] x= c.next();
      
      return x[cidx];
    }
    
    @Override
    public boolean hasNext()
    {
      return c.hasNext();
    }
  }
  
  /**
   * Iterate over the table but extract only one sub column and create a list iterator (ie forward and backward iterating)
   */
  public static class ColumnListIterator implements ListIterator<Object> {
    
    /**
     * Index where the data is found
     */
    private int cidx;
    
    /**
     * Iterator for the data
     */
    private ListIterator<Object[]> c;
    
    /**
     * Iterator for the time
     */
    private ListIterator<Long> ti;
    
    /**
     * current time
     */
    private long ct;
    
    private ColumnListIterator(Column c, int cidx, boolean end)
    {
      if( end )
      {
        final int lastIndex= c.values.size();
        this.c= c.values.listIterator(lastIndex);
        this.ti= c.timestamps.listIterator(lastIndex);
      }
      else
      {
        this.c= c.values.listIterator();
        this.ti= c.timestamps.listIterator();
      }
      this.cidx= cidx;
    }
    
    public Object next()
    {
      Object[] x= c.next();
      ct= ti.next();
      
      return x[cidx];
    }
    
    public Object previous()
    {
      Object[] x= c.previous();
      ct= ti.previous();
      
      return x[cidx];
    }
    
    public long getTimestamp()
    {
      return ct;
    }
    
    @Override
    public boolean hasNext()
    {
      return c.hasNext();
    }

    @Override
    public boolean hasPrevious()
    {
      return c.hasPrevious();
    }

    @Override
    public int nextIndex()
    {
      return c.nextIndex();
    }

    @Override
    public int previousIndex()
    {
      return c.previousIndex();
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException();
    }

    @Override
    public void set(Object e)
    {
      throw new UnsupportedOperationException();
    }

    @Override
    public void add(Object e)
    {
      throw new UnsupportedOperationException();
    }
  }
  
  public static class TableIterator implements Iterator<Object[]> {
    private Iterator<Object[]> c;
    private Column col;
    private Object[] cur;
    private Iterator<boolean[]> oi;
    /**
     * current original values
     */
    private boolean[] corig;
    /**
     * Timestamp iterator
     */
    private Iterator<Long> ti;
    
    /**
     * current timestamp
     */
    private long ct;
    
    protected TableIterator(Column c)
    {
      this.col= c;
      this.c= c.values.iterator();
      this.oi= c.orig.iterator();
      this.ti= c.timestamps.iterator();
    }

    protected TableIterator(Column c, int startPos)
    {
      this.col= c;
      this.c= c.values.listIterator(startPos);
      this.oi= c.orig.listIterator(startPos);
      this.ti= c.timestamps.listIterator(startPos);
    }

    public Object[] next()
    {
      cur= c.next();
      corig= oi.next();
      ct= ti.next();
      
      return cur;
    }
    
    @Override
    public boolean hasNext()
    {
      return c.hasNext();
    }
    
    public boolean isOriginal(int colidx)
    {
      return corig[colidx];
    }
    
    public long getTimestamp()
    {
      return ct;
    }
    
    /**
     * Returns the value of the current colum
     * 
     * @param colname
     * @return
     */
    public Object get(String colname)
    {
      for(int i= 0; i < col.names.length; i++)
        if( col.names[i].equals(colname) )
          return cur[i];
        
      return null;
    }
    
    /**
     * Set the value for the current row
     * 
     * @param colname
     *          name of the collumn to be set
     * @param value
     *          value
     * @return true if set was successful (i.e. column existed)
     */
    public boolean set(String colname, Object value)
    {
      for(int i= 0; i < col.names.length; i++)
      {
        if( col.names[i].equals(colname) )
        {
          cur[i]= value;
          return true;
        }
      }
      return false;
    }
  }
	
  public static class Column {
    public String name;
    public String[] names;
    public int[] types;
    public List<Long> timestamps;
    public List<Object[]> values;
    public List<boolean[]> orig;
    public int sample_freq;
    
    /**
     * Default construction of a column. This does not initialise the internal data structures
     */
    public Column()
    {
      
    }
    
    public Column(int size_guess, String name, int[] types, String[] names)
    {
      this.name= name;
      this.types= types;
      this.names= names;
      this.timestamps= new ArrayList<Long>(size_guess);
      this.values= new ArrayList<Object[]>(size_guess);
      this.orig= new ArrayList<boolean[]>(size_guess);
    }
    
    public Column copy()
    {
      Column ret= new Column();
      ret.name= this.name;
      ret.names= Arrays.copyOf(this.names, this.names.length);
      ret.types= Arrays.copyOf(this.types, this.types.length);
      ret.timestamps= new ArrayList<Long>(this.timestamps);
      ret.values= new ArrayList<Object[]>(this.values);
      ret.orig= new ArrayList<boolean[]>(this.orig);
      ret.sample_freq= this.sample_freq;
      return ret;
    }
    
    public void updateFrequency()
    {
      if( values.size() > 0 )
      {
        // compute check
        long sum= 0;
        long last_rect= timestamps.get(0);
        for(int i= 1; i < timestamps.size(); i++)
        {
          int diff= (int) (timestamps.get(i) - last_rect);
          last_rect= timestamps.get(i);
          sum+= diff;
        }
        // System.out.println(sum / ((double) col.timestamps.size()-1));
        sample_freq= (int) (sum / ((double) timestamps.size()-1));
        // col.sample_freq /= 10.0;
        // col.sample_freq*= 10;
      }
      else
      {
        sample_freq= 0;
      }
    }
    
    public Column merge(Column other)
    {
      final int mcolc= this.names.length;
      final int ocolc= other.names.length;
      final int ncolc= mcolc + ocolc;
      
      Column ret= new Column();
      ret.name= this.name;
      ret.names= new String[ncolc];
      System.arraycopy(this.names, 0, ret.names, 0, mcolc);
      System.arraycopy(other.names, 0, ret.names, mcolc, ocolc);
      // System.out.println("Merging " + Arrays.toString(this.names) + " " + Arrays.toString(other.names) + " " + Arrays.toString(ret.names));
      
      ret.types= new int[ncolc];
      System.arraycopy(this.types, 0, ret.types, 0, this.types.length);
      System.arraycopy(other.types, 0, ret.types, this.types.length, other.types.length);

      ret.timestamps= this.timestamps;
      ret.sample_freq= this.sample_freq;

      ret.values= new ArrayList<Object[]>(this.values.size());
      ret.orig= new ArrayList<boolean[]>(this.values.size());
      
      Iterator<Object[]> mvalues= values.iterator();
      Iterator<Object[]> ovalues= other.values.iterator();
      Iterator<boolean[]> morig= orig.iterator();
      Iterator<boolean[]> oorig= other.orig.iterator();
      
      while( mvalues.hasNext() )
      {
        Object[] row= new Object[ncolc];
        Object[] mv= mvalues.next();
        Object[] ov= ovalues.next();
        
        System.arraycopy(mv, 0, row, 0, mcolc);
        System.arraycopy(ov, 0, row, mcolc, ocolc);
        
        ret.values.add(row);
        
        boolean[] orig= new boolean[ncolc];
        boolean[] mo= morig.next();
        boolean[] oo= oorig.next();
        
        System.arraycopy(mo, 0, orig, 0, mcolc);
        System.arraycopy(oo, 0, orig, mcolc, ocolc);
        
        ret.orig.add(orig);
      }
      
      return ret;
    }
    
    public void debug()
    {
      System.out.println("-----debug-col-----");
      System.out.printf("%s: size: %d, freq:%d\n", name, values.size(),
          sample_freq);
      System.out
          .println("cols: " + names.length + "/" + Arrays.toString(names));
      System.out
          .println("types: " + types.length + "/" + Arrays.toString(types));
        /*double durms= (getEnd() - getBegin());
        if( timeUnit != TimeUnit.MilliSecond )
        {
          switch( timeUnit )
          {
            case MicroSecond:
              durms*= 1000;
              break;
            case MilliSecond:
              // can never happen
              assert(false);
              break;
            case Second:
              durms/= 1000;
              break;
            default:
              break;
          }
        }
        System.out.println("Duration: " + durms + "ms " + (durms/1000.0) + "s " + (durms/60_000.0) + "min " + (durms/3_600_000.0)+"h");*/
      System.out.println("-----debug-col-----");
    }

    public ColumnValueIterator iterateColumnValues(String column_name)
    {
      int cidx= 0;
      for(; cidx < names.length; cidx++)
        if( names[cidx].equals(column_name) )
          break;
      if( cidx == names.length )
        throw new IllegalArgumentException("Could not find column '" + column_name + "' in dataframe");
      
      return new ColumnValueIterator(this, cidx);
    }

    /**
     * Set the 'original' info of all values
     * 
     * @param value
     */
    public void setOriginal(boolean value)
    {
      boolean[] values= new boolean[this.names.length];
      Arrays.fill(values, value);
      this.orig= new ArrayList<boolean[]>(this.timestamps.size());
      final int s= timestamps.size();
      for(int i= 0; i < s; i++)
        this.orig.add(values);
    }
  }
  
  private static final String NA = "NULL";// "NA";
  
  private List<Column> cols;

  protected Column data;
  
  protected TimeUnit timeUnit= TimeUnit.MilliSecond;
  
  public Data()
  {
    cols= new ArrayList<Column>();
  }
  
  /**
   * Create a shallow copy of a data. Any modifications to the other data will be present in this data.
   * Except actions like simplification. However, any removed row in `other` will also affect this data.
   * @param other
   */
  public Data(Data other)
  {
    this.cols= other.cols;
    this.data= other.data;
  }

  public static Data createEmpty()
  {
    Data ret = new Data();
    ret.data = new Column();
    ret.data.names = new String[0];
    ret.data.types = new int[0];
    ret.data.timestamps = new ArrayList<>(0);
    ret.data.values = new ArrayList<>(0);
    ret.data.orig = new ArrayList<>(0);
    return ret;
  }
  
  public ColumnIterator iterateTableColumn(String column_name)
  {
    int cidx= 0;
    for(; cidx < data.names.length; cidx++)
      if( data.names[cidx].equals(column_name) )
        break;
    if( cidx == data.names.length )
      throw new IllegalArgumentException("Could not find column '" + column_name + "' in dataframe");
    
    return new ColumnIterator(data, cidx);
  }
  
  /**
   * Create a list iterator for a column of the table
   * @param column_name name of the column to be iterated
   * @return ColumnListIterator for the column
   */
  public ColumnListIterator iterateTableColumnList(String column_name)
  {
    return iterateTableColumnList(column_name, false);
  }
  
  /**
   * Create a list iterator for a column of the table
   * @param column_name name of the column to be iterated
   * @param end if true the iterator will be started from the end
   * @return ColumnListIterator for the column
   */
  public ColumnListIterator iterateTableColumnList(String column_name, boolean end)
  {
    int cidx= 0;
    for(; cidx < data.names.length; cidx++)
      if( data.names[cidx].equals(column_name) )
        break;
    if( cidx == data.names.length )
      throw new IllegalArgumentException("Could not find column '" + column_name + "' in dataframe");
    
    return new ColumnListIterator(data, cidx, end);
  }

  /**
   * Create an iterator. This iterator can iterate over all rows.
   * 
   * @return
   * @throws IllegalStateException
   *           if no data exists
   */
  public TableIterator iterateTable() throws IllegalStateException
  {
    if( data == null )
      throw new IllegalStateException("Cannot iterate over a non existing frame. data is null");
    return new TableIterator(data);
  }
  
  /**
   * Create an iterator. This iterator can iterate over all rows from a given starting position
   * 
   * @param startPos index / row from which the iterator should start
   * @return
   * @throws IllegalStateException
   *           if no data exists
   */
  public TableIterator iterateTable(int startPos) throws IllegalStateException
  {
    if( data == null )
      throw new IllegalStateException("Cannot iterate over a non existing frame. data is null");
    return new TableIterator(data, startPos);
  }

  public int getColumnIndex(String colname)
  {
    for(int i= 0; i < data.names.length; i++)
    {
      // System.out.println(data.names[i] + " " + colname);
      if( data.names[i].equals(colname) )
        return i;
    }
        
    throw new IllegalArgumentException("Cannot find Column '" + colname + "' in table");
  }
  
  /**
   * check whether column exists in data frame
   * @param colname
   * @return
   */
  public boolean hasColumn(String colname)
  {
    for(int i= 0; i < data.names.length; i++)
    {
      if( data.names[i].equals(colname) )
        return true;
    }
    
    return false;
  }
  
  /**
   * Add a new column as *source* for this data
   * 
   * @param col
   */
  public void addColumn(Column col)
  {
    this.cols.add(col);
  }
  
  /**
   * Add a column to the existing data. Name and timestamps of the column are ignored.
   * Timestamps are required to match the existing
   * 
   * @param col
   */
  public void addColumnDirect(Column col)
  {
    assert (data != null);
    assert (col.timestamps == null || data.timestamps.size() == col.timestamps.size());
    
    data= data.merge(col);
  }
  
  /**
   * Get first timestamp in dataframe
   * @return first timestamp or 0 if no data is present
   */
  public long getBegin()
  {
    int size= data.timestamps.size();
    if( size > 0 )
      return data.timestamps.get(0);
    else
      return 0;
  }
  
  /**
   * Get last timestamp in dataframe
   * @return last timestamp or 0 if no data is present
   */
  public long getEnd()
  {
    int size= data.timestamps.size();
    if( size > 0 )
      return data.timestamps.get(size-1);
    else
      return 0;
  }
  
  /**
   * Get the last row in the data frame
   * 
   * @return the last row
   */
  public Object[] lastRow()
  {
    return data.values.get(data.values.size()-1);
  }
  
  /**
   * Get the number of rows in the data frame
   */
  public int getRowCount()
  {
    return this.data.values.size();
  }
  
  /**
   * Create a new data object with the first column object set to be the data object
   * 
   * @return new data object
   */
  public Data Col2Data()
  {
    if( cols.size() > 0 )
    {
      Data ret= copy();
      ret.data= cols.get(0);
      return ret;
    }
    else
    {
      return null;
    }
  }

  /**
   * Get the names of the data columns
   * @return
   */
  public String[] getColumnNames()
  {
    return data.names;
  }
  
  /**
   * Get the names of the columns in the columns. I.e. all names of the individual columns
   * @return
   */
  public List<String> getColColNames()
  {
    List<String> names= new ArrayList<String>(cols.size() * 5);
    for(Column col : cols)
    {
      names.add(col.name);
    }
    return names;
  }

  /**
   * Simplify the data frame (ie merge all columns into 'data') using the supplied strategy
   * 
   * @param strategy
   *          strategy to use
   * @throws DataException
   *           if strategy could not be applied correctly
   */
  public Data simplify(Class<? extends Strategy> strategy) throws DataException
  {
    return simplify(strategy, Strategy.newParams());
  }

  /**
   * Simplify the data frame (ie merge all columns into 'data') using the supplied strategy and
   * params to the strategy
   * 
   * @param strategy
   *          strategy to use
   * @param params
   *          parameters to the strategy
   * @throws DataException
   *           if strategy could not be applied correctly
   */
  public Data simplify(Class<? extends Strategy> strategy, Map<String,Object> params) throws DataException
  {
    data= new Column();
    data.name= "data";
    
    try
    {
      Strategy strat= strategy.getConstructor(Column.class, List.class).newInstance(data, cols);
      strat.setParams(params);
      strat.simplify();
    }
    catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e )
    {
      throw new DataException(e);
    }
    
    return this;
  }
  
  public void simplifyMe(Class<? extends Strategy> strategy, Map<String,Object> params) throws DataException
  {
    Column data_old= data;
    
    data= new Column();
    data.name= "data";
    data.names= data_old.names;
    data.types= data_old.types;
    
    List<Column> l= new ArrayList<Column>(1);
    l.add(data_old);
    
    try
    {
      Strategy strat= strategy.getConstructor(Column.class, List.class).newInstance(data, l);
      strat.setParams(params);
      strat.simplify();
    }
    catch( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e )
    {
      throw new DataException(e);
    }
  }
  
  public Data copy()
  {
    Data ret= new Data();
    
    ret.cols= new ArrayList<Column>(this.cols.size());
    for(Column c : this.cols)
      ret.cols.add(c.copy());
    if( this.data != null )
    {
      ret.data= this.data.copy();
    }
    else
    {
      ret.data= null;
    }
    
    return ret;
  }
  
  public void writeFileCSV(String name) throws IOException
  {
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(name));
    
    int j;
    
    System.out.println("Writting: " + Arrays.toString(data.names));
    writer.write("rec_time;" + String.join(";", data.names) + "\n");
    // loop over all rows
    for(int i= 0; i < data.values.size(); i++)
    {
      writer.write("" + data.timestamps.get(i));
      Object[] row= data.values.get(i);
      // for(j= 0; j < row.length - 1; j++)
      //   writer.write(row[j] + ";");
      j= 0;
      while( j < row.length )
      {
        writer.write(";");
        if( row[j] != null && ( data.types[j] == Types.DECIMAL || data.types[j] == Types.DOUBLE || data.types[j] == Types.FLOAT ) )
        {
          writer.write(row[j].toString().replace('.', ','));
        }
        else
        {
          writer.write(row[j] + "");
        }
        j++;
      }
      writer.write("\n");
    }
    writer.close();
  }
  
  public void debug()
  {
    System.out.println("-----debug-----");
    System.out.printf("Columns %d\n", cols.size());
    for(Column col : cols)
    {
      System.out.printf("Column %s: size: %d, freq:%d\n", col.name, col.values.size(), col.sample_freq);
    }
    if( data != null )
    {
      System.out.printf("Data: size: %d, freq:%d ", data.values.size(), data.sample_freq);
      System.out.println("cols: " + Arrays.toString(data.names));
      double durms= (getEnd() - getBegin());
      if( timeUnit != TimeUnit.MilliSecond )
      {
        switch( timeUnit )
        {
          case MicroSecond:
            durms*= 1000;
            break;
          case MilliSecond:
            // can never happen
            assert(false);
            break;
          case Second:
            durms/= 1000;
            break;
          default:
            break;
        }
      }
      System.out.println("Duration: " + durms + "ms " + (durms/1000.0) + "s " + (durms/60_000.0) + "min " + (durms/3_600_000.0)+"h");
    }
    else
      System.out.println("No data");
    System.out.println("-----debug-----");
  }
  
  public void printTable()
  {
    System.out.println("----Table----");
    if( data != null )
    {
      System.out.println("time\t" + String.join("\t",data.names));
      for(int i= 0; i < data.timestamps.size(); i++)
      {
        Object[] values= data.values.get(i);
        String[] s= new String[values.length];
        for(int j= 0; j < values.length; j++)
          s[j]= values[j] != null ? values[j].toString() : NA;
        System.out.println(data.timestamps.get(i) + "\t" + String.join("\t",s));
      }
    }
    else
    {
      System.out.println("No data");
    }
    System.out.println("----Table----");
  }

  public static String[] usedTables(long session)
  {
    return null;
  }
  
  /**
   * Append a new row to the table. This method does not check whether the time stamps are still
   * sorted
   * 
   * @param timestamp
   *          time of the row
   * @param values
   *          values for this row
   */
  public void addRow(long timestamp, Object[] values)
  {
    data.timestamps.add(timestamp);
    data.values.add(values);
    boolean orig[]= new boolean[values.length];
    Arrays.fill(orig, true);
    data.orig.add(orig);
  }

  /**
   * Add values to the columns of the data frame (not to the data itself)
   * 
   * @param timestamp
   *          time of this row
   * @param values
   *          values to be added. Key:columnname
   */
  public void addColumnRow(long timestamp, Map<String, Object> values)
  {
    boolean found= false;
    Column col= null;
    final boolean[] orig= new boolean[] {true};
    for(java.util.Map.Entry<String, Object> value : values.entrySet())
    {
      found= false;
      for(Column c : cols)
      {
        if( c.name.equals(value.getKey()) )
        {
          found= true;
          col= c;
        }
      }
      
      if( !found )
      {
    	System.out.println("Column not found:" + value.getKey());
        col= new Column();
        col.name= value.getKey();
        col.names= new String[] {value.getKey()};
        col.orig= new ArrayList<boolean[]>();
        col.timestamps= new ArrayList<Long>();
        col.types= new int[] {Types.VARCHAR };
        col.values= new ArrayList<Object[]>();
        cols.add(col);
      }
      
      col.timestamps.add(timestamp);
      col.orig.add(orig);
      if( value.getValue().getClass().isArray() )
      {
        col.values.add((Object[]) value.getValue());
      }
      else
      {
        col.values.add(new Object[] {value.getValue()});
      }
    }
  }

  public void renameColumn(String oldname, String newname)
  {
    for(int i= 0; i < data.names.length; i++)
    {
      if( data.names[i].equals(oldname) )
      {
        data.names[i]= newname;
        return;
      }
    }
  }
  
  public void scaleColumn(String name, double factor)
  {
    for(int i= 0; i < data.names.length; i++)
    {
      if( data.names[i].equals(name) )
      {
    	if( data.types[i] == Types.DOUBLE )
    	{
    	  for(Object[] row : data.values)
    	  {
    	    row[i]= ((Double) row[i]) * factor;
    	  }
    	}
    	else if( data.types[i] == Types.INTEGER )
    	{
    	  for(Object[] row : data.values)
    	  {
    	    row[i]= ((Integer) row[i]) * factor;
    	  }
    	  data.types[i]= Types.DOUBLE;
    	}
    	else if( data.types[i] == Types.BIGINT )
    	{
    	  for(Object[] row : data.values)
    	  {
    	    row[i]= ((Long) row[i]) * factor;
    	  }
    	  data.types[i]= Types.DOUBLE;
    	}
        return;
      }
    }
  }
  
  /**
   * Get the type of a column in data frame
   * 
   * @param name
   *          of the column
   * @return type of the column if exists otherwise IllegalArgumentException will be thrown
   */
  public int getType(String name)
  {
    int idx= 0;
    for(String colName : data.names)
    {
      if( colName.equals(name) )
      {
        return data.types[idx];
      }
      idx++;
    }
    
    throw new IllegalArgumentException("Unknown column " + name);
  }

  /**
   * Remove a column from the data frame
   * @param colName name of the column
   */
  public void removeColumn(String colName)
  {
    int idx= getColumnIndex(colName);
    final int count= data.names.length-1;
    String[] names= new String[count];
    ArrayHelper.removeElement(data.names, idx, names);
    data.names= names;
    int types[]= new int[count];
    ArrayHelper.removeElement(data.types, idx, types);
    data.types= types;
    for(int i= 0; i < data.values.size(); i++)
    {
      Object[] values= data.values.get(i);
      Object[] values2= new Object[count];
      ArrayHelper.removeElement(values, idx, values2);
      data.values.set(i, values2);
      
      boolean[] orig= data.orig.get(i);
      boolean[] orig2= new boolean[count];
      ArrayHelper.removeElement(orig, idx, orig2);
      data.orig.set(i, orig2);
    }
  }

  public TimeUnit getTimeUnit()
  {
    return timeUnit;
  }

  public void setTimeUnit(TimeUnit timeUnit)
  {
    this.timeUnit= timeUnit;
  }
  
  public Column getColumn(int idx)
  {
    return cols.get(idx);
  }

  /**
   * Return a row from the data frame
   * 
   * @param index
   *          index of the row to be retrieved
   * @return row in raw format
   */
  public Object[] getRow(int index)
  {
    return data.values.get(index);
  }
  
  /**
   * Return the time of a row from the data frame
   * 
   * @param index
   *          index of the row to be retrieved
   * @return row in raw format
   */
  public long getTime(int index)
  {
    return data.timestamps.get(index);
  }

  /**
   * Get the frequency of the data column
   *
   * @return sample frequency or 0 if data does not exist
   */
  public long getFrequeny()
  {
    if( data == null )
    {
      return 0;
    }
    else
    {
      return data.sample_freq;
    }
  }
}
