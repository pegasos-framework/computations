package at.pegasos.data.loader;

public interface DataExtractor
{
  public String[] extractData(String[] lineparts);
}