package at.pegasos.data.loader;

import at.pegasos.data.Data.*;
import org.json.*;

import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.atomic.*;
import java.util.regex.*;

public class JsonLoader extends DataLoader {

  public JsonLoader setParams(FileParameters p)
  {
    this.params= p;
    return this;
  }

  public JsonLoader setParamsFromFile(String fileName)
  {
    FileParameters ret= new FileParameters();
    try
    {
      ret.fileName= fileName;
      this.params= ret;

      buildInputString();

      // TODO: newer versions of java have .results() ...
      String patternStringPoint= "\\d+[.]\\d+";
      Pattern pattern= Pattern.compile(patternStringPoint);
      Matcher matcher= pattern.matcher(allLinesIntoOne);
      int countPoint= 0;
      while( matcher.find() )
      {
        countPoint++;
      }

      String patternStringComma= "\\d+,\\d+";
      pattern= Pattern.compile(patternStringComma);
      matcher= pattern.matcher(allLinesIntoOne);
      int countComma= 0;
      while( matcher.find() )
      {
        countComma++;
      }

      if( countComma > countPoint )
        ret.dec= ",";
      else
        ret.dec= ".";
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }

    return this;
  }

  /**
   * basic file parameters for a json file
   */
  public static class FileParameters
  {
    public String fileName;

    public String dec= ".";

    public String toString()
    {
      return "JsonLoader.FileParameters(fileName=" + this.fileName + ", dec=" + this.dec + ")";
    }
  }
  
  public interface TimeExtractor {
    String getDateName();

    /**
     * Extract the time from a JSONObject
     * 
     * @param obj
     *          complete object
     * @return time of the object
     * @throws ParseException 
     * @throws JSONException 
     */
    public long extractTime(JSONObject obj) throws JSONException, ParseException;
  }
  
  public static class SimpleTimeExtractor implements TimeExtractor {
    
    private final SimpleDateFormat sdf;
    private final String dateName;
    
    public SimpleTimeExtractor(String format, String dateName)
    {
      sdf= new SimpleDateFormat(format);
      this.dateName= dateName;
    }

    @Override
    public long extractTime(JSONObject obj) throws JSONException, ParseException
    {
      return sdf.parse(obj.getString(dateName)).getTime();
    }

    @Override
    public String getDateName()
    {
      return dateName;
    }
  }

  public static class SecExtractor implements TimeExtractor {

    private final String dateName;

    public SecExtractor()
    {
      this.dateName= "SECS";
    }

    public SecExtractor(String dateName)
    {
      this.dateName= dateName;
    }

    @Override
    public long extractTime(JSONObject obj) throws JSONException, ParseException
    {
      return obj.getLong(dateName) * 1000;
    }

    @Override
    public String getDateName()
    {
      return dateName;
    }
  }

  /**
   * Parameters for current load operation
   */
  FileParameters params;
  
  private TimeExtractor timeExtractor= new SimpleTimeExtractor("yyyy/MM/dd HH:mm:ss", "date");
  
  // Temporary data
  
  /**
   * Temporary rows
   */
  private List<Map<String, Object>> rows;
  
  /**
   * Name of the columns
   */
  private Set<String> names;
  
  private Column datacol;

  private String[] dataElement= new String[] {"RIDES"};
  
  private String allLinesIntoOne= null;

  public static JsonLoader create()
  {
	JsonLoader ret= new JsonLoader();
    return ret;
  }

  /**
   * Load data from the file as specified by the parameters
   * 
   * @throws IOException
   */
  public void load() throws IOException
  {
    if( allLinesIntoOne == null )
    {
      buildInputString();
    }
    
    // build a JSON object
    JSONObject completeData= new JSONObject(allLinesIntoOne);

    JSONArray array= loadData(completeData);

    final int rowCount= array.length();

    datacol= new Column();
    datacol.timestamps= new ArrayList<Long>(rowCount);
    datacol.values= new ArrayList<Object[]>(rowCount);
    datacol.orig= new ArrayList<boolean[]>(rowCount);
    
    rows= new ArrayList<Map<String, Object>>(rowCount);
    
    names= new HashSet<String>();

    // convert data to list of maps
    array.forEach(row -> {
      if( (row instanceof JSONObject) )
      {
        JSONObject obj= (JSONObject) row;
        long date;
        try
        {
          date= timeExtractor.extractTime(obj);
          
          Map<String, Object> rowdata= new HashMap<String,Object>();
          
          extractData(obj, rowdata);
          
          datacol.timestamps.add(date);
          rows.add(rowdata);
          names.addAll(rowdata.keySet());
        }
        catch( JSONException | ParseException e )
        {
          e.printStackTrace();
        }
      }
      else
      {
        // TODO: take care of error
      }
    });
    
    determineTypes();
    
    rows2data();
    
    data.addColumn(datacol);
  }

  private void buildInputString() throws IOException
  {
    allLinesIntoOne= "";
    List<String> lines= Files.readAllLines(Paths.get(params.fileName));
    // System.out.println("Read " + lines.size() + " lines");
    StringBuilder builder= new StringBuilder(10000000);
    for(String line : lines)
    {
      builder.append(line);
    }
    allLinesIntoOne= builder.toString();
  }

  private JSONArray loadData(JSONObject completeData)
  {
    if( dataElement.length == 0 )
      return completeData.getJSONArray(dataElement[0]);
    else
    {
      JSONObject tmp= completeData;
      for(int i= 0; i < dataElement.length; i++)
      {
        Object h= tmp.get(dataElement[i]);
        if( h.getClass() == org.json.JSONObject.class )
        {
          tmp= (JSONObject) h;
        }
        // we have an array and are at the end --> we are done
        else if( h.getClass() == org.json.JSONArray.class && i == dataElement.length - 1 )
        {
          return (JSONArray) h;
        }
        else
        {
          // TODO: !!!
        }
      }
    }
    return null;
  }

  /**
   * Extract data from an object
   * 
   * @param obj
   * @param rowdata
   */
  private void extractData(Object obj, Map<String, Object> rowdata)
  {
    extractData(obj, rowdata, "");
  }

  /**
   * Extract data from an object
   * 
   * @param obj
   * @param rowdata
   * @param name
   */
  private void extractData(Object obj, Map<String, Object> rowdata, String name)
  {
    // System.out.println(name + " " + obj.getClass() + " " + obj);
    if( obj instanceof JSONObject )
    {
      // determine all columns
      String[] cols= JSONObject.getNames((JSONObject) obj);
      for(String colname : cols)
      {
        if( colname.equals(timeExtractor.getDateName()) )
          continue;
        
        Object col= ((JSONObject) obj).get(colname);
        // System.out.println(colname + " " + col.getClass());
        if( col instanceof JSONObject )
        {
          extractData((JSONObject) col, rowdata, name == "" ? colname : name + "." + colname);
        }
        else if( col instanceof JSONArray )
        {
          JSONArray cola= (JSONArray) col;
          AtomicInteger idx= new AtomicInteger();
          String tmp= name == "" ? colname + "." : name + "." + colname + ".";
          cola.forEach(o -> extractData(o, rowdata, tmp + (idx.getAndIncrement())));
        }
        else
        {
          rowdata.put(name == "" ? colname : name + "." + colname, col);
        }
      }
    }
    else
    {
      rowdata.put(name, obj);
    }
  }
  
  private void determineTypes()
  {
    String[][] data;
    
    data= new String[rows.size() < 20 ? rows.size() : 20][];
    
    final int colc= names.size();
    
    datacol.names= names.toArray(new String[0]);
    
    int j;
    for(int i= 0; i < data.length; i++)
    {
      data[i]= new String[colc];
      for(j= 0; j < colc; j++)
      {
        String col= datacol.names[j];
        if( rows.get(i).containsKey(col) )
        {
          data[i][j]= rows.get(i).get(col).toString();
        }
      }
    }
    
    datacol.types= new int[colc];
    
    final String numberPattern= "-?\\d+(\\" + params.dec + "\\d+)?";

    for(int col= 0; col < colc; col++)
    {
      int qualityString= 0;
      int qualityNumber= 0;
      int qualityTime= 0;

      for(int row= 0; row < data.length; row++)
      {
        String str;

        if( col < data[row].length )
          str= data[row][col];
        else
          continue;

        // if( data[row][col].contains(dec) )
        // decimalPresent= true;

        // Guess the DataType
        if( str == null )
        {
          continue;
        }

        // Number
        if( str.matches(numberPattern) )
        {
          qualityNumber++;
        }
        // SS
        else if( str.matches("[0-5][0-9]") )
        {
          qualityTime++;
        }
        // MM:SS
        else if( str.matches("[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // HH:MM:SS
        else if( str.matches("[0-2][0-9]:[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // H:MM:SS
        else if( str.matches("[0-9]:[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // H:MM:SS,MMM 0:02:53,300
        else if( str.matches("[0-9]:[0-5][0-9]:[0-5][0-9],[0-9][0-9][0-9]") )
        {
          qualityTime++;
        }
        // YYYY-MM-DDTHH:MM:SS 2019-05-16T04:36:22
        else if( str.matches("[0-9][0-9][0-9][0-9]\\-[0-9][0-9]\\-[0-9][0-9][Tt][0-2][0-9]:[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // YYYY-MM-DDTHH:MM:SS 2019-05-16T04:36:22.01
        else if( str
            .matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][Tt][0-2][0-9]:[0-5][0-9]:[0-5][0-9][.][0-9]+") )
        {
          qualityTime++;
        }
        // String
        else
        {
          qualityString++;
        }
      }

      // System.out.println(datacol.names[col] + " " + qualityString + " " + qualityNumber + " " + qualityTime);
      if( qualityString > qualityNumber && qualityString > qualityTime )
      {
        datacol.types[col]= Types.VARCHAR;
      }
      else if( qualityNumber > qualityString && qualityNumber > qualityTime )
      {
        datacol.types[col]= Types.DOUBLE;
      }
      else if( qualityTime > qualityNumber && qualityTime > qualityString )
      {
        datacol.types[col]= Types.DATE;
      }
      else
      {
        // If no specific type was found, try to use the given data as number
        datacol.types[col]= Types.DOUBLE;
      }
    }
    
    // j= 0;
    // for(String colname : names)
    // {
    //   System.out.println(colname + " " + datacol.types[j++]);
    // }
  }

  /**
   * Copy data from the rows into the data col
   */
  private void rows2data()
  {
    boolean[] orig= new boolean[datacol.names.length];
    for(int i= 0; i < orig.length; i++)
      orig[i]= true;
    
    final int colCount= names.size();
    for(Map<String, Object> row : rows)
    {
      Object[] rowd= new Object[colCount];
      int j= 0;
      for(String col : names)
      {
        // System.out.println(col + " " + datacol.types[j]);
        if( row.containsKey(col) )
        {
          switch(datacol.types[j] )
          {
            case Types.VARCHAR:
              rowd[j]= (String) row.get(col);
              break;
            case Types.DOUBLE:
              try
              {
                rowd[j]= Double.parseDouble(row.get(col).toString());
              }
              catch( NumberFormatException e )
              {
                // TODO: handle problem
              }
              break;
            case Types.DATE:
              throw new IllegalStateException("Not implemented");
              // TODO: implement date parsing
          }
        }
        /*else
        {
          rowd[j]= null;
        }*/
        j++;
      }
      
      datacol.values.add(rowd);
      datacol.orig.add(orig);
    }
  }

  public JsonLoader setDataElement(String... string)
  {
    dataElement= string;
    return this;
  }

  public JsonLoader setTimeExtractor(TimeExtractor timeExtractor)
  {
    this.timeExtractor= timeExtractor;
    return this;
  }
}
