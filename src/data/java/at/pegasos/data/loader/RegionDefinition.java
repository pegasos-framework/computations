package at.pegasos.data.loader;

/**
 * Definition for loading only a part of a file
 */
public class RegionDefinition extends LoadDefinition {
  /**
   * Line where the data starts. Any header should not be included in this
   */
  int startLine;
  int endLine= 0;
  
  public RegionDefinition(String name, int startLine, int endLine)
  {
    super(name);
    this.startLine= startLine;
    this.endLine= endLine;
  }
  
  public RegionDefinition(int startLine, int endLine)
  {
    super();
    this.startLine= startLine;
    this.endLine= endLine;
  }
  
  public RegionDefinition(String name, int startLine)
  {
    super(name);
    this.startLine= startLine;
  }
  
  public RegionDefinition(int startLine)
  {
    super();
    this.startLine= startLine;
  }
  
  public void setEndLine(int endLine)
  {
    this.endLine= endLine;
  }

  public int getStartLine()
  {
    return this.startLine;
  }

  public int getEndLine()
  {
    return this.endLine;
  }
}