package at.pegasos.data.loader;

import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * This time extractor simply converts a number in seconds to milliseconds
 */
public class GuessingTimeExtractor implements TimeExtractor {

  private final String[] COLNAMES= new String[] {"rec_time", "time", "date_start"};

  public static final SimpleDateFormat TimeFormats[];
  static {
    TimeFormats= new SimpleDateFormat[] {
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    };
  }

  private int rec_time_idx;

  private boolean typeDouble;
  
  public GuessingTimeExtractor()
  {
  }
  
  @Override
  public long extractTime(String[] lineparts)
  {
    if( typeDouble )
    {
      String time= lineparts[rec_time_idx];
      
      long ret;
      
      ret= (long) (Double.parseDouble(time) * 1000);
      
      return ret;
    }
    else
    {
      for(SimpleDateFormat timeFormat : TimeFormats)
      {
        try
        {
          return timeFormat.parse(lineparts[rec_time_idx]).getTime();
        }
        catch( ParseException ignored )
        {
        }
      }
      return -1;
    }
  }

  @Override
  public void init(String[] unmaskedcolnames, String[] colnames, int[] types, boolean[] mask)
  {
    rec_time_idx= -1;
    boolean found= false;
    int sIdx= 0;
    while( !found && sIdx < COLNAMES.length )
    {
      String colname= COLNAMES[sIdx++];
      for(int i= 0; i < unmaskedcolnames.length; i++)
      {
        if( unmaskedcolnames[i].equals(colname) && (types[i] == Types.DATE || types[i] == Types.TIME || types[i] == Types.DOUBLE) )
        {
          rec_time_idx= i;
          found= true;

          if( types[i] == Types.DOUBLE )
            typeDouble= true;

          break;
        }
      }
    }
  }
}