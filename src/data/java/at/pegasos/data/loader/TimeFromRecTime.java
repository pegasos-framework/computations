package at.pegasos.data.loader;

/**
 * This time extractor converts times from a given column in unix timestamp format
 */
public class TimeFromRecTime implements TimeExtractor {

  private final String colname;

  private int rec_time_idx;

  /**
   * Construct the extractor. Use the specified column as time column
   * 
   * @param colname
   *          name/label of the column containing the time information
   */
  public TimeFromRecTime(String colname)
  {
    this.colname= colname;
  }

  @Override
  public long extractTime(String[] lineparts)
  {
    return Long.parseLong(lineparts[rec_time_idx]);
  }

  @Override
  public void init(String[] unmaskedcolnames, String[] colnames, int[] types, boolean[] mask)
  {
    rec_time_idx= -1;
    for(int i= 0; i < unmaskedcolnames.length; i++)
    {
      // System.out.println(unmaskedcolnames[i]);
      if( unmaskedcolnames[i].equals(colname) )
      {
        rec_time_idx= i;
        break;
      }
    }
  }
}
