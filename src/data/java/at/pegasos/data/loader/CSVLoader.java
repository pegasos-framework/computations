package at.pegasos.data.loader;

import at.pegasos.data.Data.*;

import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.text.*;
import java.util.*;

public class CSVLoader extends DataLoader {
	
  /**
   * Parameters for loading a CSV file using the CSVLoader class
   */
  public static class Parameter
  {
    public String fileName;

    public String sep= ".";

    public String dec= ",";

    public Parameter()
    {

    }

    public Parameter(String fileName, String sep, String dec)
    {
      this.fileName= fileName;
      this.sep= sep;
      this.dec= dec;
    }

    public String toString()
    {
      return "CSVLoader.Parameter(fileName=" + this.fileName + ", sep=" + this.sep + ", dec=" + this.dec + ")";
    }
  }

  private String NA = "NULL";// "NA";
  
  private static final SimpleDateFormat sdf;
  static {
    sdf= new SimpleDateFormat("YYYY-MM-DD hh:mm:ss");
  }
  
  /**
   * Parameters for current load operation
   */
  private Parameter params;
  
  public static CSVLoader create()
  {
	CSVLoader ret= new CSVLoader();
    return ret;
  }
  
  List<String> lines;
  Column datacol;
  String sep;
  
  int rec_time_idx;
  
  String[] tmp;

  /**
   * Symbol for decimal
   */
  private String dec;
  
  private void loadLine(String line)
  {
	  tmp= line.split(sep);
  }

  private DataExtractor extractData;

  private TimeExtractor	extractTime;

  private boolean determineTypes()
  {
    String[][] data;
    
    int length= 0;
    
    data= new String[lines.size() < 20 ? lines.size() : 20][];

    if( data.length == 0 )
    {
      System.err.println("could not determine file types due to insuficient length");
      return false;
    }

    for(int i= 0; i < data.length; i++)
    {
      loadLine(lines.get(i));
      data[i]= extractData.extractData(tmp);
      if( data[i].length > length )
        length= data[i].length;
    }
    
    // previously we used the first row to determine the length
    // this caused unexpected behaviour with files with non uniform length
    datacol.types= new int[length];
	
    final String numberPattern= "-?\\d+(\\" + dec + "\\d+)?";
    
    for(int col= 0; col < data[0].length; col++)
    {
      int qualityString = 0;
	  int qualityNumber = 0;
	  int qualityTime = 0;
      
      for(int row= 0; row < data.length; row++)
      {
    	String str;
    	
    	if( col < data[row].length )
    		str= data[row][col];
    	else
    		continue;
    	
        //if( data[row][col].contains(dec) )
        //  decimalPresent= true;
        
        // Guess the DataType
        if( str == null )
        {
          continue;
        }
		
		// System.out.println(str + " " + numberPattern + " " + str.matches(numberPattern));
		
        // Number
        if( str.matches(numberPattern) )
        {
          qualityNumber++;
        }
        // SS
        else if( str.matches("[0-5][0-9]") )
        {
          qualityTime++;
        }
        // MM:SS
        else if( str.matches("[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // HH:MM:SS
        else if( str.matches("[0-2][0-9]:[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // H:MM:SS
        else if( str.matches("[0-9]:[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // H:MM:SS,MMM 0:02:53,300
        else if( str.matches("[0-9]:[0-5][0-9]:[0-5][0-9],[0-9][0-9][0-9]") )
        {
          qualityTime++;
        }
        // YYYY-MM-DDTHH:MM:SS 2019-05-16T04:36:22
        else if( str.matches("[0-9][0-9][0-9][0-9]\\-[0-9][0-9]\\-[0-9][0-9][Tt][0-2][0-9]:[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // YYYY-MM-DDTHH:MM:SS 2019-05-16T04:36:22.01
        else if( str
            .matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][Tt][0-2][0-9]:[0-5][0-9]:[0-5][0-9][.][0-9]+") )
        {
          qualityTime++;
        }
        // YYYY-MM-DD HH:MM:SS 2019-05-16 04:36:22
        else if( str.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]") )
        {
          qualityTime++;
        }
        // String
        else
        {
          qualityString++;
        }
      }

      if( qualityString > qualityNumber && qualityString > qualityTime )
      {
        datacol.types[col]= Types.VARCHAR;
        // System.out.println(col < datacol.names.length ? datacol.names[col] : "" + " is string
        // qualityString > qualityNumber && qualityString > qualityTime");
        // return DataType.STRING;
      }
      else if( qualityNumber > qualityString && qualityNumber > qualityTime )
      {
			// If the column describes a time, but the determined DataType is a number, we still use time
			// This prevents the creation of unwanted ScatterCharts.
			// if (name != null) {
			//	name = name.toLowerCase().trim();
			//	if (name.equals("zeit") || name.equals("time") || name.equals("t")) {
			//		return DataType.TIME;
			//	}
			// }
			
					// System.out.println(col < datacol.names.length ? datacol.names[col] : "" + " is number");
        datacol.types[col]= Types.DOUBLE;
        // return DataType.NUMBER;
      }
      else if( qualityTime > qualityNumber && qualityTime > qualityString )
      {
        // System.out.println(col < datacol.names.length ? datacol.names[col] : "" + " is time");
        datacol.types[col]= Types.DATE;
        // return DataType.TIME;
      }
      else
      {
        // If no specific type was found, try to use the given data as number
        // System.out.println(col < datacol.names.length ? datacol.names[col] : "" + " is number");
        datacol.types[col]= Types.DOUBLE;
        // return DataType.NUMBER;
      }
    }

    return true;
  }
  
  private void loadData()
  {
    int rows = lines.size();

    datacol.timestamps= new ArrayList<Long>(rows);
    datacol.values= new ArrayList<Object[]>(rows);
    datacol.orig= new ArrayList<boolean[]>(rows);
	  
	  boolean[] orig= new boolean[datacol.names.length];
	    for(int i= 0; i < orig.length; i++)
	      orig[i]= true;
	  
	  for(String line : lines)
	  {
		  loadLine(line);
	      String[] array= extractData.extractData(tmp);
	      long time= extractTime.extractTime(tmp);
	      
	      datacol.timestamps.add(time);
	      
	      Object[] row= new Object[datacol.names.length];
	      for(int i= 0; i < datacol.names.length; i++)
	      {
	        if( i >= array.length || array[i] == null || array[i].equals(NA) )
	          row[i]= null;
	        else
	        {
	          switch(datacol.types[i])
	          {
	            case Types.BIGINT:
	            case Types.NUMERIC:
	              if( array[i].equals("") || array[i].equals(NA) )
	                row[i]= null;
	              else
	                row[i]= Long.valueOf(array[i]);
	              break;
	            case Types.BIT:
	            case Types.INTEGER:
	            case Types.SMALLINT:
	              if( array[i].equals("") || array[i].equals(NA) )
	                row[i]= null;
	              else
	                row[i]= Integer.valueOf(array[i]);
	              break;
	            case Types.DECIMAL:
	            case Types.DOUBLE:
	            case Types.FLOAT:
	              if( array[i].equals("") || array[i].equals(NA) )
	                row[i]= null;
	              else
	                row[i]= Double.valueOf(array[i].replace(',', '.'));
	              break;
	            case Types.VARCHAR:
	                row[i]= (String) array[i];
	                break;
	                
	            case Types.DATE:
	              String h= array[i];
	              if( h.equals("" ))
	              {
	                row[i]= null;
	              }
	              else
	              {
	                try
	                {
	                  long rect= sdf.parse(array[i]).getTime();
	                  row[i]= rect;
	                }
	                catch( ParseException e )
	                {
	                  row[i]= null;
	                }
	              }
	              break;
	              
	            default:
	              throw new IllegalArgumentException("Unsupported type " + datacol.types[i]);
	          }
	        }
	      }
	      datacol.orig.add(orig);
	      datacol.values.add(row);
	    }
	    
	    if( datacol.values.size() > 0 )
	    {
	      // compute check
	      long sum= 0;
	      long last_rect= datacol.timestamps.get(0);
	      for(int i= 1; i < datacol.timestamps.size(); i++)
	      {
	        int diff= (int) (datacol.timestamps.get(i) - last_rect);
	        last_rect= datacol.timestamps.get(i);
	        sum+= diff;
	      }
	      // System.out.println(sum / ((double) col.timestamps.size()-1));
	      datacol.sample_freq= (int) (sum / ((double) datacol.timestamps.size()-1));
	      // col.sample_freq /= 10.0;
	      // col.sample_freq*= 10;
	    }
	    else
	    {
	      datacol.sample_freq= 0;
	    }
  }
  
  public CSVLoader setDecimal(String dec)
  {
    this.dec= dec;
    return this;
  }

  /**
   * Name of the file currently loaded
   */
  private String fileName;
  
  List<String> allLines;

  private HeaderDefinition headerDefinition;

  private LoadDefinition loadDefinition;

  private TimeExtractor timeExtractor= new GuessingTimeExtractor();

  private boolean[] maskDefinition;

  private void readAllLines() throws IOException
  {
	fileName= params.fileName;
	allLines= Files.readAllLines(Paths.get(fileName));
  }
  
  private String[] parseHeaderMultiRow(int firstDataRow, MultiRowHeader def) {
    int begin= firstDataRow - def.headerOffset;
    int nHR= def.number;
    
    List<String> headerl= allLines.subList(begin, begin + nHR);
    String[][] header= new String[nHR][];
    String keeps[]= new String[nHR];
    int i= 0;
    int longest= 0;
    
    for(String line : headerl)
    {
      header[i]= line.split(sep);
      keeps[i]= "";
      if( header[i].length > longest )
        longest= header[i].length;
      i++;
    }
    
    String ret[]= new String[longest];
    
    for(i= 0; i < longest; i++)
    {
      // check if all are either empty or null
      boolean atLeastOne= false;
      for(int r= 0; r < nHR; r++)
      {
        if( header[r].length > i && header[r][i] != null
            && !header[r][i].equals("") )
        {
          atLeastOne= true;
          break;
        }
      }
      
      String name= "";
      if( atLeastOne )
      {
        for(int r= 0; r < nHR; r++)
        {
          if( i < header[r].length && header[r][i] != null )
          {
            if( header[r][i].equals("") )
            {
              if( r > 0 )
                name+= def.joinSymbol;
              name+= keeps[r];
            }
            else
            {
              if( r > 0 )
                name+= def.joinSymbol;
              name+= header[r][i];
              keeps[r]= header[r][i];
            }
          }
          else if( i >= header[r].length )
          {
            if( keeps[r] != null )
            {
              if( r > 0 )
                name+= def.joinSymbol;
              name+= keeps[r];
            }
          }
        }
        // System.out.println(i + " " + name);
      }
      else
      {
        // System.out.println(i + " empty name");
      }
      
      ret[i]= name;
    }
    
    return ret;
  }
  
  private String[] parseHeaderNew(int firstDataRow, HeaderDefinition headerdef)
  {
    if( headerdef instanceof MultiRowHeader )
    {
      return parseHeaderMultiRow(firstDataRow, (MultiRowHeader) headerdef);
    }
    else if( headerdef instanceof SingleRowHeader )
    {
      return allLines.get(firstDataRow - ((SingleRowHeader) headerdef).headerOffset).split(sep);
    }
    else if( headerdef instanceof NoHeader )
    {
      return ((NoHeader) headerdef).colnames;
    }

    return null;
  }

  public CSVLoader Load() throws IOException
  {
    return this.Load(loadDefinition, headerDefinition, maskDefinition, timeExtractor);
  }

  /**
   * Load data from a file
   * 
   * @param load
   *          what to load. Currently only RegionDefintion is supported. If null the complete file
   *          is loaded
   * @param headerdef
   *          header of the data to be loaded.
   * @param mask
   *          mask for the data. If null all columns will be loaded
   * @param timeExtractor
   *          function for extracting the time information from a row
   * @return CSVLoader
   * @throws IOException
   */
  public CSVLoader Load(LoadDefinition load, HeaderDefinition headerdef, boolean[] mask, TimeExtractor timeExtractor) throws IOException
  {
    if (params == null)
      throw new IllegalStateException("Parameters for loading not set");

    // System.out.println("Loading " + (load != null ? load.name : ""));

    datacol= new Column();
    datacol.name= (load != null ? load.name : "");

    // We assume people won't load the same region from the same file twice
    if( fileName != params.fileName )
      readAllLines();

    int firstDataRow= 0;

    if( load instanceof RegionDefinition )
    {
      RegionDefinition region= (RegionDefinition) load;
      if( region.endLine == 0 )
        lines= allLines.subList(region.startLine, allLines.size());
      else
        lines= allLines.subList(region.startLine, region.endLine);
      firstDataRow= region.startLine;
    }
    // This is somewhat of a workaround ...
    else if( load == null && headerdef != null )
    {
      if( headerdef instanceof SingleRowHeader )
      {
        firstDataRow= ((SingleRowHeader) headerdef).headerOffset;
        lines= allLines.subList(firstDataRow, allLines.size());
      }
      else if( headerdef instanceof MultiRowHeader )
      {
        firstDataRow= ((MultiRowHeader) headerdef).headerOffset;
        lines= allLines.subList(firstDataRow, allLines.size());
      }
      else
        lines= allLines;
    }
    else
      lines= allLines;

    String[] header= parseHeaderNew(firstDataRow, headerdef);

    if( mask != null )
    {
      // System.out.println(Arrays.toString(header) + " " + Arrays.toString(mask) + " " + (header.length - mask.length));
      datacol.names= new String[header.length - mask.length];
      for(int i= 0, j= 0; i < header.length; i++)
      {
        if( i < mask.length && mask[i] )
        {
        }
        else
        {
          datacol.names[j++]= header[i];
        }
      }
      
      final boolean[] lmask= mask;
      // final int rsize= datacol.names.length;
      int c= 0;
      for(int i= 0; i<lmask.length; i++)
        if( lmask[i] )
          c++;
      final int lmaskc= c;
      extractData= new DataExtractor() {

          @Override
          public String[] extractData(String[] lineparts) {
              String[] ret= new String[lineparts.length - lmaskc];
              // System.out.println(Arrays.toString(lineparts));
              for(int i= 0, j= 0; i < lineparts.length; i++)
              {
                if( i < lmask.length && lmask[i] )
                {
                }
                else
                {
                  String a= lineparts[i];
                  ret[j++]= a;
                }
              }
              return ret;
          }
      };
    }
    else
    {
      datacol.names= header;
      extractData= (data) -> data;
    }

    extractTime= timeExtractor;

    // System.out.println(Arrays.toString(header));
    if( determineTypes() )
    {
      // check if determining types found more types than we have names
      if( datacol.types.length > datacol.names.length )
      {
        String[] tmp= new String[datacol.types.length];
        System.arraycopy(datacol.names, 0, tmp, 0, datacol.names.length);
        datacol.names= tmp;
      }
      // check if we could not find types for some columns
      else if( datacol.types.length < datacol.names.length )
      {
        // these columns are possibly empty. Hence we remove them
        String[] tmp= new String[datacol.types.length];
        String[] tmp2= new String[datacol.names.length - datacol.types.length];
        System.arraycopy(datacol.names, 0, tmp, 0, datacol.types.length);
        System.arraycopy(datacol.names, datacol.types.length, tmp2, 0, datacol.names.length - datacol.types.length);
        datacol.names= tmp;
        System.err.println("Empty columns detected. Remvoving " + Arrays.toString(tmp2));
      }

      timeExtractor.init(header, datacol.names, datacol.types, mask);

      loadData();

      data.addColumn(datacol);
    }
    
    return this;
  }

  /**
   * Try to determine the structure of a CSV file. Currently, this method will
   * just check whether the first line contains more semicolons (;) then commas
   * (,). If more commas are present the decimal symbol is set to (.) otherwise
   * it is set to (,).
   * 
   * @param fileName
   * @return params or null if an IOException was thrown
   */
  public static Parameter determineParams(String fileName)
  {
    Parameter ret= new Parameter();
    try
    {
      ret.fileName= fileName;
      BufferedReader reader= Files.newBufferedReader(Paths.get(fileName));
      String firstLine= reader.readLine();
      reader.close();
      final int l= firstLine.length();
      int commas= 0;
      int semic= 0;
      int tabs= 0;
      int points= 0;
      for(int i= 0; i < l; i++)
      {
        char c= firstLine.charAt(i);
        if( c == ',' )
          commas++;
        else if( c == ';' )
          semic++;
        else if( c == '\t' )
          tabs++;
        else if( c == '.' )
          points++;
      }

      if( semic == commas && commas == 0 )
      {
        if( tabs > 0 )
        {
          ret.sep= "\t";
        }
        else
          ret.sep= " ";
        if( points > commas )
          ret.dec= ".";
      }
      else
      {
        if( semic > commas )
        {
          ret.sep= ";";
          ret.dec= ",";
        }
        else
        {
          ret.sep= ",";
          ret.dec= ".";
        }
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      ret= null;
    }

    return ret;
  }
  
  public CSVLoader setParams(Parameter p)
  {
    params= p;
    sep= p.sep;
    dec= p.dec;
    return this;
  }

  /**
   * Set the parameters for the load operation based on the file i.e. determine separators
   * 
   * @param fileName
   */
  public CSVLoader determineFileParams(String fileName)
  {
    Parameter h= determineParams(fileName);

    return this.setParams(h);
  }

  /**
   * Pass on the lines from a previously loaded file. The method will treat the
   * lines as if they have been loaded from the file set by the parameters
   * 
   * @param lines
   *          loaded lines
   * @return
   */
  public CSVLoader setLines(List<String> lines)
  {
    fileName= params.fileName;
    allLines= lines;
    return this;
  }
  
  public CSVLoader setNA(String na)
  {
    this.NA= na;
    return this;
  }

  public CSVLoader setHeader(HeaderDefinition headerDefinition)
  {
    this.headerDefinition= headerDefinition;
    return this;
  }

  public CSVLoader setRegion(LoadDefinition loadDefinition)
  {
    this.loadDefinition= loadDefinition;
    return this;
  }

  public CSVLoader setTimeExtractor(TimeExtractor timeExtractor)
  {
    this.timeExtractor= timeExtractor;
    return this;
  }

  /**
   * Set which columns to include from loading
   * 
   * @param mask boolean array. entry true means this column will not be loaded
   */
  public CSVLoader setMask(boolean[] mask)
  {
    this.maskDefinition= mask;
    return this;
  }
}
