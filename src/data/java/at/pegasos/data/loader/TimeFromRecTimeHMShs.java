package at.pegasos.data.loader;

/**
 * This time extractor converts times from a given column in hour:minutes:seconds:hundrets of a
 * second
 */
public class TimeFromRecTimeHMShs implements TimeExtractor {

  private final String colname;

  private int rec_time_idx;

  /**
   * Construct the extractor. Use the specified column as time column
   * 
   * @param colname
   *          name/label of the column containing the time information
   */
  public TimeFromRecTimeHMShs(String colname)
  {
    this.colname= colname;
  }

  @Override
  public long extractTime(String[] lineparts)
  {
    String time= lineparts[rec_time_idx];
    String parts[]= time.split(":");

    long ret= 0;
    // 0:00:00:00
    // Hundreds of a second
    ret+= Integer.parseInt(parts[3]) * 10L;
    // seconds
    ret+= Integer.parseInt(parts[2]) * 1000L;
    // minutes
    ret+= (long) Integer.parseInt(parts[1]) * 1000 * 60;
    // hours
    ret+= (long) Integer.parseInt(parts[0]) * 1000 * 60 * 60;

    return ret;
  }

  @Override
  public void init(String[] unmaskedcolnames, String[] colnames, int[] types, boolean[] mask)
  {
    rec_time_idx= -1;
    for(int i= 0; i < unmaskedcolnames.length; i++)
    {
      // System.out.println(unmaskedcolnames[i]);
      if( unmaskedcolnames[i].equals(colname) )
      {
        rec_time_idx= i;
        break;
      }
    }
  }
}
