package at.pegasos.data.loader;

public class SingleRowHeader extends HeaderDefinition {
  public SingleRowHeader(int offset)
  {
    this.headerOffset= offset;
  }

  public SingleRowHeader()
  {
  }

  /**
   * headerOffset cannot be negative. The offset will be subtracted from startLine of the data.
   */
  int headerOffset;
}