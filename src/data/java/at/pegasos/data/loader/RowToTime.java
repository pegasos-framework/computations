package at.pegasos.data.loader;

/**
 * This time extractor simply converts a row to a time based on the row number.
 * It assumes rows are converted in sequential order. Therefore, this extractor
 * cannot be reused!
 */
public class RowToTime implements TimeExtractor {

  private final long freqDist;

  long time= 0;

  public RowToTime(long freqDist)
  {
    this.freqDist= freqDist;
  }

  @Override
  public long extractTime(String[] lineparts)
  {
    long ret= time;
    time+= freqDist;
    return ret;
  }

  @Override
  public void init(String[] unmaskedcolnames, String[] colnames, int[] types, boolean[] mask)
  {
    // Nothing to do here
  }
}
