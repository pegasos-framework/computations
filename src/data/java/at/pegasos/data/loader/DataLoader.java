package at.pegasos.data.loader;

import at.pegasos.data.Data;
import at.pegasos.data.TimeUnit;

public abstract class DataLoader {
  
  protected Data data;
  
  public DataLoader()
  {
    this.data= new Data();
  }
  
  public DataLoader(Data data)
  {
    this.data= data;
  }
  
  public DataLoader(DataLoader other)
  {
    this.data= other.data;
  }
  
  public Data getData()
  {
    return data;
  }
  
  /**
   * Set the timeUnit for the data frame
   * @param timeUnit
   */
  public void setTimeUnit(TimeUnit timeUnit)
  {
    this.data.setTimeUnit(timeUnit);
  }
}
