package at.pegasos.data.loader;

/**
 * Definition of what to load in the file
 */
public class LoadDefinition
{
  /**
   * Name of the data to be loaded
   */
  String name= "data";
  
  public LoadDefinition(String name)
  {
  	this.name= name;
  }
  
  public LoadDefinition()
  {
  }
}