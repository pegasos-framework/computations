package at.pegasos.data.loader;

/**
 * This time extractor tries to convert a time which is either in hms format or as seconds
 */
public class TimeFromKinovea implements TimeExtractor {
  
  private final String colname;
  
  private int rec_time_idx;
  
  public TimeFromKinovea(String colname)
  {
    this.colname= colname;
  }
  
  @Override
  public long extractTime(String[] lineparts)
  {
    String time= lineparts[rec_time_idx];
    
    if( time.contains(":") )
    {
      String parts[]= time.split(":");
      
      long ret= 0;
      // 0:00:00:00
      // Hundreds of a second
      ret+= Integer.parseInt(parts[3])* 10L;
      // seconds
      ret+= Integer.parseInt(parts[2])* 1000L;
      // minutes
      ret+= (long) Integer.parseInt(parts[1]) *1000*60;
      // hours
      ret+= (long) Integer.parseInt(parts[0]) *1000*60*60;

      return ret;
    }
    else
    {
      long ret;
      
      ret= (long) (Double.parseDouble(time) * 1000);
      
      return ret;
    }
  }

  @Override
  public void init(String[] unmaskedcolnames, String[] colnames, int[] types, boolean[] mask)
  {
    rec_time_idx= -1;
    for(int i= 0; i < unmaskedcolnames.length; i++)
    {
      // System.out.println(unmaskedcolnames[i]);
      if( unmaskedcolnames[i].equals(colname) )
      {
        rec_time_idx= i;
        break;
      }
    }
  }
}
