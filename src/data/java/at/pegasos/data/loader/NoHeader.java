package at.pegasos.data.loader;

/**
 * Specification of a 'missing' header. This can be used to inject a header.
 */
public class NoHeader extends HeaderDefinition {

  public String[] colnames;

  /**
   * Create a header using the names
   * 
   * @param names
   *          names of the columns
   */
  public NoHeader(String... names)
  {
    this.colnames= names;
  }
}