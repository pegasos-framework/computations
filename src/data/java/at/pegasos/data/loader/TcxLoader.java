package at.pegasos.data.loader;

import java.io.File;
import java.io.IOException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import at.pegasos.data.Data;
import at.pegasos.data.loader.DataLoader;
import at.pegasos.data.Data.Column;

public class TcxLoader extends DataLoader {
  
  private final static SimpleDateFormat sdf;
  static {
    sdf= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
  }

  private File file;
  
  private TcxLoader()
  {
    // cols= new HashMap<String, TmpColumn>();
  }
  
  public static TcxLoader create(String filename)
  {
    TcxLoader ret= new TcxLoader();
    ret.file= new File(filename);
    return ret;
  }
  
  public TcxLoader loadFile()
  {
    try
    {
      openFile();
      reset();
      readFile();
      
      col.updateFrequency();
      data= new Data();
      data.addColumn(col);
    }
    catch( ParserConfigurationException | SAXException | IOException | ParseException | ClassNotFoundException | InterruptedException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
    
    return this;
  }
  
  private Document doc;
  
  long start;
  long last_time;

  private Column col;
  
  private void openFile() throws ParserConfigurationException, SAXException, IOException, ParseException
  {
    DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder= dbFactory.newDocumentBuilder();
    doc= dBuilder.parse(file);
    doc.getDocumentElement().normalize();
    
    NodeList nList= doc.getElementsByTagName("Id");
    
    assert (nList.getLength() == 1);
    
    String x= nList.item(0).getChildNodes().item(0).getNodeValue();
    
    // System.out.println(x);
    // "2016-08-15T14:43:59.000Z"
    Date d= sdf.parse(x);
    
    start= d.getTime();
  }
  
  public void readFile() throws ClassNotFoundException, IOException, InterruptedException, ParseException
  {
    NodeList points= doc.getElementsByTagName("Trackpoint");
    col= new Column(points.getLength(), "",
        new int[] {Types.DOUBLE, Types.DOUBLE, Types.DOUBLE, Types.INTEGER, Types.DOUBLE, Types.INTEGER, Types.INTEGER, Types.DOUBLE},
        new String[] {"gps_lat", "gps_lon", "altitude_m", "hr", "distance_m", "speed_mm_s", "cad", "power"});
    for(int i= 0; i < points.getLength(); i++)
    {
      Element point= (Element) points.item(i);
      
      parseEntry(point);
    }
  }
  
  double last_dist_m= 0;
  long last_dist_t= -1;
  double speed;
  
  private void reset()
  {
    last_time= 0;
    last_dist_m= 0;
    last_dist_t= -1;
    speed= 0;
  }
  
  private static final boolean[] orig= new boolean[] {true, true, true, true, true, true, true, true};

  private void parseEntry(Element point) throws ParseException
  {
 // Time
    String x= point.getElementsByTagName("Time").item(0).getChildNodes().item(0).getNodeValue();
    Date d= sdf.parse(x);
    // System.out.println(d + " " + d.getTime());
    col.timestamps.add(d.getTime() - start);
    Object[] data= new Object[8];
    
    /*
     <Trackpoint xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
     <Time>2019-07-31T17:01:00.000Z</Time>
     <Position><LatitudeDegrees>48.18978167</LatitudeDegrees><LongitudeDegrees>16.35179333</LongitudeDegrees></Position>
     <AltitudeMeters>191.417</AltitudeMeters>
     <DistanceMeters>4.400000095367432</DistanceMeters>
     <HeartRateBpm><Value>151</Value></HeartRateBpm>
     <Cadence>77</Cadence><SensorState>Present</SensorState>
     <Extensions><TPX xmlns="http://www.garmin.com/xmlschemas/ActivityExtension/v2"><Watts>0</Watts></TPX></Extensions></Trackpoint>
         */
    
    if( point.getElementsByTagName("Position").getLength() > 0 )
    {
      x= point.getElementsByTagName("LatitudeDegrees").item(0).getChildNodes().item(0).getNodeValue();
      data[0]= Double.parseDouble(x);
      x= point.getElementsByTagName("LongitudeDegrees").item(0).getChildNodes().item(0).getNodeValue();
      data[1]= Double.parseDouble(x);
    }
    
    if( point.getElementsByTagName("AltitudeMeters").getLength() > 0 )
    {
      x= point.getElementsByTagName("AltitudeMeters").item(0).getChildNodes().item(0).getNodeValue();
      data[2]= Double.parseDouble(x);
    }
    
    if( point.getElementsByTagName("HeartRateBpm").getLength() > 0 )
    {
      // <HeartRateBpm><Value>126</Value></HeartRateBpm>
      x= point.getElementsByTagName("Value").item(0).getChildNodes().item(0).getNodeValue();
      data[3]= Integer.parseInt(x);
    }
    
    if( point.getElementsByTagName("DistanceMeters").getLength() > 0 )
    {
      // <DistanceMeters>9.137747022840712</DistanceMeters>
      x= point.getElementsByTagName("DistanceMeters").item(0).getChildNodes().item(0).getNodeValue();
      double dist= Double.parseDouble(x);
      data[4]= dist;
      if( last_dist_t != -1 )
        speed= (dist - last_dist_m) * 1000 / ((double) (d.getTime() - last_dist_t)) * 1000;
      else
        speed= (dist - last_dist_m) * 1000;
      data[5]= (int) speed;
      
      last_dist_m= dist;
      last_dist_t= d.getTime();
    }
    
    if( point.getElementsByTagName("Cadence").getLength() > 0 )
    {
      // <DistanceMeters>9.137747022840712</DistanceMeters>
      x= point.getElementsByTagName("Cadence").item(0).getChildNodes().item(0).getNodeValue();
      data[6]= Integer.parseInt(x);
    }
    
    if( point.getElementsByTagName("Watts").getLength() > 0 )
    {
      // <DistanceMeters>9.137747022840712</DistanceMeters>
      x= point.getElementsByTagName("Watts").item(0).getChildNodes().item(0).getNodeValue();
      data[7]= Double.parseDouble(x);
    }
    
    col.values.add(data);
    col.orig.add(orig);
  }

  public String getSport()
  {
    return doc.getElementsByTagName("Activity").item(0).getAttributes().getNamedItem("Sport").getNodeValue();
  }
}
