package at.pegasos.data.loader;

public interface TimeExtractor {
  /**
   * Initialise the extractor. This method is called before the first extract.
   * The parameters should offer everything needed in order to initialise the
   * internal structures
   * 
   * @param unmaskedcolnames
   *          names of all columns in the file
   * @param colnames
   *          names of all columns to be used during the load
   * @param types
   *          types of all used columns
   * @param mask
   *          mask for loading
   */
  public void init(String[] unmaskedcolnames, String[] colnames, int[] types, boolean[] mask);

  /**
   * Extract the time from a line. The parts of the line are unmasked (i.e.
   * all columns are present)
   * 
   * @param lineparts
   *          all parts of the line
   * @return time of the current line
   */
  public long extractTime(String[] lineparts);
}