package at.pegasos.data.loader;

/**
 * Specification for a header with multiple rows
 */
public class MultiRowHeader extends HeaderDefinition {
  /**
   * Construct a multi row header. This header is offset by the data using the length parameter
   * @param length length and offset of the header
   */
  public MultiRowHeader(int length)
  {
    headerOffset= number= length;
  }
  
  /**
   * Construct a multi row header. This header is offset by the data using the length parameter
   * @param length length and offset of the header
   * @param joinSymbol symbol used for joining the rows
   */
  public MultiRowHeader(int length, String joinSymbol)
  {
    headerOffset= number= length;
    this.joinSymbol= joinSymbol;
  }

  /**
   * headerOffset cannot be negative. The offset will be subtracted from
   * startLine of the data.
   */
  int headerOffset;
  
  /**
   * Number of rows of the header
   */
  int number;
  
  String joinSymbol= "";
}