package at.pegasos.data.loader;

/**
 * This time extractor simply converts a number in seconds to milliseconds
 */
public class SecToRecTime implements TimeExtractor {
  
  private final String colname;
  
  private int rec_time_idx;
  
  public SecToRecTime(String colname)
  {
    this.colname= colname;
  }
  
  @Override
  public long extractTime(String[] lineparts)
  {
    String time= lineparts[rec_time_idx];
    
    long ret;
    
    ret= (long) (Double.parseDouble(time) * 1000);
    
    return ret;
  }

  @Override
  public void init(String[] unmaskedcolnames, String[] colnames, int[] types, boolean[] mask)
  {
    rec_time_idx= -1;
    for(int i= 0; i < unmaskedcolnames.length; i++)
    {
      // System.out.println(unmaskedcolnames[i]);
      if( unmaskedcolnames[i].equals(colname) )
      {
        rec_time_idx= i;
        break;
      }
    }
  }
}
