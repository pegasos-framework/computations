package at.pegasos.data;

public class ArrayHelper {
  /**
   * Copy all of src to dest except element at index
   * 
   * @param src
   *          source
   * @param index
   *          index of the element to be 'removed'
   * @param dest
   *          destination of operation
   */
  public static void removeElement(Object[] src, int index, Object[] dest)
  {
    System.arraycopy(src, 0, dest, 0, index);
    System.arraycopy(src, index + 1, dest, index, src.length - index - 1);
    
  }

  /**
   * Copy all of src to dest except element at index
   * 
   * @param src
   *          source
   * @param index
   *          index of the element to be 'removed'
   * @param dest
   *          destination of operation
   */
  public static void removeElement(int[] src, int index, int[] dest)
  {
    System.arraycopy(src, 0, dest, 0, index);
    System.arraycopy(src, index + 1, dest, index, src.length - index - 1);
  }

  /**
   * Copy all of src to dest except element at index
   * 
   * @param src
   *          source
   * @param index
   *          index of the element to be 'removed'
   * @param dest
   *          destination of operation
   */
  public static void removeElement(boolean[] src, int index, boolean[] dest)
  {
    System.arraycopy(src, 0, dest, 0, index);
    System.arraycopy(src, index + 1, dest, index, src.length - index - 1);
  }
}
