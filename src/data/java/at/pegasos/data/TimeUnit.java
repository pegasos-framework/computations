package at.pegasos.data;

public enum TimeUnit {
  MilliSecond,
  Second,
  MicroSecond
}
