package at.pegasos.data;

/**
 * An object/value with an assigned time stamp. It can be used in two ways: as an object with an
 * assigned time or as an object with an assigned times pan
 * 
 * @param <T>
 */
public class TimedObject<T> {
  private final long timestamp;
  private final long end;
  private final T value;
  
  private final boolean span;
  
  /**
   * Create an object with an assigned time
   * @param ts time stamp
   * @param v value
   */
  public TimedObject(long ts, T v)
  {
    this.timestamp= ts;
    this.end= 0;
    this.value= v;
    span= false;
  }
  
  /**
   * Create an object with an assigned time span
   * @param start start of the time span
   * @param end end of the time span
   * @param v value
   */
  public TimedObject(long start, long end, T v)
  {
    this.timestamp= start;
    this.end= end;
    this.value= v;
    span= true;
  }
  
  public T getValue()
  {
    return value;
  }
  
  public boolean isSpan()
  {
    return span;
  }
  
  public long getStart()
  {
    return timestamp;
  }
  
  public long getTimestamp()
  {
    return timestamp;
  }
  
  public long getEnd()
  {
    return end;
  }
}
