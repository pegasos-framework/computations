package at.pegasos.generator;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public abstract class CodeGenerator {
  public static final String INDENT = "  ";
  protected int indent_size;
  
  protected PrintStream out;

  /**
   * Absolute path of the output file. Only valid if setOutputToPrintStream was used
   */
  private String filename;
  
  protected String package_name;
  protected String classname;
  protected Set<String> imports;
  protected String extend_stmt;
  protected String implement_stmt;
  
  protected String type;

  /**
   * Access modifier of the class
   */
  protected String classAccess= "public";
  
  public CodeGenerator()
  {
	init();
  }
  
  public CodeGenerator(String pkg_name, String classname)
  {
    this.package_name= pkg_name;
    this.classname= classname;
    init();
  }
  
  public CodeGenerator(String pkg_name, String classname, String extend, String implement)
  {
    this.package_name= pkg_name;
    this.classname= classname;
    this.extend_stmt= extend;
    this.implement_stmt= implement;
    init();
  }
  
  public CodeGenerator(String pkg_name, String classname, String extend, String implement, String type)
  {
    this.package_name= pkg_name;
    this.classname= classname;
    this.extend_stmt= extend;
    this.implement_stmt= implement;
    this.type= type;
    init();
  }
  
  private void init()
  {
    this.imports= new HashSet<String>();
    if( this.type == null )
    {
      this.type= "class";
    }
  }
  
  public void setExtends(String extend)
  {
    this.extend_stmt= extend;
  }
  
  protected void addImport(String imp)
  {
    this.imports.add(imp);
  }
  
  public void setOputputToPrintStream(Path basedir) throws IOException
  {
    String[] pkgs= package_name.split("[.]");
    Path p= basedir;
    for(String pkg : pkgs)
    {
      p= p.resolve(pkg);
    }
    
    if( !Files.exists(p) )
      Files.createDirectories(p);
    
    p= p.resolve(classname + ".java");
    
    filename= p.toAbsolutePath().toString();
    
    this.out= new PrintStream(Files.newOutputStream(p));
  }
  
  public void setOutput(PrintStream output)
  {
    this.out = output;
  }
  
  public PrintStream getOutputStream()
  {
    return this.out;
  }
  
  public void closeOutput()
  {
  	this.out.close();
  }
  
  protected void output(int indent, String line)
  {
    for(int i = 0; i < indent; i++)
      out.print(INDENT);
    out.println(line);
  }
  
  protected void empty(int indent)
  {
    output(indent, "");
  }
  
  protected void output(String line)
  {
    output(indent_size, line);
  }
  
  protected void empty()
  {
    empty(indent_size);
  }
  
  protected void indent()
  {
    indent_size++;
  }
  
  protected void indent_less()
  {
    indent_size--;
  }
  
  public int getIndent()
  {
    return indent_size;
  }

  public void setIndent(int indent)
  {
    this.indent_size= indent;
  }
  
  protected void header()
  {
	if( package_name == null )
	  throw new IllegalStateException("Name of package cannot be null");
	if( classname == null )
	  throw new IllegalStateException("Name of class cannot be null");
	
	output("package " + package_name + ";");
    empty();
    empty();
    
    for(String imp : imports)
      output("import " + imp + ";");
    if( imports.size() > 0 )
      empty();
  }
  
  protected void classBegin(String... class_annotations)
  {
    if( classname == null )
      throw new IllegalStateException("Name of class cannot be null");

    if( class_annotations != null )
    {
      for(String anno : class_annotations)
        output(anno);
    }

    String line= classAccess + " " + type + " " + classname;
    if( extend_stmt != null )
      line+= " extends " + extend_stmt;
    if( implement_stmt != null )
      line+= " implements " + implement_stmt;
    output(line + " {");
  }

  /**
   * Generate the footer for a file
   */
  protected void footer()
  {
    // if( indent_size > 0 )
    //   indent_size= 0;
    
    output("}");
    output("// End of generated class " + classname);
    empty();
  }
  
  /**
   * Generate the footer for a file without a class
   */
  protected void footerNoClass()
  {
    if( indent_size > 0 )
      indent_size= 0;
    
    output("// End of generated class");
    empty();
  }
  
  /**
   * Output the header of a constructor for this class
   * @param parameters parameters for this constructor
   */
  protected void constructor_begin(String[] parameters)
  {
    String params;
    if( parameters == null )
      params= "";
    else
      params= String.join(", ", parameters);
    
    output("public " + classname + "(" + params + ")");
    output("{");
    indent();
  }
  
  protected void method_begin(String access_mod, String returntype, String methodname_params)
  {
    method_begin(access_mod, returntype, methodname_params, false);
  }
  
  protected void method_begin(String access_mod, String returntype, String methodname_params, boolean override)
  {
    if( override ) output("@Override");
    output(access_mod + " " + returntype + " " + methodname_params);
    output("{");
    indent();
  }
  
  /**
   * Closes the opened method. (This also includes decreasing the indent)
   */
  protected void method_end()
  {
    indent_less();
    output("}");
    empty();
  }
  
  /**
   * Output a member declaration
   * @param access
   * @param type
   * @param name
   */
  protected void member(String access, String type, String name)
  {
    output(access + " " + type + " " + name + ";");
  }
  
  /**
   * Output a member declaration
   * @param access
   * @param type
   * @param name
   */
  protected void member(String access, String type, String name, String value)
  {
    output(access + " " + type + " " + name + "= " + value + ";");
  }
  
  /**
   * Output an assignment
   * @param variable
   * @param value
   */
  protected void assign(String variable, String value)
  {
    output(variable + "= " + value + ";");
  }
  
  /**
   * Output a call/invocation of a function/method
   * 
   * @param name
   *            name of the method or function to be invoked
   * @param parameters
   *            parameters for the call
   */
  protected void call(String name, String... parameters)
  {
    output(name + "(" + String.join(",", parameters) + ");");
  }

  /**
   * Output a call/invocation of a function/method
   * 
   * @param variable
   */
  protected void call(String name)
  {
    call(name, "");
  }
  
  protected void whileLoopBegin(String condition)
  {
	output("while( " + condition + ")");
	output("{");
	indent();
  }
	
  protected void whileLoopEnd()
  {
    indent_less();
	output("}");
  }
  
  protected void ifStatementBegin(String condition)
  {
    output("if( " + condition + " )");
    output("{");
    indent();
  }
  
  protected void ifStatementEnd()
  {
    indent_less();
    output("}");
  }
  
  protected void ifStatementElse()
  {
    output("else");
    output("{");
    indent();
  }

  abstract public void generate() throws GeneratorException;

  public Collection<? extends String> getGeneratedFilenames()
  {
    assert(filename != null);
    return Arrays.asList(filename);
  }
}
