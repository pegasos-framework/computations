package at.pegasos.generator.formula;

import java.util.Map;
import java.util.Stack;

import at.pegasos.formula.parser.ClassMethod;
import at.pegasos.formula.parser.JavaFunction;
import at.pegasos.formula.parser.Method;
import grammar.CodeContext;
import node.BinaryNode;
import node.DataSubsetSpec;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.NegateNode;
import node.UnaryNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierPropertyNode;
import node.ValueIdentifierNode;
import node.arithmetic.BinaryArithmeticExpression;
import node.arithmetic.ExponentNode;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.EulerConst;
import node.value.StringNode;
import node.value.ValueNode;

public class ExpressionToCode implements ExpressionVisitor {
	
	private StringBuffer code;
	
  CodeContext codeContext= CodeContext.Expression;

	private Map<String, String> replacements;
	
  private final Stack<CodeContext> restoreContext;
	
	private boolean inExpWithE= false;
	
  public ExpressionToCode(ExpressionNode expr)
  {
    code= new StringBuffer(100);
    this.restoreContext= new Stack<CodeContext>();
    expr.performVisit(this, VisitationOrder.PseudoInOrder);
  }

  public ExpressionToCode(ExpressionNode expr, CodeContext context)
  {
    this.codeContext= context;
    code= new StringBuffer(100);
    this.restoreContext= new Stack<CodeContext>();
    expr.performVisit(this, VisitationOrder.PseudoInOrder);
  }
	/*
	 if( expr instanceof UnaryNode ) {
			System.out.println("|- unary " + expr);
			if( expr instanceof BracketNode ) {
				BracketNode n= (BracketNode) expr;
				print(n.getExpr(), indent+1);
			}
	 */

  public ExpressionToCode(ExpressionNode expr, Map<String, String> replacements)
  {
    code= new StringBuffer(100);
    this.restoreContext= new Stack<CodeContext>();
    this.replacements= replacements;
    expr.performVisit(this, VisitationOrder.PseudoInOrder);
  }

	@Override
	public void visit(UserValueIdentifierNode userValueIdentifierNode) {
		if( codeContext == CodeContext.InvocationArgument ) {
			if( userValueIdentifierNode.getTime() == null ) {
				code.append("\"" + userValueIdentifierNode.getIdentifier() + "\"");
			} else {
				code.append("\"" + userValueIdentifierNode.getIdentifier() + "\", \"" + userValueIdentifierNode.getTime().getTime() + "\", " + userValueIdentifierNode.getTime().getOffset());
			}
		} else {
			if( userValueIdentifierNode.getTime() == null ) {
				code.append("computer.getValue(\"" + userValueIdentifierNode.getIdentifier() + "\").getDoubleValue()");
			} else {
				code.append("value" + userValueIdentifierNode.getTime().getTime() + "Or(\"" + userValueIdentifierNode.getIdentifier() + "\", " + userValueIdentifierNode.getTime().getOffset() + ", 0)");
			}
		}
	}

  @Override
  public void visit(DataValueIdentifierNode dataValueIdentifierNode)
  {
    if( codeContext == CodeContext.InvocationArgument )
    {
      if( dataValueIdentifierNode.getSubset() != null )
      {
        code.append(",");
      }

      code.append("\"" + dataValueIdentifierNode.getIdentifier() + "\"");
    }
    else if( replacements != null )
    {
      code.append(replacements.get(dataValueIdentifierNode.getIdentifier()));
    }
  }

	@Override
	public void visit(ValueNode valueNode) {
		if( inExpWithE && valueNode instanceof EulerConst )
			return;
		
		if( valueNode instanceof StringNode ) {
			code.append("\"" + valueNode.getValueAsString() + "\"");
		} else {
			code.append(valueNode.getValueAsString());
		}
	}

	@Override
	public void visit(ValueIdentifierNode valueIdentifierNode) {
		if( codeContext == CodeContext.InvocationArgument ) {
			code.append("\"" + valueIdentifierNode.getIdentifier() + "\"");
		} else {
			code.append("computer.getValue(\"" + valueIdentifierNode.getIdentifier() + "\").getDoubleValue()");
		}
		// code.append("computer.getValue(\"" + valueIdentifierNode.getIdentifier() + "\").getDoubleValue()");
	}
	
    @Override
    public void visit(ValueIdentifierPropertyNode valueIdentifierPropertyNode)
    {
      code.append("computer.getValue(\"" + valueIdentifierPropertyNode.getIdentifier() + "\").getSubValue(\"" +
          valueIdentifierPropertyNode.getProperty() + "\").getDoubleValue()");
    }

  @Override
  public void preVisit(BinaryNode binaryNode)
  {
    if( binaryNode instanceof ExponentNode )
    {
      if( binaryNode.getLeft() instanceof EulerConst )
      {
        code.append("Math.exp(");
        inExpWithE= true;
      }
      else
      {
        code.append("Math.pow(");
      }
    }
    else if( binaryNode instanceof DataSubsetSpec )
    {
      code.append("new at.pegasos.computer.Subset(");
      restoreContext.push(codeContext);
      codeContext= CodeContext.InvocationArgument;
    }
  }

  @Override
  public void visit(BinaryNode binaryNode)
  {
    if( binaryNode instanceof BinaryArithmeticExpression )
    {
      if( binaryNode instanceof ExponentNode )
      {
        if( !inExpWithE )
        {
          code.append(", ");
        }
      }
      else
      {
        code.append(" " + ((BinaryArithmeticExpression) binaryNode).getOperator() + " ");
      }
    }
    else if( binaryNode instanceof DataSubsetSpec )
    {
      code.append(", ");
    }
  }

  @Override
  public void postVisit(BinaryNode binaryNode)
  {
    if( inExpWithE )
      inExpWithE= false;

    if( binaryNode instanceof ExponentNode )
    {
      code.append(")");
    }
    else if( binaryNode instanceof DataSubsetSpec )
    {
      code.append(")");
      codeContext= restoreContext.pop();
    }
  }

	@Override
	public void visit(ExprListNode exprListNode) {
    // TODO is there really nothing to do here?
	}

	@Override
	public void visit(InvocationNode invocationNode) {
    // TODO is there really nothing to do here?
	}

	@Override
	public void visit(UnaryNode unaryNode) {
    // TODO what about all the other unary nodes?
		if( unaryNode instanceof NegateNode ) {
			code.append("-");
		}
	}

  @Override
  public void preVisit(InvocationNode invocationNode)
  {
    // TODO: is it save to assume that only java functions and classMethods can come here?
    Method m= invocationNode.getMethod();
    if( m instanceof ClassMethod )
    {
      code.append(((ClassMethod) m).getMethodName());
    }
    else if( m instanceof JavaFunction )
    {
      code.append(((JavaFunction) m).getFunctionName());
    }
    else
      throw new IllegalArgumentException("Cannot generate direct code from " + m);
    code.append("(");
    restoreContext.push(codeContext);
    codeContext= CodeContext.InvocationArgument;
  }

  @Override
  public void postVisit(InvocationNode invocationNode)
  {
    code.append(")");
    codeContext= restoreContext.pop();
  }

	public String getCode() {
		return code.toString();
	}

  @Override
  public void midPostVisit(ExprListNode exprListNode)
  {
    if( codeContext == CodeContext.InvocationArgument )
      code.append(", ");
    else
      throw new IllegalStateException();
  }

	@Override
	public void preVisit(BracketNode bracketNode) {
		code.append("(");
	}

	@Override
	public void postVisit(BracketNode bracketNode) {
		code.append(")");
	}
}
