package at.pegasos.generator.formula;

import at.pegasos.formula.generator.ComputationGenerator;
import at.pegasos.formula.parser.ClassMethodUser;
import at.pegasos.formula.parser.Method;
import at.pegasos.generator.GeneratorException;
import node.ExpressionNode;
import node.TimeEnum;
import node.TimespecNode;
import node.UserAssignmentNode;
import node.UserValueIdentifierNode;
import node.expression.InvocationNode;

public class UserValue extends ComputationGenerator {

  /**
   * The value the computation is generated for
   */
  private UserValueIdentifierNode userval;
  private ExpressionNode val;
  private String code;

  public UserValue(UserAssignmentNode assignment, String packageName)
  {
    super(packageName,
        "UserValue" + assignment.getIdent().getIdentifier()
            + (assignment.getIdent().getTime() != null ? assignment.getIdent().getTime().toNameString() : ""),
        assignment.getExpr());

    setExtends(classNameFromTime(assignment.getIdent().getTime()));

    userval= assignment.getIdent();
  }

  @Override
  public void generate() throws GeneratorException
  {
    boolean useSuperCompute= false;

    preProcessStatement();

    if( expr instanceof InvocationNode )
    {
      InvocationNode invoc= (InvocationNode) expr;
      // Method method= FunctionStack.getIdentifierMethod(invoc.getMethodName().getName(),
      // invoc.getActualParamList());
      Method method= invoc.getMethod();

      // System.out.println(method);

      if( method.hasDirectClass() )
      {
        generateClass(invoc, userval);
        useSuperCompute= true;

        // The sub-statements might have some dependencies
        if( deps.size() > 0 )
        {
          generateDependencyMethod();
        }

        code= new ExpressionToCode(expr).getCode();
      }
      else if( method instanceof ClassMethodUser )
      {
        userMethodComputation(invoc, (ClassMethodUser) method);
      }
      else
      {
        simpleComputation();
      }
    }
    else
    {
      simpleComputation();
    }

    generateSubFormulas();

    method_begin("public", "void", "compute(Computer computer)", true);

    generateSubFormulaInvocations();

    if( useSuperCompute )
      output("super.compute(computer);");
    else
    {
      call("setComputer", "computer");
      assign("value", code);
    }

    // call("debug");

    method_end();

    indent_less();
    footer();
  }

  private void userMethodComputation(InvocationNode invoc, ClassMethodUser method) throws GeneratorException
  {
    addImport("at.pegasos.computer.Computer");
    addImport("at.pegasos.computer.computations." + classNameFromTime(userval.getTime()));

    addImport("at.pegasos.computer.SessionComputation");

    if( !isHeaderPresent() )
    {
      header();
      setHeaderPresent(true);
    }

    classBegin("@SessionComputation");
    indent();

    method_begin("public", "String", "getName()", true);
    output("return \"" + userval.getIdentifier() + "\";");

    method_end();

    String call= method.getMethodName() + userval.getTime().toNameString() + "(";
    val= invoc.getActualParamList().getElementAt(0);
    if( !(val instanceof UserValueIdentifierNode) )
    {
      // TODO: infer information about original statement
      throw new GeneratorException("First parameter has to be a user value");
    }
    // The parameter used for this invocation
    UserValueIdentifierNode uvalparam= (UserValueIdentifierNode) val;
    if( uvalparam.getTime() != null )
    {
      // TODO: see above
      throw new GeneratorException("Cannot specify time for this method");
    }
    call+= "\"" + uvalparam.getIdentifier() + "\", ";

    if( invoc.getActualParamList().getSize() > 1 )
    {
      call+= new ExpressionToCode(invoc.getActualParamList().getElementAt(1)).getCode();
      if( invoc.getActualParamList().getSize() > 2 )
      {
        call+= ", " + new ExpressionToCode(invoc.getActualParamList().getElementAt(2)).getCode();
      }
      else
      {
        call+= ", " + method.getDefaultValue();
      }
    }
    else
    {
      call+= "0, " + method.getDefaultValue();
    }
    call+= ")";

    code= call;
  }

  private void simpleComputation()
  {
    addImport("at.pegasos.data.compute.Computer");
    addImport("at.pegasos.computer.computations." + classNameFromTime(userval.getTime()));

    addImport("at.pegasos.computer.SessionComputation");

    if( !isHeaderPresent() )
    {
      header();
      setHeaderPresent(true);
    }

    classBegin("@SessionComputation");
    indent();

    method_begin("public", "String", "getName()", true);
    output("return \"" + userval.getIdentifier() + "\";");
    method_end();

    code= new ExpressionToCode(expr).getCode();
  }

  public static String classNameFromTime(TimespecNode timespecNode)
  {
    if( timespecNode != null )
    {
      TimeEnum time= timespecNode.getTime();
      switch( time )
      {
        case Day:
          return "UserDayComputation";
        case Week:
          return "UserWeekComputation";
        case Month:
          return "UserMonthComputation";
        case Year:
          return "UserYearComputation";
        default:
          return null;
      }
    }
    else
    {
      return "UserFromNowComputation";
    }
  }
}
