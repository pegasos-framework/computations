package at.pegasos.generator.formula;

import java.util.HashSet;
import java.util.Set;

import node.BinaryNode;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.UnaryNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierPropertyNode;
import node.ValueIdentifierNode;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.ValueNode;

public class DependencyVisitor implements ExpressionVisitor {
	
	Set<String> dependencies;

	public DependencyVisitor(ExpressionNode expr) {
		dependencies= new HashSet<String>(20);
		expr.performVisit(this, VisitationOrder.PreOrder);
	}

	@Override
	public void visit(UserValueIdentifierNode expr) {
		dependencies.add(expr.getIdentifier());
	}

	@Override
	public void visit(DataValueIdentifierNode dataValueIdentifierNode) {
		dependencies.add(dataValueIdentifierNode.getIdentifier());
	}

	@Override
	public void visit(ValueNode numberNode) {
		// For now we can ignore this. Number, ... is no dependency
	}

	@Override
	public void visit(ValueIdentifierNode valueIdentifierNode) {
		dependencies.add(valueIdentifierNode.getIdentifier());
	}

	@Override
	public void visit(ValueIdentifierPropertyNode valueIdentifierNode) {
		dependencies.add(valueIdentifierNode.getIdentifier());
	}

	@Override
	public void visit(BinaryNode binaryNode) {
		// We can ignore this. We will visit the children
	}

	/*@Override
	public void visit(NegateNode negateNode) {
		// We can ignore this. We will visit the child
	}

	@Override
	public void visit(BracketNode bracketNode) {
		// We can ignore this. We will visit the child
	}*/

	@Override
	public void visit(ExprListNode exprListNode) {
		// We can ignore this. We will visit the children
	}

	@Override
	public void visit(InvocationNode invocationNode) {
		// TODO decide about method name checking
	}
	
	public Set<String> getDependencies() {
		return dependencies;
	}

	@Override
	public void visit(UnaryNode unaryNode) {
		// We can ignore this. We will visit the child
	}

	@Override
	public void preVisit(BinaryNode binaryNode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postVisit(BinaryNode binaryNode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preVisit(InvocationNode invocationNode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postVisit(InvocationNode invocationNode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void midPostVisit(ExprListNode exprListNode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preVisit(BracketNode bracketNode) {
	}

	@Override
	public void postVisit(BracketNode bracketNode) {
	}
}
