package at.pegasos.formula.generator;

import java.util.Stack;

import at.pegasos.formula.parser.Type;
import at.pegasos.formula.parser.TypeInferer;
import node.BinaryNode;
import node.DataAssignmentNode;
import node.DataSubsetSpec;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.IdentifierAssignmentNode;
import node.IdentifierNode;
import node.StatementListNode;
import node.UnaryNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierPropertyNode;
import node.ValueIdentifierNode;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.NumberNode;
import node.value.ValueNode;

public class StatementSimplifier implements ExpressionVisitor {

  private final ExpressionNode expr;
  private final StatementListNode ret;

  private Stack<ExpressionNode> nodeStack;

  int valIdx= 0;

  public StatementSimplifier(ExpressionNode expr)
  {
    this.expr= expr;
    ret= new StatementListNode();
    nodeStack= new Stack<ExpressionNode>();
  }

  /**
   * Simplify the statement. This will alter the original statement
   *
   * @return additional statements (i.e. parts of the simplified statement)
   */
  public StatementListNode simplify()
  {
    expr.performVisit(this, VisitationOrder.PostOder);

    InvocationAnalyser.setUsageContext(ret);

    return ret;
  }

  @Override
  public void visit(UserValueIdentifierNode userValueIdentifierNode)
  {
    nodeStack.add(userValueIdentifierNode);
  }

  @Override
  public void visit(ValueIdentifierPropertyNode userValueIdentifierNode)
  {
    nodeStack.add(userValueIdentifierNode);
  }

  @Override
  public void visit(DataValueIdentifierNode dataValueIdentifierNode)
  {
    if( dataValueIdentifierNode.getSubset() != null )
    {
      DataSubsetSpec sub= dataValueIdentifierNode.getSubset();
      if( !sub.getStart().isSimple() )
      {
        IdentifierNode i= simplify(sub.getStart());
        sub.setStart(i);
      }

      if( !sub.getEnd().isSimple() )
      {
        IdentifierNode i= simplify(sub.getEnd());
        sub.setEnd(i);
      }
    }
    nodeStack.add(dataValueIdentifierNode);
  }

  @Override
  public void visit(ValueNode valueNode)
  {
    nodeStack.add(valueNode);
  }

  @Override
  public void visit(ValueIdentifierNode valueIdentifierNode)
  {
    nodeStack.add(valueIdentifierNode);
  }

  @Override
  public void visit(BinaryNode binaryNode)
  {
    nodeStack.add(binaryNode);
  }

  @Override
  public void visit(ExprListNode exprListNode)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void visit(InvocationNode invocationNode)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void visit(UnaryNode unaryNode)
  {
    // TODO Auto-generated method stub

  }

  private IdentifierNode simplify(ExpressionNode node)
  {
    Type t= TypeInferer.inferType(node);
    switch( t )
    {
      case Column:
        DataValueIdentifierNode dvalue= new DataValueIdentifierNode(newTmpVar());
        ret.addElement(new DataAssignmentNode(dvalue, node));
        return dvalue;

      case SingleValue:
        ValueIdentifierNode ivalue= new ValueIdentifierNode(newTmpVar());
        ret.addElement(new IdentifierAssignmentNode(ivalue, node));
        return ivalue;

      case MutliValue:
      case Undecided:
      default:
        throw new IllegalStateException("Not implemented: " + t);
    }
  }

  @Override
  public void preVisit(BinaryNode binaryNode)
  {
    
  }

  @Override
  public void postVisit(BinaryNode binaryNode)
  {
    if( binaryNode instanceof DataSubsetSpec )
    {
      if( !(binaryNode.getLeft() instanceof NumberNode) )
      {
        IdentifierNode i= simplify(binaryNode.getLeft());
        binaryNode.setLeft(i);
      }

      if( !(binaryNode.getRight() instanceof NumberNode) )
      {
        IdentifierNode i= simplify(binaryNode.getRight());
        binaryNode.setRight(i);
      }
    }
    else
    {
      if( !binaryNode.getLeft().isSimple() )
      {
        IdentifierNode i= simplify(binaryNode.getLeft());
        binaryNode.setLeft(i);
      }

      if( !binaryNode.getRight().isSimple() )
      {
        IdentifierNode i= simplify(binaryNode.getRight());
        binaryNode.setRight(i);
      }
    }
  }

  @Override
  public void preVisit(InvocationNode invocationNode)
  {
    // TODO Auto-generated method stub
  }

  private String newTmpVar()
  {
    return "tmp" + (++valIdx);
  }

  @Override
  public void postVisit(InvocationNode invocationNode)
  {
    // TODO Auto-generated method stub
    ExprListNode args= new ExprListNode();
    Stack<ExpressionNode> tmpStack= new Stack<ExpressionNode>();

    for(int i= 0; i < invocationNode.getActualParamList().getSize(); i++)
    {
      tmpStack.add(collect());
    }

    for(int i= 0; i < invocationNode.getActualParamList().getSize(); i++)
    {
      args.addElement(tmpStack.pop());
    }
    invocationNode.setParamList(args);
    nodeStack.add(invocationNode);
  }

  @Override
  public void midPostVisit(ExprListNode exprListNode)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void preVisit(BracketNode bracketNode)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void postVisit(BracketNode bracketNode)
  {
    System.out.println("postVisit(BracketNode bracketNode)");
    // TODO Auto-generated method stub

  }

  /**
   * Collect elements from the node stack. If, for example, a binary node is the element to
   * retrieve, also the next to nodes are removed
   * 
   * @return next element to be added to the computation graph
   */
  private ExpressionNode collect()
  {
    ExpressionNode n= nodeStack.pop();

    if( n instanceof BinaryNode )
    {
      nodeStack.pop();
      nodeStack.pop();
    }

    if( n.isSimple() )
    {
      return n;
    }
    else
    {
      IdentifierNode ident= simplify(n);
      return ident;
    }
  }
}
