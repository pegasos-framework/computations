package at.pegasos.formula.generator;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import at.pegasos.computer.Computer;
import at.pegasos.formula.parser.Method;
import at.pegasos.generator.GeneratorException;
import at.pegasos.generator.formula.ExpressionToCode;
import lombok.Getter;
import node.BinaryNode;
import node.DataAssignmentNode;
import node.DataValueIdentifierNode;
import node.ExpressionVisitor;
import node.IdentifierNode;
import node.UnaryNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierPropertyNode;
import node.ValueIdentifierNode;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.ValueNode;

public class DataComputation extends ComputationGenerator {

  /**
   * The value the computation is generated for
   */
  private IdentifierNode val;

  private Map<String, String> dataFields;

  /**
   * Whether all fields are necessary in order to compute this row
   */
  private boolean allNecessary= true;

  public DataComputation(DataAssignmentNode ass, String packageName)
  {
    super(packageName, "Value" + ass.getIdent().getIdentifier(), ass.getExpr());

    val= ass.getIdent();
  }

  @Override
  public void generate() throws GeneratorException
  {
    preProcessStatement();
    boolean useSuperCompute= false;

    if( expr instanceof InvocationNode )
    {
      InvocationNode invoc= (InvocationNode) expr;
      // Method method= FunctionStack.getDataMethod(invoc.getMethodName().getName());
      Method method= invoc.getMethod();

      if( method.hasDirectClass() )
      {
        generateClass((InvocationNode) expr, val);
        useSuperCompute= true;
      }
      else
      {
        // System.err.println("Cannot generate class for " + expr);
        generatePlainHeader();
      }
    }
    else
    {
      generatePlainHeader();
    }

    generateSubFormulas();

    method_begin("public", "void", "compute(Computer computer)", true);

    generateSubFormulaInvocations();

    if( useSuperCompute )
    {
      output("super.compute(computer);");
    }
    else
    {
      // initialise the result
      assign("at.pegasos.data.Data data", "computer.getData()");
      call("init", "data");

      // create local variables for the items of the formula
      createDataVariables();

      // create iterator
      assign("at.pegasos.data.Data.TableIterator it", "data.iterateTable()");
      output("int count;");

      // while( it.hasNext() )
      whileLoopBegin("it.hasNext()");
      // count= 0;
      assign("count", "0");
      // Object[] row= it.next();
      assign("Object[] row", "it.next()");
      // TODO: incorporate other datatypes
      for(Entry<String, String> field : dataFields.entrySet())
      {
        assign("Object v" + field.getValue(), "row[" + field.getValue() + "]");
        ifStatementBegin("it.isOriginal(" + field.getValue() + ") && v" + field.getValue() + " != null");
        output("count++;");
        assign("val" + field.getKey(), "at.pegasos.data.compute.Helper.asDouble(v" + field.getValue() + ")");
        ifStatementEnd();
      }

      if( allNecessary )
      {
        ifStatementBegin("count >= " + dataFields.size());

        Map<String, String> replacements= new HashMap<String, String>(dataFields.size());
        for(Entry<String, String> rep : dataFields.entrySet())
        {
          replacements.put(rep.getKey(), "val" + rep.getKey());
        }

        String code= new ExpressionToCode(expr, replacements).getCode();
        assign("double v", code);
        output("res.values.add(new Double[] {v});");
        output("res.orig.add(ORIG_HELP);");

        ifStatementEnd();

        ifStatementElse();

        output("res.values.add(new Double[] {null});");
        output("res.orig.add(ORIG_HELP);");

        ifStatementEnd();
      }
      else
      {
        // TODO:
      }

      whileLoopEnd();
    }

    method_end();

    // Generate the dependency check
    generateDependencyMethod();

    indent_less();
    footer();
  }

  private void generatePlainHeader()
  {
    addImport("at.pegasos.computer.SessionComputation");
    addImport("at.pegasos.data.Data");
    addImport("at.pegasos.data.Data.TableIterator");
    addImport(at.pegasos.computer.computations.BasicColumnComputation.class.getCanonicalName());
    addImport(Computer.class.getCanonicalName());
    addImport(at.pegasos.data.compute.Helper.class.getCanonicalName());

    if( !isHeaderPresent() )
    {
      header();
      setHeaderPresent(true);

      setExtends("BasicColumnComputation");

      classBegin("@SessionComputation");
    }
    else
    {
      setExtends(at.pegasos.computer.computations.BasicColumnComputation.class.getCanonicalName());

      classBegin();
    }

    empty();
    indent();

    constructor_begin(null);
    call("super", "\"" + val.getIdentifier() + "\"", "java.sql.Types.DOUBLE");
    method_end();
  }

  private void createDataVariables()
  {
    FieldsExtractor f= new FieldsExtractor();
    dataFields= f.getDataFields();

    for(Entry<String, String> field : dataFields.entrySet())
    {
      // final int power= data.getColumnIndex(src_name);
      assign("final int " + field.getValue(), "data.getColumnIndex(\"" + field.getKey() + "\")");
      assign("double val" + field.getKey(), "0");
    }
  }

	@Getter
	private class FieldsExtractor implements ExpressionVisitor {
		
		// private Map<String, String> userFields;
		private Map<String, String> dataFields;
		
		public FieldsExtractor() {
			dataFields= new HashMap<String, String>();
			expr.performVisit(this, VisitationOrder.PseudoInOrder);
		}

		@Override
		public void visit(UserValueIdentifierNode userValueIdentifierNode) {
			// TODO speed up by caching user value?
		}

		@Override
		public void visit(DataValueIdentifierNode dataValueIdentifierNode) {
			if( !dataFields.containsKey(dataValueIdentifierNode.getIdentifier()) ) {
				dataFields.put(dataValueIdentifierNode.getIdentifier(), "field" + dataFields.size());
			}
		}

		@Override
		public void visit(ValueNode valueNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(ValueIdentifierNode valueIdentifierNode) {
			// TODO Auto-generated method stub
		}

		@Override
		public void visit(ValueIdentifierPropertyNode valueIdentifierNode) {
			// TODO Auto-generated method stub
		}

		@Override
		public void visit(BinaryNode binaryNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(ExprListNode exprListNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(InvocationNode invocationNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void visit(UnaryNode unaryNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void preVisit(BinaryNode binaryNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void postVisit(BinaryNode binaryNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void preVisit(InvocationNode invocationNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void postVisit(InvocationNode invocationNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void midPostVisit(ExprListNode exprListNode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void preVisit(BracketNode bracketNode) {
		}

		@Override
		public void postVisit(BracketNode bracketNode) {
		}
		
	}
}
