package at.pegasos.formula.generator;

import grammar.AssignmentType;
import node.BinaryNode;
import node.DataAssignmentNode;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.IdentifierAssignmentNode;
import node.StatementListNode;
import node.StatementNode;
import node.UnaryNode;
import node.UserAssignmentNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierPropertyNode;
import node.ValueIdentifierNode;
import node.ExpressionVisitor.VisitationOrder;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.ValueNode;

public class InvocationAnalyser {

  private static class UsageContextInserter implements ExpressionVisitor {

    private AssignmentType type;

    public UsageContextInserter(AssignmentType type)
    {
      this.type= type;
    }

    @Override
    public void visit(UserValueIdentifierNode userValueIdentifierNode)
    {
    }

    @Override
    public void visit(ValueIdentifierPropertyNode userValueIdentifierNode)
    {
    }

    @Override
    public void visit(DataValueIdentifierNode dataValueIdentifierNode)
    {
    }

    @Override
    public void visit(ValueNode valueNode)
    {
    }

    @Override
    public void visit(ValueIdentifierNode valueIdentifierNode)
    {
    }

    @Override
    public void visit(BinaryNode binaryNode)
    {
    }

    @Override
    public void visit(ExprListNode exprListNode)
    {
    }

    @Override
    public void visit(InvocationNode invocationNode)
    {
      invocationNode.setUsageContext(type);
    }

    @Override
    public void visit(UnaryNode unaryNode)
    {
    }

    @Override
    public void preVisit(BinaryNode binaryNode)
    {
    }

    @Override
    public void postVisit(BinaryNode binaryNode)
    {
    }

    @Override
    public void preVisit(InvocationNode invocationNode)
    {
    }

    @Override
    public void postVisit(InvocationNode invocationNode)
    {
    }

    @Override
    public void midPostVisit(ExprListNode exprListNode)
    {
    }

    @Override
    public void preVisit(BracketNode bracketNode)
    {
    }

    @Override
    public void postVisit(BracketNode bracketNode)
    {
    }
  }

  private static class MethodInserter implements ExpressionVisitor {

    @Override
    public void visit(UserValueIdentifierNode userValueIdentifierNode)
    {
    }

    @Override
    public void visit(ValueIdentifierPropertyNode userValueIdentifierNode)
    {
    }

    @Override
    public void visit(DataValueIdentifierNode dataValueIdentifierNode)
    {
    }

    @Override
    public void visit(ValueNode valueNode)
    {
    }

    @Override
    public void visit(ValueIdentifierNode valueIdentifierNode)
    {
    }

    @Override
    public void visit(BinaryNode binaryNode)
    {
    }

    @Override
    public void visit(ExprListNode exprListNode)
    {
    }

    @Override
    public void visit(InvocationNode invocationNode)
    {
      invocationNode.getMethod();
    }

    @Override
    public void visit(UnaryNode unaryNode)
    {
    }

    @Override
    public void preVisit(BinaryNode binaryNode)
    {
    }

    @Override
    public void postVisit(BinaryNode binaryNode)
    {
    }

    @Override
    public void preVisit(InvocationNode invocationNode)
    {
    }

    @Override
    public void postVisit(InvocationNode invocationNode)
    {
    }

    @Override
    public void midPostVisit(ExprListNode exprListNode)
    {
    }

    @Override
    public void preVisit(BracketNode bracketNode)
    {
    }

    @Override
    public void postVisit(BracketNode bracketNode)
    {
    }
  }

  public static void setUsageContext(StatementListNode node)
  {
    for(StatementNode statement : node)
    {
      if( statement == null )
        continue;

      if( statement instanceof UserAssignmentNode )
      {
        UsageContextInserter visitor= new UsageContextInserter(AssignmentType.User);
        UserAssignmentNode ass= (UserAssignmentNode) statement;
        ass.getExpr().performVisit(visitor, VisitationOrder.PreOrder);
        ass.getExpr().performVisit(new MethodInserter(), VisitationOrder.PreOrder);
      }
      else if( statement instanceof IdentifierAssignmentNode )
      {
        UsageContextInserter visitor= new UsageContextInserter(AssignmentType.Identifier);
        IdentifierAssignmentNode ass= (IdentifierAssignmentNode) statement;
        ass.getExpr().performVisit(visitor, VisitationOrder.PreOrder);
        ass.getExpr().performVisit(new MethodInserter(), VisitationOrder.PreOrder);
      }
      else if( statement instanceof DataAssignmentNode )
      {
        UsageContextInserter visitor= new UsageContextInserter(AssignmentType.Data);
        DataAssignmentNode ass= (DataAssignmentNode) statement;
        ass.getExpr().performVisit(visitor, VisitationOrder.PreOrder);
        ass.getExpr().performVisit(new MethodInserter(), VisitationOrder.PreOrder);
      }
      else
      {
        System.err.println("Unkown statement type " + statement.getClass());
      }
    }
  }

  public static void setUsageContextIdentifier(ExpressionNode expr)
  {
    UsageContextInserter visitor= new UsageContextInserter(AssignmentType.Identifier);
    expr.performVisit(visitor, VisitationOrder.PreOrder);
    expr.performVisit(new MethodInserter(), VisitationOrder.PreOrder);
  }

  public static void setUsageContextData(ExpressionNode expr)
  {
    UsageContextInserter visitor= new UsageContextInserter(AssignmentType.Data);
    expr.performVisit(visitor, VisitationOrder.PreOrder);
    expr.performVisit(new MethodInserter(), VisitationOrder.PreOrder);
  }

  public static void setUsageContextUser(ExpressionNode expr)
  {
    UsageContextInserter visitor= new UsageContextInserter(AssignmentType.User);
    expr.performVisit(visitor, VisitationOrder.PreOrder);
    expr.performVisit(new MethodInserter(), VisitationOrder.PreOrder);
  }
}
