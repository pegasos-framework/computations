package at.pegasos.formula.generator;

import java.util.Set;

import at.pegasos.formula.GetGenerator;
import at.pegasos.formula.parser.ComputerClass;
import at.pegasos.formula.parser.Method;
import at.pegasos.generator.CodeGenerator;
import at.pegasos.generator.GeneratorException;
import at.pegasos.generator.formula.DependencyVisitor;
import at.pegasos.generator.formula.ExpressionToCode;
import grammar.CodeContext;
import node.DataAssignmentNode;
import node.ExpressionNode;
import node.IdentifierAssignmentNode;
import node.IdentifierNode;
import node.StatementListNode;
import node.StatementNode;
import node.UserAssignmentNode;
import node.expression.InvocationNode;

public abstract class ComputationGenerator extends CodeGenerator {

  private boolean headerPresent= false;

  /**
   * The expression to be generated by this computation generator
   */
  protected ExpressionNode expr;

  /**
   * The original expression before any processing was done by this facility
   */
  protected ExpressionNode exprOrig;

  protected Set<String> deps;

  protected StatementListNode stmts;

  public ComputationGenerator(String pkg_name, String classname, ExpressionNode expr)
  {
    super(pkg_name, classname);
    this.expr= expr;
    deps= new DependencyVisitor(expr).getDependencies();
  }

  protected void preProcessStatement()
  {
    exprOrig= expr.copy();
    stmts= new StatementSimplifier(expr).simplify();
  }

  /**
   * Generate the subformula classes of this formula.
   *
   * @throws GeneratorException
   */
  protected void generateSubFormulas() throws GeneratorException
  {
    if( stmts.size() > 0 )
    {
      for(int i= 0; i < stmts.size(); i++)
      {
        StatementNode stmt= stmts.elementAt(i);
        // PrintTree.print(stmt);
        ComputationGenerator gen= GetGenerator.get(stmt);

        if( gen != null )
        {
          gen.setOutput(this.getOutputStream());
          gen.setHeaderPresent(isHeaderPresent());
          gen.setIndent(getIndent());
          gen.generate();
        }
      }

      empty();
    }
  }

  /**
   * Generate the computations of the subformulas. For every subformula a computer.directcompute()
   * will be generated.
   */
  protected void generateSubFormulaInvocations()
  {
    for(int i= 0; i < stmts.size(); i++)
    {
      StatementNode statement= stmts.elementAt(i);
      if( statement instanceof UserAssignmentNode )
      {
        throw new IllegalStateException("Well that is strange and should not happend");
      }
      else if( statement instanceof IdentifierAssignmentNode )
      {
        IdentifierAssignmentNode ass= (IdentifierAssignmentNode) statement;
        output("computer.directcompute(new Value" + ass.getIdent().getIdentifier() + "());");
      }
      else if( statement instanceof DataAssignmentNode )
      {
        DataAssignmentNode ass= (DataAssignmentNode) statement;
        output("computer.directcompute(new Value" + ass.getIdent().getIdentifier() + "());");
      }
    }
  }

  /**
   * Generate the getDependencies() method of the computation
   * @param expr
   */
  protected void generateDependencyMethod()
  {
    if( !headerPresent )
    {
      if( deps.size() > 0 )
      {
        member("private static final", "java.util.Set<String>", "dependencies");
        output("static {");
        indent();
        assign("dependencies", "new java.util.HashSet<String>()");
        for(String dep : deps)
        {
          output("dependencies.add(\"" + dep + "\");");
        }
        indent_less();
        output("}");
      }

      method_begin("public", "java.util.Collection<String>", "getDependencies()");
      if( deps.size() > 0 )
      {
        output("return dependencies;");
      }
      else
      {
        output("return null;");
      }
      method_end();
    }
    else
    {
      method_begin("public", "java.util.Collection<String>", "getDependencies()");
      if( deps.size() > 0 )
      {
        assign("java.util.Set<String> dependencies", "new java.util.HashSet<String>()");
        for(String dep : deps)
        {
          output("dependencies.add(\"" + dep + "\");");
        }
        output("return dependencies;");
      }
      else
      {
        output("return null;");
      }
      method_end();
    }
  }

  /**
   * Generate a class for an invocation that can be directly computed
   *
   * @param invoc
   *          InvocationNode which is directly computable
   * @param val
   *          Identifier for the result
   */
  protected void generateClass(InvocationNode invoc, IdentifierNode val)
  {
    Method tmethod= invoc.getMethod();
    // System.out.println(invoc.getMethodName().getName() + " " + tmethod);
    assert (invoc.getMethod() instanceof ComputerClass);

    ComputerClass method= (ComputerClass) tmethod;

    addImport("at.pegasos.computer.Computer");
    addImport("at.pegasos.computer.SessionComputation");

    setExtends(method.getDirectClass());

    if( !headerPresent )
    {
      header();
      setHeaderPresent(true);
      classBegin("@SessionComputation");
    }
    else
      classBegin();

    empty();
    indent();

    constructor_begin(null);

    String supercall= "super(";
    supercall+= "\"" + val.getIdentifier() + "\", ";

    String params= new ExpressionToCode(invoc.getActualParamList(), CodeContext.InvocationArgument).getCode();

    supercall+= params + ");";

    output(supercall);

    method_end();
  }

  public boolean isHeaderPresent()
  {
    return headerPresent;
  }

  public void setHeaderPresent(boolean headerPresent)
  {
    this.headerPresent= headerPresent;
  }
}
