package at.pegasos.formula.generator;

import at.pegasos.formula.parser.Method;
import at.pegasos.generator.GeneratorException;
import at.pegasos.generator.formula.ExpressionToCode;
import node.IdentifierAssignmentNode;
import node.IdentifierNode;
import node.expression.InvocationNode;

public class ValueComputation extends ComputationGenerator {
	
	/**
	 * The value the computation is generated for
	 */
	private IdentifierNode val;

  public ValueComputation(IdentifierAssignmentNode ass, String packageName)
  {
    super(packageName, "Value" + ass.getIdent().getIdentifier(), ass.getExpr());

    val= ass.getIdent();
  }

  @Override
  public void generate() throws GeneratorException
  {
    preProcessStatement();
    // System.out.println("Generating: " + expr);
    boolean useSuperCompute= false;
    // System.out.println("Extra statements:");
    // PrintTree.print(stmts, true);
    // System.out.println("Statement");
    // PrintTree.print(expr, true);

    if( expr instanceof InvocationNode )
    {
      InvocationNode invoc= (InvocationNode) expr;
      // Method method= FunctionStack.getIdentifierMethod(invoc.getMethodName().getName(), invoc.getActualParamList());
      Method method= invoc.getMethod();

      // System.out.println(method);

      if( method.hasDirectClass() )
      {
        generateClass(invoc, val);
        useSuperCompute= true;

        // The sub-statements might have some dependencies
        if( deps.size() > 0 )
        {
          generateDependencyMethod();
        }
      }
      else
      {
        simpleComputation();
      }
    }
    else
    {
      simpleComputation();
    }

    generateSubFormulas();

    method_begin("public", "void", "compute(Computer computer)", true);
    generateSubFormulaInvocations();

    if( useSuperCompute )
      output("super.compute(computer);");
    else
    {
      String code= new ExpressionToCode(expr).getCode();
      assign("value", code);
    }
    method_end();

    indent_less();
    footer();
  }
  
  private void simpleComputation()
  {
    addImport("at.pegasos.computer.Computer");
    addImport("at.pegasos.computer.SessionComputation");
    addImport(at.pegasos.computer.computations.BasicValueComputation.class.getCanonicalName());

    if( !isHeaderPresent() )
    {
      header();
      setHeaderPresent(true);

      setExtends("BasicValueComputation");

      classBegin("@SessionComputation");
    }
    else
    {
      setExtends(at.pegasos.computer.computations.BasicValueComputation.class.getCanonicalName());

      classBegin();
    }

    empty();
    indent();

    nameFunction();

    generateDependencyMethod();
  }

	/**
	 * Generate the getName method
	 */
	private void nameFunction() {
		method_begin("public", "String", "getName()", true);
		output("return \"" + val.getIdentifier() + "\";");
		method_end();
	}
}
