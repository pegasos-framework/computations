package at.pegasos.formula;

import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import at.pegasos.formula.generator.InvocationAnalyser;
import at.pegasos.generator.CodeGenerator;
import at.pegasos.parser.OutputInterface;
import at.pegasos.parser.OutputStreamLogger;
import grammar.parser;
import java_cup.runtime.Symbol;
import node.StatementListNode;
import node.StatementNode;
import util.PrintTree;

public class Formula2Class implements OutputInterface {

  public static class Parameters {
    @Parameter(names= "--input")
    public String inputfile;

    @Parameter()
    public List<String> main;

    @Parameter(names= "--outputdir")
    public String classdir= "./parserout";

    @Parameter(names= "--package")
    public String packageName= GetGenerator.DEFAULT_PACKAGENAME;

    @Parameter(names= "--userpackage")
    public String packageNameUser= GetGenerator.DEFAULT_USERPACKAGENAME;

    public boolean debuggeneration= true;

    public String basedir;
  }

  private Logger log= LoggerFactory.getLogger("Parser");

  private Parameters params;

  /**
   * Files generated from the formula specification
   */
  private List<String> generated;

  /**
   * Path/directory where the java files will be generated
   */
  private Path outdir;

  /**
   * Formula file
   */
  private Path inFile;

  private boolean forceSubLogger;

  public static void main(String[] argv) throws Exception
  {
    Parameters p= new Parameters();
    new JCommander(p, argv);
    System.out.println(Arrays.toString(argv));

    Formula2Class f= new Formula2Class(p);
    f.run();
  }

  public Formula2Class(Parameters p)
  {
    this.params= p;
  }

  /**
   * Set inFile and outdir based on the parameters
   */
  private void setPaths()
  {
    if( params.basedir != null )
    {
      Path base= Paths.get(params.basedir);
      if( Paths.get(params.inputfile).isAbsolute() )
      {
        log.error("Cannot specify basedir and absolute input file");
        log.error("Basedir: " + params.basedir);
        log.error("Inputfile: " + params.inputfile);
        throw new RuntimeException();
      }
      inFile= base.resolve(params.inputfile);
      outdir= base.resolve(params.classdir);
    }
    else
    {
      inFile= Paths.get(params.inputfile);
      outdir= Paths.get(params.classdir);
    }
  }

  public void run() throws Exception
  {
    if( params.main != null && params.inputfile != null )
    {
      log.error("Cannot specify input and a main file together");
      log.error("Using inputput file " + params.inputfile);
    }
    else if( params.main != null )
    {
      params.inputfile= params.main.get(0);
    }

    setPaths();

    if( !Files.exists(inFile) )
    {
      throw new RuntimeException("Input file " + inFile.toString() + " does not exist");
    }

    parser p= new parser(inFile.toFile());
    Symbol s;
    s= p.parse();

    log.info("#Errors: " + p.getSyntaxErrorCount());
    if( p.getSyntaxErrorCount() > 0 )
      return;

    generated= new ArrayList<String>(((StatementListNode) s.value).size());

    if( !Files.exists(outdir) )
      Files.createDirectories(outdir);

    InvocationAnalyser.setUsageContext((StatementListNode) s.value);

    // System.out.println("Symbol: " + s);
    // System.out.println((StatementListNode) s.value);
    // p.debug_parse(); //For debugging
    if( forceSubLogger )
    {
      PrintTree.print((StatementListNode) s.value, log, Level.DEBUG);
    }
    else
    {
      PrintTree.print((StatementListNode) s.value);
    }

    CodeGenerator gen= null;
    for(StatementNode statement : ((StatementListNode) s.value))
    {
      try
      {
        if( statement != null && params.debuggeneration )
        {
          gen= GetGenerator.get(statement.copy(), params.packageName, params.packageNameUser);
          if( gen != null )
          {
            if( forceSubLogger )
            {
              gen.setOutput(new PrintStream(new OutputStreamLogger(log, Level.DEBUG)));
            }
            else
            {
              gen.setOutput(System.out);
            }
            gen.generate();
          }
        }

        gen= GetGenerator.get(statement, params.packageName, params.packageNameUser);
        if( gen != null )
        {
          gen.setOputputToPrintStream(outdir);
          gen.generate();
          generated.addAll(gen.getGeneratedFilenames());
        }
      }
      catch(Exception e)
      {
        log.error("Error generating statement for " + statement);
        throw e;
      }
    }
  }

  /**
   * Get the list of generated files
   *
   * @return list of absolute file paths
   */
  public List<String> getOutputFiles()
  {
    return generated;
  }

  @Override
  public void setLogger(Logger logger)
  {
    this.log= logger;
  }

  @Override
  public void forceLoggerOnSub(boolean force)
  {
    this.forceSubLogger= force;
  }
}
