package at.pegasos.formula.parser;

import lombok.ToString;

@ToString
public class ClassMethod extends Method {

  private final String methodName;

  /**
   * Create a new method
   *
   * @param type
   *          return type of the method
   * @param parametersDefintion
   *          definition of the parameters for this method
   * @param methodName
   *          name of the method
   * @param context
   *          context in which this method can be used
   */
  public ClassMethod(Type type, ParametersDefintion parametersDefintion, String methodName, FunctionContext context)
  {
    super(type, parametersDefintion, context);
    this.methodName= methodName;
  }

  @Override
  public boolean hasDirectClass()
  {
    return false;
  }

  public String getMethodName()
  {
    return methodName;
  }

  @Override
  public Method copy()
  {
    return new ClassMethod(getType(), getParametersDefintion(), getMethodName(), getContext());
  }
}
