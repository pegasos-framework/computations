package at.pegasos.formula.parser;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import node.expression.ExprListNode;

@Getter
@EqualsAndHashCode
public class ParametersDefintion {

  private final ParameterDefintion[] params;

  public ParametersDefintion(ParameterDefintion... params)
  {
    // TODO: check sense
    /*
     * We should check for sense here. A <many/type1> followed by a <many/type1> does not make sense
     * as well as <many/type1> followed by <one/type1>, etc. i.e. for now we assume that every
     * <many/typeX> is followed by <one|many,typeY> with X != Y
     */
    this.params= params;
  }

  /**
   * Check whether this parameter definition matches the concrete parameters
   *
   * @param actualParamList
   *          actual parameters to check whether they can be applied for this definition
   * @return true if actualParamList can be used for this definition
   */
  public boolean matches(ExprListNode actualParamList)
  {
    final int aPLs= actualParamList.getSize();
    final int ps= params.length;

    int pli= 0;
    int pi;
    for(pi= 0; pi < ps; pi++)
    {
      if( pli < aPLs )
      {
        Type t= TypeInferer.inferType(actualParamList.getElementAt(pli));
        switch( params[pi].getArity() )
        {
          case One:
            if( !params[pi].getType().equals(t) )
              return false;
            pli++;
            break;
          case Many:
            // check if is matching. If yes go to the loop
            if( !params[pi].getType().equals(t) )
              return false;

            do
            {
              pli++;
              if( pli < aPLs )
                t= TypeInferer.inferType(actualParamList.getElementAt(pli));
            } while( pli < aPLs && params[pi].getType().equals(t) );
            break;

          default:
            throw new IllegalArgumentException("Not implemented");
        }
      }
      else
      {
        return false;
      }
    }

    // So far all arguments have matched. Not it remains to check whether the params have some
    // extra/unmatched ones left i.e. check whether we have successfully matches all parameters in
    // the actualParameterList
    return pli == aPLs;
  }
}
