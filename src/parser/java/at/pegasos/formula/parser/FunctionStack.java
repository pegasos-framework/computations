package at.pegasos.formula.parser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.pegasos.formula.parser.ParameterDefintion.Arity;
import grammar.AssignmentType;
import node.expression.ExprListNode;

public class FunctionStack {
  private static final Map<String, List<Method>> definitions;

  static
  {
    definitions= new HashMap<String, List<Method>>();

    definitions.put("meanmax",
        Arrays.asList(new ComputerClass(Type.MutliValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
                new ParameterDefintion(Arity.Many, Type.SingleValue)),
            at.pegasos.computer.computations.data.MeanMaxPeaks.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("meanmaxsub",
        Arrays.asList(new ComputerClass(Type.MutliValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
                new ParameterDefintion(Arity.Many, Type.SingleValue)),
            at.pegasos.computer.computations.data.MeanMaxPeaksSubValue.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("omnisub",
        Arrays.asList(new ComputerClass(Type.MutliValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
            at.pegasos.computer.computations.data.MeanMaxPeaksOmniDomainSubValue.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("timeinzone",
        Arrays.asList(new ComputerClass(Type.MutliValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
                new ParameterDefintion(Arity.Many, Type.SingleValue)),
            at.pegasos.computer.computations.data.ZoneAnalysis.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("timeinzonesub",
        Arrays.asList(new ComputerClass(Type.MutliValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
                new ParameterDefintion(Arity.Many, Type.SingleValue)),
            at.pegasos.computer.computations.data.ZoneAnalysisMulti.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("timemovingavg",
        Arrays.asList(new ComputerClass(Type.Column,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
                new ParameterDefintion(Arity.One, Type.SingleValue)),
            at.pegasos.computer.computations.math.ColumnExponentialMovingAverage.class.getCanonicalName(), FunctionContext.PureDataFunction)));
    definitions.put("exptimemovingavg",
        Arrays.asList(new ComputerClass(Type.Column,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
                new ParameterDefintion(Arity.One, Type.SingleValue)),
            at.pegasos.computer.computations.math.ColumnExponentialMovingAverage.class.getCanonicalName(), FunctionContext.PureDataFunction)));
    definitions.put("cumulatedarea",
        Arrays.asList(new ComputerClass(Type.Column,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
            at.pegasos.computer.computations.math.ColumnCumulatedArea.class.getCanonicalName(), FunctionContext.PureDataFunction)));
    definitions.put("avg",
        Arrays.asList(
            new ComputerClass(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
                at.pegasos.computer.computations.math.ColumnAverage.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("min",
        Arrays.asList(
            new ComputerClass(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
                at.pegasos.computer.computations.math.ColumnMin.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("max",
        Arrays.asList(
            new ComputerClass(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
                at.pegasos.computer.computations.math.ColumnMax.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("area",
        Arrays.asList(
            new ComputerClass(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
                at.pegasos.computer.computations.math.ColumnArea.class.getCanonicalName(), FunctionContext.DataIdentifierFunction)));
    definitions.put("first",
        Arrays.asList(
            new ComputerClass(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
                at.pegasos.computer.computations.First.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));
    definitions.put("firstGreater",
        Arrays.asList(
            new ComputerClass(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
                new ParameterDefintion(Arity.One, Type.SingleValue)),
                at.pegasos.computer.computations.data.FirstGreater.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));

    // data
    definitions.put("length",
        Arrays.asList(
            new ComputerClass(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
                at.pegasos.computer.computations.data.Length.class.getCanonicalName(), FunctionContext.PureIdentifierFunction)));

    // Mathematics
    definitions.put("sin", Arrays.asList(
        new JavaFunction(Type.SingleValue, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue)),
            "Math.sin", FunctionContext.IdentifierUserFunction),
        new JavaFunction(Type.Column, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue)),
            "Math.sin", FunctionContext.PureDataFunction),
        new ComputerClass(Type.Column, new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column)),
            at.pegasos.computer.computations.math.Sin.class.getCanonicalName(), FunctionContext.PureDataFunction)));

    definitions.put("sum",
        Arrays.asList(
            // sum without param
            new ClassMethodUser(Type.SingleValue,
                new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue)), "sessionSum",
                FunctionContext.PureUserFunction, 0d),
            // sum with offset
            new ClassMethodUser(Type.SingleValue,
                new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue),
                    new ParameterDefintion(Arity.One, Type.SingleValue)),
                "sessionSum", FunctionContext.PureUserFunction, 0d),
            // sum with offset and defaultValue
            new ClassMethodUser(Type.SingleValue,
                new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue),
                    new ParameterDefintion(Arity.One, Type.SingleValue),
                    new ParameterDefintion(Arity.One, Type.SingleValue)),
                "sessionSum", FunctionContext.PureUserFunction, 0d)));

    definitions.put("sessionSumDay", Arrays.asList(new ClassMethod(Type.SingleValue,
        new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue),
            new ParameterDefintion(Arity.One, Type.SingleValue), new ParameterDefintion(Arity.One, Type.SingleValue)),
        "sessionSumDay", FunctionContext.PureUserFunction)));
    definitions.put("sessionSumWeek",
        Arrays.asList(new ClassMethod(Type.SingleValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue),
                new ParameterDefintion(Arity.One, Type.SingleValue)),
            "sessionSumWeek", FunctionContext.PureUserFunction)));
    definitions.put("sessionSumMonth",
        Arrays.asList(new ClassMethod(Type.SingleValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue),
                new ParameterDefintion(Arity.One, Type.SingleValue)),
            "sessionSumMonth", FunctionContext.PureUserFunction)));
    definitions.put("sessionSumYear",
        Arrays.asList(new ClassMethod(Type.SingleValue,
            new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue),
                new ParameterDefintion(Arity.One, Type.SingleValue)),
            "sessionSumYear", FunctionContext.PureUserFunction)));
  }
  
  public static boolean hasMethod(String methodName)
  {
    return definitions.containsKey(methodName);
  }

  /*public static Method getDataMethod(String methodName)
  {
    List<Method> methods= definitions.get(methodName);
    if( methods == null )
      return null;

    int matches= 0;
    Method ret= null;
    for(Method method : methods)
    {
      if( method.getContext().canComputeData() )
      {
        matches++;
        ret= method;
      }
    }

    if( matches > 1 )
    {
      System.err.println("Multiple matching definitions for " + methodName + " as a data method");
    }

    return ret;
  }

  public static Method getIdentifierMethod(String methodName)
  {
    List<Method> methods= definitions.get(methodName);
    if( methods == null )
      return null;

    int matches= 0;
    Method ret= null;
    for(Method method : methods)
    {
      if( method.getContext().canComputeIdentifier() )
      {
        matches++;
        ret= method;
      }
    }

    if( matches > 1 )
    {
      System.err.println("Multiple matching definitions for " + methodName + " as identifier method");
    }

    return ret;
  }

  public static Method getUserMethod(String methodName)
  {
    List<Method> methods= definitions.get(methodName);
    if( methods == null )
      return null;

    int matches= 0;
    Method ret= null;
    for(Method method : methods)
    {
      if( method.getContext().canComputeUser() )
      {
        matches++;
        ret= method;
      }
    }

    if( matches > 1 )
    {
      System.err.println("Multiple matching definitions for " + methodName + " as identifier method");
    }

    return ret;
  }*/

  public static Method getMethod(String methodName, AssignmentType type, ExprListNode actualParamList)
  {
    switch( type )
    {
      case Data:
        return getDataMethod(methodName, actualParamList);
      case Identifier:
        return getIdentifierMethod(methodName, actualParamList);
      case User:
        return getUserMethod(methodName, actualParamList);
      default:
        throw new IllegalStateException();
    }
  }

  public static Method getDataMethod(String methodName, ExprListNode actualParamList)
  {
    List<Method> methods= definitions.get(methodName);
    if( methods == null )
      return null;

    int matches= 0;
    Method ret= null;
    for(Method method : methods)
    {
      // System.out.println("Check if " + method + " can be used with " + actualParamList + " " + method.getContext().canComputeIdentifier() + " " + method.getParametersDefintion().matches(actualParamList));
      if( method.getContext().canComputeData() && method.getParametersDefintion().matches(actualParamList) )
      {
        matches++;
        ret= method;
      }
    }

    if( matches > 1 )
    {
      System.err.println("Multiple matching definitions for " + methodName + " as data method");
    }
    else if( matches == 0 )
      System.err.println("Method not found");

    return ret;
  }

  public static Method getIdentifierMethod(String methodName, ExprListNode actualParamList)
  {
    List<Method> methods= definitions.get(methodName);
    if( methods == null )
      return null;

    int matches= 0;
    Method ret= null;
    for(Method method : methods)
    {
      // System.out.println("Check if " + method + " can be used with " + actualParamList + " " + method.getContext().canComputeIdentifier() + " " + method.getParametersDefintion().matches(actualParamList));
      if( method.getContext().canComputeIdentifier() && method.getParametersDefintion().matches(actualParamList) )
      {
        matches++;
        ret= method;
      }
    }

    if( matches > 1 )
    {
      System.err.println("Multiple matching definitions for " + methodName + " as identifier method");
    }

    return ret;
  }

  public static Method getUserMethod(String methodName, ExprListNode actualParamList)
  {
    List<Method> methods= definitions.get(methodName);
    if( methods == null )
      return null;
    // else
    //  System.err.println("No function name " + methodName + " defined");

    int matches= 0;
    Method ret= null;
    for(Method method : methods)
    {
      // System.out.println("Check if " + method + " can be used with " + actualParamList + " " + method.getContext().canComputeIdentifier() + " " + method.getParametersDefintion().matches(actualParamList));
      if( method.getContext().canComputeUser() && method.getParametersDefintion().matches(actualParamList) )
      {
        matches++;
        ret= method;
      }
    }

    if( matches > 1 )
    {
      System.err.println("Multiple matching definitions for " + methodName + " as identifier method");
    }

    return ret;
  }
}
