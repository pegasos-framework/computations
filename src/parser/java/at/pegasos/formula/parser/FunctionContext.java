package at.pegasos.formula.parser;

/**
 * Defines where a function can be used.
 */
public abstract class FunctionContext {
  public String toString()
  {
    return "FunctionContext(D:" + canComputeData() + ",U:" + canComputeUser() + ",I:" + canComputeIdentifier() + ")";
  }

  /**
   * Return whether this function can be used to compute a data column
   * 
   * @return true if it can compute a data column
   */
  public abstract boolean canComputeData();

  /**
   * Return whether this function can be used to compute a user value
   * 
   * @return true if it can compute a user value
   */
  public abstract boolean canComputeUser();

  /**
   * Return whether this function can be used to compute an identifier
   * 
   * @return true if it can compute an identifier
   */
  public abstract boolean canComputeIdentifier();

  /**
   * Context for a function that can only be used to compute a user value
   */
  public static final FunctionContext PureUserFunction;

  /**
   * Context for a function that can only be used to compute a data column
   */
  public static final FunctionContext PureDataFunction;

  /**
   * Context for a function that can only be used to compute an identifier
   */
  public static final FunctionContext PureIdentifierFunction;

  /**
   * Context for a function that can be used for both data and identifier computations
   */
  public static final FunctionContext DataIdentifierFunction;

  /**
   * Context for a function that can be used for both identifier and user computations
   */
  public static final FunctionContext IdentifierUserFunction;

  static
  {
    PureUserFunction= new FunctionContext() {

      @Override
      public boolean canComputeUser()
      {
        return true;
      }

      @Override
      public boolean canComputeIdentifier()
      {
        return false;
      }

      @Override
      public boolean canComputeData()
      {
        return false;
      }
    };

    PureDataFunction= new FunctionContext() {

      @Override
      public boolean canComputeUser()
      {
        return false;
      }

      @Override
      public boolean canComputeIdentifier()
      {
        return false;
      }

      @Override
      public boolean canComputeData()
      {
        return true;
      }
    };

    PureIdentifierFunction= new FunctionContext() {

      @Override
      public boolean canComputeUser()
      {
        return false;
      }

      @Override
      public boolean canComputeIdentifier()
      {
        return true;
      }

      @Override
      public boolean canComputeData()
      {
        return false;
      }
    };

    DataIdentifierFunction= new FunctionContext() {

      @Override
      public boolean canComputeUser()
      {
        return false;
      }

      @Override
      public boolean canComputeIdentifier()
      {
        return true;
      }

      @Override
      public boolean canComputeData()
      {
        return true;
      }
    };

    IdentifierUserFunction= new FunctionContext() {

      @Override
      public boolean canComputeUser()
      {
        return true;
      }

      @Override
      public boolean canComputeIdentifier()
      {
        return true;
      }

      @Override
      public boolean canComputeData()
      {
        return false;
      }
    };
  }
}
