package at.pegasos.formula.parser;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper= true)
public class ClassMethodUser extends ClassMethod {

  private double defaultValue;

  public ClassMethodUser(Type type, ParametersDefintion parametersDefintion, String methodName, FunctionContext context,
      Double defaultValue)
  {
    super(type, parametersDefintion, methodName, context);
  }

  @Override
  public Method copy()
  {
    return new ClassMethodUser(getType(), getParametersDefintion(), getMethodName(), getContext(), defaultValue);
  }
}
