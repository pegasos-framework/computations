package at.pegasos.formula.parser;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode
public abstract class Method {
	private final Type type;
	private final ParametersDefintion parametersDefintion;

	/**
	 * Context in which the function can be applied
	 */
	private final FunctionContext context;
	
  /**
   * Create a new method
   *
   * @param type
   *          return type of the method
   * @param parametersDefintion
   *          definition of the parameters for this method
   * @param context
   *          context in which this method can be used
   */
  public Method(Type type, ParametersDefintion parametersDefintion, FunctionContext context)
  {
		this.type= type;
		this.parametersDefintion= parametersDefintion;
		this.context= context;
	}

  public abstract boolean hasDirectClass();

  public abstract Method copy();
}
