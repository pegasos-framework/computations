package at.pegasos.formula.parser;

import lombok.ToString;

@ToString
public class JavaFunction extends Method {

  private final String functionName;

  /**
   * Create a new method
   *
   * @param type
   *          return type of the method
   * @param parametersDefintion
   *          definition of the parameters for this method
   * @param functionName
   *          name of the method
   * @param context
   *          context in which this method can be used
   */
  public JavaFunction(Type type, ParametersDefintion parametersDefintion, String functionName, FunctionContext context)
  {
    super(type, parametersDefintion, context);
    this.functionName= functionName;
  }

  @Override
  public boolean hasDirectClass()
  {
    return false;
  }

  public String getFunctionName()
  {
    return functionName;
  }

  @Override
  public Method copy()
  {
    return new JavaFunction(getType(), getParametersDefintion(), getFunctionName(), getContext());
  }
}
