package at.pegasos.formula.parser;

import lombok.ToString;

@ToString
public class ComputerClass extends Method {

  private final String directClass;

  /**
   * Create a new method
   *
   * @param type
   *          return type of the method
   * @param parametersDefintion
   *          definition of the parameters for this method
   * @param directClass
   *          class representing this method. null if none exists
   * @param context
   *          context in which this method can be used
   */
  public ComputerClass(Type type, ParametersDefintion parametersDefintion, String directClass, FunctionContext context)
  {
    super(type, parametersDefintion, context);
    this.directClass= directClass;
  }

  @Override
  public boolean hasDirectClass()
  {
    return true;
  }

  public String getDirectClass()
  {
    return directClass;
  }

  @Override
  public Method copy()
  {
    return new ComputerClass(getType(), getParametersDefintion(), directClass, getContext());
  }
}
