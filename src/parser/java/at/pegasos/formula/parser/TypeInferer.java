package at.pegasos.formula.parser;

import java.util.Stack;

import node.BinaryNode;
import node.DataSubsetSpec;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.UnaryNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierPropertyNode;
import node.ValueIdentifierNode;
import node.arithmetic.BinaryArithmeticExpression;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.ValueNode;

public class TypeInferer implements ExpressionVisitor {

  private final ExpressionNode expr;
  
  private Stack<Type> typeStack;

  public TypeInferer(ExpressionNode expr)
  {
    this.expr= expr;
    this.typeStack= new Stack<Type>();
  }

  @Override
  public void visit(UserValueIdentifierNode userValueIdentifierNode)
  {
    typeStack.add(Type.SingleValue);
  }

  @Override
  public void visit(DataValueIdentifierNode dataValueIdentifierNode)
  {
    typeStack.add(Type.Column);
  }

  @Override
  public void visit(ValueNode valueNode)
  {
    typeStack.add(Type.SingleValue);
  }

  @Override
  public void visit(ValueIdentifierNode valueIdentifierNode)
  {
    // TODO could be multivalue (not implemented now?)
    typeStack.add(Type.SingleValue);
  }

  @Override
  public void visit(ValueIdentifierPropertyNode valueIdentifierPropertyNode)
  {
    typeStack.add(Type.SingleValue);
  }

  @Override
  public void visit(BinaryNode binaryNode)
  {
    if( binaryNode instanceof BinaryArithmeticExpression )
    {
      Type t1= typeStack.pop();
      Type t2= typeStack.pop();
      
      if( t1 == Type.Column || t2 == Type.Column )
        typeStack.add(Type.Column);
      else if( t1 == Type.SingleValue && t2 == Type.SingleValue )
        typeStack.add(Type.SingleValue);
      else
      {
        System.err.println("Cannot decide type of " + binaryNode);
        typeStack.add(Type.Undecided);
      }
    }
    else if( binaryNode instanceof DataSubsetSpec )
    {
      // We are just ignoring this
    }
    else
      throw new IllegalStateException("Not implemented yet");
  }

  @Override
  public void visit(ExprListNode exprListNode)
  {
    // Nothing to do here
  }

  @Override
  public void visit(InvocationNode invocationNode)
  {
    typeStack.add(invocationNode.getMethod().getType());
  }

  @Override
  public void visit(UnaryNode unaryNode)
  {
    // TODO what to do here?
  }

  @Override
  public void preVisit(BinaryNode binaryNode)
  {
    // Nothing to do here
  }

  @Override
  public void postVisit(BinaryNode binaryNode)
  {
    // Nothing to do here
  }

  @Override
  public void preVisit(InvocationNode invocationNode)
  {
    // Nothing to do here
  }

  @Override
  public void postVisit(InvocationNode invocationNode)
  {
    // Nothing to do here
  }

  @Override
  public void midPostVisit(ExprListNode exprListNode)
  {
    // Nothing to do here
  }

  @Override
  public void preVisit(BracketNode bracketNode)
  {
    // Nothing to do here
  }

  @Override
  public void postVisit(BracketNode bracketNode)
  {
    // Nothing to do here
  }

  private Type infer()
  {
    expr.performVisit(this, VisitationOrder.PostOder);
    return typeStack.pop();
  }

  public static Type inferType(ExpressionNode expr)
  {
    TypeInferer inferer= new TypeInferer(expr);
    return inferer.infer();
  }
}
