package at.pegasos.formula.parser;

import lombok.Getter;

@Getter
public class ParameterDefintion {
  public enum Arity {
    One, Many,/*,
    ZeroOrOne,
    ZeroOrMany*/
  }

  private final Arity arity;
  private final Type type;

  public ParameterDefintion(Arity arity, Type type)
  {
    this.arity= arity;
    this.type= type;
  }
}
