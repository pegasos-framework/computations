package at.pegasos.formula;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import at.pegasos.formula.generator.InvocationAnalyser;
import at.pegasos.generator.CodeGenerator;
import grammar.parser;
import java_cup.runtime.Symbol;
import node.StatementListNode;
import node.StatementNode;

public class Formula2Code {

  public static class Parameters {
    @Parameter()
    public List<String> inputfiles= new ArrayList<String>();
  }

  private Parameters params;

  public static void main(String[] argv) throws Exception
  {
    Parameters p= new Parameters();
    new JCommander(p, argv);

    Formula2Code f= new Formula2Code(p);
    f.run();
  }

  public Formula2Code(Parameters p)
  {
    this.params= p;
  }

  public void run() throws Exception
  {
    for(String inputfile : params.inputfiles)
    {
      System.out.println("Parsing " + inputfile);
      parser p= new parser(new File(inputfile));
      Symbol s;
      s= p.parse();

      InvocationAnalyser.setUsageContext((StatementListNode) s.value);

      CodeGenerator gen= null;
      for(StatementNode statement : ((StatementListNode) s.value))
      {
        System.out.println(statement);
        gen= GetGenerator.get(statement);

        if( gen != null )
        {
          gen.setOutput(System.out);
          gen.generate();
        }
      }
    }
  }
}
