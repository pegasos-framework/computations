package at.pegasos.formula;

import at.pegasos.formula.generator.ComputationGenerator;
import at.pegasos.formula.generator.DataComputation;
import at.pegasos.formula.generator.ValueComputation;
import at.pegasos.generator.formula.UserValue;
import node.DataAssignmentNode;
import node.IdentifierAssignmentNode;
import node.StatementNode;
import node.UserAssignmentNode;

public class GetGenerator {

  public static final String DEFAULT_PACKAGENAME= "at.test.calculations";
  public static final String DEFAULT_USERPACKAGENAME= "at.test.usercalculations";

  private static String packageName= DEFAULT_PACKAGENAME;

  private static String userPackageName= DEFAULT_USERPACKAGENAME;

  /**
   * Get a generator for a statement using the standard settings.
   * 
   * @param statement
   *          statement for which a generator is needed
   * @return generator for the statement
   */
  public static ComputationGenerator get(StatementNode statement)
  {
    return get(statement, packageName, userPackageName);
  }

  /**
   * Get a generator for a statement. <bold>This getter will also change the used package names
   * according to the used parameters!</bold>
   * 
   * @param statement
   *          statement for which a generator is needed
   * @param packageName
   *          package to be used for all calculations except user calculations
   * @param packageNameUser
   *          package to be used for all user calculations
   * @return generator for the statement
   */
  public static ComputationGenerator get(StatementNode statement, String packageName, String packageNameUser)
  {
    setPackageName(packageName);
    setUserPackageName(packageNameUser);

    if( statement instanceof UserAssignmentNode )
    {
      UserAssignmentNode ass= (UserAssignmentNode) statement;
      return new UserValue(ass, userPackageName);
    }
    else if( statement instanceof IdentifierAssignmentNode )
    {
      IdentifierAssignmentNode ass= (IdentifierAssignmentNode) statement;
      return new ValueComputation(ass, packageName);
    }
    else if( statement instanceof DataAssignmentNode )
    {
      DataAssignmentNode ass= (DataAssignmentNode) statement;
      return new DataComputation(ass, packageName);
    }
    else
    {
      System.out.println("Skipping " + statement);
      return null;
    }
  }

  public static void setUserPackageName(String packageNameUser)
  {
    userPackageName= packageNameUser;
  }

  public static void setPackageName(String packageName)
  {
    GetGenerator.packageName= packageName;
  }
}
