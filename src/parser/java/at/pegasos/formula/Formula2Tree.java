package at.pegasos.formula;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import at.pegasos.formula.generator.InvocationAnalyser;
import grammar.parser;
import java_cup.runtime.Symbol;
import node.StatementListNode;
import util.PrintTree;

public class Formula2Tree {

  private static class Parameters {
    @Parameter()
    public List<String> inputfiles= new ArrayList<String>();

    @Parameter(names= "--simple", description="Output isSimple for nodes")
    public boolean simple= false;
  }

  private Parameters params;

  public static void main(String[] argv) throws Exception
  {
    Parameters p= new Parameters();
    new JCommander(p, argv);
    
    BasicConfigurator.configure();

    Formula2Tree f= new Formula2Tree(p);
    f.run();
  }

  public Formula2Tree(Parameters p)
  {
    this.params= p;
  }

  public void run() throws Exception
  {
    for(String inputfile : params.inputfiles)
    {
      System.out.println("Parsing " + inputfile);
      parser p= new parser(new File(inputfile));
      Symbol s;
      s= p.parse();

      System.out.println("#Errors: " + p.getSyntaxErrorCount());
      if( p.getSyntaxErrorCount() > 0 )
        return;

      System.out.println((StatementListNode) s.value);
      InvocationAnalyser.setUsageContext((StatementListNode) s.value);
      PrintTree.print((StatementListNode) s.value, params.simple);
    }
  }
}
