package at.pegasos.parser;

import org.slf4j.Logger;

public interface OutputInterface {
  /**
   * Set the logger for this facility
   * 
   * @param logger
   */
  public void setLogger(Logger logger);

  /**
   * Set whether the logger of this facility should be forced on all enclosed facilities. If true
   * all components of this class should use the logger of this class. Consequently, this method
   * should be called after setLogger has been called.
   * 
   * @param force
   *          force state
   */
  public void forceLoggerOnSub(boolean force);
}
