package at.pegasos.parser;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.event.Level;

public class OutputStreamLogger extends OutputStream {

  private final ByteArrayOutputStream baos= new ByteArrayOutputStream(1000);
  private final Logger logger;
  private final Level level;

  public OutputStreamLogger(Logger logger, Level debug)
  {
    this.logger= logger;
    this.level= debug;
  }

  @Override
  public void write(int b)
  {
    if( b == '\n' )
    {
      String line= baos.toString();
      baos.reset();

      switch( level )
      {
        case TRACE:
          logger.trace(line);
          break;
        case DEBUG:
          logger.debug(line);
          break;
        case ERROR:
          logger.error(line);
          break;
        case INFO:
          logger.info(line);
          break;
        case WARN:
          logger.warn(line);
          break;
      }
    }
    else
    {
      baos.write(b);
    }
  }
}
