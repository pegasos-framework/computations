package grammar;

public enum AssignmentType {
  Data,
  User,
  Identifier
}
