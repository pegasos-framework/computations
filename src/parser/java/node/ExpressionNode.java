package node;

import node.ExpressionVisitor.VisitationOrder;

public abstract class ExpressionNode extends Node {

  public abstract void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order);

  /**
   * Test whether an expression is simple.
   * 
   * An expression is simple when:
   * <ul>
   * <li>Is a value</li>
   * <li>does not require additional computations</li>
   * <li>or most important: can be directly translated to code using Expr2Code</li>
   * </ul>
   * 
   * @return true if it is simple
   */
  public abstract boolean isSimple();

  @Override
  public abstract ExpressionNode copy();
}
