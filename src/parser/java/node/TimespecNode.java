package node;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper= false)
public class TimespecNode extends Node {

  private String offset;
  private int offsetI;
  private TimeEnum time;

  public TimespecNode(TimeEnum time, String number)
  {
    this.time= time;
    this.offset= number;
    this.offsetI= Integer.parseInt(number);
  }

  public TimespecNode(TimeEnum time)
  {
    this.time= time;
    this.offset= null;
    this.offsetI= 0;
  }

  /**
   * Get a name/text for this timespec. time is directly translated. Any offset is converted into P
   * (plus) or M (minus)
   * 
   * @return
   */
  public String toNameString()
  {
    if( offset != null )
    {
      if( offsetI > 0 )
        return time + "P" + offsetI;
      else if( offsetI < 0 )
        return time + "M" + (-1 * offsetI);
      else
        return "" + time;
    }
    else
      return "" + time;
  }

  public TimeEnum getTime()
  {
    return time;
  }

  public String getOffset()
  {
    return "" + offsetI;
  }

  @Override
  public Node copy()
  {
    TimespecNode ret= new TimespecNode(time);
    ret.offset= this.offset;
    ret.offsetI= this.offsetI;
    return ret;
  }
}
