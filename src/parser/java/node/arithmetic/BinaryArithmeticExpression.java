package node.arithmetic;

import lombok.Getter;
import lombok.ToString;
import node.BinaryNode;
import node.DataValueIdentifierNode;
import node.ExpressionNode;

@Getter
@ToString
public abstract class BinaryArithmeticExpression extends BinaryNode {
	
	private String operator;

	public BinaryArithmeticExpression(ExpressionNode left, ExpressionNode right, String operator) {
		super(left, right);
		this.operator= operator;
	}

  public boolean isSimple()
  {
    if( left instanceof DataValueIdentifierNode || right instanceof DataValueIdentifierNode )
      return false;

    return left.isSimple() && right.isSimple();
  }

  public abstract BinaryArithmeticExpression copy();
}
