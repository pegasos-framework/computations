package node.arithmetic;

import node.ExpressionNode;

public class TimesNode extends BinaryArithmeticExpression {

	public TimesNode(ExpressionNode left, ExpressionNode right) {
		super(left, right, "*");
	}

  @Override
  public BinaryArithmeticExpression copy()
  {
    return new TimesNode(left, right);
  }
}
