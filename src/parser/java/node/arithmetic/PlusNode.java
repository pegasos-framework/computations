package node.arithmetic;

import node.ExpressionNode;

public class PlusNode extends BinaryArithmeticExpression {

	public PlusNode(ExpressionNode left, ExpressionNode right) {
		super(left, right, "+");
	}

  @Override
  public BinaryArithmeticExpression copy()
  {
    return new PlusNode(left, right);
  }
}
