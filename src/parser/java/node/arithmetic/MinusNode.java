package node.arithmetic;

import node.ExpressionNode;

public class MinusNode extends BinaryArithmeticExpression {

	public MinusNode(ExpressionNode left, ExpressionNode right) {
		super(left, right, "-");
	}

  @Override
  public BinaryArithmeticExpression copy()
  {
    return new MinusNode(left, right);
  }
}
