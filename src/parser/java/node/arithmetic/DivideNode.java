package node.arithmetic;

import node.ExpressionNode;

public class DivideNode extends BinaryArithmeticExpression {

	public DivideNode(ExpressionNode left, ExpressionNode right) {
		super(left, right, "/");
	}

  @Override
  public BinaryArithmeticExpression copy()
  {
    return new DivideNode(left, right);
  }
}
