package node.arithmetic;

import node.ExpressionNode;

public class ExponentNode extends BinaryArithmeticExpression {

  public ExponentNode(ExpressionNode left, ExpressionNode right)
  {
    super(left, right, "^");
  }

  @Override
  public BinaryArithmeticExpression copy()
  {
    return new ExponentNode(left, right);
  }
}
