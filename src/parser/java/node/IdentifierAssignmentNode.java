package node;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=false)
public class IdentifierAssignmentNode extends StatementNode {
	private IdentifierNode ident;
	private ExpressionNode expr;

	public IdentifierAssignmentNode(IdentifierNode ident, ExpressionNode expr) {
		this.ident = ident;
		this.expr = expr;
	}

  @Override
  public StatementNode copy()
  {
    IdentifierAssignmentNode ret= new IdentifierAssignmentNode((IdentifierNode) ident.copy(), (ExpressionNode) expr.copy());
    return ret;
  }
}
