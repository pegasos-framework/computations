package node;

import lombok.Data;
import lombok.EqualsAndHashCode;
import node.ExpressionVisitor.VisitationOrder;

@Data
@EqualsAndHashCode(callSuper= true)
public class UserValueIdentifierNode extends IdentifierNode {
  private TimespecNode time;

  public UserValueIdentifierNode(String identifier)
  {
    super(identifier);
  }

  public UserValueIdentifierNode(String identifier, TimespecNode timespec)
  {
    super(identifier);
    this.time= timespec;
  }

  @Override
  public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order)
  {
    expressionVisitor.visit(this);
  }

  @Override
  public ExpressionNode copy()
  {
    UserValueIdentifierNode ret= new UserValueIdentifierNode(getIdentifier());
    ret.time= this.time;
    return ret;
  }
}
