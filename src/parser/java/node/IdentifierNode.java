package node;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper= false)
public abstract class IdentifierNode extends ExpressionNode {

  private String identifier;

  public IdentifierNode(String identifier)
  {
    this.identifier= identifier;
  }

  /**
   * Get the name / identifier of this node
   *
   * @return name of the identifier
   */
  public String getIdentifier()
  {
    return identifier;
  }

  /**
   * Per definition a identifier node is always simple
   *
   * @return true
   */
  @Override
  public boolean isSimple()
  {
    return true;
  }
}
