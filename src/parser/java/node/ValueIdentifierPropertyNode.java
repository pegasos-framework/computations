package node;

import lombok.Getter;
import node.ExpressionVisitor.VisitationOrder;

@Getter
public class ValueIdentifierPropertyNode extends IdentifierNode {
  private String property;

  public ValueIdentifierPropertyNode(String identifier, String property)
  {
    super(identifier);
    this.property= property;
  }

  @Override
  public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order)
  {
    expressionVisitor.visit(this);
  }

  @Override
  public ExpressionNode copy()
  {
    return new ValueIdentifierPropertyNode(getIdentifier(), property);
  }
}
