package node;

public abstract class StatementNode extends Node {

  public abstract StatementNode copy();
}
