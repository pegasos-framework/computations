package node;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import node.ExpressionVisitor.VisitationOrder;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper=false)
public abstract class BinaryNode extends ExpressionNode {

  protected ExpressionNode left;
  protected ExpressionNode right;

  public BinaryNode(ExpressionNode left, ExpressionNode right)
  {
    this.left= left;
    this.right= right;
  }

	public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order) {
		expressionVisitor.preVisit(this);
		switch (order) {
		case PseudoInOrder:
		case InOrder:
			left.performVisit(expressionVisitor, order);
			expressionVisitor.visit(this);
			right.performVisit(expressionVisitor, order);
			break;
		case PostOder:
			left.performVisit(expressionVisitor, order);
			right.performVisit(expressionVisitor, order);
			expressionVisitor.visit(this);
			break;
		case PreOrder:
			expressionVisitor.visit(this);
			left.performVisit(expressionVisitor, order);
			right.performVisit(expressionVisitor, order);
			break;
		default:
			throw new IllegalArgumentException();
		}
		expressionVisitor.postVisit(this);
	}
}
