package node;

public class NegateNode extends UnaryNode {
	
	public NegateNode(ExpressionNode expr) {
		super(expr);
	}

  @Override
  public UnaryNode copy()
  {
    return new NegateNode(expr.copy());
  }
}
