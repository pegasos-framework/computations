package node.expression;

import at.pegasos.formula.parser.ClassMethod;
import at.pegasos.formula.parser.FunctionStack;
import at.pegasos.formula.parser.JavaFunction;
import at.pegasos.formula.parser.Method;
import grammar.AssignmentType;
import lombok.Getter;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.ExpressionVisitor.VisitationOrder;

@Getter
public class InvocationNode extends ExpressionNode {

  private MethodNameNode methodName;
  private ExprListNode actualParamList;

  /**
   * Underlying method. Should not be accessed directly but via getMethod()
   */
  private Method method;
  private AssignmentType usageContext;

  public InvocationNode(MethodNameNode methodName, ExprListNode actualParamList)
  {
    this.methodName= methodName;
    this.actualParamList= actualParamList;
  }

  public InvocationNode(MethodNameNode methodName, ExprListNode actualParamList, AssignmentType type)
  {
    this.methodName= methodName;
    this.actualParamList= actualParamList;
    this.usageContext= type;
  }

  public void setUsageContext(AssignmentType type)
  {
    this.usageContext= type;
  }

  public String toString()
  {
    String ret= methodName.getName() + " [";
    if( method == null )
    {
      ret+= "method not infered";
    }
    else
    {
      ret+= method;
    }
    ret+= "] (" + actualParamList + ")";

    return ret;
  }

  @Override
  public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order)
  {
    expressionVisitor.preVisit(this);
    switch( order )
    {
      case InOrder:
        throw new IllegalStateException("No in order of non binary trees");
      case PseudoInOrder:
      case PostOder:
        actualParamList.performVisit(expressionVisitor, order);
        expressionVisitor.visit(this);
        break;
      case PreOrder:
        expressionVisitor.visit(this);
        actualParamList.performVisit(expressionVisitor, order);
        break;
      default:
        throw new IllegalArgumentException();
    }
    expressionVisitor.postVisit(this);
  }

  public Method getMethod()
  {
    if( method == null )
      method= FunctionStack.getMethod(methodName.getName(), usageContext, actualParamList);
    return method;
  }

  @Override
  public boolean isSimple()
  {
    if( getMethod() instanceof JavaFunction || getMethod() instanceof ClassMethod )
    {
      return actualParamList.isSimple();
    }
    return false;
  }

  public void setParamList(ExprListNode args)
  {
    this.actualParamList= args;
  }

  @Override
  public ExpressionNode copy()
  {
    InvocationNode ret= new InvocationNode((MethodNameNode) methodName.copy(), (ExprListNode) actualParamList.copy());
    if( this.method != null )
      ret.method= this.method.copy();
    if( this.usageContext != null )
      ret.usageContext= AssignmentType.valueOf("" + this.usageContext);
    return ret;
  }

  @Override
  public boolean equals(Object o)
  {
    if( !(o instanceof InvocationNode) )
      return false;

    InvocationNode other= (InvocationNode) o;

    if( this.methodName.equals(other.methodName) && this.actualParamList.equals(other.actualParamList) )
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
