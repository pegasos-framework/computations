package node.expression;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.ExpressionVisitor.VisitationOrder;

@ToString
@EqualsAndHashCode(callSuper= false)
public class ExprListNode extends ExpressionNode {

  private List<ExpressionNode> exprs;

  public ExprListNode()
  {
    exprs= new ArrayList<ExpressionNode>();
  }

  public ExprListNode(ExpressionNode... exprs)
  {
    this.exprs= new ArrayList<ExpressionNode>(exprs.length);
    for(ExpressionNode expr : exprs)
      this.exprs.add(expr);
  }

  public void addElement(ExpressionNode expr)
  {
    exprs.add(expr);
  }

  /**
   * Get the number of statements in this list
   *
   * @return number of statements
   */
  public int getSize()
  {
    return exprs.size();
  }

	@Override
	public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order) {
		switch (order) {
		case InOrder:
			throw new IllegalStateException("No in order of non binary trees");
		case PseudoInOrder:
		case PostOder:
			int i= 0;
			for(; i < exprs.size()-1; i++) {
				exprs.get(i).performVisit(expressionVisitor, order);
				expressionVisitor.midPostVisit(this);
			}
			if( exprs.size() > 0 ) {
				exprs.get(i).performVisit(expressionVisitor, order);
			}
			expressionVisitor.visit(this);
			break;
		case PreOrder:
			expressionVisitor.visit(this);
			i= 0;
			for (; i < exprs.size() - 1; i++) {
				exprs.get(i).performVisit(expressionVisitor, order);
				expressionVisitor.midPostVisit(this);
			}
			if( exprs.size() > 0 ) {
				exprs.get(i).performVisit(expressionVisitor, order);
			}
			break;
		default:
			throw new IllegalArgumentException();
		}
	}

  /**
   * Check whether all expressions are simple
   *
   * @return true if all expressions in the list are simple
   */
  @Override
  public boolean isSimple()
  {
    for(ExpressionNode expr : exprs)
    {
      if( !expr.isSimple() )
      {
        return false;
      }
    }
    return true;
  }

  public ExpressionNode getElementAt(int index)
  {
    return exprs.get(index);
  }

  @Override
  public ExpressionNode copy()
  {
    List<ExpressionNode> retlist= new ArrayList<ExpressionNode>(exprs.size());
    ExprListNode ret= new ExprListNode();
    ret.exprs= retlist;
    for(ExpressionNode node : exprs)
    {
      retlist.add(node.copy());
    }
    return ret;
  }

  /*@Override
  public boolean equals(Object o)
  {
    if( !(o instanceof ExprListNode) )
      return false;
    
    ExprListNode other= (ExprListNode) o;
    if( getSize() != other.getSize() )
      return false;
    
    final int size= getSize();
    for(int i= 0; i < size; i++)
    {
      if( !exprs.get(i).equals(other.exprs.get(i)) )
      {
        return false;
      }
    }
    return true;
  }*/
}
