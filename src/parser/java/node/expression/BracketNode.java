package node.expression;

import node.ExpressionNode;
import node.ExpressionVisitor;
import node.ExpressionVisitor.VisitationOrder;
import node.UnaryNode;

public class BracketNode extends UnaryNode {
	
	public BracketNode(ExpressionNode expr) {
		super(expr);
	}
	
	@Override
	public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order) {
		expressionVisitor.preVisit(this);
		switch (order) {
		case PseudoInOrder:
		case InOrder:
			expr.performVisit(expressionVisitor, order);
			expressionVisitor.visit(this);
			break;
		case PostOder:
			expr.performVisit(expressionVisitor, order);
			expressionVisitor.visit(this);
			break;
		case PreOrder:
			expressionVisitor.visit(this);
			expr.performVisit(expressionVisitor, order);
			break;
		default:
			throw new IllegalArgumentException();
		}
		expressionVisitor.postVisit(this);
	}

  @Override
  public UnaryNode copy()
  {
    return new BracketNode(expr);
  }
}
