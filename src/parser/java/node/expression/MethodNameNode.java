package node.expression;

import node.Node;

public class MethodNameNode extends Node {
  private String ident;

  public MethodNameNode(String ident)
  {
    this.ident= ident;
  }

  public String getName()
  {
    return ident;
  }

  @Override
  public Node copy()
  {
    return new MethodNameNode(ident);
  }

  @Override
  public boolean equals(Object o)
  {
    if( !(o instanceof MethodNameNode) )
      return false;

    return this.ident.equals(((MethodNameNode) o).ident);
  }
}
