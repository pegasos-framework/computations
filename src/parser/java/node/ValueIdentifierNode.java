package node;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import node.ExpressionVisitor.VisitationOrder;

@ToString(callSuper= true)
@EqualsAndHashCode(callSuper= true)
public class ValueIdentifierNode extends IdentifierNode {
  public ValueIdentifierNode(String identifier)
  {
    super(identifier);
  }

  @Override
  public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order)
  {
    expressionVisitor.visit(this);
  }

  @Override
  public ExpressionNode copy()
  {
    return new ValueIdentifierNode(getIdentifier());
  }
}
