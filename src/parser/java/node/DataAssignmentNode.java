package node;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper= false)
public class DataAssignmentNode extends StatementNode {
	private DataValueIdentifierNode ident;
	private ExpressionNode expr;

	public DataAssignmentNode(DataValueIdentifierNode ident, ExpressionNode expr) {
		this.ident = ident;
		this.expr = expr;
	}

  @Override
  public StatementNode copy()
  {
    DataAssignmentNode ret= new DataAssignmentNode((DataValueIdentifierNode) ident.copy(), (ExpressionNode) expr.copy());
    return ret;
  }
}
