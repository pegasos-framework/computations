package node;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper= true)
@ToString(callSuper=true)
public class DataSubsetSpec extends BinaryNode {

  public DataSubsetSpec(ExpressionNode start, ExpressionNode end)
  {
    super(start, end);
  }

  @Override
  public ExpressionNode copy()
  {
    return new DataSubsetSpec(left.copy(), right.copy());
  }

  @Override
  public boolean isSimple()
  {
    return left.isSimple() && right.isSimple();
  }

  public ExpressionNode getStart()
  {
    return left;
  }

  public ExpressionNode getEnd()
  {
    return right;
  }

  public void setStart(IdentifierNode left)
  {
    setLeft(left);
  }

  public void setEnd(IdentifierNode end)
  {
    setRight(end);
  }
}
