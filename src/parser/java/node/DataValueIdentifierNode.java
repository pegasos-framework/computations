package node;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import node.ExpressionVisitor.VisitationOrder;

@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
@Getter
public class DataValueIdentifierNode extends IdentifierNode {

  private DataSubsetSpec subset;

  public DataValueIdentifierNode(String identifier)
  {
    super(identifier);
  }

  public DataValueIdentifierNode(String identifier, DataSubsetSpec subset)
  {
    super(identifier);
    this.subset= subset;
  }

  @Override
  public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order)
  {
    switch( order )
    {
      case InOrder:
        throw new IllegalStateException("No in order of non binary trees");
      case PseudoInOrder:
      case PostOder:
        if( subset != null )
        {
          subset.performVisit(expressionVisitor, order);
        }
        expressionVisitor.visit(this);
        break;
      case PreOrder:
        expressionVisitor.visit(this);
        if( subset != null )
        {
          subset.performVisit(expressionVisitor, order);
        }
        break;
      default:
        throw new IllegalArgumentException();
    }
  }

  @Override
  public ExpressionNode copy()
  {
    DataValueIdentifierNode ret= new DataValueIdentifierNode(getIdentifier());
    if( this.subset != null )
      ret.subset= (DataSubsetSpec) this.subset.copy();
    return ret;
  }
}
