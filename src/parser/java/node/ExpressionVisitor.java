package node;

import node.BinaryNode;
import node.DataValueIdentifierNode;
import node.UnaryNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierNode;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.ValueNode;

public interface ExpressionVisitor {

	enum VisitationOrder {
		/**
		 * Strict in order. Ie for non binary nodes an exception should be raised
		 */
		InOrder, PreOrder, PostOder,
		/**
		 * A pseudo in order. Same as InOrder for binary nodes. For n-ary nodes (with n
		 * > 2) each of the sub trees will be traversed but the root will be visited
		 * after each child. e.g.
		 * 
		 * <pre>
		 *         a
		 *       / | \
		 *      b  c d
		 * </pre>
		 * 
		 * should be b, a (mid), c, a (mid),d, a. (mid) denotes a `mid` visit with a
		 * corresponding callback (e.g. midPostVisit))
		 * 
		 */
		PseudoInOrder
	}

	public void visit(UserValueIdentifierNode userValueIdentifierNode);

	public void visit(DataValueIdentifierNode dataValueIdentifierNode);

	public void visit(ValueNode valueNode);

	public void visit(ValueIdentifierNode valueIdentifierNode);

	public void visit(ValueIdentifierPropertyNode valueIdentifierPropertyNode);

	public void visit(BinaryNode binaryNode);

	public void visit(ExprListNode exprListNode);

	public void visit(InvocationNode invocationNode);

	public void visit(UnaryNode unaryNode);

	public void preVisit(BinaryNode binaryNode);

	public void postVisit(BinaryNode binaryNode);

	public void preVisit(InvocationNode invocationNode);

	public void postVisit(InvocationNode invocationNode);

	public void midPostVisit(ExprListNode exprListNode);

	public void preVisit(BracketNode bracketNode);

	public void postVisit(BracketNode bracketNode);
}
