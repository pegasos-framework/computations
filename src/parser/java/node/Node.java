package node;

import lombok.ToString;

@ToString
public abstract class Node {
  public abstract Node copy();
}
