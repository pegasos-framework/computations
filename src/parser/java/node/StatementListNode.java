package node;

import java.util.Iterator;
import java.util.Vector;

import lombok.ToString;

@ToString
public class StatementListNode extends Node implements Iterable<StatementNode> {
	private Vector<StatementNode> list = new Vector<StatementNode>();
	private String separator;
	
	public StatementListNode() {
		this.separator = ";";
	}

	public StatementListNode(String separator) {
		this.separator = separator;
	}

	public int size() {
		return list.size();
	}

	public StatementNode elementAt(int i) {
		return (StatementNode) list.elementAt(i);
	}

	public StatementListNode addElement(StatementNode element) {
		list.addElement(element);
		return this;
	}

	@Override
	public Iterator<StatementNode> iterator() {
		return list.iterator();
	}

  @Override
  public Node copy()
  {
    Vector<StatementNode> retlist= new Vector<StatementNode>(list.size());
    StatementListNode ret= new StatementListNode();
    ret.separator= this.separator;
    ret.list= retlist;
    for(StatementNode node : list)
    {
      retlist.add(node.copy());
    }
    return ret;
  }

	/*
	public String toString() {
		String result = "";
		for (int i = 0; i < size(); i++) {
			if (i > 0)
				result += separator;
			StatementNode element = elementAt(i);
			result += element;
		}
		return result;
	}
	*/
}
