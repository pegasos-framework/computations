package node.value;

import lombok.ToString;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.ExpressionVisitor.VisitationOrder;

@ToString
public class NumberNode extends ValueNode {

	private String number;

	public NumberNode(String number) {
		this.number= number;
	}

	@Override
	public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order) {
		expressionVisitor.visit(this);
	}

	@Override
	public String getValueAsString() {
		return number;
	}

  @Override
  public ExpressionNode copy()
  {
    return new NumberNode(number);
  }
  
  @Override
  public boolean equals(Object o)
  {
    if( !(o instanceof NumberNode) )
      return false;
    
    return this.number.equals(((NumberNode) o).number);
  }
}
