package node.value;

import node.ExpressionNode;
import node.ExpressionVisitor;
import node.ExpressionVisitor.VisitationOrder;

public abstract class ValueNode extends ExpressionNode {
  public abstract String getValueAsString();

  @Override
  public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order)
  {
    expressionVisitor.visit(this);
  }

  /**
   * Per definition a value node is always simple
   *
   * @return true
   */
  @Override
  public boolean isSimple()
  {
    return true;
  }
}
