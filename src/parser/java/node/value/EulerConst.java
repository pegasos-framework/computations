package node.value;

import node.ExpressionNode;

public class EulerConst extends ValueNode {

	@Override
	public String getValueAsString() {
		return "" + Math.E;
	}

  @Override
  public ExpressionNode copy()
  {
    return new EulerConst();
  }
}
