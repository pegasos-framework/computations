package node.value;

import lombok.ToString;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.ExpressionVisitor.VisitationOrder;

@ToString
public class StringNode extends ValueNode {

  private String value;

  public StringNode(String value)
  {
    this.value= value;
  }

	@Override
	public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order) {
		expressionVisitor.visit(this);
	}

  @Override
  public String getValueAsString()
  {
    return value;
  }

  @Override
  public ExpressionNode copy()
  {
    return new StringNode(value);
  }
}
