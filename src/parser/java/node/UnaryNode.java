package node;

import lombok.Getter;
import node.ExpressionVisitor.VisitationOrder;

@Getter
public abstract class UnaryNode extends ExpressionNode {
	
	protected ExpressionNode expr;
	
	public UnaryNode(ExpressionNode expr) {
		this.expr= expr;
	}

	@Override
	public void performVisit(ExpressionVisitor expressionVisitor, VisitationOrder order) {
		switch (order) {
		case PseudoInOrder:
		case InOrder:
			expr.performVisit(expressionVisitor, order);
			expressionVisitor.visit(this);
			break;
		case PostOder:
			expr.performVisit(expressionVisitor, order);
			expressionVisitor.visit(this);
			break;
		case PreOrder:
			expressionVisitor.visit(this);
			expr.performVisit(expressionVisitor, order);
			break;
		default:
			throw new IllegalArgumentException();
		}
	}

  /**
   * Check whether this node is simple.
   * 
   * @return true if the underlying expression is simple
   */
  @Override
  public boolean isSimple()
  {
    return expr.isSimple();
  }

  @Override
  public abstract UnaryNode copy();
}
