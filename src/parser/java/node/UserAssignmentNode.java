package node;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = false)
public class UserAssignmentNode extends StatementNode {
	private UserValueIdentifierNode ident;
	private ExpressionNode expr;

	public UserAssignmentNode(UserValueIdentifierNode ident, ExpressionNode expr) {
		this.ident = ident;
		this.expr = expr;
	}

  @Override
  public StatementNode copy()
  {
    UserAssignmentNode ret= new UserAssignmentNode((UserValueIdentifierNode) ident.copy(), (ExpressionNode) expr.copy());
    return ret;
  }
}
