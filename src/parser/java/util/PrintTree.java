package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import node.BinaryNode;
import node.DataAssignmentNode;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.ExpressionVisitor;
import node.IdentifierAssignmentNode;
import node.StatementListNode;
import node.StatementNode;
import node.UnaryNode;
import node.UserAssignmentNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierPropertyNode;
import node.ValueIdentifierNode;
import node.arithmetic.BinaryArithmeticExpression;
import node.expression.BracketNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.value.ValueNode;

public class PrintTree implements ExpressionVisitor {

  private Logger logger= LoggerFactory.getLogger(PrintTree.class);
  private Level logLevel= Level.DEBUG;

  private int indent;
  private final boolean printSimple;

  /**
   * line which is currently produced.
   */
  String line= "";

  public PrintTree(boolean printSimple)
  {
    indent= 0;
    this.printSimple= printSimple;
  }

  public static void print(StatementListNode node)
  {
    print(node, false);
  }

  public static void print(StatementListNode node, boolean printSimple)
  {
    PrintTree p= new PrintTree(printSimple);
    p.printIt(node);
  }
  
  public static void print(ExpressionNode node)
  {
    print(node, false);
  }
  
  public static void print(StatementListNode node, Logger log, Level level)
  {
    PrintTree p= new PrintTree(false);
    p.logger= log;
    p.logLevel= level;
    p.printIt(node);
  }

  public static void print(ExpressionNode node, boolean printSimple)
  {
    PrintTree p= new PrintTree(printSimple);
    node.performVisit(p, VisitationOrder.PreOrder);
  }

  public static void print(StatementNode stmt)
  {
    print(stmt, false);
  }
  
  public static void print(StatementNode statement, boolean printSimple)
  {
    PrintTree p= new PrintTree(printSimple);
    p.printIt(statement);
  }

  private void printIt(StatementNode statement)
  {
    if( statement == null )
      return;

    finishLine("|-" + statement);

    if( statement instanceof UserAssignmentNode )
    {
      printLineBegin();
      UserAssignmentNode ass= (UserAssignmentNode) statement;
      finishLine(ass.getIdent().toString());
      ass.getExpr().performVisit(this, VisitationOrder.PreOrder);
    }
    else if( statement instanceof IdentifierAssignmentNode )
    {
      printLineBegin();
      IdentifierAssignmentNode ass= (IdentifierAssignmentNode) statement;
      finishLine(ass.getIdent().toString());
      ass.getExpr().performVisit(this, VisitationOrder.PreOrder);
    }
    else if( statement instanceof UserAssignmentNode )
    {
      UserAssignmentNode ass= (UserAssignmentNode) statement;
      ass.getExpr().performVisit(this, VisitationOrder.PreOrder);
    }
    else if( statement instanceof DataAssignmentNode )
    {
      DataAssignmentNode ass= (DataAssignmentNode) statement;
      ass.getExpr().performVisit(this, VisitationOrder.PreOrder);
    }
    else
    {
      System.err.println("Unkown statement type " + statement.getClass());
    }
  }

  private void printIt(StatementListNode node) {
		for(StatementNode statement : node) {
			indent= 1;
			printIt(statement);
		}
	}
	
  private void printLineBegin()
  {
    for(int i= 0; i < indent; i++)
      line+= "  ";
    line+= "|-";
  }

  private void finishLine(String output)
  {
    line+= output;
    finishLine();
  }

  private void finishLine()
  {
    switch(logLevel)
    {
      case DEBUG:
        logger.debug(line);
        break;
      case ERROR:
        logger.error(line);
        break;
      case INFO:
        logger.info(line);
        break;
      case TRACE:
        logger.trace(line);
        break;
      case WARN:
        logger.warn(line);
        break;
      default:
        logger.error("Unsuppored loglevel " + logLevel);
        break;
    }
    line= "";
  }

  @Override
  public void visit(UserValueIdentifierNode userValueIdentifierNode)
  {
    printLineBegin();
    finishLine("user:" + userValueIdentifierNode.getIdentifier() + "[" + userValueIdentifierNode.getTime() + "]");
  }

  @Override
  public void visit(DataValueIdentifierNode dataValueIdentifierNode)
  {
    printLineBegin();
    finishLine("data:" + dataValueIdentifierNode.getIdentifier());
  }

  @Override
  public void visit(ValueNode valueNode)
  {
    printLineBegin();
    finishLine(valueNode.getValueAsString());
  }

  @Override
  public void visit(ValueIdentifierNode valueIdentifierNode)
  {
    printLineBegin();
    finishLine(valueIdentifierNode.getIdentifier());
  }
  
  @Override
  public void visit(ValueIdentifierPropertyNode valueIdentifierNodeProp)
  {
    printLineBegin();
    finishLine(valueIdentifierNodeProp.getIdentifier() + "[" + valueIdentifierNodeProp.getProperty() + "]");
  }

  @Override
  public void visit(BinaryNode binaryNode)
  {
    printLineBegin();
    if( binaryNode instanceof BinaryArithmeticExpression )
    {
      line+= (((BinaryArithmeticExpression) binaryNode).getOperator());
    }
    else
    {
      line+= ("binary(" + binaryNode.getClass().getName() + ")");
    }
    if( printSimple )
      finishLine(" simple: " + binaryNode.isSimple());
    else
      finishLine("");
    indent++;
  }

	@Override
	public void visit(ExprListNode exprListNode) {
		// TODO Auto-generated method stub
		
	}

  @Override
  public void visit(InvocationNode invocationNode)
  {
    printLineBegin();
    // line+= (invocationNode.getMethodName().getName());
    line+= (invocationNode);
    if( printSimple )
    {
      finishLine(" simple: " + invocationNode.isSimple() + ", params simple: " + invocationNode.getActualParamList().isSimple());
    }
    else
      finishLine("");
    indent++;
  }

  @Override
  public void visit(UnaryNode unaryNode)
  {
    if( unaryNode instanceof BracketNode )
    {
      printLineBegin();
      finishLine("()");
      indent++;
    }
    else
    {
      printLineBegin();
      finishLine("???unary???");
      indent++;
    }
  }

	@Override
	public void preVisit(BinaryNode binaryNode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postVisit(BinaryNode binaryNode) {
		indent--;
	}

	@Override
	public void preVisit(InvocationNode invocationNode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postVisit(InvocationNode invocationNode) {
		indent--;
	}

	@Override
	public void midPostVisit(ExprListNode exprListNode) {
		// TODO Auto-generated method stub
		
	}

  @Override
  public void preVisit(BracketNode bracketNode)
  {
    // Nothing to do here. Brackets are handled in visit unary node
  }

  @Override
  public void postVisit(BracketNode bracketNode)
  {
    // Nothing to do here. Brackets are handled in visit unary node
  }
}
