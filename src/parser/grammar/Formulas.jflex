package grammar;

import java_cup.runtime.Symbol;

%%

%{
	private int lineNumber = 1;
	public int lineNumber() { return lineNumber; }
	
	public Symbol token( int tokenType ) {
		// System.err.println( "Obtain token " + sym.terminalNames[tokenType] + " \"" + yytext() + "\"" );
		return new Symbol( tokenType, yychar, yychar + yytext().length(), yytext() );
	}
%}

%public
%class Formulas
// change return type of yylex to Symbol
%type Symbol
// Turn on character counting
%char

data        =   data
user        =   user
number		=	[0-9]+
decimalNumber = [0-9]+[\.][0-9]*
octDigit	=	[0-7]
hexDigit	=	[0-9a-fA-F]
ident		=	[A-Za-z][A-Za-z0-9\_]*
space		=	[\ \t]
newline		=	\r|\n|\r\n
escchar		=	\\([ntbrfva\\\'\"\?]|{octDigit}+|[xX]{hexDigit}+)
schar		=	[^\'\"\\\r\n]|{escchar}
stringconst	=	\"{schar}*\"

%%

"="			{ return token( sym.ASSIGN ); }
"+"			{ return token( sym.PLUS ); }
"-"			{ return token( sym.MINUS ); }
"*"			{ return token( sym.TIMES ); }
"/"			{ return token( sym.DIVIDE ); }
"("			{ return token( sym.LEFT ); }
")"			{ return token( sym.RIGHT ); }
"^"			{ return token( sym.EXPONENT ); }
"["         { return token( sym.LB ); }
"]"         { return token( sym.RB ); }
","			{ return token( sym.COMMA ); }
d			{ return token( sym.DAY ); }
w			{ return token( sym.WEEK ); }
e           { return token( sym.EULERNUMBER ); }
m			{ return token( sym.MONTH ); }
y			{ return token( sym.YEAR ); }

{newline}	{ lineNumber++; return token( sym.NEWLINE ); }
{space}		{ }

{data}	{ return token( sym.DATA ); }
{user}	{ return token( sym.USER ); }
{number}	{ return token( sym.NUMBER ); }
{decimalNumber} {return token( sym.DECNUMBER ); }
{ident}		{ return token( sym.IDENT ); }
{stringconst}	{ return token( sym.STRINGVALUE ); }

.			{ return token( sym.error ); }
<<EOF>>		{ return token( sym.EOF ); }
