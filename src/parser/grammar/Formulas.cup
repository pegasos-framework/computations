package grammar;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Hashtable;

import java_cup.runtime.Symbol;

import node.*;
import node.expression.*;
import node.arithmetic.*;
import node.value.*;

parser code
	{:
	private Formulas lexer;
	private File file;
	private int syntaxErrorCount = 0;

	public parser( File file ) {
		this.file = file;
		try {
			lexer = new Formulas( new FileReader( file ) );
		}
		catch ( IOException exception ) {
			throw new Error( "Unable to open file \"" + file + "\"" );
		}
	}
	
	public parser( String line ) {
		this.file = null;
		
		lexer = new Formulas( new StringReader(line) );
	}

	public void report_error( String message, Object info ) {
		System.err.println( file + " ( " + lexer.lineNumber() + " ): " + message );
		try {
			if ( info instanceof Symbol ) {
				Symbol symbol = ( Symbol ) info;
				printText( symbol.left, symbol.right );
			}
		}
		catch ( IOException e ) {
		}
	}

	private void printText( int left, int right ) throws IOException {
		Reader sourceReader = new FileReader( file );
		int veryLeft = Math.max( left - 50, 0 ),
			veryRight = Math.min( right + 20, 
				( int ) file.length() );
		char[] text = new char[ veryRight - veryLeft ];
		char[] underline = new char[ veryRight - veryLeft ];
		sourceReader.skip( veryLeft );
		sourceReader.read( text );
		for ( int i = 0; i < text.length; i++ ) {
			if ( text[ i ] < ' ' ) {
				text[ i ] = '|';
				underline[ i ] = '|';
				}
			else
				underline[ i ] = ' ';
			if ( left <= veryLeft + i && veryLeft + i < right )
				underline[ i ] = '^';
			}
		printLine( text );
		printLine( underline );
		sourceReader.close();
	}

	private static void printLine( char[] text ) {
		for ( int i = 0; i < text.length; i++ )
			System.err.print( text[ i ] );
		System.err.println();
		}

	public void syntax_error( Symbol currToken ) {
		report_error( "Syntax Error", currToken );
		syntaxErrorCount++;
		}
		
	public int getSyntaxErrorCount() {
		return syntaxErrorCount;
	}
	:};

scan with
	{:
	return  lexer.yylex();
	:};

terminal USER, DATA, LB, RB, COMMA, DAY, WEEK, MONTH, YEAR,
 LEFT, RIGHT, NEWLINE, PLUS, MINUS, TIMES, DIVIDE, ASSIGN, EXPONENT;
terminal String NUMBER;
terminal String DECNUMBER;
terminal String STRINGVALUE;
terminal String IDENT;
terminal EULERNUMBER;

nonterminal UserValueIdentifierNode UserVal;
nonterminal DataValueIdentifierNode DataVal;
nonterminal TimespecNode TIMESPEC;
nonterminal TimeEnum TIMESPECTIME;
nonterminal DataSubsetSpec DATASUBSETSPEC;
nonterminal StatementListNode StmtList;
nonterminal StatementNode Stmt;
nonterminal MethodNameNode MethodName;
nonterminal ExprListNode ExprListOpt, ExprList;
nonterminal ExpressionNode
	AssignExpr, OrExpr, AndExpr, RelExpr,
	AddExpr, ExpExpr, MulExpr, PrefixExpr, PostfixExpr, Primary, LiteralValue;

start with StmtList;

TIMESPECTIME::= 
		DAY
		{:
			RESULT = TimeEnum.Day;
		:}
	|
	 	WEEK
	 	{:
	 		RESULT = TimeEnum.Week;
		:}
	|
		MONTH
		{:
			RESULT = TimeEnum.Month;
		:}
	|
		YEAR
		{:
			RESULT = TimeEnum.Year;
		:}
	;

TIMESPEC::= 
		TIMESPECTIME:time
		{:
			RESULT = new TimespecNode(time);
		:}
	|
		TIMESPECTIME:time PLUS NUMBER:number
		{:
			RESULT = new TimespecNode(time, number);
		:}
	|
		TIMESPECTIME:time MINUS NUMBER:number
		{:
			RESULT = new TimespecNode(time, "-" + number);
		:}
	;

DATASUBSETSPEC::=
		RelExpr:start COMMA RelExpr:end
		{:
			RESULT = new DataSubsetSpec(start, end);
		:}
	;

UserVal::= 
		USER LB IDENT:ident RB
		{:
			RESULT = new UserValueIdentifierNode(ident);
		:}
 	|
 		USER LB IDENT:ident COMMA TIMESPEC:time RB
 		{:
 			// System.out.println("Setting " + ident.getClass() + " for " + time);
 			RESULT = new UserValueIdentifierNode(ident, time);
 		:}
	;

DataVal::=
		DATA LB IDENT:ident RB
		{:
			RESULT = new DataValueIdentifierNode(ident);
		:}
	|
		DATA LB IDENT:ident COMMA DATASUBSETSPEC:subset RB
		{:
			RESULT = new DataValueIdentifierNode(ident, subset);
		:}
	;


StmtList::=
		{:
			RESULT = new StatementListNode();
		:}
	|
		StmtList:stmtList Stmt:stmt
		{:
			stmtList.addElement( stmt );
			RESULT = stmtList;
		:};

Stmt::=
		IDENT:ident ASSIGN RelExpr:expr NEWLINE
		{:
			RESULT = new IdentifierAssignmentNode(new ValueIdentifierNode(ident), expr);
		:}
	|
		UserVal:userval ASSIGN RelExpr:expr NEWLINE
		{:
			// System.out.println("Assigning " + userval + " " + expr);
			RESULT = new UserAssignmentNode(userval, expr);
		:}
	|
		DataVal:dataval ASSIGN RelExpr:expr NEWLINE
		{:
			RESULT = new DataAssignmentNode(dataval, expr);
		:}
	|
//		Expr:expr NEWLINE
//		{:
//		System.out.println( "Expression= " + expr.intValue() );
//		:}
//	|
		error NEWLINE
	|
		NEWLINE
	;

RelExpr::=
		AddExpr:expr
		{:
			RESULT = expr;
		:}
	;

AddExpr::=
		AddExpr:expr1 PLUS MulExpr:expr2
		{:
			RESULT = new PlusNode( expr1, expr2 );
		:}
	|
		AddExpr:expr1 MINUS MulExpr:expr2
		{:
			RESULT = new MinusNode( expr1, expr2 );
		:}
	|
		MulExpr:expr
		{:
			RESULT = expr;
		:}
	;

MulExpr::=
		MulExpr:expr1 TIMES ExpExpr:expr2
		{:
			RESULT = new TimesNode( expr1, expr2 );
		:}
	|
		MulExpr:expr1 DIVIDE ExpExpr:expr2
		{:
			RESULT = new DivideNode( expr1, expr2 );
		:}
	|
//		MulExpr:expr1 MOD ExpExpr:expr2
//		{:
//		RESULT = new ModNode( expr1, expr2 );
//		:}
//	|
		ExpExpr:expr
		{:
			RESULT = expr;
		:}
	;

ExpExpr::=
		ExpExpr:expr1 EXPONENT PrefixExpr:expr2
		{:
			RESULT = new ExponentNode( expr1, expr2 );
		:}
	|
		PrefixExpr:expr
		{:
			RESULT = expr;
		:}
	;

PrefixExpr::=
//		MINUS PrefixExpr:expr
//		{:
//			RESULT = new NegateNode( expr );
//		:}
//	|
//		NOT PrefixExpr:expr
//		{:
//		RESULT = new NotNode( expr );
//		:}
//	|
		PostfixExpr:expr
		{:
			RESULT = expr;
		:}
	;

PostfixExpr::=
		Primary:expr
		{:
			RESULT = expr;
		:}
	;

Primary::=
		LEFT RelExpr:expr RIGHT
		{:
			RESULT = new BracketNode(expr);
		:}
	|
		LiteralValue:value
		{:
			RESULT = value;
		:}
	|
		MethodName:methodName LEFT ExprListOpt:actualParams RIGHT
		{:
			if( !at.pegasos.formula.parser.FunctionStack.hasMethod(methodName.getName()) )
			{
				System.err.println("Warning: Unknown function " + methodName.getName() + " at line " + lexer.lineNumber());
			}
			RESULT = new InvocationNode( methodName, actualParams );
		:}
	;

LiteralValue::=
		IDENT:ident
		{:
			RESULT = new ValueIdentifierNode(ident);
		:}
	|
		IDENT:ident LB STRINGVALUE:ident2 RB
		{:
			RESULT = new ValueIdentifierPropertyNode(ident, ident2.substring( 1, ident2.length() - 1 ));
		:}
	|
		UserVal:val
		{:
			RESULT = val;
		:}
	|
		DataVal:val
		{:
			RESULT = val;
		:}
	|
		NUMBER:number
		{:
			RESULT = new NumberNode(number);
		:}
	|
		DECNUMBER:number
		{:
			RESULT = new NumberNode(number);
		:}
	|
		MINUS NUMBER:number
		{:
			RESULT = new NumberNode("-" + number);
		:}
	|
		MINUS DECNUMBER:number
		{:
			RESULT = new NumberNode("-" + number);
		:}
//	|
//		CHARVALUE:value
//		{:
//		RESULT = new CharValueNode(
//			Convert.parseChar( value.substring( 1, value.length() - 1 ) ) );
//		:}
	|
		STRINGVALUE:value
		{:
			RESULT = new StringNode( value.substring( 1, value.length() - 1 ) );
		:}
	|
		EULERNUMBER
		{:
			RESULT = new EulerConst();
		:}
//	|
//		NULL
//		{:
//		RESULT = new NullValueNode();
//		:}
	;

// Functionnames
MethodName::=
		IDENT:ident
		{:
			RESULT = new MethodNameNode( ident );
		:}
	;

// Expressionlist: ie parameters for a method invocation
ExprListOpt::=
		ExprList:exprList 
		{:
			RESULT = exprList;
		:}
	|
		/* Empty */
		{:
			RESULT = new ExprListNode();
		:}
	;

ExprList::=
		ExprList:exprList COMMA RelExpr:expr
		{:
			exprList.addElement( expr );
			RESULT = exprList;
		:}
	|
		RelExpr:expr
		{:
			ExprListNode exprList = new ExprListNode();
			exprList.addElement( expr );
			RESULT = exprList;
		:}
	;
