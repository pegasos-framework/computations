package at.pegasos.data.manager;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import at.pegasos.data.DataException;
import at.pegasos.data.loader.TcxLoader;
import at.pegasos.data.simplify.FixedRateSpline;

public class TxcDataManager extends DataManager {
  
  public static final SimpleDateFormat PolarDateFormat;
  static {
    PolarDateFormat= new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
  }

  /**
   * Loader used for loading the file
   */
  private TcxLoader tcxLoader;
  
  /**
   * Date of the file
   */
  private Date date;

  @Override
  public DataManager getInstance()
  {
    return new TxcDataManager();
  }

  @Override
  public boolean acceptFile(String filename)
  {
    initFileName(filename);

    if( extension.toLowerCase().equals("tcx") )
    {
      tcxLoader= TcxLoader.create(filename);
      int l= baseName.length();
      try
      {
        date= PolarDateFormat.parse(baseName.substring(l-20, l-1));
      }
      catch( ParseException e )
      {
        System.err.println(e.getMessage());
        e.printStackTrace();
        return false;
      }
      return true;
    }
    else
    {
      tcxLoader= null;
      return false;
    }
  }

  public void processFile() throws DataException
  {
    System.out.println("Load File");
    data= tcxLoader.loadFile().getData();
  }

  @Override
  public void normalise() throws DataException
  {
    String sport= tcxLoader.getSport();

    // data.debug();
    data= data.Col2Data();
    if( DEBUG )
    {
      try
      {
        data.writeFileCSV("debug/" + baseName + ".uninterpolated.csv");
      }
      catch( IOException e )
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    HashMap<String, Object> params= new HashMap<String, Object>();
    FixedRateSpline.SingleCol(params);
    data.simplifyMe(FixedRateSpline.class, params);

    if( sport.equals("Running") )
    {
      data.renameColumn("power", "running_power");
    }
    else if( sport.equals("Biking") )
    {
      data.renameColumn("power", "bike_power");
    }

    data.renameColumn("alt", "altitude_m");
  }

  @Override
  public long getStartTime()
  {
    return date.getTime();
  }

  @Override
  public boolean hasStartTime()
  {
    return true;
  }

  @Override
  public String getPerson() throws NoPersonInfoException
  {
    throw new NoPersonInfoException();
  }

  @Override
  public boolean hasPersonInfo()
  {
    return false;
  }
}
