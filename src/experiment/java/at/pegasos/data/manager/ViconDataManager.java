package at.pegasos.data.manager;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import at.pegasos.data.loader.*;
import at.pegasos.data.loader.CSVLoader.Parameter;
import at.pegasos.data.*;
import at.pegasos.data.simplify.FixedRateSpline;

public class ViconDataManager extends DataManager {

  private CSVLoader loader;

  private Parameter p;

  /**
   * Lines in the file to be loaded
   */
  private List<String> lines= null;

  /**
   * Data defintions found in the file
   */
  private List<RegionDefinition> ranges= new ArrayList<RegionDefinition>();

  /**
   * Frequencies of the data definitions
   */
  List<Integer> frequencies= new ArrayList<Integer>();

  private int maxFreq;

  private int base;

  @Override
  public DataManager getInstance()
  {
    return new ViconDataManager();
  }

  @Override
  public boolean acceptFile(String filename)
  {
    initFileName(filename);

    if( extension.toLowerCase().equals("csv") )
    {
      p= CSVLoader.determineParams(filename);
      loader= CSVLoader.create().setParams(p);

      // TODO: check if it is actually a vicon file

      return true;
    }
    else
      return false;
  }

  @Override
  public void normalise() throws DataException
  {
    Map<String, Object> params= new HashMap<String, Object>();
    FixedRateSpline.Frequency(params, (int) ((base / (double) maxFreq)));

    data.simplify(FixedRateSpline.class, params);
  }

  @Override
  public void processFile() throws DataException
  {
    findRegions();

    loader.setLines(lines);

    base= 1000;
    
    if( maxFreq > 1000 )
    {
      loader.setTimeUnit(TimeUnit.MicroSecond);
      base= 1000_000;
    }

    int i= 0;
    for(RegionDefinition range : ranges)
    {
      try
      {
        loader.Load(range, new MultiRowHeader(3,"."), null, new RowToTime((int) ((base / (double) frequencies.get(i++)))));
      }
      catch( IOException e )
      {
        e.printStackTrace();
        throw new DataException(e);
      }
    }

    data= loader.getData();
  }

  private static boolean isNumeric(String string)
  {
    final int l= string.length();
    if( l == 0 )
      return false;
    for(int i= 0; i < l; i++)
    {
      char c= string.charAt(i);
      if( c < '0' || c > '9' )
        return false;
    }
    return true;
  }

  @Override
  public long getStartTime()
  {
    throw new IllegalStateException("Cannot determine date from vicon file");
  }

  @Override
  public boolean hasStartTime()
  {
    return false;
  }

  @Override
  public String getPerson() throws NoPersonInfoException
  {
    throw new NoPersonInfoException();
  }

  @Override
  public boolean hasPersonInfo()
  {
    return false;
  }

  private void findRegions() throws DataException
  {
    try
    {
      lines= Files.readAllLines(Paths.get(filename));
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new DataException(e);
    }

    boolean n2num= false;
    boolean n1num= false;
    boolean n2o1= false;
    boolean n1o1= false;
    boolean onlyone= false;
    boolean isnum;
    String f1= null, f2= null;

    int lineNr= 0;
    maxFreq= 0;

    for(String line : lines)
    {
      int firstSep= line.indexOf(p.sep);
      String firstItem= line.substring(0, firstSep);
      String remainLine= line.substring(firstSep);

      isnum= isNumeric(firstItem);
      onlyone= remainLine.split(p.sep).length == 0;

      if( firstItem.equals("") && n1num && !n2num && n2o1 && n1o1 && !onlyone )
      {
        System.out.println("Begin at " + lineNr + " " + Integer.parseInt(f1) + " " + f2);
        RegionDefinition r= new RegionDefinition(f2, lineNr + 3);
        if( ranges.size() > 0 )
          ranges.get(ranges.size() - 1).setEndLine(lineNr - 2);
        ranges.add(r);
        int freq= Integer.parseInt(f1);
        frequencies.add(freq);
        if( freq > maxFreq )
          maxFreq= freq;
      }

      n2num= n1num;
      n1num= isnum;
      n2o1= n1o1;
      n1o1= onlyone;
      lineNr++;
      f2= f1;
      f1= firstItem;
    }
  }
}
