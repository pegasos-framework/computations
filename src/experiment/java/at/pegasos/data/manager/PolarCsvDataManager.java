package at.pegasos.data.manager;

import java.io.*;
import java.sql.Types;
import java.text.*;
import java.util.*;

import at.pegasos.data.DataException;
import at.pegasos.data.loader.*;

public class PolarCsvDataManager extends DataManager {

  public static final SimpleDateFormat PolarCsvDateFormat;
  static {
      PolarCsvDateFormat= new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
  }

  private Date date;

  @Override
  public DataManager getInstance()
  {
    return new PolarCsvDataManager();
  }

  @Override
  public boolean acceptFile(String filename)
  {
    initFileName(filename);

    // TODO: decide if this is actually a file in Polar's format
    if( extension.toLowerCase().equals("csv") )
    {
      try
      {
        date= PolarCsvDateFormat.parse(baseName);
      }
      catch( ParseException e )
      {
        System.err.println(e.getMessage());
        e.printStackTrace();
        return false;
      }
      return true;
    }
    else
    {
      return false;
    }
  }

  @Override
  public void processFile() throws DataException
  {
    try
    {
      CSVLoader loader= CSVLoader.create()
        .setParams(new CSVLoader.Parameter(filename, ",", "."))
        // The first data row is in line 4
        .setRegion(new RegionDefinition(4))
        // the header is one line above the data
        .setHeader(new SingleRowHeader(1))
        // Time is placed in the column 'Time' and is in 'H:M:S' format
        .setTimeExtractor(new TimeFromRecTimeHMS("Time"))
        // Mask sample rate and 'Pace (min/km)' column
        .setMask(new boolean[] {true, false, false, false, true})
        // and now load
        .Load();

      data= loader.getData();
    }
    catch( IOException e )
    {
      throw new DataException(e);
    }
  }

  @Override
  public void normalise() throws DataException
  {
    data= data.Col2Data();
    
    for(String col : new String[] {"HR (bpm)", "Speed (km/h)", "Cadence", "Altitude (m)", "Stride length (m)",
        "Distances (m)", "Temperatures (C)", "Power (W)"})
    {
      if( data.getType(col) == Types.VARCHAR )
      {
        data.removeColumn(col);
      }
    }

    data.renameColumn("Power (W)", "bike_power");
    data.renameColumn("Distances (m)", "distance_m");
    
    if( data.hasColumn("Speed (km/h)") )
    {
      data.scaleColumn("km", 1 / 3.6 / 1000.0);
      data.renameColumn("Speed (km/h)", "speed_mm_s");
    }

    /*if( data.getType("power") == Types.VARCHAR )
    {
      data.removeColumn("power");
    }
    else
    {
      data.renameColumn("power", "bike_power");
    }
    
    for(String col : new String[] {"hr", "cad", "km"} )
    {
      if( data.getType(col) == Types.VARCHAR )
      {
        data.removeColumn(col);
      }
    }

    if( data.hasColumn("km") )
    {
      data.scaleColumn("km", 1000);
      data.renameColumn("km", "distance_m");
    }*/
  }
  
  @Override
  public long getStartTime()
  {
    return date.getTime();
  }

  @Override
  public boolean hasStartTime()
  {
    return true;
  }

  @Override
  public String getPerson() throws NoPersonInfoException
  {
    throw new NoPersonInfoException();
  }

  @Override
  public boolean hasPersonInfo()
  {
    return false;
  }
}
