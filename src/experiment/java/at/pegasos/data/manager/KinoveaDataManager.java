package at.pegasos.data.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.pegasos.data.DataException;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.CSVLoader.Parameter;
import at.pegasos.data.loader.NoHeader;
import at.pegasos.data.loader.RegionDefinition;
import at.pegasos.data.loader.TimeExtractor;
import at.pegasos.data.loader.TimeFromKinovea;
import at.pegasos.data.simplify.MergeKeepNA;

public class KinoveaDataManager extends DataManager {

  private CSVLoader loader;

  private Parameter p;

  @Override
  public DataManager getInstance()
  {
    return new KinoveaDataManager();
  }

  @Override
  public boolean acceptFile(String filename)
  {
    initFileName(filename);

    if( extension.toLowerCase().equals("txt") )
    {
      p= CSVLoader.determineParams(filename);
      loader= CSVLoader.create().setParams(p);

      BufferedReader reader;
      try
      {
        reader= Files.newBufferedReader(Paths.get(filename));
        String firstLine= reader.readLine();
        reader.close();

        if( firstLine.startsWith("#Kinovea Trajectory data export") )
          return true;
        else
          return false;
      }
      catch( IOException e )
      {
        e.printStackTrace();
        return false;
      }
    }
    else
      return false;
  }

  @Override
  public void normalise() throws DataException
  {
    Map<String, Object> params= new HashMap<String, Object>();

    try
    {
      data.simplify(MergeKeepNA.class, params);
    }
    catch( DataException e )
    {
      e.printStackTrace();
      throw new DataException(e);
    }
  }

  @Override
  public void processFile() throws DataException
  {
    try
    {
      List<String> lines= Files.readAllLines(Paths.get(p.fileName));
      List<String> sublines= Files.readAllLines(Paths.get(p.fileName));
      List<RegionDefinition> ranges= new ArrayList<RegionDefinition>();

      int lineIdx= 0;

      // ditch the first three lines;
      lineIdx= 3;
      sublines= lines.subList(lineIdx, lines.size());

      for(String line : sublines)
      {
        if( line.startsWith("# ") )
        {
          System.out.println("Begin at " + lineIdx + " " + line.substring(2));
          RegionDefinition r= new RegionDefinition(line.substring(2), lineIdx + 3);
          if( ranges.size() > 0 )
            ranges.get(ranges.size() - 1).setEndLine(lineIdx - 2);
          ranges.add(r);
        }

        lineIdx++;
      }

      loader.setLines(lines);

      for(RegionDefinition range : ranges)
      {
        loader.Load(range, new NoHeader("rec_time", "X", "Y"), new boolean[] {true}, getTimeExtractor());
      }

      data= loader.getData();
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new DataException(e);
    }
  }

  private TimeExtractor getTimeExtractor()
  {
    return new TimeFromKinovea("rec_time");
  }

  @Override
  public long getStartTime() throws NoStarttimeException
  {
    throw new NoStarttimeException("Cannot determine date from Kinovea file");
  }

  @Override
  public boolean hasStartTime()
  {
    return false;
  }

  @Override
  public String getPerson() throws NoPersonInfoException
  {
    throw new NoPersonInfoException();
  }

  @Override
  public boolean hasPersonInfo()
  {
    return false;
  }
}
