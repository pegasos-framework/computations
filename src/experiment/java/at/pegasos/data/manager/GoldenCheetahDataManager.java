package at.pegasos.data.manager;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import at.pegasos.data.*;
import at.pegasos.data.loader.*;

public class GoldenCheetahDataManager extends DataManager {
  
  public static final SimpleDateFormat GoldenCheetahCsvFormat;
  static {
      GoldenCheetahCsvFormat= new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
  }

  /**
   * Date of the file
   */
  private Date date;
  
  @Override
  public DataManager getInstance()
  {
    return new GoldenCheetahDataManager();
  }

  @Override
  public boolean acceptFile(String filename)
  {
    initFileName(filename);

    // TODO: decide if golden cheetah or polar

    if( extension.toLowerCase().equals("csv") )
    {
      try
      {
        date= GoldenCheetahCsvFormat.parse(baseName);
      }
      catch( ParseException e )
      {
//        System.err.println(e.getMessage());
//        e.printStackTrace();
        return false;
      }
      return true;
    }
    else
    {
      return false;
    }
  }

  @Override
  public void normalise() throws DataException
  {
    data= normaliseCsv(data);
  }

  @Override
  public void processFile() throws DataException
  {
    try
    {
      CSVLoader loader= CSVLoader.create()
          .determineFileParams(filename)
          .setHeader(new SingleRowHeader(1))
          .setTimeExtractor(new SecToRecTime("secs"))
          .Load();

      data= loader.getData();
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new DataException(e);
    }
  }

  private static Data normaliseCsv(Data data) throws DataException
  {
//    System.out.println("File loaded. Normalising");
    data= data.Col2Data();

    if( data.hasColumn("power") )
    {
      data.renameColumn("power", "bike_power");
    }

    if( data.hasColumn("km") )
    {
      data.scaleColumn("km", 1000);
      data.renameColumn("km", "distance_m");
    }

//    data.debug();

    return data;
  }

  @Override
  public long getStartTime()
  {
    return date.getTime();
  }

  @Override
  public boolean hasStartTime()
  {
    return true;
  }

  @Override
  public String getPerson() throws NoPersonInfoException
  {
    throw new NoPersonInfoException();
  }

  @Override
  public boolean hasPersonInfo()
  {
    return false;
  }
}
