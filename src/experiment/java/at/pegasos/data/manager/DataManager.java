package at.pegasos.data.manager;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import at.pegasos.data.Data;
import at.pegasos.data.DataException;

/**
 * Manager class for loading data from a file. Any use of a manager needs to follow the following
 * pattern:
 * <ul>
 * <li>check if the file can be loaded: acceptFile</li>
 * <li>process the file: processFile</li>
 * <li>normalise the file: normalise</li>
 * </ul>
 * When a file has been accepted by the manager <code>getStartTime</code> should return the date of
 * the file. It is up to the manager how the date is defined or it is determined.
 */
public abstract class DataManager {

  public class NoStarttimeException extends Exception {
    private static final long serialVersionUID= 7405299736070075263L;

    public NoStarttimeException()
    {
      super();
    }

    public NoStarttimeException(String string)
    {
      super(string);
    }
  }

  public class NoPersonInfoException extends Exception {
    private static final long serialVersionUID= 7405299736070075263L;

    public NoPersonInfoException()
    {
      super();
    }

    public NoPersonInfoException(String string)
    {
      super(string);
    }
  }

  protected boolean DEBUG= false;

  /**
   * Base name of the file entity
   */
  protected String baseName;

  protected String extension;
  protected String filename;

  /**
   * Path/folder where the file is located
   */
  protected Path parent;

  /**
   * Output
   */
  protected Data data;

  /**
   * Set debug status of DataManager. Per default debugging is turned of. If switched on the
   * component will
   * <ul>
   * <li>write the time normalised data to 'debug/{filename}.csv'
   * </ul>
   * 
   * @param debug
   */
  public void setDebug(boolean debug)
  {
    DEBUG= debug;
  }

  /**
   * Initialise the file name. This will set basename and extension
   * 
   * @param filename
   *          Name of the file to be loaded / checked
   */
  protected void initFileName(String filename)
  {
    this.filename= filename;
    this.parent= Paths.get(filename).toAbsolutePath().getParent();
    String fn= Paths.get(filename).getFileName().toString();
    int index= fn.lastIndexOf(".");
    baseName= fn.substring(0, index);
    extension= fn.substring(index + 1);
  }

  public abstract DataManager getInstance();

  /**
   * Check whether this manager accepts the file
   * 
   * @param filename
   *          file which is tested
   * @return true if the manager accepts the file
   * @throws DataException
   */
  public abstract boolean acceptFile(String filename) throws DataException;

  public abstract void processFile() throws DataException;

  public abstract void normalise() throws DataException;

  /**
   * Get the time of the file. Files/managers not having such an information i.e. filetypes without
   * a start date are allowed to raise a NoStarttimeException
   * 
   * @return begin time of the file
   * @throws NoStarttimeException
   *           if no information about the start date is available
   */
  public abstract long getStartTime() throws NoStarttimeException;

  /**
   * Check whether a manager or file has info about the person
   * 
   * @return true if the manager or concrete instance has information about a person
   */
  public abstract boolean hasStartTime();

  /**
   * Get the ID of the person of this file
   *
   * @return ID/Name/etc. of the person of this file
   * @throws NoPersonInfoException
   *           if no information about the person is available
   */
  public abstract String getPerson() throws NoPersonInfoException;

  /**
   * Check whether a manager or file has a starttime
   * 
   * @return true if the manager or concrete instance has a starttime
   */
  public abstract boolean hasPersonInfo();

  public Data getData()
  {
    return data;
  }

  protected void debugData()
  {
    if( DEBUG )
    {
      try
      {
        data.writeFileCSV("debug/" + baseName + ".csv");
      }
      catch( IOException e )
      {
        // TODO: handle
        e.printStackTrace();
      }
    }
  }

  /**
   * Get the full filename
   * 
   * @return
   */
  public String getFilename()
  {
    return filename;
  }
}
