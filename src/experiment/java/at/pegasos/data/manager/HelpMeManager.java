package at.pegasos.data.manager;

import java.io.IOException;

import at.pegasos.data.DataException;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.NoHeader;
import at.pegasos.data.loader.RowToTime;
import at.pegasos.data.simplify.Strategy;
import at.pegasos.data.simplify.TimeNormalise;

public class HelpMeManager extends DataManager {
  
  @Override
  public DataManager getInstance()
  {
    return new HelpMeManager();
  }

  @Override
  public boolean acceptFile(String filename)
  {
    initFileName(filename);

    if( extension.toLowerCase().equals("txt") )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  @Override
  public void normalise() throws DataException
  {
    data.simplify(TimeNormalise.class, TimeNormalise.SetOutputN(Strategy.OnSingleColUseOnlyName(Strategy.newParams(), true), 101));
  }

  @Override
  public void processFile() throws DataException
  {
    try
    {
      data= CSVLoader.create().setParams(CSVLoader.determineParams(filename))
          .Load(null, new NoHeader("x1", "x2", "v", "a", "m"), null, new RowToTime(1)).getData();
    }
    catch( IOException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  public long getStartTime() throws NoStarttimeException
  {
    throw new NoStarttimeException();
  }

  @Override
  public boolean hasStartTime()
  {
    return false;
  }

  @Override
  public String getPerson() throws NoPersonInfoException
  {
    throw new NoPersonInfoException();
  }

  @Override
  public boolean hasPersonInfo()
  {
    return false;
  }
}
