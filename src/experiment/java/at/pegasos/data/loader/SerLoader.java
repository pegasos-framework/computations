package at.pegasos.data.loader;

import at.pegasos.data.*;
import at.pegasos.data.Data.*;
import at.pegasos.serverinterface.tools.TimedData;
import at.univie.mma.serverinterface.tools.SensorDataParser;

import java.io.*;
import java.sql.*;
import java.util.*;

public class SerLoader extends DataLoader {

  private File file;
  private final Map<String, TmpColumn> cols;
  private boolean useSensorNumbers;
  private SerLoader()
  {
    cols = new HashMap<>();
  }

  public static SerLoader create(String filename)
  {
    SerLoader ret = new SerLoader();
    ret.file = new File(filename);
    return ret;
  }

  private static int getType(String sensorName/*, String field*/)
  {
    if( sensorName.equals("Setting") )
    {
      return Types.VARCHAR;
    }
    // else if( sensorName.equals)
    // TODO:!!!! hier kann nicht immer double stehen
    return Types.DOUBLE;
  }

  public SerLoader useSensorNumbers(boolean use)
  {
    this.useSensorNumbers = use;
    return this;
  }

  public SerLoader loadFile()
  {
    SensorDataParser parser = new SensorDataParser(file);
    try
    {
      parser.parse();
      List<TimedData> data = parser.getData();
      int size_guess = data.size();
      long end_guess = 0;
      if( data.size() > 0 )
        end_guess = data.get(data.size() - 1).time;

      int didx = 0;
      for(TimedData it : data)
      {
        String sensorName;
        String type;

        int ppos = it.type.indexOf('.');
        sensorName = it.type.substring(0, ppos);
        type = it.type.substring(ppos + 1);

        if( sensorName.equals("Setting") )
        {
          if( it.time > end_guess )
          {
            long time = it.time % end_guess;
            TimedData replace = new TimedData(time, it.type, it.data, it.sensornr);
            data.set(didx, replace);
          }
        }

        String colname;
        if( useSensorNumbers )
        {
          colname = sensorName + it.sensornr + "." + type;
        }
        else
        {
          colname = sensorName + "." + type;
        }

        TmpColumn col = cols.get(colname);
        // Check if column exists
        if( col == null )
        {
          col = new TmpColumn();
          col.col = new Column();

          col.col.name = colname;
          col.col.names = new String[] {type};

          col.col.timestamps = new ArrayList<>(size_guess);
          col.col.values = new ArrayList<>(size_guess);
          col.col.orig = new ArrayList<>(size_guess);

          col.col.types = new int[col.col.names.length];

          for(int i = 0; i < col.col.names.length; i++)
          {
            col.col.types[i] = getType(sensorName/*, type*/);
          }

          cols.put(colname, col);
        }

        int idx = col.col.timestamps.size();
        // if( type.equals("BT-CONTROLLER") ) System.out.println(type + " " + it.time + " " + col.last_time + " " + (it.time <= col.last_time));
        if( it.time <= col.last_time )
        {
          idx--;
          ListIterator<Long> li = col.col.timestamps.listIterator(idx);
          // System.out.println(it.type + " Non monotonic " + idx + " " + it.time + " " + li.hasPrevious());

          long lit = -1;

          while( li.hasPrevious() && it.time < (lit = li.previous()) )
            idx--;

          if( it.time <= lit || it.time == col.last_time )
          {
            System.err.println("Dropping entry " + it.time + " for column " + it.type);
            continue;
          }
          // System.out.println(it.type + " insert at " + idx + " " + it.time);
        }
        else
        {
          col.last_time = it.time;
        }

        col.col.timestamps.add(idx, it.time);
        if( it.data.equals("") )
        {
          col.col.values.add(idx, new Object[] {null});
        }
        else
        {
          switch( col.col.types[0] )
          {
            case Types.BIGINT:
            case Types.NUMERIC:
              col.col.values.add(idx, new Long[] {Long.parseLong(it.data)});
              break;
            case Types.BIT:
            case Types.INTEGER:
            case Types.SMALLINT:
              col.col.values.add(idx, new Integer[] {Integer.parseInt(it.data)});
              break;
            case Types.DECIMAL:
            case Types.DOUBLE:
            case Types.FLOAT:
              col.col.values.add(idx, new Double[] {Double.parseDouble(it.data)});
              break;
            case Types.VARCHAR:
              col.col.values.add(idx, new String[] {it.data});
              break;
            default:
              throw new IllegalArgumentException("Unsupported type " + col.col.types[0]);
          }
        }
        col.col.orig.add(new boolean[] {true});

        didx++;
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
      return null;
    }

    return this;
  }

  @Override public Data getData()
  {
    Data data = new Data();
    for(TmpColumn col : cols.values())
    {
      col.col.updateFrequency();
      data.addColumn(col.col);
    }
    return data;
  }

  private static class TmpColumn {
    Column col;
    private long last_time = Long.MIN_VALUE;
  }
}
