package at.pegasos.experiment;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.pegasos.computer.ComputerBuilder;
import at.pegasos.computer.ComputerBuilderException;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.Value;
import at.pegasos.computer.Computation;
import at.pegasos.computer.Computer;
import at.pegasos.computer.providers.SessionValuesProvider;
import at.pegasos.computer.providers.UserValueProvider;
import at.pegasos.data.manager.DataManager;
import at.pegasos.data.simplify.MergeKeepNA;
import at.pegasos.data.simplify.Strategy;
import lombok.ToString;

@ToString
public abstract class BaseSimpleExperiment implements Step {

  public static final SimpleDateFormat DateFormat;
  static
  {
    DateFormat= new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
  }

  /**
   * Folders where the input data for the experiment is located
   */
  private List<String> folders= new ArrayList<String>();

  /**
   * Instances of manager to be used in the experiment for loading the data. This instance are used
   * to obtain new instances using the .getInstance() method
   */
  protected List<DataManager> managers= new ArrayList<DataManager>(5);

  protected List<DataManager> files= Collections.synchronizedList(new ArrayList<DataManager>(100));

  protected DataManager file;

  protected UserValueProvider fuvp;

  protected SessionValuesProvider svp;

  protected int sessionId;

  /**
   * User for whom the experiment is performed. Might be null depending on the experiment type
   */
  protected String userId;

  Data output;

  private boolean debugData= false;

  private boolean debugDataLoading= true;

  protected Logger logger= LoggerFactory.getLogger(BaseSimpleExperiment.class);

  /**
   * Directory where the experiment is being executed
   */
  protected final Path runDirectory;

  /**
   * Resources possibly containing resources (executable code) for this experiment
   */
  protected List<String> computationResources= new ArrayList<String>(5);

  private final List<String> sessionpkgs= new ArrayList<String>(2);

  private final List<Computation> sessioncomps;

  private final List<String> userpkgs= new ArrayList<String>(1);

  private final List<Computation> usercomps;

  /**
   * Whether sub directories should be included while searching for files
   */
  protected boolean includeSubDirectories= false;

  public BaseSimpleExperiment(Path runDirectory)
  {
    this.runDirectory= runDirectory;

    sessioncomps= new ArrayList<Computation>(5);
    /*sessioncomps.add(new Distance());
    sessioncomps.add(new WorkingTime());
    sessioncomps.add(new Duration());*/

    usercomps= new ArrayList<Computation>(5);
  }

  protected void loadFolder(String folderName)
  {
    final File folder= new File(folderName);

    if( folder.listFiles() == null )
    {
      logger.error(folderName + " does not exist or does not contain files");
      return;
    }

    logger.debug("Examining " + folderName + " for input files");

    for(final File fileEntry : folder.listFiles())
    {
      if( !fileEntry.isDirectory() )
      {
        Path path= Paths.get(fileEntry.getAbsolutePath());
        
        for(DataManager manager : managers )
        {
          // logger.debug("Examining " + path.toString() + " with " + manager);
          
          DataManager inst= manager.getInstance();
          try
          {
            if( inst.acceptFile(path.toString()) )
            {
              files.add(inst);
              break;
            }
            logger.debug("manager rejected the file {}", path.toString());
          }
          catch( DataException e )
          {
            e.printStackTrace();
          }
        }
      }
      else if( includeSubDirectories )
      {
        loadFolder(fileEntry.getAbsolutePath());
      }
    }
  }

  protected void loadFolders()
  {
    folders.parallelStream().forEach(folder -> loadFolder(folder));
  }

  /**
   * Set whether computed data should be persisted. If true data will be saved in folder debug
   * 
   * @param debugData
   *          debug status
   */
  public void setDebugData(boolean debugData)
  {
    this.debugData= debugData;
  }
  
  protected void preprocessData()
  {
    loadFolders();

    logger.debug("{} files found", files.size());
    sortFiles();

    files.parallelStream().forEach(file -> {
      try
      {
        file.processFile();
        file.normalise();
      }
      catch( DataException e )
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    });
  }

  protected void finishExperiment()
  {
    try
    {
      output.debug();
      
      Map<String, Object> params= new HashMap<String, Object>();
      
      Strategy.OnSingleColUseOnlyName(params, true);
      
      output.simplify(MergeKeepNA.class, params);
      // output= output.Col2Data();
      output.writeFileCSV("output.csv");
      
      fuvp.persist();
    }
    catch( DataException | IOException e )
    {
      System.err.println(e.getMessage());
      e.printStackTrace();
    }
  }
  
  public abstract void run();
  
  /**
   * Sort the entries by their data in ascending order
   */
  protected abstract void sortFiles();
  
  private static Map<String, String> toMap(Collection<Value> values)
  {
    Map<String, String> ret= new HashMap<String, String>(values.size());
    for(Value v : values)
    {
      ret.put(v.getName(), v.getStringValue());
    }
    return ret;
  }

  protected void addUserComputation(Computation computation)
  {
    this.usercomps.add(computation);
  }

  protected Collection<Computation> getUserComputations()
  {
    return usercomps;
  }

  public void addUserComputationPackage(String packageNameUser)
  {
    logger.info("Adding userpackage: " + packageNameUser);
    userpkgs.add(packageNameUser);
  }

  protected Collection<String> getUserComputationPackages()
  {
    return userpkgs;
  }

  protected Collection<Computation> getSessionComputations()
  {
    return sessioncomps;
  }

  public void addSessionComputation(Computation computation)
  {
    sessioncomps.add(computation);
  }

  public void addSesionComputationPackage(String packageName)
  {
    sessionpkgs.add(packageName);
  }
  
  protected Collection<String> getSessionComputationPackages()
  {
    return sessionpkgs;
  }

  protected void compute() throws DataException, ComputerBuilderException
  {
    compute(null);
  }

  protected void compute(Calendar date) throws DataException, ComputerBuilderException
  {
    Data data= file.getData();
    // System.out.println("Before computer set up");
    ComputerBuilder builder= new ComputerBuilder(data)
        .setUserValueProvider(fuvp)
        .setSessionValuesProvider(svp)
        .setUserId(userId)
        .addComputations(getSessionComputations())
        .addCompiledSources(computationResources)
        .addComputationPackages(getSessionComputationPackages());

    Computer com= builder.getComputer();
    com.setSessionId("" + sessionId);
    com.fetchUserValues();

    if( date != null )
      com.setDate(date);

    com.computeSessionMetrics();

    if( debugData )
    {
      try
      {
        Files.createDirectories(Paths.get("debug"));
        data.writeFileCSV("debug/" + file.getFilename());
      }
      catch( IOException e )
      {
        e.printStackTrace();
      }
    }

    Map<String, Object> values= new HashMap<String, Object>();
    if( date != null )
    {
      values.put("date", date.get(Calendar.YEAR) + "-" + (date.get(Calendar.MONTH)+1) + "-" + date.get(Calendar.DAY_OF_MONTH) + " " + date.get(Calendar.HOUR_OF_DAY) + ":" + date.get(Calendar.MINUTE));
    }
    values.put("file", file.getFilename());
    values.putAll(toMap(com.getComputedValues()));
    
    output.addColumnRow(sessionId, values);
  }

  /**
   * Add a folder potentially containing data
   * 
   * @param folder
   */
  public void addInputFolder(String folder)
  {
    this.folders.add(folder);
  }

  /**
   * Add a manager for loading data to the experiment
   * 
   * @param manager
   */
  public void addManager(DataManager manager)
  {
    this.managers.add(manager);
  }

  /**
   * Add a computation resource. this can be either a .jar file or a folder where .class files are
   * located.
   * 
   * @param resource
   */
  public void addComputationResource(String resource)
  {
    this.computationResources.add(resource); 
  }
  
  @Override
  public void setLogger(Logger logger)
  {
    this.logger= logger;
  }
  
  public void forceLoggerOnSub(boolean force)
  {
    // TODO
  }

  /**
   * Set whether subfolders of input folders should be scanned as well (default: false)
   * @param includeSub
   */
  public void setIncludeSubDirectories(boolean includeSub)
  {
    this.includeSubDirectories= includeSub;
  }

  public Data getResult()
  {
    return output;
  }

  public UserValueProvider getUserValueProvider()
  {
    return fuvp;
  }
}
