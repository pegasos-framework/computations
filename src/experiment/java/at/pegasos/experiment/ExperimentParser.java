package at.pegasos.experiment;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.*;
import java.util.*;

import org.slf4j.*;
import org.yaml.snakeyaml.Yaml;

import com.beust.jcommander.JCommander;

import at.pegasos.data.manager.DataManager;
import at.pegasos.experiment.steps.JarStep;
import at.pegasos.formula.Formula2Class;

public class ExperimentParser {
  private final Logger log= LoggerFactory.getLogger(ExperimentParser.class);

  /**
   * Default folder to be used as input if no 'folder' statement was specified for an experiment
   */
  private static final String DEFAULT_DATA_FOLDER= "data";

  /**
   * Parent directory of the experiment
   */
  private Path runDirectory;

  private Map<String, Object> rawSteps;

  /**
   * The parse steps in order
   */
  private final List<Step> steps;

  private final File inputFile;

  public ExperimentParser(File inputFile)
  {
    this.inputFile= inputFile;
    steps= new ArrayList<Step>();
  }

  public boolean parse() throws FileNotFoundException, ExperimentException
  {
    runDirectory= Paths.get(inputFile.toString()).toAbsolutePath().getParent();
    if( loadFile() )
    {
      return parseSteps();
    }
    else
      return false;
  }

  @SuppressWarnings("unchecked")
  private boolean loadFile() throws FileNotFoundException, ExperimentException
  {
    Yaml yaml= new Yaml();
    InputStream inputStream= new FileInputStream(inputFile);

    runDirectory= Paths.get(inputFile.toString()).toAbsolutePath().getParent();

    rawSteps= null;
    try
    {
      Object o= yaml.load(inputStream);
      if( o instanceof Map )
      {
        rawSteps= (Map<String, Object>) o;
      }
      else
      {
        log.error("Malformatted experiment specification {}", inputFile);
        throw new ExperimentException("Malformatted experiment specification");
      }
    }
    catch( Exception e )
    {
      log.error(e.getMessage());
      e.printStackTrace();

      if( e instanceof ExperimentException )
        throw e;

      return false;
    }

    if( rawSteps == null )
      return false;

    log.debug("{}", rawSteps.toString());

    return true;

//    log.debug(rawSteps.get("steps").getClass().toString());
  }

  private boolean parseSteps()
  {
    if( !rawSteps.containsKey("steps") )
    {
      log.error("Steps definition missing");
      return false;
    }

    if( !(rawSteps.get("steps") instanceof Iterable<?>) )
    {
      log.error("There is something wrong with your step definition.");
      log.error("Steps are required to be a list");
      return false;
    }

    @SuppressWarnings("unchecked")
    Iterable<String> steps= (Iterable<String>) rawSteps.get("steps");
    for(String step : steps)
    {
      // first determine if step is simple
      String parts[]= step.split(" ");
      if( parts.length > 1 )
      {
        if( parts[0].equals("parse") )
        {
          String[] args= new String[parts.length - 1];
          System.arraycopy(parts, 1, args, 0, args.length);
          log.debug(Arrays.toString(args));
          Formula2Class.Parameters p= new Formula2Class.Parameters();
          new JCommander(p, args);

          // runDirectory is already an absolute path so we can use it directly
          p.basedir= runDirectory.toString();

          Step c= new ParseAndCompileStep(p);
          this.steps.add(c);
        }
        else if( parts[0].equals("jar") )
        {
          String[] args= new String[parts.length - 1];
          System.arraycopy(parts, 1, args, 0, args.length);
          parseJarStep(args);
        }
        else
        {
          log.error("Unkown step " + parts[0]);
          System.err.println("Unkown step " + parts[0]);
          return false;
        }
      }
      else
      {
        if( step.equals("parse") )
        {
          Formula2Class.Parameters p = new Formula2Class.Parameters();
          p.inputfile = "formulas.txt";

          // runDirectory is already an absolute path so we can use it directly
          p.basedir= runDirectory.toString();

          Step c = new ParseAndCompileStep(p);
          this.steps.add(c);
        }
        else if( step.equals("jar") )
        {
          parseJarStep(new String[] {"formulas.jar"});
        }
        else if( !rawSteps.containsKey(step) )
        {
          log.error("Missing defintion of step " + step);
          log.error(step + " referenced in step list but no definition of step found");
          return false;
        }
        else
        {
          parseExperimentStepDefinition(step, rawSteps);
        }
      }
    }
    return true;
  }


  /**
   * Parse the long specification of a step
   *
   * @param step
   *          name of the step
   * @param obj
   *          the complete parse experiment specification
   */
  private void parseExperimentStepDefinition(String step, Map<String, Object> obj)
  {
    @SuppressWarnings("unchecked")
    Map<String, Object> def= (Map<String, Object>) obj.get(step);

    String type;
    if( def == null || !def.containsKey("type") )
    {
      // System.err.println("Error parsing " + step + " 'type' is required");
      type= "simple";
    }
    else
    {
      type= def.get("type").toString().toLowerCase();
    }

    BaseSimpleExperiment s= null;
    if( type.equals("simple") )
    {
      log.debug("simple experiment");

      // SingleUserExperiment s= new SingleUserExperiment();
      s= new SimpleExperiment(runDirectory);
    }
    else if( type.equals("singleuser") )
    {
      log.debug("single user experiment");
      s= new SingleUserExperiment(runDirectory);
    }
    else if( type.equals("multiuser") )
    {
      log.debug("multiuser experiment");
      s= new MultiUserExperiment(runDirectory);

      if( def.containsKey("pattern") )
      {
        ((MultiUserExperiment) s).setPattern(def.get("pattern").toString());
      }
    }

    if( s != null )
    {
      // try to get a parse or parse and compile step so that we can determine what packages we can
      // use
      int i= steps.size() - 1;
      if( i >= 0 && steps.get(i) instanceof ParserStepInterface )
      {
        ParserStepInterface p= (ParserStepInterface) steps.get(i);
        s.addSesionComputationPackage(p.getParserParameters().packageName);
        s.addUserComputationPackage(p.getParserParameters().packageNameUser);
        s.addComputationResource(resolvePath(p.getParserParameters().classdir));
      }

      // TODO: if no previous parsing step was found revert to default settings for packages
      // sessionpkgs.add("at.test.calculations");
      // userpkgs.add("at.test.usercalculations");

      if( def != null && def.containsKey("folder") )
      {
        Path p= Paths.get(def.get("folder").toString());
        if( p.isAbsolute() )
        {
          s.addInputFolder(p.toString());
        }
        else
        {
          s.addInputFolder(runDirectory.resolve(p).toString());
        }
      }
      else
        s.addInputFolder(runDirectory.resolve(DEFAULT_DATA_FOLDER).toString());

      if( def != null && def.containsKey("folders") )
      {
        @SuppressWarnings("unchecked")
        Iterable<String> folders= (Iterable<String>) def.get("folders");
        for(String folder : folders)
        {
          s.addInputFolder(folder);
        }
      }

      if( def != null && def.containsKey("subfolders") )
      {
        s.setIncludeSubDirectories(Boolean.parseBoolean(def.get("subfolders").toString()));
      }

      if( def != null && !def.containsKey("manager") && !def.containsKey("managers") )
      {
        // TODO: report error and fail
      }

      if( def != null && def.containsKey("manager") )
      {
        String manager = def.get("manager").toString();
        setManager(s, manager);
      }

      this.steps.add(s);
    }
    else
    {
      log.error("Unknown experiment type " + type);
    }
  }

  private void parseJarStep(String[] args)
  {
    if( args.length == 1 )
    {
      int i= steps.size() - 1;
      if( i >= 0 && steps.get(i) instanceof ParserStepInterface )
      {
        ParserStepInterface p= (ParserStepInterface) steps.get(i);

        JarStep jar= new JarStep();

        jar.setInputDirectory(Paths.get(p.getParserParameters().basedir).resolve(p.getParserParameters().classdir));
        jar.addPackage(p.getParserParameters().packageName);
        jar.addPackage(p.getParserParameters().packageNameUser);
        jar.setOutputName(Paths.get(p.getParserParameters().basedir).resolve(args[0]).toString());

        this.steps.add(jar);
      }
      else
      {
        // TODO: failure
      }
    }
    else
    {
      JarStep.Parameters p= new JarStep.Parameters();
      new JCommander(p, args);
      
      JarStep jar= new JarStep();
      jar.setParameters(p);

      this.steps.add(jar);
    }
  }

  /**
   * Resolve a path. The path will be resolved in relation to the experiment specifications
   * location. If the path is absolute it will be treated as an absolute path. If it is relative it
   * will be resolve in relation to the directory of the experiment configuration
   * 
   * @param path
   *          path to be resolved
   * @return resolved path
   */
  private String resolvePath(String path)
  {
    Path p= Paths.get(path);
    if( p.isAbsolute() )
    {
      return p.toAbsolutePath().toString();
    }
    else
    {
      return runDirectory.resolve(p).toString();
    }
  }

  private void setManager(BaseSimpleExperiment s, String manager)
  {
    if( !trySetManager(s, "at.pegasos.data.manager." + manager) )
    {
      trySetManager(s, manager);
    }
  }

  private boolean trySetManager(BaseSimpleExperiment s, String manager)
  {
    try
    {
      Class<?> c= Class.forName("at.pegasos.data.manager." + manager);
      Object o= c.getConstructor().newInstance();
      if( o instanceof DataManager )
      {
        s.addManager((DataManager) o);
        return true;
      }
      else
      {
        // TODO: fail
      }
    }
    catch( ClassNotFoundException e )
    {
      // TODO Auto-generated catch block
      // e.printStackTrace();
      return false;
    }
    catch( InstantiationException | IllegalAccessException e )
    {
      // TODO Auto-generated catch block. could not instantiate manager
      e.printStackTrace();
    }
    catch( IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return false;
  }

  public List<Step> getSteps()
  {
    return steps;
  }
}
