package at.pegasos.experiment;

import java.nio.file.Path;
import java.util.Comparator;

import at.pegasos.computer.*;
import at.pegasos.computer.providers.*;
import at.pegasos.data.*;
import at.pegasos.data.manager.DataManager;
import lombok.ToString;

@ToString(callSuper=true)
public class SimpleExperiment extends BaseSimpleExperiment {
  
  public SimpleExperiment(Path runDirectory)
  {
    super(runDirectory);

    fuvp= new DummyUserValuesProvider();
  }
  
  public void run()
  {
    preprocessData();
    
    sessionId= 1;
    
    output= new Data();
    
    // fuvp= FileUserValuesProvider.getInstance(userId);
    svp= SimpleSessionValuesProvider.getInstance();
    
    for(DataManager file : files)
    {
      try
      {
        this.file= file;
        logger.info("Starting experiment for " + file.getFilename());
        
        compute();        
        
        sessionId++;
      }
      catch( ComputerBuilderException | DataException e )
      {
        logger.error(e.getMessage());
        e.printStackTrace();
      }
    }
   
    finishExperiment();
    
    logger.info("finished");
  }
  
  /**
   * Sort the entries by their data in ascending order
   */
  protected void sortFiles()
  {
    files.sort(new Comparator<DataManager>() {
      
      @Override
      public int compare(DataManager o1, DataManager o2)
      {
        return o1.getFilename().compareTo(o2.getFilename());
      }
    });
  }
}
