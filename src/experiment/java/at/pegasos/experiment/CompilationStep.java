package at.pegasos.experiment;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.Diagnostic.Kind;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import at.pegasos.parser.OutputStreamLogger;
import lombok.ToString;

@ToString
public class CompilationStep implements Step {

  private Logger logger= LoggerFactory.getLogger(CompilationStep.class);

  /**
   * Output of the compile task
   */
  private Writer compilerOutput= null;

  private List<String> javaFiles= new ArrayList<String>();

  private boolean error;

  public static void main(String[] args) throws ExperimentException
  {
    new CompilationStep().run();
  }

  public void setInput(ParseStep input)
  {
    this.javaFiles= (List<String>) input.getGeneratedFiles();
  }

  public void setInput(List<String> input)
  {
    this.javaFiles= input;
  }

  public void run() throws ExperimentException
  {
    // Get an instance of the compiler
    JavaCompiler compiler= ToolProvider.getSystemJavaCompiler();

    // Setup the file manager
    StandardJavaFileManager fileManager= compiler.getStandardFileManager(null, null, null);

    Iterable<? extends JavaFileObject> units;
    units= fileManager.getJavaFileObjectsFromStrings(javaFiles);

    // Derive the classpath
    List<String> options= Arrays.asList("-classpath", System.getProperty("java.class.path"));
    // System.out.println(System.getProperty("java.class.path"));

    boolean finished= false;
    error= false;
    try
    {
      // compile
      CompilationTask task= compiler.getTask(compilerOutput, fileManager, new DiagnosticListener<JavaFileObject>() {

        @Override
        public void report(Diagnostic<? extends JavaFileObject> diagnostic)
        {
          logger.error(diagnostic.toString());
          if( diagnostic.getKind() == Kind.ERROR )
          {
            error= true;
          }
        }
      }, options, null, units);
      task.call();
      finished= true;
    }
    catch( Exception e )
    {
      throw new ExperimentException(e);
    }

    // Cleanup
    try
    {
      fileManager.close();
    }
    catch( IOException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    if( error || !finished )
    {
      logger.error("Compilation faild");
      throw new ExperimentException("Compilation failed");
    }
  }

  @Override
  public void setLogger(Logger logger)
  {
    this.logger= logger;
  }

  @Override
  public void forceLoggerOnSub(boolean force)
  {
    if( true )
    {
      compilerOutput= new PrintWriter(new OutputStreamLogger(logger, Level.INFO));
    }
  }
}
