package at.pegasos.experiment;

public class ExperimentException extends Exception {

  public ExperimentException(Exception e)
  {
    super(e);
  }

  public ExperimentException()
  {
    super();
  }

  public ExperimentException(String message)
  {
    super(message);
  }

  private static final long serialVersionUID= -2803521053880458314L;

}
