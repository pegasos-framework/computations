package at.pegasos.experiment.steps;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.jar.*;

import org.slf4j.*;

import com.beust.jcommander.*;

import at.pegasos.experiment.*;

public class JarStep implements Step {
  
  public static class Parameters {
    @Parameter(names= "--out")
    public String output;

    @Parameter()
    public List<String> main;
    
    @Parameter(names= "--indir")
    public String baseDir;

    @Parameter(names= "--package")
    public List<String> packageNames;
  }

  private Logger logger= LoggerFactory.getLogger(JarStep.class);

  // private boolean force= false;

  private String outname;

  private Path basedir;

  private final List<String> packages= new ArrayList<String>();

  private JarOutputStream jarOut;
  
  public static void main(String[] args) throws ExperimentException
  {
    Parameters p= new Parameters();
    new JCommander(p, args);
    
    JarStep jarstep= new JarStep();
    jarstep.setParameters(p);
    jarstep.run();
  }

  public void setParameters(Parameters p)
  {
    if( p.output != null )
      setOutputName(p.output);
    else
      setOutputName(p.main.get(0));
    
    if( p.packageNames != null )
    {
      for(String pkg : p.packageNames)
      {
        addPackage(pkg);
      }
    }
    
    if( p.baseDir != null )
      basedir= Paths.get(p.baseDir);
  }

  @Override
  public void setLogger(Logger logger)
  {
    this.logger= logger;
  }

  @Override
  public void forceLoggerOnSub(boolean force)
  {
    // this.force= force;
  }

  @Override
  public void run() throws ExperimentException
  {
    logger.debug("Running jar step {}:{}", basedir, outname);
    
    Path out= basedir.resolve(outname);
    if( Files.exists(out) )
    {
      logger.debug("{} exists --> delete", out);
      try
      {
        Files.delete(out);
      }
      catch( IOException e )
      {
        e.printStackTrace();
        throw new ExperimentException(e);
      }
    }
    
    File outFile= new File(outname);

    try
    {
      BufferedOutputStream bo= new BufferedOutputStream(new FileOutputStream(outFile));

      Manifest manifest = new Manifest();
      manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
      jarOut= new JarOutputStream(bo, manifest);
      for(String pkg : packages)
      {
        addPackageToJar(pkg);
      }

      jarOut.close();
      bo.close();
    }
    catch( IOException e )
    {
      e.printStackTrace();
      throw new ExperimentException(e);
    }
  }

  private void addPackageToJar(String pkg) throws IOException
  {
    String dir= pkg.replaceAll("[.]", "/");
    logger.debug("addPackageToJar: {} = {}", pkg, dir);

    final File folder= basedir.resolve(dir).normalize().toAbsolutePath().toFile();
    int idx= basedir.normalize().toAbsolutePath().toString().length();
    logger.debug("{} {}", folder, idx);

    if( folder.listFiles() == null )
    {
      logger.warn("{} does not exist or does not contain files", folder);
      return;
    }

    String name= folder.getPath();
    if( !name.endsWith("/") )
      name+= "/";
    JarEntry entry= new JarEntry(name);
    entry.setTime(folder.lastModified());
    jarOut.putNextEntry(entry);
    jarOut.closeEntry();

    for(final File fileEntry : folder.listFiles())
    {
      if( !fileEntry.isDirectory() )
      {
        Path path= Paths.get(fileEntry.getAbsolutePath());

        BufferedInputStream bi= new BufferedInputStream(new FileInputStream(path.toFile()));

        logger.debug("{}", fileEntry.toString().substring(idx+1));
        JarEntry je= new JarEntry(fileEntry.toString().substring(idx+1));
        je.setTime(fileEntry.lastModified());
        jarOut.putNextEntry(je);

        byte[] buf= new byte[1024];
        int anz;

        while( (anz= bi.read(buf)) != -1 )
        {
          jarOut.write(buf, 0, anz);
        }
        jarOut.closeEntry();

        bi.close();
      }
    }

    jarOut.closeEntry();
  }

  public void setInputDirectory(Path path)
  {
    this.basedir= path;
  }

  public void setOutputName(String outname)
  {
    this.outname= outname;
  }

  public String getOutputName()
  {
    return this.outname;
  }

  public void addPackage(String packageName)
  {
    this.packages.add(packageName);
  }

}
