package at.pegasos.experiment;

import at.pegasos.parser.OutputInterface;

public interface Step extends OutputInterface {
  // public void setInput(Object input);
  
  // public Object getOutput();
  
  public void run() throws ExperimentException;
}
