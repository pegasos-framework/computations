package at.pegasos.experiment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.pegasos.formula.Formula2Class;
import at.pegasos.formula.Formula2Class.Parameters;
import lombok.ToString;

@ToString
public class ParseStep implements Step {

  private List<File> inputFiles= new ArrayList<File>();

  private List<String> generatedFiles;

  private Formula2Class parser;

  private final Parameters parameters;

  private Logger logger= LoggerFactory.getLogger(ParseStep.class);

  private boolean force= false;

  public ParseStep(Parameters p)
  {
    this.parameters= p;

    generatedFiles= new ArrayList<String>(100);

    parser= new Formula2Class(p);
  }

  public void run() throws ExperimentException
  {
    try
    {
      if( force )
      {
        parser.setLogger(logger);
        parser.forceLoggerOnSub(force);
      }
      parser.run();
      generatedFiles.addAll(parser.getOutputFiles());
    }
    catch( Exception e )
    {
      e.printStackTrace();
      throw new ExperimentException(e);
    }
  }

  public List<String> getGeneratedFiles()
  {
    return generatedFiles;
  }

  public Parameters getParameters()
  {
    return parameters;
  }

  @Override
  public void setLogger(Logger logger)
  {
    this.logger= logger;
  }

  @Override
  public void forceLoggerOnSub(boolean force)
  {
    this.force= force;
  }
}
