package at.pegasos.experiment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.pegasos.formula.Formula2Class.Parameters;
import lombok.ToString;

@ToString
public class ParseAndCompileStep implements Step, ParserStepInterface {

  private Parameters params;

  private Logger logger= LoggerFactory.getLogger(ParseAndCompileStep.class);

  private boolean force= false;

  public ParseAndCompileStep(Parameters p)
  {
    this.params= p;
  }

  public Parameters getParserParameters()
  {
    return params;
  }

  public void run() throws ExperimentException
  {
    try
    {
      ParseStep s= new ParseStep(params);
      if( force )
      {
        s.setLogger(logger);
        s.forceLoggerOnSub(force);
      }
      s.run();

      CompilationStep c= new CompilationStep();
      c.setInput(s);
      if( force )
      {
        c.setLogger(logger);
        c.forceLoggerOnSub(force);
      }
      c.run();
    }
    catch( Exception e )
    {
      e.printStackTrace();
      throw new ExperimentException(e);
    }
  }

  @Override
  public void setLogger(Logger logger)
  {
    this.logger= logger;
  }

  @Override
  public void forceLoggerOnSub(boolean force)
  {
    this.force= force;
  }
}
