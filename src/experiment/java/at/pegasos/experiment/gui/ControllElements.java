package at.pegasos.experiment.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;

import org.slf4j.LoggerFactory;

import at.pegasos.experiment.ExperimentException;
import at.pegasos.experiment.GUI;
import at.pegasos.experiment.Runner;
import at.pegasos.experiment.Step;
import at.pegasos.experiment.Runner.RunnerStateListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

public class ControllElements extends HBox {

  /**
   * 
   */
  private final GUI gui;
  private TextField fileName;
  private Button btnChoose;
  private Button btnRun;

  /**
   * Last file user selected. This is used to open same directory / file again
   */
  private File lastFile= null;

  public ControllElements(GUI gui)
  {
    super();
    this.gui= gui;

    final FileChooser fileChooser= new FileChooser();

    fileName= new TextField();

    btnChoose= new Button("Choose");
    btnRun= new Button("Run");

    btnRun.setDisable(true);

    btnChoose.setOnAction(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event)
      {
        if( lastFile != null )
        {
          fileChooser.setInitialDirectory(Paths.get(lastFile.getAbsolutePath()).getParent().toFile());
        }
        else
          fileChooser.setInitialFileName("experiment.yaml");
        File file= fileChooser.showOpenDialog(ControllElements.this.gui.primaryStage);
        if( file != null )
        {
          setFile(file);
        }
      }
    });

    btnRun.setOnAction(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event)
      {
        (new Thread() {
          @Override
          public void run()
          {
            ControllElements.this.gui.runner.addStateListener(new RunnerStateListener() {

              @Override
              public void OnStepScheduled(Step step, Runner reference)
              {
                reference.setStepLogger(LoggerFactory.getLogger(step.getClass().getName()));
              }

              @Override
              public void OnStepCompleted(Step step, Runner reference)
              {
                // TODO Auto-generated method stub

              }
            });
            ControllElements.this.gui.runner.run();
          }
        }).start();
      }

    });
    
    Button clear= new Button("Clear");
    clear.setOnAction(event -> {
      ControllElements.this.gui.getOutput().clear();
    });

    getChildren().addAll(new Label("File: "), fileName, btnChoose, btnRun, clear);
    setSpacing(5);
  }

  public void setFile(File file)
  {
    fileName.setText(file.getAbsolutePath());
    lastFile= file;

    try
    {
      gui.runner.setFile(file);
      gui.runner.parseSteps();
      btnRun.setDisable(false);
    }
    catch( FileNotFoundException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch( ExperimentException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}