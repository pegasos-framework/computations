package at.pegasos.experiment.gui;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.spi.LoggingEvent;

import at.pegasos.experiment.GUI;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

public class ExperimentOutput extends VBox {

  private class TextAreaAppender extends WriterAppender {

    /**
     * Format and then append the loggingEvent to the stored TextArea.
     *
     * @param loggingEvent
     */
    @Override
    public void append(final LoggingEvent loggingEvent)
    {
      final String message= this.layout.format(loggingEvent);

      // Append formatted message to text area using the Thread.
      try
      {
        Platform.runLater(new Runnable() {
          @Override
          public void run()
          {
            // loggingEvent.getLevel();

            try
            {
              /*if( output.getText().length() == 0 )
              {
                output.setText(message);
              }
              else
              {
                output.selectEnd();
                output.insertText(output.getText().length(), message);
              }*/
              Text text= new Text(message);
              // text.setWrappingWidth(scroll.getWidth()-5);
              // text.setWrappingWidth(20);
              // text.getStyleClass().add("fancytext");
              // text.setStyle("-fx-font: 100px Tahoma");
              // out.setPrefWidth();
              out.getChildren().add(text);
              // out.layout();
              scroll.setVvalue(1.0);
            }
            catch( final Throwable t )
            {
              System.out.println("Unable to append log to text area: " + t.getMessage());
            }
          }
        });
      }
      catch( final IllegalStateException e )
      {
        // ignore case when the platform hasn't yet been initialised
      }
    }

    public void clear()
    {
      out.getChildren().clear();
    }
  }

  // private TextArea output;

  private TextFlow out;

  private TextAreaAppender appender;

  private ScrollPane scroll;

  public ExperimentOutput(GUI gui)
  {
    Label lblOutput= new Label("Output:");
    getChildren().add(lblOutput);

    /*output= new TextArea();
    output.setEditable(false);
    output.setWrapText(true);*/

    out= new TextFlow();
    out.setTextAlignment(TextAlignment.LEFT);

    // Set Hgrow for TextField
    // HBox.setHgrow(output, Priority.ALWAYS);
    HBox.setHgrow(out, Priority.ALWAYS);
    // getChildren().add(output);
    scroll= new ScrollPane();
    HBox.setHgrow(scroll, Priority.ALWAYS);
    VBox.setVgrow(scroll, Priority.ALWAYS);
    scroll.setContent(out);
    scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
    getChildren().add(scroll);

    widthProperty().addListener((obs, oldVal, newVal) -> {
      // Do whatever you want
      out.setPrefWidth(newVal.doubleValue() - 10);
    });

    // Define log pattern layout
    PatternLayout layout= new PatternLayout("%c{1} - %m%n");

    // Add console appender to root logger
    appender= new TextAreaAppender();
    appender.setLayout(layout);
    appender.setThreshold(Level.INFO);
    Logger.getRootLogger().addAppender(appender);
  }
  
  public void clear()
  {
    appender.clear();
  }
}