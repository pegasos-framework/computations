package at.pegasos.experiment;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.*;

public class Runner {

  public interface RunnerStateListener {
    public void OnStepCompleted(Step step, Runner reference);

    public void OnStepScheduled(Step step, Runner reference);
  }

  private class StateListeneres {
    private List<RunnerStateListener> listeners= new ArrayList<RunnerStateListener>(2);

    public void addListener(RunnerStateListener listener)
    {
      listeners.add(listener);
    }

    public void OnStepCompleted(Step step)
    {
      for(RunnerStateListener listener : listeners)
      {
        listener.OnStepCompleted(step, Runner.this);
      }
    }

    public void OnStepScheduled(Step step)
    {
      for(RunnerStateListener listener : listeners)
      {
        listener.OnStepScheduled(step, Runner.this);
      }
    }
  }

  private Logger log= LoggerFactory.getLogger(Runner.class);
  private Logger childlogger;

  private List<Step> steps;

  /**
   * Parent directory of the experiment
   */
  private Path runDirectory;

  private StateListeneres listeners;
  private File inputFile;

  public Runner()
  {
    listeners= new StateListeneres();
  }

  public void parseSteps() throws FileNotFoundException, ExperimentException
  {
    ExperimentParser parser= new ExperimentParser(inputFile);
    parser.parse();
    steps= parser.getSteps();
  }

  /**
   * Resolve a path. The path will be resolved in relation to the experiment specifications
   * location. If the path is absolute it will be treated as an absolute path. If it is relative it
   * will be resolve in relation to the directory of the experiment configuration
   * 
   * @param path
   *          path to be resolved
   * @return resolved path
   */
  private String resolvePath(String path)
  {
    Path p= Paths.get(path);
    if( p.isAbsolute() )
    {
      return p.toAbsolutePath().toString();
    }
    else
    {
      return runDirectory.resolve(p).toString();
    }
  }

  /**
   * Attempts to clean all output directories of the steps in the pipeline
   */
  private void cleanTemp()
  {
    Set<String> dirs= new HashSet<String>();
    for(Step step : steps)
    {
      if( step instanceof ParserStepInterface )
      {
        dirs.add(resolvePath(((ParserStepInterface) step).getParserParameters().classdir));
      }
    }
  }

  public void run()
  {
    // TODO: it might be a good idea to move this outside of run
    cleanTemp();

    try
    {
      for(Step step : steps)
      {
        log.info("Running " + step);
        listeners.OnStepScheduled(step);
        if( childlogger != null )
        {
          step.setLogger(childlogger);
          step.forceLoggerOnSub(true);
        }
        step.run();
        listeners.OnStepCompleted(step);
      }
    }
    catch( ExperimentException e )
    {
      log.error("Running failed: " + e.getMessage());
      // TODO:? log.error(e.getStackTrace());
    }
  }

  public void addStateListener(RunnerStateListener listener)
  {
    this.listeners.addListener(listener);
  }

  public static void main(String[] args) throws FileNotFoundException, ExperimentException
  {
    Runner e= new Runner();
    if( args.length == 0 )
      e.setFile(new File("experiment.yml"));
    else
      e.setFile(new File(args[0]));

    // Set up a simple configuration that logs on the console.
    BasicConfigurator.configure();

    e.parseSteps();
    e.run();
  }
  
  public void setFile(File file)
  {
    runDirectory= Paths.get(file.toString()).toAbsolutePath().getParent();
    inputFile= file;
  }

  public void setRunDirectory(Path dir)
  {
    this.runDirectory = dir;
  }

  public void setStepLogger(Logger logger)
  {
    childlogger= logger;
  }

  public List<Step> getSteps()
  {
    return steps;
  }

  public void setSteps(List<Step> steps)
  {
    this.steps = steps;
  }
}
