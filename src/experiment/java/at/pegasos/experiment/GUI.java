package at.pegasos.experiment;

import at.pegasos.experiment.gui.ControllElements;
import at.pegasos.experiment.gui.ExperimentOutput;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GUI extends Application {

  public Stage primaryStage;

  public Runner runner;

  private ControllElements controllElements;

  private ExperimentOutput output;

  /**
   * @param args
   *          the command line arguments
   */
  public static void main(String[] args)
  {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception
  {
    this.primaryStage= primaryStage;
    this.runner= new Runner();

    VBox dragTarget= new VBox();

    output= new ExperimentOutput(this);
    controllElements= new ControllElements(this);

    VBox.setVgrow(output, Priority.ALWAYS);

    dragTarget.getChildren().addAll(controllElements, output);
    dragTarget.setOnDragOver(new EventHandler<DragEvent>() {

      @Override
      public void handle(DragEvent event)
      {
        if( event.getGestureSource() != dragTarget && event.getDragboard().hasFiles() )
        {
          /* allow for both copying and moving, whatever user chooses */
          event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
      }
    });

    dragTarget.setOnDragDropped(new EventHandler<DragEvent>() {

      @Override
      public void handle(DragEvent event)
      {
        Dragboard db= event.getDragboard();
        boolean success= false;
        if( db.hasFiles() )
        {
          // dropped.setText(db.getFiles().toString());
          controllElements.setFile(db.getFiles().get(0));
          success= true;
        }
        /*
         * let the source know whether the string was successfully transferred and used
         */
        event.setDropCompleted(success);

        event.consume();
      }
    });

    StackPane root= new StackPane();
    root.getChildren().add(dragTarget);

    dragTarget.setPadding(new Insets(10));

    Scene scene= new Scene(root, 640, 480);

    primaryStage.setTitle("Calculator - Experiment Runner");
    primaryStage.setScene(scene);
    primaryStage.setMaximized(true);
    primaryStage.show();
  }

  public ExperimentOutput getOutput()
  {
    return output;
  }
}
