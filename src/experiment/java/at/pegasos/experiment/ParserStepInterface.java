package at.pegasos.experiment;

import at.pegasos.formula.Formula2Class.Parameters;

public interface ParserStepInterface {

  public Parameters getParserParameters();
}
