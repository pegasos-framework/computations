package at.pegasos.experiment;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import at.pegasos.computer.ComputerBuilder;
import at.pegasos.computer.ComputerBuilderException;
import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.computer.Computer;
import at.pegasos.data.manager.DataManager;
import at.pegasos.data.manager.DataManager.NoStarttimeException;
import at.pegasos.data.simplify.MergeKeepNA;
import at.pegasos.data.simplify.Strategy;
import lombok.ToString;

@ToString(callSuper= true)
public class SingleUserExperiment extends BaseSimpleExperiment {

  private Calendar curdate;

  private int sessionId;

  /**
   * User for whom the experiment is performed
   */
  private String userId;

  public SingleUserExperiment(Path runDirectory)
  {
    super(runDirectory);
  }

  public void run()
  {
    preprocessData();

    sessionId= 1;

    output= new Data();

    fuvp= FileUserValuesProvider.getInstance(runDirectory, userId);
    svp= SimpleSessionValuesProvider.getInstance();

    // files= files.subList(files.size()-2, files.size());
    curdate= Calendar.getInstance();
    curdate.clear();
    try
    {
      curdate.setTimeInMillis(files.get(0).getStartTime());
      curdate.set(Calendar.HOUR_OF_DAY, 23);
      curdate.set(Calendar.MINUTE, 59);
      curdate.set(Calendar.SECOND, 59);
      curdate.add(Calendar.DAY_OF_YEAR, -1);
    }
    catch( NoStarttimeException e )
    {
      logger.error("Could not determine starttime of experiment. Starting with " + curdate);
    }

    Calendar c= Calendar.getInstance();

    for(DataManager file : files)
    {
      try
      {
        this.file= file;
        logger.info("Starting experiment for " + file.getFilename());

        c.setTimeInMillis(file.getStartTime());

        checkDate(c);

        compute(c);

        sessionId++;
      }
      catch( ComputerBuilderException | DataException | NoStarttimeException e )
      {
        logger.error(e.getMessage());
        e.printStackTrace();
      }
    }

    checkDate(c);
    c.add(Calendar.DAY_OF_YEAR, 1);
    checkDate(c);

    try
    {
      output.debug();

      Map<String, Object> params= new HashMap<String, Object>();

      Strategy.OnSingleColUseOnlyName(params, true);

      output.simplify(MergeKeepNA.class, params);
      // output= output.Col2Data();
      output.writeFileCSV("output.csv");

      fuvp.persist();
    }
    catch( DataException | IOException e )
    {
      logger.error(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Sort the entries by their data in ascending order
   */
  protected void sortFiles()
  {
    files.sort(new Comparator<DataManager>() {

      @Override
      public int compare(DataManager o1, DataManager o2)
      {
        try
        {
          return ((Long) o1.getStartTime()).compareTo(o2.getStartTime());
        }
        catch( NoStarttimeException e )
        {
          logger.error("Could not determine starttime of " + o1 + " or " + o2 + ". Operation not supported.");
          e.printStackTrace();
          return 0;
        }
      }
    });
  }

  private void checkDate(Calendar c)
  {
    while( curdate.get(Calendar.YEAR) < c.get(Calendar.YEAR) || curdate.get(Calendar.YEAR) == c.get(Calendar.YEAR)
        && curdate.get(Calendar.DAY_OF_YEAR) < c.get(Calendar.DAY_OF_YEAR) )
    {
      // System.out.println("Computing daily values " + curdate.get(Calendar.YEAR) + "-" + (curdate.get(Calendar.MONTH)+1) + "-" + curdate.get(Calendar.DAY_OF_MONTH));
      logger.info("Computing daily values " + DateFormat.format(curdate.getTime()));
      ComputerBuilder builder= new ComputerBuilder()
          .setUserValueProvider(fuvp)
          .setSessionValuesProvider(svp)
          .setDate(curdate)
          .addComputations(getUserComputations())
          .addCompiledSources(computationResources)
          .addComputationPackages(getUserComputationPackages());

      Computer com= builder.getComputerUser();
      com.computeUserMetrics();

      curdate.add(Calendar.DAY_OF_YEAR, 1);
    }
  }
}
