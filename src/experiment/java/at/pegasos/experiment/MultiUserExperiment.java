package at.pegasos.experiment;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.LoggerFactory;

import at.pegasos.computer.ComputerBuilder;
import at.pegasos.computer.ComputerBuilderException;
import at.pegasos.computer.providers.FileMultiUserValuesProvider;
import at.pegasos.computer.providers.MultiUserSessionValuesProvider;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.computer.Computer;
import at.pegasos.data.manager.DataManager;
import at.pegasos.data.manager.DataManager.NoPersonInfoException;
import at.pegasos.data.manager.DataManager.NoStarttimeException;
import at.pegasos.data.simplify.MergeKeepNA;
import at.pegasos.data.simplify.Strategy;
import lombok.ToString;

@ToString(callSuper=true)
public class MultiUserExperiment extends BaseSimpleExperiment {

  private Calendar curdate;

  private Set<String> persons;
  
  private Pattern pattern;

  /**
   * Whether the pattern for the matching files contains info on how to extract the date If true the
   * pattern will be used for determining the date. Else the manager will be used for getting the
   * date of a file
   */
  private boolean patternWithDate= false;

  /**
   * Whether the pattern for the matchin files contains info on hwo to extract the person. If true
   * the pattern will be used for determining the persone. Else the manager will be used for getting
   * the person
   */
  private boolean patternWithPerson= false;

  public MultiUserExperiment(Path location)
  {
    super(location);
    logger= LoggerFactory.getLogger(MultiUserExperiment.class);
    this.persons= new HashSet<String>();
  }

  public void run()
  {
    preprocessData();
    
    determinePersons();

    sessionId= 1;

    output= new Data();

    fuvp= new FileMultiUserValuesProvider();
    svp= new MultiUserSessionValuesProvider();

    curdate= Calendar.getInstance();
    curdate.clear();
    curdate.setTimeZone(TimeZone.getTimeZone("GMT-0"));
    try
    {
      long date= getDate(files.get(0));
      curdate.setTimeInMillis(date);
      curdate.set(Calendar.HOUR_OF_DAY, 23);
      curdate.set(Calendar.MINUTE, 59);
      curdate.set(Calendar.SECOND, 59);
      logger.info("Starting experiment at {} {}", date, curdate);
      if( date > 86_400_000 )
      {
        curdate.add(Calendar.DAY_OF_YEAR, -1);
        logger.info("Starting experiment at {}", DateFormat.format(curdate.getTime()));
      }
    }
    catch( NoStarttimeException e )
    {
      logger.error("Could not determine starttime of experiment. Starting with " + curdate);
    }

    Calendar c= Calendar.getInstance();
    c.setTimeZone(TimeZone.getTimeZone("GMT-0"));

    for(DataManager file : files)
    {
      try
      {
        this.file= file;
        userId= getPersonInfo(file);
        c.setTimeInMillis(getDate(file));

        // logger.info("Starting experiment for " + file.getFilename() + " person: " + userId + " " + DateFormat.format(c));
        
        checkDate(c);

        // It is important that we set the user after checkDate as this method changes the user
        fuvp.setUser(userId);
        // TODO: this should be moved into the experiment. No?
        // svp.setUserId(userId);

        compute(c);

        sessionId++;
      }
      catch( ComputerBuilderException | DataException | NoStarttimeException e )
      {
        logger.error(e.getMessage());
        e.printStackTrace();
      }
    }

    checkDate(c);
    c.add(Calendar.DAY_OF_YEAR, 1);
    checkDate(c);

    try
    {
      output.debug();

      Map<String, Object> params= new HashMap<String, Object>();

      Strategy.OnSingleColUseOnlyName(params, true);

      output.simplify(MergeKeepNA.class, params);
      // output= output.Col2Data();
      output.writeFileCSV("output.csv");

      fuvp.persist();
    }
    catch( DataException | IOException e )
    {
      logger.error(e.getMessage());
      e.printStackTrace();
    }
  }

  private void determinePersons()
  {
    for(DataManager file : files)
    {
      persons.add(getPersonInfo(file));
    }
  }

  private String getPersonInfo(DataManager file)
  {
    if( patternWithPerson )
    {
      Matcher m= pattern.matcher(file.getFilename());
      if( m.matches() )
      {
        return m.group("person");
      }
      throw new IllegalArgumentException("could not determine person from " + file.getFilename());
    }
    else
    {
      try
      {
        return file.getPerson();
      }
      catch( NoPersonInfoException e )
      {
        // this should not happen
        e.printStackTrace();
        return null;
      }
    }
  }
  
  private long getDate(DataManager file) throws NoStarttimeException
  {
    if( patternWithDate )
    {
      Matcher m= pattern.matcher(file.getFilename());
      if( m.matches() )
      {
        return Long.parseLong(m.group("date")) * 1000 * 60 * 60 * 24;
      }
      throw new IllegalArgumentException("could not determine date from " + file.getFilename());
    }
    else
    {
      return file.getStartTime();
    }
  }

  /**
   * Sort the entries by their data in ascending order
   */
  protected void sortFiles()
  {
    if( patternWithDate )
    {
      files.sort(new Comparator<DataManager>() {
        
        @Override
        public int compare(DataManager o1, DataManager o2)
        {
          Matcher m1= pattern.matcher(o1.getFilename());
          Matcher m2= pattern.matcher(o2.getFilename());
          if( m1.matches() && m2.matches() )
          {
            return ((Long) Long.parseLong(m1.group("date"))).compareTo(Long.parseLong(m2.group("date")));
          }
          else
          {
            logger.error("could not find date in " + o1.getFilename() + " and " + o2.getFilename());
            return 0;
          }
        }
      });
    }
    else
    {
      files.sort(new Comparator<DataManager>() {
  
        @Override
        public int compare(DataManager o1, DataManager o2)
        {
          try
          {
            return ((Long) o1.getStartTime()).compareTo(o2.getStartTime());
          }
          catch( NoStarttimeException e )
          {
            e.printStackTrace();
            return 0;
          }
        }
      });
    }
  }

  private void checkDate(Calendar c)
  {
    while( curdate.get(Calendar.YEAR) < c.get(Calendar.YEAR) || curdate.get(Calendar.YEAR) == c.get(Calendar.YEAR)
        && curdate.get(Calendar.DAY_OF_YEAR) < c.get(Calendar.DAY_OF_YEAR) )
    {
      for(String person : persons)
      {
        fuvp.setUser(person);
        logger.info("Computing daily values for " + person + " on " + DateFormat.format(curdate.getTime()));

        ComputerBuilder builder= new ComputerBuilder()
            .setUserValueProvider(fuvp)
            .setSessionValuesProvider(svp)
            .setDate(curdate)
            .setUserId(person)
            .addComputations(getUserComputations())
            .addCompiledSources(computationResources)
            .addComputationPackages(getUserComputationPackages());

        Computer com= builder.getComputerUser();
        logger.debug("Computing user metrics");
        com.computeUserMetrics();
      }

      curdate.add(Calendar.DAY_OF_YEAR, 1);
    }
  }
  
  public void setPattern(String pattern)
  {
    this.pattern= Pattern.compile(pattern);
    if( pattern.contains("<date>") )
      patternWithDate= true;
    if( pattern.contains("<person>") )
      patternWithPerson= true;
  }

  public Pattern getPattern()
  {
    return pattern;
  }
}
