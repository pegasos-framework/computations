package at.pegasos.computer;

import static org.junit.Assert.assertEquals;

import at.pegasos.computer.computations.sport.coggan.ATL;
import org.junit.Test;

import at.pegasos.computer.providers.DummyUserValuesProvider;
import at.pegasos.data.DataException;

public class TestComputerBuilder {
  @Test
  public void loadingCoggainProvidesComputations() throws DataException, ComputerBuilderException
  {
    ComputerBuilder builder= new ComputerBuilder()
        .setUserValueProvider(new DummyUserValuesProvider())
        // .addCompiledSource(Paths.get(getClass().getResource("/test.csv").getPath().toString()).getParent().toString())
        .addCompiledSource("bin")
        .addComputationPackage(ATL.class.getPackageName());
    builder.getComputer();

    // We cannot compute IF, TSS, NP. All other classes are currently not annotated
    assertEquals(3, builder.getUnresolvedComputations().size());
    assertEquals(3, builder.getAllClasses().size());
  }

  @Test
  public void loadingFromJarWorks() throws DataException, ComputerBuilderException
  {
    ComputerBuilder builder= new ComputerBuilder()
        .setUserValueProvider(new DummyUserValuesProvider())
        .addComputationPackage(getClass().getResource("/test.jar").getFile(), "at");

    builder.getComputer();

    assertEquals(1, builder.getUnresolvedComputations().size());
  }
}
