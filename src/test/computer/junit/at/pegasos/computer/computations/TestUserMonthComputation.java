package at.pegasos.computer.computations;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.TimedValue;
import at.pegasos.computer.Computer;

public class TestUserMonthComputation {

  public static class TestComputation extends UserMonthComputation {

    @Override
    public String getName()
    {
      return "Test";
    }

    @Override
    public void compute(Computer computer)
    {
      setComputer(computer);
      value= 1;
    }
  }

  private Computer computer;

  private Calendar JanFirst;
  private Calendar JanLast;
  private Calendar FebFirst;
  private Calendar FebLast;

  @Before
  public void setUp()
  {
    computer= new Computer();

    JanFirst= Calendar.getInstance();
    JanFirst.clear();
    JanFirst.set(Calendar.YEAR, 2020);
    JanFirst.set(Calendar.MONTH, Calendar.JANUARY);
    JanFirst.set(Calendar.DAY_OF_MONTH, 1);

    JanLast= Calendar.getInstance();
    JanLast.clear();
    JanLast.set(Calendar.YEAR, 2020);
    JanLast.set(Calendar.MONTH, Calendar.JANUARY);
    JanLast.set(Calendar.DAY_OF_MONTH, 31);
    JanLast.set(Calendar.HOUR_OF_DAY, 23);
    JanLast.set(Calendar.MINUTE, 59);
    JanLast.set(Calendar.SECOND, 59);
    JanLast.set(Calendar.MILLISECOND, 999);

    FebFirst= Calendar.getInstance();
    FebFirst.clear();
    FebFirst.set(Calendar.YEAR, 2020);
    FebFirst.set(Calendar.MONTH, Calendar.FEBRUARY);
    FebFirst.set(Calendar.DAY_OF_MONTH, 1);

    FebLast= Calendar.getInstance();
    FebLast.clear();
    FebLast.set(Calendar.YEAR, 2020);
    FebLast.set(Calendar.MONTH, Calendar.FEBRUARY);
    FebLast.set(Calendar.DAY_OF_MONTH, 29);
    FebLast.set(Calendar.HOUR_OF_DAY, 23);
    FebLast.set(Calendar.MINUTE, 59);
    FebLast.set(Calendar.SECOND, 59);
    FebLast.set(Calendar.MILLISECOND, 999);
  }

  @Test
  public void test_correctMonthJan()
  {
    computer.setDate(JanFirst);

    TestComputation c= new TestComputation();
    c.compute(computer);
    TimedValue val= c.getResult();

    assertEquals(JanFirst.getTimeInMillis(), val.getStart());
    assertEquals(JanLast.getTimeInMillis(), val.getEnd());
  }

  @Test
  public void test_correctMonthFebLeap()
  {
    Calendar date= Calendar.getInstance();
    date.clear();
    date.set(Calendar.YEAR, 2020);
    date.set(Calendar.MONTH, Calendar.FEBRUARY);
    date.set(Calendar.DAY_OF_MONTH, 15);

    computer.setDate(date);

    TestComputation c= new TestComputation();
    c.compute(computer);
    TimedValue val= c.getResult();

    assertEquals(FebFirst.getTimeInMillis(), val.getStart());
    assertEquals(FebLast.getTimeInMillis(), val.getEnd());
  }

  @Test
  public void test_correctMonthFebNoLeap()
  {
    Calendar date= Calendar.getInstance();
    date.clear();
    date.set(Calendar.YEAR, 2019);
    date.set(Calendar.MONTH, Calendar.FEBRUARY);
    date.set(Calendar.DAY_OF_MONTH, 15);

    computer.setDate(date);

    TestComputation c= new TestComputation();
    c.compute(computer);
    TimedValue val= c.getResult();

    date.set(Calendar.DAY_OF_MONTH, 1);
    assertEquals(date.getTimeInMillis(), val.getStart());

    date.set(Calendar.MONTH, Calendar.FEBRUARY);
    date.set(Calendar.DAY_OF_MONTH, 28);
    date.set(Calendar.HOUR_OF_DAY, 23);
    date.set(Calendar.MINUTE, 59);
    date.set(Calendar.SECOND, 59);
    date.set(Calendar.MILLISECOND, 999);
    assertEquals(date.getTimeInMillis(), val.getEnd());
  }
}
