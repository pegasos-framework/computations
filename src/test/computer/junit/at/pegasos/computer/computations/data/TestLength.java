package at.pegasos.computer.computations.data;

import static org.junit.Assert.assertEquals;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;

public class TestLength {

  Length c= new Length("b", "a");
  Data empty;
  Data one;

  @Before
  public void setUp()
  {
    empty= new Data();
    Column e= new Column();
    e.name= "empty";
    e.names= new String[] {"a"};
    e.types= new int[] {Types.INTEGER};
    e.orig= new ArrayList<boolean[]>(0);
    e.timestamps= new ArrayList<Long>(0);
    e.values= new ArrayList<Object[]>(1);
    empty.addColumn(e);
    empty= empty.Col2Data();
    
    e= new Column();
    e.name= "one";
    e.names= new String[] {"a"};
    e.types= new int[] {Types.INTEGER};
    e.orig= new ArrayList<boolean[]>(0);
    e.timestamps= new ArrayList<Long>(0);
    e.values= new ArrayList<Object[]>(1);
    e.values.add(new Integer[] {1});
    one= new Data();
    one.addColumn(e);
    one= one.Col2Data();
  }

  @Test
  public void test_correctDependency()
  {
    assertEquals(true, c.getDependencies().contains("a"));
  }

  @Test
  public void test_correctName()
  {
    assertEquals("b", c.getName());
  }

  @Test
  public void test_correctSizeEmpty()
  {
    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);
    computer.setData(empty);

    c.compute(computer);
    Value val= c.getResult();
    assertEquals(Integer.valueOf(0), val.getIntValue());
  }

  @Test
  public void test_correctSizeOne()
  {
    Computer computer= new Computer();
    computer.setData(one);

    c.compute(computer);
    Value val= c.getResult();
    assertEquals(Integer.valueOf(1), val.getIntValue());
  }
}
