package at.pegasos.computer.computations.data;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.*;

import at.pegasos.data.*;
import at.pegasos.computer.Computation;
import at.pegasos.computer.Computer;
import at.pegasos.data.loader.*;

public class TestMeanMaxPeaksSubValue {
  Computation c1= new MeanMaxPeaksSubValue("out", "value", 1);
  Computation c2= new MeanMaxPeaksSubValue("out", "value", 2);
  Data data;

  @Before
  public void setUp()
  {
    CSVLoader loader= CSVLoader.create()
        .determineFileParams(getClass().getResource("/test.zones.csv").getFile())
        .setHeader(new NoHeader("sec", "value"))
        .setTimeExtractor(new SecToRecTime("sec"));
    try
    {
      loader.Load();
      data= loader.getData().Col2Data();
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
  }

  @Test
  public void test_correctDependency()
  {
    assertEquals(1, c1.getDependencies().size());
    assertEquals(true, c1.getDependencies().contains("value"));
  }

  @Test
  public void test_correctName()
  {
    assertEquals("out", c1.getName());
  }

  @Test
  public void test1()
  {
    Computer computer= new Computer();
    computer.setData(data);

    c1.compute(computer);

    Value res= ((MeanMaxPeaksSubValue) c1).getResult();

    assertEquals(true, res.hasSubValues());
    assertEquals(Double.valueOf(1009), res.getSubValue("1.0").getDoubleValue());
    assertEquals("out", res.getName());
  }

  @Test
  public void test2()
  {
    Computer computer= new Computer();
    computer.setData(data);

    c2.compute(computer);
    
    Value res= ((MeanMaxPeaksSubValue) c2).getResult();

    assertEquals(true, res.hasSubValues());
    assertEquals(Double.valueOf((1009 + 109)/2), res.getSubValue("2.0").getDoubleValue());
    assertEquals("out", res.getName());
  }
}
