package at.pegasos.computer.computations.data;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.*;

import org.junit.*;

import at.pegasos.data.*;
import at.pegasos.computer.Computer;
import at.pegasos.data.loader.*;
import at.pegasos.data.loader.SecToRecTime;

public class TestZoneAnalysisMulti {
  private final static String ZONESNAME= "zones";

  ZoneAnalysisMulti c= new ZoneAnalysisMulti("out", "value", ZONESNAME);
  Data empty;
  Data data;

  @Before
  public void setUp()
  {
    CSVLoader loader= CSVLoader.create()
        .determineFileParams(getClass().getResource("/test.zones.csv").getFile())
        .setHeader(new NoHeader("sec", "value"))
        .setTimeExtractor(new SecToRecTime("sec"));
    try
    {
      loader.Load();
      data= loader.getData().Col2Data();
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
  }

  @Test
  public void test_correctDependency()
  {
    assertEquals(0, c.getDependencies().size());
  }

  @Test
  public void test_correctName()
  {
    assertEquals("value_ZoneAnalysis", c.getName());
  }

  @Test
  public void test()
  {
    Computer computer= new Computer();
    computer.setData(data);

    List<Value> values= new ArrayList<Value>(6);
    values.add(new NumberValue(ZONESNAME + "_0", -10));
    values.add(new NumberValue(ZONESNAME + "_1", 0));
    values.add(new NumberValue(ZONESNAME + "_2", 10));
    values.add(new NumberValue(ZONESNAME + "_3", 100));
    values.add(new NumberValue(ZONESNAME + "_4", 1000));

    computer.addPredefinedSessionValues(values);

    c.compute(computer);
    // This is a bit hacky but we hope we know what we are doing
    List<Value> vals= (List<Value>) c.getResult();
    System.out.println(vals);
    assertEquals(true, vals.get(0).hasSubValues());
    Map<String, Value> subV= vals.get(0).getSubValues();
    // t0
    assertEquals(1, subV.get("0").getDoubleValue().intValue());
    // t1
    assertEquals(10, subV.get("1").getDoubleValue().intValue());
    // t2
    assertEquals(10, subV.get("2").getDoubleValue().intValue());
    // t3
    assertEquals(10, subV.get("3").getDoubleValue().intValue());
    // t4
    assertEquals(9, subV.get("4").getDoubleValue().intValue());
  }
}
