package at.pegasos.computer.computations.data;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.*;

import org.junit.*;

import at.pegasos.data.*;
import at.pegasos.computer.Computer;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.CSVLoader.Parameter;
import at.pegasos.data.loader.NoHeader;
import at.pegasos.data.loader.SecToRecTime;

public class TestZoneAnalysis {
  private final static String ZONESNAME= "zones";

  ZoneAnalysis c= new ZoneAnalysis("out", "value", ZONESNAME);
  Data empty;
  Data data;

  @Before
  public void setUp()
  {
    CSVLoader loader= CSVLoader.create();
    // Parameter p= CSVLoader.determineParams("/home/martin/projects/Computer/src/test/resources/test.zones.csv");
    Parameter p= CSVLoader.determineParams(getClass().getResource("/test.zones.csv").getFile());
    loader.setParams(p);
    try
    {
      loader.Load(null, new NoHeader("sec", "value"), null, new SecToRecTime("sec"));
      data= loader.getData().Col2Data();
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
  }

  @Test
  public void test_correctDependency()
  {
    assertEquals(0, c.getDependencies().size());
  }

  @Test
  public void test_correctName()
  {
    assertEquals("value_ZoneAnalysis", c.getName());
  }

  @Test
  public void test()
  {
    Computer computer= new Computer();
    computer.setData(data);

    List<Value> values= new ArrayList<Value>(6);
    values.add(new NumberValue(ZONESNAME + "_0", -10));
    values.add(new NumberValue(ZONESNAME + "_1", 0));
    values.add(new NumberValue(ZONESNAME + "_2", 10));
    values.add(new NumberValue(ZONESNAME + "_3", 100));
    values.add(new NumberValue(ZONESNAME + "_4", 1000));

    computer.addPredefinedSessionValues(values);

    c.compute(computer);
    // This is a bit hacky but we hope we know what we are doing
    List<Value> vals= (List<Value>) c.getResult();
    // t0
    assertEquals(1, vals.get(0).getDoubleValue().intValue());
    // t1
    assertEquals(10, vals.get(3).getDoubleValue().intValue());
    // t2
    assertEquals(10, vals.get(6).getDoubleValue().intValue());
    // t3
    assertEquals(10, vals.get(9).getDoubleValue().intValue());
    // t4
    assertEquals(9, vals.get(12).getDoubleValue().intValue());
  }
}
