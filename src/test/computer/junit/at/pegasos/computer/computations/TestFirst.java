package at.pegasos.computer.computations;

import static org.junit.Assert.assertEquals;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;

public class TestFirst {

  First c= new First("b", "a");
  First d= new First("b", "d");
  First f= new First("b", "f");
  Data empty;
  Data one;
  Data oned;
  Data onef;

  @Before
  public void setUp()
  {
    empty= new Data();
    Column e= new Column();
    e.name= "empty";
    e.names= new String[] {"a"};
    e.types= new int[] {Types.INTEGER};
    e.orig= new ArrayList<boolean[]>(0);
    e.timestamps= new ArrayList<Long>(0);
    e.values= new ArrayList<Object[]>(1);
    empty.addColumn(e);
    empty= empty.Col2Data();

    e= new Column();
    e.name= "one";
    e.names= new String[] {"a"};
    e.types= new int[] {Types.INTEGER};
    e.orig= new ArrayList<boolean[]>(0);
    e.timestamps= new ArrayList<Long>(0);
    e.values= new ArrayList<Object[]>(1);
    e.values.add(new Integer[] {2});
    one= new Data();
    one.addColumn(e);
    one= one.Col2Data();

    e= new Column();
    e.name= "one";
    e.names= new String[] {"d"};
    e.types= new int[] {Types.DOUBLE};
    e.orig= new ArrayList<boolean[]>(0);
    e.timestamps= new ArrayList<Long>(0);
    e.values= new ArrayList<Object[]>(1);
    e.values.add(new Double[] {2d});
    oned= new Data();
    oned.addColumn(e);
    oned= oned.Col2Data();

    e= new Column();
    e.name= "one";
    e.names= new String[] {"f"};
    e.types= new int[] {Types.FLOAT};
    e.orig= new ArrayList<boolean[]>(0);
    e.timestamps= new ArrayList<Long>(0);
    e.values= new ArrayList<Object[]>(1);
    e.values.add(new Float[] {2f});
    onef= new Data();
    onef.addColumn(e);
    onef= onef.Col2Data();
  }

  @Test
  public void test_correctDependency()
  {
    assertEquals(true, c.getDependencies().contains("a"));
  }

  @Test
  public void test_correctName()
  {
    assertEquals("b", c.getName());
  }

  @Test
  public void test_NoValueOnEmpty()
  {
    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);
    computer.setData(empty);

    c.compute(computer);
    Value val= c.getResult();
    assertEquals(null, val);
  }

  @Test
  public void test_correctFirstElement()
  {
    Computer computer= new Computer();
    computer.setData(one);

    c.compute(computer);
    Value val= c.getResult();
    assertEquals(Integer.valueOf(2), val.getIntValue());
  }

  @Test
  public void test_correctFirstDElement()
  {
    Computer computer= new Computer();
    computer.setData(oned);

    d.compute(computer);
    Value val= d.getResult();
    assertEquals(Double.valueOf(2), val.getDoubleValue());
  }

  @Test
  public void test_correctFirstFElement()
  {
    Computer computer= new Computer();
    computer.setData(onef);

    f.compute(computer);
    Value val= f.getResult();
    assertEquals(Double.valueOf(2), val.getDoubleValue());
  }
}
