package at.pegasos.computer.computations.column;

import at.pegasos.computer.computations.math.ColumnArea;
import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.SegmentedData;
import at.pegasos.data.TimeUnit;
import at.pegasos.data.Value;
import at.pegasos.computer.Computer;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.NoHeader;
import at.pegasos.data.loader.SecToRecTime;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import static org.junit.Assert.*;

public class TestColumnArea {
  public final String RESNAME = "area";

  ColumnArea area = new ColumnArea(RESNAME, "a");
  Data empty;
  Data test;

  private final static String CONST_DATAS=
          "1\t1\n" +
          "2\t1\n" +
          "3\t1\n" +
          "4\t1\n" +
          "5\t1\n" +
          "6\t1\n" +
          "7\t1\n" +
          "8\t1\n" +
          "9\t1\n" +
          "10\t1\n";

  private final static String TRAP_DATAS=
          "1\t1\n" +
          "2\t1\n" +
          "3\t1\n" +
          "4\t2\n" +
          "5\t3\n" +
          "6\t2\n" +
          "7\t1\n" +
          "8\t1\n" +
          "9\t1\n" +
          "10\t1\n";

  private final static String TRAP_DATA_VAR=
          "1\t1\n" +
          "2\t1\n" +
          "3\t1\n" +
          "4\t2\n" +
          "6\t3\n" +
          "8\t2\n" +
          "9\t1\n" +
          "10\t1\n" +
          "11\t1\n" +
          "12\t1\n";

  private void loadData(String data_string)
  {
    empty= new Data();
    Column e= new Column();
    e.name= "empty";
    e.names= new String[] {"a"};
    e.types= new int[] {Types.INTEGER};
    e.orig= new ArrayList<boolean[]>(0);
    e.timestamps= new ArrayList<Long>(0);
    e.values =new ArrayList<Object[]>(1);
    empty.addColumn(e);
    empty= empty.Col2Data();
    
    try
    {
      CSVLoader loader= CSVLoader.create()
          .setParams(new CSVLoader.Parameter("asdf.csv", "\t", "."))
          .setLines(Arrays.asList(data_string.split("\n")));

      test= loader
          .Load(null, new NoHeader("rec_time", "a"), null, new SecToRecTime("rec_time"))
          .getData()
          .Col2Data();
    }
    catch( IOException e1 )
    {
      e1.printStackTrace();
      fail();
    }
  }

  @Test
  public void test_correctDependency()
  {
    assertTrue(area.getDependencies().contains("a"));
  }

  @Test
  public void test_correctName()
  {
    assertEquals(RESNAME, area.getName());
  }

  @Test
  public void constant()
  {
    loadData(CONST_DATAS);
    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);
    computer.setData(test);

    area.compute(computer);
    Value res= area.getResult();

    assertEquals(Double.valueOf(9), res.getDoubleValue());
  }

  @Test
  public void constant_respectsTimeUnits()
  {
    loadData(CONST_DATAS);
    test.setTimeUnit(TimeUnit.Second);

    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);
    computer.setData(test);

    area.compute(computer);
    Value res= area.getResult();

    assertEquals(Double.valueOf(9000), res.getDoubleValue());

    test.setTimeUnit(TimeUnit.MicroSecond);
    area.compute(computer);
    res= area.getResult();
    assertEquals(0.009, res.getDoubleValue(), 0.00000000000000001);
  }

  @Test
  public void constant_respectsSegments()
  {
    loadData(CONST_DATAS);
    test = new SegmentedData(test);
    ((SegmentedData) test).segment(new long[] {4000});
    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);
    computer.setData(test);

    area.compute(computer);
    Value res= area.getResult();

    assertEquals(Double.valueOf(9), res.getDoubleValue());
    assertEquals(Double.valueOf(3), res.getSegmentValue(1).getDoubleValue());
    assertEquals(Double.valueOf(6), res.getSegmentValue(2).getDoubleValue());
  }

  @Test
  public void trapezoid()
  {
    loadData(TRAP_DATAS);
    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);
    computer.setData(test);

    area.compute(computer);
    Value res= area.getResult();

    assertEquals(Double.valueOf(13), res.getDoubleValue());
  }

  @Test
  public void trapezoid_var()
  {
    loadData(TRAP_DATA_VAR);
    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);
    computer.setData(test);

    area.compute(computer);
    Value res= area.getResult();

    assertEquals(Double.valueOf(18), res.getDoubleValue());
  }
}
