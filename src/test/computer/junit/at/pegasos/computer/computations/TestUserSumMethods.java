package at.pegasos.computer.computations;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.MultiUserSessionValuesProvider;
import at.pegasos.data.NumberValue;
import at.pegasos.computer.BasicUserValueComputation;
import at.pegasos.computer.Computer;

public class TestUserSumMethods {

  private MultiUserSessionValuesProvider vp;
  private FileUserValuesProvider uvp;

  @Before
  public void setUp()
  {
    vp= new MultiUserSessionValuesProvider();
    vp.setUserId("test");

    uvp= FileUserValuesProvider.getEmptyInstance();

    Calendar c= Calendar.getInstance();
    c.set(2020, 1, 1, 1, 1);

    vp.addSessionMetrics(c, Arrays.asList(new NumberValue("test", 1)));

    // our weeks run from monday to sunday. Construct a corner case.
    // Values on sunday (prev week), monday, saturday, and monday next week
    c.set(2020, 1, 23, 1, 1);
    vp.addSessionMetrics(c, Arrays.asList(new NumberValue("test", 1)));
    c.set(2020, 1, 24, 1, 1);
    vp.addSessionMetrics(c, Arrays.asList(new NumberValue("test", 1)));
    c.set(2020, 1, 29, 1, 1);
    vp.addSessionMetrics(c, Arrays.asList(new NumberValue("test", 1)));
    c.set(2020, 2, 2, 1, 1);
    vp.addSessionMetrics(c, Arrays.asList(new NumberValue("test", 1)));

    // some corner cases for the year
    c.set(2019, 11, 31, 1, 1);
    vp.addSessionMetrics(c, Arrays.asList(new NumberValue("test", 1)));
    c.set(2021, 0, 1, 1, 1);
    vp.addSessionMetrics(c, Arrays.asList(new NumberValue("test", 1)));
  }

  @Test
  public void test_sumDayFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 1, 1, 10, 1);

    com.setDate(c);
    com.setSessionValuesProvider(vp);
    com.setUserValueProvider(uvp);

    int size= uvp.getSize();
    com.directcompute(new BasicUserValueComputation() {

      @Override
      public String getName()
      {
        return "Test";
      }

      @Override
      public Collection<String> getDependencies()
      {
        return null;
      }

      @Override
      public void compute(Computer computer)
      {
        setComputer(computer);
        value= sessionSumDay("test", 0, 0);
      }
    });

    // stack grown
    assertEquals(size + 1, uvp.getSize());

    // correct value comes out
    assertEquals(new NumberValue("Test", 1), uvp.getLast());
  }

  @Test
  public void test_sumDayNotFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 1, 2, 10, 1);

    com.setDate(c);
    com.setSessionValuesProvider(vp);
    com.setUserValueProvider(uvp);

    int size= uvp.getSize();
    com.directcompute(new BasicUserValueComputation() {

      @Override
      public String getName()
      {
        return "Test";
      }

      @Override
      public Collection<String> getDependencies()
      {
        return null;
      }

      @Override
      public void compute(Computer computer)
      {
        setComputer(computer);
        value= sessionSumDay("test", 0, -1);
      }
    });

    // stack grown
    assertEquals(size + 1, uvp.getSize());

    // correct value comes out. In this case our default value
    assertEquals(new NumberValue("Test", -1), uvp.getLast());
  }

  @Test
  public void test_sumWeekFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 1, 23, 10, 1);

    for(int i= 0; i < 7; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumWeek("test", 0, 0);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i, new NumberValue("Test", 2), uvp.getLast());
    }
  }

  @Test
  public void test_sumPrevWeekFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 1, 23, 10, 1);

    for(int i= 0; i < 7; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumWeek("test", -1, 0);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i, new NumberValue("Test", 1), uvp.getLast());
    }
  }

  @Test
  public void test_sumNextWeekFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 1, 23, 10, 1);

    for(int i= 0; i < 7; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumWeek("test", 1, 0);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i, new NumberValue("Test", 1), uvp.getLast());
    }
  }

  @Test
  public void test_sumMonthFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 0, 31, 10, 1);
    for(int i= 0; i < 29; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumMonth("test", 0, 0);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i, new NumberValue("Test", 4), uvp.getLast());
    }
  }

  @Test
  public void test_sumPrevMonthFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 1, 29, 10, 1);
    for(int i= 0; i < 31; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumMonth("test", -1, 0);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i, new NumberValue("Test", 4), uvp.getLast());
    }
  }

  @Test
  public void test_sumMonthNotFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 2, 31, 10, 1);
    for(int i= 0; i < 30; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumMonth("test", 0, -1);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i, new NumberValue("Test", -1), uvp.getLast());
    }
  }

  @Test
  public void test_sumYearFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 0, 1, 10, 1);
    for(int i= 0; i < 365; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumYear("test", 0, 0);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i + " " + c, new NumberValue("Test", 5), uvp.getLast());
    }
  }

  @Test
  public void test_sumPrevYearFound()
  {
    Computer com= new Computer();

    Calendar c= Calendar.getInstance();
    c.set(2020, 0, 1, 10, 1);
    for(int i= 0; i < 365; i++)
    {
      c.add(Calendar.DAY_OF_YEAR, 1);

      com.setDate(c);
      com.setSessionValuesProvider(vp);
      com.setUserValueProvider(uvp);

      int size= uvp.getSize();
      com.directcompute(new BasicUserValueComputation() {

        @Override
        public String getName()
        {
          return "Test";
        }

        @Override
        public Collection<String> getDependencies()
        {
          return null;
        }

        @Override
        public void compute(Computer computer)
        {
          setComputer(computer);
          value= sessionSumYear("test", -1, 0);
        }
      });

      // stack grown by exactly one. We do not want multiple values for this
      assertEquals(size + 1, uvp.getSize());

      // correct value comes out
      assertEquals("We failed on " + i + " " + c, new NumberValue("Test", 1), uvp.getLast());
    }
  }
}
