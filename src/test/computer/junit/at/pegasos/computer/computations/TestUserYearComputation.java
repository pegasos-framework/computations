package at.pegasos.computer.computations;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.Test;

import at.pegasos.data.TimedValue;
import at.pegasos.computer.Computer;

public class TestUserYearComputation {

  public static class TestComputation extends UserYearComputation {

    @Override
    public String getName()
    {
      return "Test";
    }

    @Override
    public void compute(Computer computer)
    {
      setComputer(computer);
      value= 1;
    }
  }

  @Test
  public void test_correctCurrentDate()
  {
    Calendar date= Calendar.getInstance();

    Computer computer= new Computer();
    computer.setDate(date);

    TestComputation c= new TestComputation();
    c.compute(computer);
    TimedValue val= c.getResult();

    date.set(Calendar.MONTH, Calendar.JANUARY);
    date.set(Calendar.DAY_OF_MONTH, 1);
    date.set(Calendar.HOUR_OF_DAY, 0);
    date.set(Calendar.MINUTE, 0);
    date.set(Calendar.SECOND, 0);
    date.set(Calendar.MILLISECOND, 0);
    assertEquals(date.getTimeInMillis(), val.getStart());

    date.set(Calendar.MONTH, Calendar.DECEMBER);
    date.set(Calendar.DAY_OF_MONTH, 31);
    date.set(Calendar.HOUR_OF_DAY, 23);
    date.set(Calendar.MINUTE, 59);
    date.set(Calendar.SECOND, 59);
    date.set(Calendar.MILLISECOND, 999);
    assertEquals(date.getTimeInMillis(), val.getEnd());
  }

  @Test
  public void test_correct2020()
  {
    Calendar date= Calendar.getInstance();
    date.set(Calendar.YEAR, 2020);

    Computer computer= new Computer();
    computer.setDate(date);

    TestComputation c= new TestComputation();
    c.compute(computer);
    TimedValue val= c.getResult();

    date.set(Calendar.MONTH, Calendar.JANUARY);
    date.set(Calendar.DAY_OF_MONTH, 1);
    date.set(Calendar.HOUR_OF_DAY, 0);
    date.set(Calendar.MINUTE, 0);
    date.set(Calendar.SECOND, 0);
    date.set(Calendar.MILLISECOND, 0);
    assertEquals(date.getTimeInMillis(), val.getStart());

    date.set(Calendar.MONTH, Calendar.DECEMBER);
    date.set(Calendar.DAY_OF_MONTH, 31);
    date.set(Calendar.HOUR_OF_DAY, 23);
    date.set(Calendar.MINUTE, 59);
    date.set(Calendar.SECOND, 59);
    date.set(Calendar.MILLISECOND, 999);
    assertEquals(date.getTimeInMillis(), val.getEnd());
  }
}
