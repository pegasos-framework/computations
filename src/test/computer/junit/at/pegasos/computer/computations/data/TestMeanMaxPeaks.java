package at.pegasos.computer.computations.data;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.*;

import org.junit.*;

import at.pegasos.data.*;
import at.pegasos.computer.Computation;
import at.pegasos.computer.Computer;
import at.pegasos.data.loader.*;
import at.pegasos.data.loader.SecToRecTime;

public class TestMeanMaxPeaks {
  Computation c1= new MeanMaxPeaks("out", "value", 1);
  Computation c2= new MeanMaxPeaks("out", "value", 2);
  Data data;

  @Before
  public void setUp()
  {
    CSVLoader loader= CSVLoader.create()
        .determineFileParams(getClass().getResource("/test.zones.csv").getFile())
        .setHeader(new NoHeader("sec", "value"))
        .setTimeExtractor(new SecToRecTime("sec"));
    try
    {
      loader.Load();
      data= loader.getData().Col2Data();
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
  }

  @Test
  public void test_correctDependency()
  {
    assertEquals(1, c1.getDependencies().size());
    assertEquals(true, c1.getDependencies().contains("value"));
  }

  @Test
  public void test_correctName()
  {
    assertEquals("out", c1.getName());
  }

  @Test
  public void test1()
  {
    Computer computer= new Computer();
    computer.setData(data);

    c1.compute(computer);
    // This is a bit hacky but we hope we know what we are doing
    List<Value> vals= (List<Value>) ((MeanMaxPeaks) c1).getResult();

    assertEquals(false, vals.get(0).hasSubValues());
    assertEquals(Double.valueOf(1009), vals.get(0).getDoubleValue());
    assertEquals("out1.0", vals.get(0).getName());
  }

  @Test
  public void test2()
  {
    Computer computer= new Computer();
    computer.setData(data);

    c2.compute(computer);
    // This is a bit hacky but we hope we know what we are doing
    List<Value> vals= (List<Value>) ((MeanMaxPeaks) c2).getResult();

    assertEquals(false, vals.get(0).hasSubValues());
    assertEquals(Double.valueOf((1009 + 109)/2), vals.get(0).getDoubleValue());
    assertEquals("out2.0", vals.get(0).getName());
  }
}
