package at.pegasos.computer.providers;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.NumberValue;

public class TestMuliUserSessionValuesProvider {

  private NumberValue v1;
  private NumberValue v2;
  private NumberValue v3;

  @Before
  public void setUp()
  {
    v1= new NumberValue("Test");
    v1.setValue(1);
    v2= new NumberValue("Test");
    v2.setValue(2);
    v3= new NumberValue("Test2");
    v3.setValue(3);
  }

  @Test
  public void testAddRetrieve_correctname()
  {
    MultiUserSessionValuesProvider vp= new MultiUserSessionValuesProvider();
    vp.setUserId("test");
    vp.addSessionMetrics(Calendar.getInstance(), Arrays.asList(v1, v3));
    assertEquals(true, vp.getValuesDay("Test", Calendar.getInstance()).contains(v1));
    assertEquals(false, vp.getValuesDay("Test", Calendar.getInstance()).contains(v3));
  }

  @Test
  public void testAddRetrieve_daycorrect()
  {
    MultiUserSessionValuesProvider vp= new MultiUserSessionValuesProvider();
    vp.setUserId("test");
    Calendar c= Calendar.getInstance();
    c.add(Calendar.DAY_OF_YEAR, -1);
    vp.addSessionMetrics(Calendar.getInstance(), Arrays.asList(v1));
    vp.addSessionMetrics(c, Arrays.asList(v2));
    assertEquals(true, vp.getValuesDay("Test", Calendar.getInstance()).contains(v1));
    assertEquals(false, vp.getValuesDay("Test", Calendar.getInstance()).contains(v2));
    assertEquals(false, vp.getValuesDay("Test", c).contains(v1));
    assertEquals(true, vp.getValuesDay("Test", c).contains(v2));
  }

  @Test
  public void testAddRetrieve_sessionidnoeffect()
  {
    MultiUserSessionValuesProvider vp= new MultiUserSessionValuesProvider();
    vp.setUserId("test");
    vp.addSessionMetrics(Calendar.getInstance(), Arrays.asList(v1));
    assertEquals(true, vp.getValuesDay("Test", Calendar.getInstance()).contains(v1));
    vp.setSessionId("a");
    assertEquals(true, vp.getValuesDay("Test", Calendar.getInstance()).contains(v1));
  }

  @Test
  public void testAddRetrieveMultiUser()
  {
    MultiUserSessionValuesProvider vp= new MultiUserSessionValuesProvider();
    vp.setUserId("test");
    vp.addSessionMetrics(Calendar.getInstance(), Arrays.asList(v1));
    vp.setUserId("test2");
    vp.addSessionMetrics(Calendar.getInstance(), Arrays.asList(v2));
    vp.setUserId("test");
    assertEquals(true, vp.getValuesDay("Test", Calendar.getInstance()).contains(v1));
    assertEquals(false, vp.getValuesDay("Test", Calendar.getInstance()).contains(v2));
    vp.setUserId("test2");
    assertEquals(false, vp.getValuesDay("Test", Calendar.getInstance()).contains(v1));
    assertEquals(true, vp.getValuesDay("Test", Calendar.getInstance()).contains(v2));
  }
}
