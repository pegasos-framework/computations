package at.pegasos.computer.providers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestConcurrentFileMuliUserSessionValuesProvider extends TestUserValueProvider {

  @Before
  public void setUp()
  {
    uvp= new ConcurrentFileMultiUserValuesProvider(true);
  }
  
  @Rule
  public TemporaryFolder folder= new TemporaryFolder();
  
  @Test
  public void emptyFile() throws IOException
  {
    File test= folder.newFile();
    BufferedWriter writer= Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write("person,value_name,date_end,value\n");
    writer.close();
    // "person", "value_name", "date_end", "value"
    uvp= new ConcurrentFileMultiUserValuesProvider(test.getAbsolutePath());
  }
}
