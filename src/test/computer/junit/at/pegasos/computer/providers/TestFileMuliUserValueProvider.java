package at.pegasos.computer.providers;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class TestFileMuliUserValueProvider extends TestUserValueProvider {

  @Before
  public void setUp()
  {
    uvp= new FileMultiUserValuesProvider(true);
  }
  
  @Test
  public void empty()
  {
    uvp.setUser("1");
    assertEquals(0, uvp.getValues(Calendar.getInstance()).size());
  }
}
