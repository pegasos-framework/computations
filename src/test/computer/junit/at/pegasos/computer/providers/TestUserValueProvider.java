package at.pegasos.computer.providers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.NumberValue;
import at.pegasos.data.TimedValue;
import at.pegasos.data.Value;

public abstract class TestUserValueProvider {

  private NumberValue v1;
  private NumberValue v2;
  private NumberValue v3;
  protected UserValueProvider uvp;

  @Before
  public void setUpTestUserSessionValuesProvider()
  {
    v1= new NumberValue("Test");
    v1.setValue(1);
    v2= new NumberValue("Test");
    v2.setValue(2);
    v3= new NumberValue("Test2");
    v3.setValue(3);
  }

  @Test
  public void addValuesSameDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));
  }

  @Test
  public void addValuesNextDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    c.add(Calendar.DAY_OF_YEAR, 1);
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));
  }

  @Test
  public void addValuesPrevDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    c.add(Calendar.DAY_OF_YEAR, -1);
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(0, vals.size());
  }

  @Test
  public void addCloseRetrieveSameDay()
  {
    Calendar c= Calendar.getInstance();
    Calendar c2= Calendar.getInstance();
    c2.add(Calendar.DAY_OF_YEAR, 1);

    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    uvp.setValue(new TimedValue(c.getTimeInMillis(), c2.getTimeInMillis(), v1));
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));
  }

  @Test
  public void valuesClosed()
  {
    Calendar c= Calendar.getInstance();
    Calendar c2= Calendar.getInstance();
    c2.add(Calendar.DAY_OF_YEAR, 1);

    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    uvp.setValue(new TimedValue(c2.getTimeInMillis(), v2));
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));
    vals= uvp.getValues(c2);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v2, v));
  }

  @Test
  public void valuesClosed2()
  {
    Calendar c= Calendar.getInstance();
    Calendar c2= Calendar.getInstance();
    c2.add(Calendar.DAY_OF_YEAR, 1);

    uvp.setUser("test");
    // add the value
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));

    // close and change the value
    uvp.setValue(new TimedValue(c.getTimeInMillis(), c2.getTimeInMillis(), v2));

    vals= uvp.getValues(c2);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v2, v));

    // continue one day. Now the value should not be available
    c2.add(Calendar.DAY_OF_YEAR, 2);
    vals= uvp.getValues(c2);
    assertEquals(0, vals.size());
  }

  @Test
  public void addFetchValueSameDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    TimedValue val= uvp.fetchValue("Test", c);
    assertNotEquals(null, val);
    assertEquals(v1, val.getValue());
  }

  @Test
  public void addFetchValueNextDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), v1));
    c.add(Calendar.DAY_OF_YEAR, 1);
    TimedValue val= uvp.fetchValue("Test", c);
    assertNotEquals(null, val);
    assertEquals(v1, val.getValue());
  }

  @Test
  public void addSpanFetchValueSameDay()
  {
    Calendar c= Calendar.getInstance();
    Calendar c2= Calendar.getInstance();
    c2.add(Calendar.DAY_OF_YEAR, 3);

    uvp.setUser("test");
    uvp.setValue(new TimedValue(c.getTimeInMillis(), c2.getTimeInMillis(), v1));
    TimedValue val= uvp.fetchValue("Test", c);
    assertNotEquals(null, val);
    assertEquals(v1, val.getValue());
    c.add(Calendar.DAY_OF_YEAR, 1);

    val= uvp.fetchValue("Test", c);
    assertNotEquals(null, val);
    assertEquals(v1, val.getValue());
  }

  @Test
  public void setValuesSameDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValues(c, Arrays.asList(new Value[] {v1}));
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));
  }

  @Test
  public void setValuesNextDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValues(c, Arrays.asList(new Value[] {v1}));
    c.add(Calendar.DAY_OF_YEAR, 1);
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));
  }

  @Test
  public void setValuesPrevDay()
  {
    Calendar c= Calendar.getInstance();
    uvp.setUser("test");
    uvp.setValues(c, Arrays.asList(new Value[] {v1}));
    c.add(Calendar.DAY_OF_YEAR, -1);
    Collection<Value> vals= uvp.getValues(c);
    assertEquals(0, vals.size());
  }

  @Test
  public void setCloseRetrieveSameDay()
  {
    Calendar c= Calendar.getInstance();
    Calendar c2= Calendar.getInstance();
    c2.add(Calendar.DAY_OF_YEAR, 1);

    uvp.setUser("test");
    uvp.setValues(c, Arrays.asList(new Value[] {v1}));

    Collection<Value> vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));

    // Now lets change the value for the following day
    uvp.setValues(c2, Arrays.asList(new Value[] {v2}));
    
    // Value today should remain unchanged
    vals= uvp.getValues(c);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v1, v));
    // value tomorrow should be changed
    vals= uvp.getValues(c2);
    assertEquals(1, vals.size());
    vals.stream().forEach(v -> assertEquals(v2, v));
  }
}
