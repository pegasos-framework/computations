package at.pegasos.computer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.Calendar;

import at.pegasos.computer.computations.BikePowerFilter;
import at.pegasos.computer.computations.data.ZoneAnalysis;
import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.computer.computations.BikePower30sMA;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.DataManager;
import at.pegasos.data.compute.Computer;
import at.pegasos.data.compute.Distance;
import at.pegasos.data.compute.Duration;
import at.pegasos.data.compute.WorkingTime;
import at.pegasos.data.compute.coggan.IntensityFactor;
import at.pegasos.data.compute.coggan.NormalizedPower;
import at.pegasos.data.compute.coggan.TSS;
import at.pegasos.data.compute.coggan.VI;

public class TestManager {
  public static void main(String[] args) throws DataException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ComputerBuilderException
  {
    new TestManager().testCsv();
    
    new TestManager().testTcx1();
    
    new TestManager().testTcx2();
  }
  
  public void testCsv() throws DataException, ComputerBuilderException
  {
    // Data data= DataManager.getData("2019-10-14_073247.ser");
    Data data= DataManager.getData("/home/martin/schmelz/MMA/Computer/opendata/1b5177e3-e424-4ec0-a52a-793e61970795/2018_06_02_10_29_47.csv");
    
    Computer com= new ComputerBuilder()
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputation(new Duration())
        .addComputation(new BikePowerFilter())
        .addComputation(
            new BikePower30sMA())
        .addComputation(
            new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
        .addComputation(new NormalizedPower())
        .addComputation(new IntensityFactor())
        .addComputation(new WorkingTime()).addComputation(new TSS())
        .addComputation(new VI()).addComputation(new Distance())
        .getComputer();
    com.setSessionId("" + 1);
    
    com.setDate(Calendar.getInstance());
    com.fetchUserValues();
    
    com.setData(data);
    
    com.computeSessionMetrics();
    
    com.debugValues();
  }
  
  public void testTcx1() throws DataException, ComputerBuilderException
  {
    Data data= DataManager.getData("/home/martin/Downloads/Martin_Dobiasch_2019-08-29_07-10-18.tcx");
    // data.debug();
    
    Computer com= new ComputerBuilder(data)
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputation(new Duration())
        .addComputation(new BikePowerFilter())
        .addComputation(
            new BikePower30sMA())
        .addComputation(
            new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
        .addComputation(new NormalizedPower())
        .addComputation(new IntensityFactor())
        .addComputation(new WorkingTime()).addComputation(new TSS())
        .addComputation(new VI()).addComputation(new Distance())
        .getComputer();
    com.setSessionId("" + 1);
    
    com.setDate(Calendar.getInstance());
    com.fetchUserValues();
    
    com.computeSessionMetrics();
    
    com.debugValues();
  }
  
  public void testTcx2() throws DataException, IOException, ComputerBuilderException
  {
    Data data= DataManager.getData("/home/martin/Downloads/Martin_Dobiasch_2019-07-31_19-00-58.tcx");
    // data.debug();

    Computer com= new ComputerBuilder(data)
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputation(new Duration())
        .addComputation(new BikePowerFilter())
        .addComputation(
            new BikePower30sMA())
        .addComputation(
            new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
        .addComputation(new NormalizedPower())
        .addComputation(new IntensityFactor())
        .addComputation(new WorkingTime()).addComputation(new TSS())
        .addComputation(new VI()).addComputation(new Distance())
        .getComputer();
    com.setSessionId("" + 1);
    
    com.setDate(Calendar.getInstance());
    com.fetchUserValues();
    
    com.computeSessionMetrics();
    
    com.debugValues();
  }
}
