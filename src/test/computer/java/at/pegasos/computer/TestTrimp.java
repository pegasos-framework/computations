package at.pegasos.computer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.Calendar;

import at.pegasos.computer.computations.math.ColumnAverage;
import at.pegasos.computer.computations.sport.Trimp;
import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.DataManager;
import at.pegasos.data.compute.Computer;
import at.pegasos.data.compute.Duration;

public class TestTrimp {
  public static void main(String[] args) throws DataException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ComputerBuilderException
  {
    // new TestAltitude().testTcx1();
    
    new TestTrimp().testTcx2();
    
    new TestTrimp().testTcx3();
  }
  
  public void testTcx1() throws DataException, ComputerBuilderException
  {
    Data data= DataManager.getData("/home/martin/Downloads/Martin_Dobiasch_2019-08-29_07-10-18.tcx");
    test(data);
  }
  
  public void testTcx2() throws DataException, IOException, ComputerBuilderException
  {
    Data data= DataManager.getData("/home/martin/Downloads/Martin_Dobiasch_2019-07-31_19-00-58.tcx");
    // data.debug();
    test(data);
  }
  
  public void testTcx3() throws DataException, IOException, ComputerBuilderException
  {
    Data data= DataManager.getData("/home/martin/Downloads/Martin_Dobiasch_2019-10-20_07-53-50.tcx");
    // data.debug();
    test(data);
  }
  
  private void test(Data data) throws ComputerBuilderException
  {
    Computer com= new ComputerBuilder(data)
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputation(new Duration())
        .addComputation(new ColumnAverage("hr_AVERAGE", "hr"))
        .addComputation(new Trimp())
        .getComputer();
    com.setSessionId("" + 1);
    
    com.setDate(Calendar.getInstance());
    com.fetchUserValues();
    
    com.computeSessionMetrics();
    
    // com.debugValues();
    
    double trimp= Helper.getValue(com.getComputedValues(), "Trimp").getDoubleValue();
    
    System.out.println(trimp);
  }
}
