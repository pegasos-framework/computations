package at.pegasos.computer;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.Calendar;

import at.pegasos.computer.computations.math.ColumnMovingAverage;
import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.DataManager;
import at.pegasos.data.compute.Computer;
import at.pegasos.data.compute.computations.Altitude;
import at.pegasos.data.compute.computations.Altitude2;
import at.pegasos.data.compute.computations.Grade;

public class TestAltitude {
  public static void main(String[] args) throws DataException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ComputerBuilderException
  {
    new TestAltitude().testTcx1();
    
    new TestAltitude().testTcx2();
    
    new TestAltitude().testTcx3();
  }
  
  public void testTcx1() throws DataException, ComputerBuilderException
  {
    System.out.println("testTcx1");
    Data data= DataManager.getData(new File(getClass().getResource("/First_Last_2019-08-29_07-10-18.tcx").getFile()).getAbsolutePath());
    try
    {
      test(data);
    }
    catch( IllegalStateException e )
    {
      System.out.println("IllegalStateException: " + e.getMessage());
    }
  }
  
  public void testTcx2() throws DataException, IOException, ComputerBuilderException
  {
    System.out.println("testTcx2");
    Data data= DataManager.getData(new File(getClass().getResource("/First_Last_2019-07-31_19-00-58.tcx").getFile()).getAbsolutePath());
    // data.debug();
    test(data);
  }
  
  public void testTcx3() throws DataException, IOException, ComputerBuilderException
  {
    System.out.println("testTcx3");
    Data data= DataManager.getData(new File(getClass().getResource("/First_Last_2019-10-20_07-53-50.tcx").getFile()).getAbsolutePath());
    // data.debug();
    test(data);
  }
  
  private void test(Data data) throws ComputerBuilderException
  {
    Computer com= new ComputerBuilder(data)
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputation(new Grade())
        .addComputation(new Altitude())
        .addComputation(new Altitude2())
        .addComputation(new ColumnMovingAverage("grade_5s", "grade", 5))
        .getComputer();
    com.setSessionId("" + 1);
    
    com.setDate(Calendar.getInstance());
    com.fetchUserValues();
    
    com.computeSessionMetrics();
    
    // com.debugValues();
    
    double asc1= Helper.getValueAsDouble(com.getComputedValues(), "ASCENT");
    double asc2= Helper.getValueAsDouble(com.getComputedValues(), "ASCENT2");
    double desc1= Helper.getValueAsDouble(com.getComputedValues(), "DESCENT");
    double desc2= Helper.getValueAsDouble(com.getComputedValues(), "DESCENT2");
    
    System.out.println(asc1 + " " + desc1);
    System.out.println(asc2 + " " + desc2);
  }
}
