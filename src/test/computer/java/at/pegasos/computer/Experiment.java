package at.pegasos.computer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import at.pegasos.computer.computations.BikePowerFilter;
import at.pegasos.computer.computations.data.ZoneAnalysis;
import at.pegasos.computer.computations.math.ColumnMovingAverage;
import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.DataManager;
import at.pegasos.data.Value;
import at.pegasos.data.compute.Computer;
import at.pegasos.data.compute.Distance;
import at.pegasos.data.compute.Duration;
import at.pegasos.data.compute.WorkingTime;
import at.pegasos.data.compute.coggan.ATL;
import at.pegasos.data.compute.coggan.CTL;
import at.pegasos.data.compute.coggan.DailyTSS;
import at.pegasos.data.compute.coggan.IntensityFactor;
import at.pegasos.data.compute.coggan.NormalizedPower;
import at.pegasos.data.compute.coggan.TSB;
import at.pegasos.data.compute.coggan.TSS;
import at.pegasos.data.compute.coggan.VI;
import at.pegasos.data.simplify.MergeKeepNA;
import at.pegasos.tool.util.Metadata;
import at.pegasos.tool.util.MetadataParser;

// TODO: refactor to use SingleUserExperiment
public class Experiment {
  
  private static class DataFile {
    @Override
    public String toString()
    {
      return "DataFile [file=" + file + ", meta=" + meta + "]";
    }
    public Path file;
    public Metadata meta;
  }
  
  protected List<DataFile> files= new ArrayList<DataFile>(100);
  
  private DataFile file;
  
  private Calendar curdate;
  
  private FileUserValuesProvider fuvp;
  
  private int sessionId;
  
  Data output;
  
  public static void main(String[] args) throws ComputerBuilderException
  {
    Experiment ex= new Experiment();
    ex.run();
  }
  
  public void run() throws ComputerBuilderException
  {
    fetchFiles();
    
    sessionId= 1;
    
    output= new Data();
    
    fuvp= FileUserValuesProvider.getInstance(Paths.get("."), "1");
    
    // files= files.subList(files.size()-2, files.size());
    curdate= Calendar.getInstance();
    curdate.setTimeInMillis(Long.parseLong(files.get(0).meta.getValue("Starttime")));
    curdate.set(Calendar.HOUR_OF_DAY, 23);
    curdate.set(Calendar.MINUTE, 59);
    
    Calendar c= Calendar.getInstance();
    
    for(DataFile file : files)
    {
      try
      {
        this.file= file;
        System.out.println(file.file.toString());
        
        c.setTimeInMillis(Long.parseLong(file.meta.getValue("Starttime")));
        
        checkDate(c);
        
        compute(c);        
        /*com.clearComputations();
        com.setDate(c);
        com.addComputation(new CTL());
        com.addComputation(new ATL());
        com.computeUserMetrics();*/
        // System.out.println(com.getComputedValues().toString());
        
        sessionId++;
      }
      catch( DataException e )
      {
        System.err.println(e.getMessage());
        e.printStackTrace();
      }
      
      /*try
      {
        uploadFile(file);
      }
      catch( ClassNotFoundException | InterruptedException | IOException e )
      {
        e.printStackTrace();
      }*/
    }
   
    checkDate(c);
    c.add(Calendar.DAY_OF_YEAR, 1);
    checkDate(c);
    
    try
    {
      output.simplify(MergeKeepNA.class, new HashMap<String,Object>());
      output.writeFileCSV("output.csv");
      
      fuvp.persist();
    }
    catch( DataException | IOException e )
    {
      System.err.println(e.getMessage());
      e.printStackTrace();
    }
    
  }
  
  private void fetchFiles()
  {
    final File folder= new File("data");
    for(final File fileEntry : folder.listFiles())
    {
      if( !fileEntry.isDirectory() && fileEntry.getName().toLowerCase().endsWith(".ser") )
      {
        Path path= Paths.get(fileEntry.getAbsolutePath());
        Path parent= path.toAbsolutePath().getParent();
        String fn= path.getFileName().toString();
        fn= fn.substring(0, fn.length()-4);
        
        if( Files.exists(parent.resolve(fn + ".meta")) )
        {
          // System.out.println(fileEntry + " has meta");
          try
          {
            Metadata meta= MetadataParser.parse(Files.readAllLines(parent.resolve(fn + ".meta")));
            if( checkMeta(meta) )
              addFile(path, meta);
            else
              System.err.println("Could not add file " + fileEntry + " due to error in metadata");
          }
          catch( IOException | ArrayIndexOutOfBoundsException e )
          {
            System.err.println(parent.resolve(fn + ".meta"));
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
        else
          System.out.println(fileEntry + " has no meta");
      }
    }
    
    files.sort(new Comparator<DataFile>() {

      @Override
      public int compare(DataFile o1, DataFile o2)
      {
        return ((Long) Long.parseLong(o1.meta.getValue("Starttime"))).compareTo(Long.parseLong(o2.meta.getValue("Starttime")));
      }
    });
  }
  
  private void addFile(Path path, Metadata meta)
  {
    DataFile e= new DataFile();
    e.file= path;
    e.meta= meta;
    files.add(e);
  }
  
  private static boolean checkMeta(Metadata meta)
  {
    return meta.hasKey("Param0") && meta.hasKey("Param1") && meta.hasKey("Param2") && meta.hasKey("User");
  }
  
  private static Map<String, String> toMap(Collection<Value> values)
  {
    Map<String, String> ret= new HashMap<String, String>(values.size());
    for(Value v : values)
    {
      ret.put(v.getName(), v.getStringValue());
    }
    return ret;
  }
  
  private void checkDate(Calendar c) throws ComputerBuilderException
  {
    while( curdate.get(Calendar.YEAR) < c.get(Calendar.YEAR) || 
             curdate.get(Calendar.YEAR) == c.get(Calendar.YEAR) && curdate.get(Calendar.DAY_OF_YEAR) < c.get(Calendar.DAY_OF_YEAR) )
    {
      System.out.println("Computing daily values " + curdate.get(Calendar.YEAR) + "-" + (curdate.get(Calendar.MONTH)+1) + "-" + curdate.get(Calendar.DAY_OF_MONTH));
      Computer com= new ComputerBuilder()
          .setUserValueProvider(fuvp)
          .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
          .setDate(curdate)
          .addComputation(new CTL())
          .addComputation(new ATL())
          .addComputation(new TSB())
          .addComputation(new DailyTSS())
          .getComputer();
      com.computeUserMetrics();
      
      curdate.add(Calendar.DAY_OF_YEAR, 1);
    }
  }
  
  private void compute(Calendar c) throws DataException, ComputerBuilderException
  {
    Data data= DataManager.getData(file.file.toString());
    // System.out.println("Before computer set up");
    Computer com= new ComputerBuilder()
        .setUserValueProvider(fuvp)
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputation(new Duration())
        .addComputation(new BikePowerFilter())
        .addComputation(
            new ColumnMovingAverage("bike_power_filtered", "bike_power30", 30))
        .addComputation(
            new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
        .addComputation(new NormalizedPower())
        .addComputation(new IntensityFactor())
        .addComputation(new WorkingTime()).addComputation(new TSS())
        .addComputation(new VI()).addComputation(new Distance())
        .getComputer();
    com.fetchUserValues();
    com.setSessionId("" + sessionId);
    
    boolean has_power= false;
    try
    {
      data.getColumnIndex("bike_power");
      has_power= true;
    }
    catch( IllegalArgumentException e )
    {
      
    }

    com.setDate(c);
    
    if( has_power )
      com.computeSessionMetrics();
    
    /*try
    {
      data.writeFileCSV("debug/test.csv");
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }*/

    // System.out.println(com.getComputedValues().toString());
    
    Map<String, Object> values= new HashMap<String, Object>();
    values.put("date", c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH) + "-" + c.get(Calendar.DAY_OF_MONTH) + " " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE));
    values.put("file", file.file.toString());
    values.putAll(toMap(com.getComputedValues()));
    
    output.addColumnRow(sessionId, values);
  }
}
