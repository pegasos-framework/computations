package at.pegasos.computer.computations;

import at.pegasos.computer.computations.data.MeanMaxPeaks;
import at.pegasos.data.compute.SessionComputation;

@SessionComputation
public class CyclingMPP extends MeanMaxPeaks {

  public CyclingMPP()
  {
    super("mmp", "bike_power", 1,3,5,10,30,60,120,180,300,600,1200,1800,60*60);
  }
}
