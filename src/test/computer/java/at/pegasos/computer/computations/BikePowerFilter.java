package at.pegasos.computer.computations;

import at.pegasos.data.compute.ColumnFilter;
import at.pegasos.data.compute.SessionComputation;

@SessionComputation
public class BikePowerFilter extends ColumnFilter {
  public BikePowerFilter()
  {
    super("bike_power_filtered", "bike_power", o -> ((Double) o) >= 0 && ((Double) o) < 2500);
  }
}
