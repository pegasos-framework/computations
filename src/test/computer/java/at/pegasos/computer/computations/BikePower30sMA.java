package at.pegasos.computer.computations;

import at.pegasos.computer.computations.math.ColumnMovingAverage;
import at.pegasos.data.compute.SessionComputation;

@SessionComputation
public class BikePower30sMA extends ColumnMovingAverage {
  public BikePower30sMA()
  {
    super("bike_power_filtered", "bike_power30", 30);
  }
}
