package at.pegasos.computer;

import java.util.List;

import at.pegasos.data.Value;

public class Helper {
  public static Value getValue(List<Value> values, String name)
  {
    for(Value v : values)
    {
      if( v.getName().equals(name) )
        return v;
    }
    return null;
  }
  
  public static double getValueAsDouble(List<Value> values, String name)
  {
    Value v= getValue(values, name);
    if( v == null )
    {
      System.err.println("Value " + name + " not found");
      return -1d;
    }
    else
      return v.getDoubleValue();
  }
}
