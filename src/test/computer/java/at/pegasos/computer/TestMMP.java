package at.pegasos.computer;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import at.pegasos.computer.computations.CyclingMPP;
import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.SecToRecTime;
import at.pegasos.data.loader.SingleRowHeader;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.DataManager;
import at.pegasos.data.compute.Computer;
import at.pegasos.data.compute.Computation;
import at.pegasos.data.compute.SessionComputation;

public class TestMMP {
  
  public static void main(String[] args) throws DataException, IOException,
      InstantiationException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, NoSuchMethodException, SecurityException, ComputerBuilderException
  {
    test1();

    // test2();

    test3();

    test4();
  }
  
  public static void test2() throws DataException, IOException
  {
	  Calendar c= Calendar.getInstance();
	    c.clear();
	    c.set(2018, 06, 07);
	    // System.out.println(c);
	  
    CSVLoader loader= CSVLoader.create()
        .determineFileParams("2019_10_13_07_54_05.csv")
        .setHeader(new SingleRowHeader(1))
        .setTimeExtractor(new SecToRecTime("secs"))
        .Load();
    Data data= loader.getData();
    data= data.Col2Data();
    // data.debug();
    data.writeFileCSV("testa.csv");
    
    /*data= normaliseCSV(data);
    data.writeFileCSV("testb.csv");
    
    Computer com= new Computer(data).setUserValueProvider(FileUserValuesProvider.getInstance(1))
            .setSessionValuesProvider(FileSessionValuesProvider.getInstance())
        .addComputation(new ColumnMovingAverage("bike_power", "bike_power30", 30))
        .addComputation(new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
        .addComputation(new NormalizedPower())
        .addComputation(new IntensityFactor())
        .addComputation(new WorkingTime())
        .addComputation(new TSS())
        .addComputation(new VI())
        .addComputation(new Distance())
        .fetchUserValues()
        .setSessionId(1234L)
        .computeSessionMetrics();

        System.out.println(com.getComputedValues().toString());
        
        com.clearComputations();
        com.setDate(c);
        com.addComputation(new CTL());
        com.addComputation(new ATL())
        .computeUserMetrics();
        // com.debugValues();
        System.out.println(com.getComputedValues().toString());
    */
  }
  
  public static void test1() throws DataException, IOException
  {
	// try { Thread.sleep(30000);} catch(Exception e) {}
	  
    /*Calendar c= Calendar.getInstance();
    c.clear();
    c.set(2018, 06, 18);
    // System.out.println(c);
    c.add(Calendar.MILLISECOND, -1);
    // System.out.println(c);
    
    System.out.println("Load File");
    Data data= SerLoader.create("2018-07-28_173432.ser").useSensorNumbers(true).loadFile().getData();
    System.out.println("File loaded. Normalising");
    data= normaliseSer(data);
    data.writeFileCSV("test.csv");
    
    // Computer com= new Computer(data).setUserValueProvider(new MockUserValueProvider())
    Computer com= new Computer(data).setUserValueProvider(FileUserValuesProvider.getInstance(1))
        .setSessionValuesProvider(FileSessionValuesProvider.getInstance())
    .addComputation(new ColumnMovingAverage("bike_power", "bike_power30", 30))
    .addComputation(new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
    .addComputation(new NormalizedPower())
    .addComputation(new IntensityFactor())
    .addComputation(new WorkingTime())
    .addComputation(new TSS())
    .addComputation(new VI())
    .addComputation(new Distance())
    .fetchUserValues()
    .setSessionId(1234L)
    .computeSessionMetrics();

    System.out.println(com.getComputedValues().toString());
    
    com.clearComputations();
    com.setDate(c);
    com.addComputation(new CTL());
    com.addComputation(new ATL())
    .computeUserMetrics();
    // com.debugValues();
    System.out.println(com.getComputedValues().toString());*/
  }
  
  public static void test3() throws DataException, IOException, ComputerBuilderException
  {
    Calendar c= Calendar.getInstance();
    c.clear();
    c.set(2019, 9, 14);
    // System.out.println(c);
    
    System.out.println("Load File");
    // Data data= SerLoader.create("2019-10-14_073247.ser").useSensorNumbers(true).loadFile().getData();
    Data data= DataManager.getData(new File(TestMMP.class.getResource("/2019-10-14_073247.ser").getFile()).getAbsolutePath());
    System.out.println("File loaded. Normalising");
    // data= normaliseSer(data);
    data.writeFileCSV("test31.csv");
    
    // Computer com= new Computer(data).setUserValueProvider(new MockUserValueProvider())
    Computer com= new ComputerBuilder(data)
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
    .addComputation(new CyclingMPP())
    .getComputer();
    com.fetchUserValues();
    com.setSessionId("" + 1234L);
    com.setDate(c);
    com.computeSessionMetrics();

    System.out.println(com.getComputedValues().toString());
  }
  
  public static void test4() throws DataException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ComputerBuilderException
  {
    Set<Class<? extends Object>> allClasses;
    
    List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
    classLoadersList.add(ClasspathHelper.contextClassLoader());
    classLoadersList.add(ClasspathHelper.staticClassLoader());

    Reflections reflections2 = new Reflections(new ConfigurationBuilder()
        .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner(), new TypeAnnotationsScanner())
        .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
        .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("at.pegasos.computer"))));
    
    allClasses= reflections2.getTypesAnnotatedWith(SessionComputation.class);
    
    Calendar c= Calendar.getInstance();
    c.clear();
    c.set(2019, 9, 14);
    // System.out.println(c);
    
    System.out.println("Load File");
    // Data data= SerLoader.create("2019-10-14_073247.ser").useSensorNumbers(true).loadFile().getData();
    Data data= DataManager.getData(new File(TestMMP.class.getResource("/2019-10-14_073247.ser").getFile()).getAbsolutePath());
    System.out.println("File loaded. Normalising");
    // data= normaliseSer(data);
    data.writeFileCSV("test31.csv");
    
    // Computer com= new Computer(data).setUserValueProvider(new MockUserValueProvider())
    Computer com= new ComputerBuilder(data)
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .getComputer();
    com.fetchUserValues();
    com.setSessionId("" + 1234L);
    for(Class<? extends Object> x : allClasses)
      com.addComputation((Computation) x.getConstructor().newInstance());
    com.setDate(c);
    com.computeSessionMetrics();

    System.out.println(com.getComputedValues().toString());
  }
}
