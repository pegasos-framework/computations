package at.pegasos.computer;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.DataManager;
import at.pegasos.data.compute.Computation;
import at.pegasos.data.compute.Computer;
import at.pegasos.data.compute.SessionComputation;

public class TestDependencies {
  public static void main(String[] args) throws DataException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ComputerBuilderException
  {
    new TestDependencies().test1();
    new TestDependencies().test2();
  }
  
  private Set<Computation> computations;
  
  private Set<String> provided;
  
  private Computer computer;
  
  public void test1() throws DataException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ComputerBuilderException
  {
    Set<Class<? extends Object>> allClasses;
    
    List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
    classLoadersList.add(ClasspathHelper.contextClassLoader());
    classLoadersList.add(ClasspathHelper.staticClassLoader());

    Reflections reflections2 = new Reflections(new ConfigurationBuilder()
        .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner(), new TypeAnnotationsScanner())
        .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
        .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("at.pegasos.data.compute"))
            .include(FilterBuilder.prefix("at.pegasos.computer.computations"))));
    
    allClasses= reflections2.getTypesAnnotatedWith(SessionComputation.class);
    
    computations= new HashSet<Computation>(allClasses.size());
    
    System.out.println("Load File");
    // Data data= SerLoader.create("2019-10-14_073247.ser").useSensorNumbers(true).loadFile().getData();
    Data data= DataManager.getData(new File(TestMMP.class.getResource("/2019-10-14_073247.ser").getFile()).getAbsolutePath());
    System.out.println("File loaded. Normalising");
    
    Calendar date= Calendar.getInstance();
    
    FileUserValuesProvider fuvp= FileUserValuesProvider.getInstance(Paths.get("."), "1");
    
    computer= new ComputerBuilder(data)
        .setUserValueProvider(fuvp)
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .getComputer();
    computer.setSessionId("" + 1234L);
    
    computer.setDate(date);
    computer.fetchUserValues();
    
    provided= new HashSet<String>();
    
    loadComputations(allClasses);
    
    for(String name : data.getColumnNames())
    {
      provided.add(name);
    }
    fuvp.getValues(Calendar.getInstance()).forEach(v -> provided.add(v.getName()));
    
    int setSize, newSize= 0;
    do
    {
      System.out.println("Run");
      setSize= computations.size();
      processComputations();
      newSize= computations.size();
    } while (setSize > newSize );
    System.out.println("Computations with unmet dependencies: " + computations.toString());
    System.out.println("Provided: " + provided.toString());
  }
  
  private void processComputations()
  {
    List<Computation> toRemove= new ArrayList<Computation>(computations.size());
    // List<Computation> toRemove= new ArrayList<Computation>(computations.size());
    for(Computation computation : computations)
    {
      Collection<String> dependencies= computation.getDependencies(); 
      if( dependencies == null || provided.containsAll(dependencies) )
      {
        toRemove.add(computation);
        computer.addComputation(computation);
        provided.addAll(Computer.provides(computation));
      }
    }
    computations.removeAll(toRemove);
  }
  
  private void loadComputations(Set<Class<? extends Object>> allClasses) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
  {
    for(Class<? extends Object> clasz : allClasses)
    {
      if( !Computation.class.isAssignableFrom(clasz) )
      {
        System.err.println(clasz + " needs to implement " + Computation.class.getCanonicalName());
        continue;
      }
      
      Computation c= (Computation) clasz.getConstructor().newInstance();
      // System.out.println(c.getDependencies().toString());
      computations.add(c);
    }
  }
  
  private void test2() throws DataException, ComputerBuilderException
  {
    System.out.println("Load File");
    // Data data= SerLoader.create("2019-10-14_073247.ser").useSensorNumbers(true).loadFile().getData();
    Data data= DataManager.getData(new File(TestMMP.class.getResource("/2019-10-14_073247.ser").getFile()).getAbsolutePath());
    System.out.println("File loaded. Normalising");
    
    Calendar date= Calendar.getInstance();
    
    computer= new ComputerBuilder(data)
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputationPackage("at.pegasos.computer.computations")
        .addComputationPackage("at.pegasos.data.compute")
        .getComputer();
    computer.setSessionId("" + 1234L);
    
    computer.setDate(date);
    computer.fetchUserValues();
    
    computer.computeSessionMetrics();
  }
}
