package at.pegasos.data;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.pegasos.computer.ComputerBuilder;
import at.pegasos.computer.ComputerBuilderException;
import at.pegasos.computer.computations.data.ZoneAnalysis;
import at.pegasos.computer.computations.math.ColumnMovingAverage;
import at.pegasos.computer.providers.FileUserValuesProvider;
import at.pegasos.computer.providers.SimpleSessionValuesProvider;
import at.pegasos.data.compute.Computer;
import at.pegasos.data.compute.Distance;
import at.pegasos.data.compute.WorkingTime;
import at.pegasos.data.compute.coggan.ATL;
import at.pegasos.data.compute.coggan.CTL;
import at.pegasos.data.compute.coggan.IntensityFactor;
import at.pegasos.data.compute.coggan.NormalizedPower;
import at.pegasos.data.compute.coggan.TSS;
import at.pegasos.data.compute.coggan.VI;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.SecToRecTime;
import at.pegasos.data.loader.SerLoader;
import at.pegasos.data.loader.SingleRowHeader;
import at.pegasos.data.simplify.FixedRateSpline;

public class Test2 {
  
  public static Data normaliseSer(Data data) throws DataException
  {
	  data.debug();
	  
    List<String> names= data.getColColNames();
    
    System.out.println(names.toString());
    
    // Find bike sensors present
    int power_minidx= Integer.MAX_VALUE;
    boolean powerPresent= false;
    for(String name : names)
    {
      String nameH= name.replace("BikeNr", "Bike");
      
      if( nameH.matches("Bike[0-9]+.power") )
      {
        powerPresent= true;
        
        // Number present
        if( nameH.indexOf('.') > 4 )
        {
          int idx= Integer.parseInt(nameH.substring(4, nameH.indexOf('.')));
          if( idx < power_minidx )
          {
            power_minidx= idx;
          }
        }
        // No number present
        else
        {
          
        }
      }
    }
    
    Map<String, Object> params= new HashMap<String, Object>();
    
    if( powerPresent )
    {
      for(String name : names)
      {
        String nameH= name.replace("BikeNr", "Bike");
        
        if( nameH.matches("Bike[0-9]+.power") )
        {
          powerPresent= true;
          
          // Number present
          if( nameH.indexOf('.') > 4 )
          {
            int idx= Integer.parseInt(nameH.substring(4, nameH.indexOf('.')));
            if( idx != power_minidx )
            {
              FixedRateSpline.dropColumn(params, name);
            }
          }
          // No number present
          else
          {
            if( power_minidx != Integer.MAX_VALUE )
            {
              FixedRateSpline.dropColumn(params, name);
            }
          }
        }
      }
    }
    
    params.put(FixedRateSpline.PARAM_ROUND_ALL, true);
    FixedRateSpline.OnSingleColUseOnlyName(params, true);
    
    System.out.println(params.toString());
    data.simplify(FixedRateSpline.class, params);
    
    if( powerPresent )
    {
      data.renameColumn("Bike" + power_minidx + ".power", "bike_power");
      data.renameColumn("BikeNr" + power_minidx + ".power", "bike_power");
    }
    
    // data.debug();
    
    return data;
  }
  
  public static Data normaliseCSV(Data data) throws DataException
  {
    Map<String, Object> params= new HashMap<String, Object>();
    
    // params.put(FixedRateSpline.PARAM_ROUND_ALL, true);
    FixedRateSpline.Round(params, "power");
    FixedRateSpline.Round(params, "hr");
    FixedRateSpline.Round(params, "cad");
    FixedRateSpline.OnSingleColUseOnlyName(params, true);
    
    System.out.println(params.toString());
    data.simplify(FixedRateSpline.class, params);
    
    data.renameColumn("data_power", "bike_power");
    data.renameColumn("data_cad", "bike_cad");
    data.scaleColumn("data_km", 1000);
    data.renameColumn("data_km", "bike_distance_m");

    return data;
  }
  
  public static void main(String[] args) throws DataException, IOException, ComputerBuilderException
  {
    test1();
    
    test2();
	  
	test3();
  }
  
  public static void test2() throws DataException, IOException, ComputerBuilderException
  {
	  Calendar c= Calendar.getInstance();
	    c.clear();
	    c.set(2018, 06, 07);
	    // System.out.println(c);
	  
    CSVLoader loader= CSVLoader.create()
        .determineFileParams("2019_10_13_07_54_05.csv")
        .setHeader(new SingleRowHeader(1))
        .setTimeExtractor(new SecToRecTime("secs"))
        .Load();
    Data data= loader.getData();
    data= data.Col2Data();
    // data.debug();
    data.writeFileCSV("testa.csv");
    
    data= normaliseCSV(data);
    data.writeFileCSV("testb.csv");
    
    Computer com= new ComputerBuilder()
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
        .addComputation(new ColumnMovingAverage("bike_power30", "bike_power", 30))
        .addComputation(new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
        .addComputation(new NormalizedPower())
        .addComputation(new IntensityFactor())
        .addComputation(new WorkingTime())
        .addComputation(new TSS())
        .addComputation(new VI())
        .addComputation(new Distance())
        .getComputer();
    com.fetchUserValues();
    com.setSessionId("" + 1234L);
    com.computeSessionMetrics();

        System.out.println(com.getComputedValues().toString());
        
        com.clearComputations();
        com.setDate(c);
        com.addComputation(new CTL());
        com.addComputation(new ATL());
        com.computeUserMetrics();
        // com.debugValues();
        System.out.println(com.getComputedValues().toString());
  }
  
  public static void test1() throws DataException, IOException, ComputerBuilderException
  {
	// try { Thread.sleep(30000);} catch(Exception e) {}
	  
    Calendar c= Calendar.getInstance();
    c.clear();
    c.set(2018, 06, 18);
    // System.out.println(c);
    c.add(Calendar.MILLISECOND, -1);
    // System.out.println(c);
    
    System.out.println("Load File");
    Data data= SerLoader.create("2018-07-28_173432.ser").useSensorNumbers(true).loadFile().getData();
    System.out.println("File loaded. Normalising");
    data= normaliseSer(data);
    data.writeFileCSV("test.csv");
    
    // Computer com= new Computer(data).setUserValueProvider(new MockUserValueProvider())
    Computer com= new ComputerBuilder()
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
    .addComputation(new ColumnMovingAverage("bike_power30", "bike_power", 30))
    .addComputation(new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
    .addComputation(new NormalizedPower())
    .addComputation(new IntensityFactor())
    .addComputation(new WorkingTime())
    .addComputation(new TSS())
    .addComputation(new VI())
    .addComputation(new Distance())
    .getComputer();
    com.fetchUserValues();
    com.setSessionId("" + 1234L);
    com.computeSessionMetrics();

    System.out.println(com.getComputedValues().toString());
    
    com.clearComputations();
    com.setDate(c);
    com.addComputation(new CTL());
    com.addComputation(new ATL());
    com.computeUserMetrics();
    // com.debugValues();
    System.out.println(com.getComputedValues().toString());
  }
  
  public static void test3() throws DataException, IOException, ComputerBuilderException
  {
    Calendar c= Calendar.getInstance();
    c.clear();
    c.set(2019, 9, 14);
    // System.out.println(c);
    
    System.out.println("Load File");
    Data data= SerLoader.create("2019-10-14_073247.ser").useSensorNumbers(true).loadFile().getData();
    System.out.println("File loaded. Normalising");
    data= normaliseSer(data);
    data.writeFileCSV("test31.csv");
    
    // Computer com= new Computer(data).setUserValueProvider(new MockUserValueProvider())
    Computer com= new ComputerBuilder()
        .setUserValueProvider(FileUserValuesProvider.getInstance(Paths.get("."), "1"))
        .setSessionValuesProvider(SimpleSessionValuesProvider.getInstance())
    .addComputation(new ColumnMovingAverage("bike_power30", "bike_power", 30))
    .addComputation(new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"))
    .addComputation(new NormalizedPower())
    .addComputation(new IntensityFactor())
    .addComputation(new WorkingTime())
    .addComputation(new TSS())
    .addComputation(new VI())
    .addComputation(new Distance())
    .getComputer();
    com.fetchUserValues();
    com.setSessionId("" + 1234L);
    com.computeSessionMetrics();

    System.out.println(com.getComputedValues().toString());
    
    com.clearComputations();
    com.setDate(c);
    com.addComputation(new CTL());
    com.addComputation(new ATL());
    com.computeUserMetrics();
    // com.debugValues();
    System.out.println(com.getComputedUserValues().toString());
    
    System.out.println("Load File");
    data= SerLoader.create("2019-10-15_072017.ser").useSensorNumbers(true).loadFile().getData();
    System.out.println("File loaded. Normalising");
    data= normaliseSer(data);
    data.writeFileCSV("test32.csv");
    
    com.clearComputations();
    com.setData(data);
    com.addComputation(new ColumnMovingAverage("bike_power", "bike_power30", 30));
    com.addComputation(new ZoneAnalysis("POWER_ZONE", "bike_power", "POWER_ZONE"));
    com.addComputation(new NormalizedPower());
    com.addComputation(new IntensityFactor());
    com.addComputation(new WorkingTime());
    com.addComputation(new TSS());
    com.addComputation(new VI());
    com.addComputation(new Distance());
    com.fetchUserValues();
    com.setSessionId("" + 1234L);
    com.computeSessionMetrics();

    System.out.println(com.getComputedValues().toString());
    
    c.set(2019, 9, 15);
    
    com.clearComputations();
    com.setDate(c);
    com.addComputation(new CTL());
    com.addComputation(new ATL());
    com.computeUserMetrics();
    // com.debugValues();
    System.out.println(com.getComputedUserValues().toString());
  }
}
