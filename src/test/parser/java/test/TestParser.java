package test;

import java.io.File;

import at.pegasos.formula.generator.DataComputation;
import at.pegasos.formula.generator.ValueComputation;
import at.pegasos.generator.CodeGenerator;
import at.pegasos.generator.formula.ExpressionToCode;
import at.pegasos.generator.formula.UserValue;
import grammar.parser;
import java_cup.runtime.Symbol;
import node.DataAssignmentNode;
import node.IdentifierAssignmentNode;
import node.StatementListNode;
import node.StatementNode;
import node.UserAssignmentNode;
import node.expression.InvocationNode;
import util.PrintTree;

public class TestParser {
	public static void main(String[] argv) {
		String dirName = null;
		try {
			for (int i = 0; i < argv.length; i++) {
				if (argv[i].equals("-dir")) {
					i++;
					if (i >= argv.length)
						throw new Error("Missing directory name");
					dirName = argv[i];
				} else {
					throw new Error("Usage: java Main -dir directory");
				}
			}
			if (dirName == null)
				dirName = ".";
			
			// System.setErr(new PrintStream(new FileOutputStream(new File(dirName, "program.err"))));
			// System.setOut(new PrintStream(new FileOutputStream(new File(dirName,
			// "program.out"))));

			parser p = new parser(new File(dirName, "program3.in"));
			Symbol s= p.parse();
			
			System.out.println("#Errors: " + p.getSyntaxErrorCount());
			if( p.getSyntaxErrorCount() > 0 )
				return;
			// System.out.println("Symbol: " + s);
			System.out.println((StatementListNode) s.value);
			// p.debug_parse(); //For debugging
			PrintTree.print((StatementListNode) s.value);
			
			CodeGenerator gen;
			for(StatementNode statement : ((StatementListNode) s.value)) {
				if( statement instanceof UserAssignmentNode ) {
					UserAssignmentNode ass= (UserAssignmentNode) statement;
					// System.out.println(ass.getExpr().genCode(CodeContext.Expression));
					gen= new UserValue(ass, "at.test");
					gen.setOutput(System.out);
					// gen.generate();
					System.out.println(new ExpressionToCode(ass.getExpr()).getCode());
				} else if( statement instanceof IdentifierAssignmentNode ) {
					IdentifierAssignmentNode ass= (IdentifierAssignmentNode) statement;
					System.out.println(new ExpressionToCode(ass.getExpr()).getCode());
					if( ass.getExpr() instanceof InvocationNode ) {
						
					}
					
					gen= new ValueComputation(ass, "at.test");
					gen.setOutput(System.out);
					gen.generate();
				} else if (statement instanceof DataAssignmentNode) {
					DataAssignmentNode ass = (DataAssignmentNode) statement;

					gen = new DataComputation(ass, "at.test");
					gen.setOutput(System.out);
					gen.generate();
				} 
			}
		} catch (Exception exception) {
			System.err.println("Exception in Main " + exception.toString());
			exception.printStackTrace();
		}
	}
}
