package generator;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.pegasos.formula.generator.InvocationAnalyser;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.arithmetic.DivideNode;
import node.arithmetic.ExponentNode;
import node.arithmetic.MinusNode;
import node.arithmetic.PlusNode;
import node.arithmetic.TimesNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.expression.MethodNameNode;
import node.value.NumberNode;
import node.value.StringNode;

public class TestIsSimple {

  private NumberNode one= new NumberNode("1");
  private DataValueIdentifierNode col= new DataValueIdentifierNode("value");
  private InvocationNode sessionSum= new InvocationNode(new MethodNameNode("sessionSumDay"),
      new ExprListNode(new StringNode("Value"), one, one));
  private InvocationNode sin1= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(one));
  private InvocationNode sincol= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(col));

  @Before
  public void setUp() throws Exception
  {
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void colArith_notSimple()
  {
    ExpressionNode n= new TimesNode(col, col);
    assertEquals(false, n.isSimple());
    n= new ExponentNode(col, one);
    assertEquals(false, n.isSimple());
    n= new PlusNode(col, col);
    assertEquals(false, n.isSimple());
    n= new MinusNode(col, col);
    assertEquals(false, n.isSimple());
    n= new DivideNode(col, col);
    assertEquals(false, n.isSimple());
  }

  @Test
  public void testUserMethods_areSimple()
  {
    ExpressionNode n= sessionSum.copy();
    InvocationAnalyser.setUsageContextUser(n);
    assertEquals(true, n.isSimple());
  }

  @Test
  public void sin1_isSimple()
  {
    ExpressionNode n= sin1.copy();
    InvocationAnalyser.setUsageContextUser(n);
    assertEquals(true, n.isSimple());
    InvocationAnalyser.setUsageContextData(n);
    assertEquals(true, n.isSimple());
    InvocationAnalyser.setUsageContextIdentifier(n);
    assertEquals(true, n.isSimple());
  }

  @Test
  public void sincol_NotSimple()
  {
    ExpressionNode n= sincol.copy();
    InvocationAnalyser.setUsageContextData(n);
    assertEquals(false, n.isSimple());
  }

  @Test
  public void sin1sin1_isSimple()
  {
    ExpressionNode n= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(sin1.copy()));
    InvocationAnalyser.setUsageContextUser(n);
    assertEquals(true, n.isSimple());
    InvocationAnalyser.setUsageContextData(n);
    assertEquals(true, n.isSimple());
    InvocationAnalyser.setUsageContextIdentifier(n);
    assertEquals(true, n.isSimple());
  }

  @Test
  public void sinOfSessionSum_isSimple()
  {
    ExpressionNode n= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(sessionSum.copy()));
    InvocationAnalyser.setUsageContextUser(n);
    assertEquals(true, n.isSimple());
  }

  @Test
  public void sinOfSessionSum1_isSimple()
  {
    ExpressionNode n= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(new TimesNode(sessionSum.copy(), one)));
    InvocationAnalyser.setUsageContextUser(n);
    assertEquals(true, n.isSimple());
  }
}
