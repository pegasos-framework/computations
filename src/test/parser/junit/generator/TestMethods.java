package generator;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.pegasos.formula.generator.ValueComputation;
import at.pegasos.generator.CodeGenerator;
import at.pegasos.generator.GeneratorException;
import grammar.AssignmentType;
import node.ExpressionNode;
import node.IdentifierAssignmentNode;
import node.ValueIdentifierNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.expression.MethodNameNode;
import node.value.NumberNode;

public class TestMethods {
  
  private NumberNode one= new NumberNode("1");
  private ValueIdentifierNode value= new ValueIdentifierNode("value");
  
  @Before
  public void setUp() throws Exception
  {
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void test()
  {
    ExpressionNode expr= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(one), AssignmentType.Identifier);
    CodeGenerator gen= new ValueComputation(new IdentifierAssignmentNode(value, expr), "at.test");
    
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, "UTF-8") )
    {
      gen.setOutput(ps);

      gen.generate();

      String data= new String(baos.toByteArray(), StandardCharsets.UTF_8);
      // System.out.println(data);

      assertEquals("package at.test;\n" + 
          "\n" + 
          "\n" + 
          "import at.pegasos.computer.computations.BasicValueComputation;\n" +
          "import at.pegasos.computer.SessionComputation;\n" +
          "import at.pegasos.computer.Computer;\n" +
          "\n" +
          "@SessionComputation\n" + 
          "public class Valuevalue extends BasicValueComputation {\n" + 
          "\n" + 
          "  @Override\n" + 
          "  public String getName()\n" + 
          "  {\n" + 
          "    return \"value\";\n" + 
          "  }\n" + 
          "  \n" + 
          "  public java.util.Collection<String> getDependencies()\n" + 
          "  {\n" + 
          "    return null;\n" + 
          "  }\n" + 
          "  \n" + 
          "  @Override\n" + 
          "  public void compute(Computer computer)\n" + 
          "  {\n" + 
          "    value= Math.sin(1);\n" +
          "  }\n" + 
          "  \n" + 
          "}\n" + 
          "// End of generated class Valuevalue\n\n",
          data);
    }
    catch( UnsupportedEncodingException e )
    {
      e.printStackTrace();
      fail();
    }
    catch( GeneratorException e )
    {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void test_extractMethod()
  {
    String t= "package at.test;\n" +
        "\n" +
        "\n" +
        "import at.pegasos.computer.computations.BasicValueComputation;\n" +
        "import at.pegasos.computer.Computer;\n" +
        "import at.pegasos.computer.SessionComputation;\n" +
        "\n" +
        "@SessionComputation\n" +
        "public class Valuevalue extends BasicValueComputation {\n" +
        "\n" +
        "  @Override\n" +
        "  public String getName()\n" +
        "  {\n" +
        "    return \"value\";\n" +
        "  }\n" +
        "  \n" +
        "  public java.util.Collection<String> getDependencies()\n" +
        "  {\n" +
        "    return null;\n" +
        "  }\n" +
        "  \n" +
        "  @Override\n" +
        "  public void compute(Computer computer)\n" +
        "  {\n" +
        "    value= sin(1);\n" +
        "  }\n" +
        "  \n" +
        "}\n" +
        "// End of generated class Valuevalue\n\n";

    assertEquals(GeneratorTest.extractMethod(t, "public void compute"), "  public void compute(Computer computer)\n" +
        "  {\n" +
        "    value= sin(1);\n" +
        "  }\n");
  }
}
