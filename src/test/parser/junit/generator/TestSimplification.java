package generator;

import static org.junit.Assert.*;

import org.junit.Test;

import at.pegasos.formula.generator.*;
import node.*;
import node.arithmetic.*;
import node.expression.*;
import node.value.NumberNode;

public class TestSimplification {

  private NumberNode one= new NumberNode("1");
  private NumberNode two= new NumberNode("2");
  private DataValueIdentifierNode col= new DataValueIdentifierNode("value");
  private DataValueIdentifierNode colWithSubset= new DataValueIdentifierNode("value", new DataSubsetSpec(one, two));
  private DataValueIdentifierNode colWithSubsetMethods= new DataValueIdentifierNode("value",
      new DataSubsetSpec(one, new InvocationNode(new MethodNameNode("length"), new ExprListNode(col))));
  private DataValueIdentifierNode colWithSubsetMethodsArit= new DataValueIdentifierNode("value", new DataSubsetSpec(one,
      new DivideNode(new InvocationNode(new MethodNameNode("length"), new ExprListNode(col)), two)));
  private InvocationNode sin1= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(one));
  private InvocationNode sin1sin1= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(sin1.copy()));
  private InvocationNode sincol= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(col));
  private BinaryArithmeticExpression multi= new TimesNode(one, two);

  @Test
  public void directColumnComputation_notSimplified()
  {
    ExpressionNode n= new TimesNode(col, col);
    ExpressionNode ncopy= n.copy();

    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  @Test
  public void sin1_notSimplified()
  {
    ExpressionNode n= sin1;
    ExpressionNode ncopy= n.copy();

    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  @Test
  public void sincol_notSimplified()
  {
    ExpressionNode n= sincol;
    ExpressionNode ncopy= n.copy();

    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  @Test
  public void sin1sin1_notSimplified()
  {
    ExpressionNode n= sin1sin1;
    ExpressionNode ncopy= n.copy();

    InvocationAnalyser.setUsageContextUser(n);
    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  @Test
  public void colWithSubset_notSimplified()
  {
    ExpressionNode n= colWithSubset;
    ExpressionNode ncopy= n.copy();

    InvocationAnalyser.setUsageContextIdentifier(ncopy);
    StatementSimplifier s= new StatementSimplifier(ncopy);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  @Test
  public void colWithSubsetMethods_simplified()
  {
    ExpressionNode n= colWithSubsetMethods;
    ExpressionNode ncopy= n.copy();

    InvocationAnalyser.setUsageContextIdentifier(ncopy);
    StatementSimplifier s= new StatementSimplifier(ncopy);
    StatementListNode x= s.simplify();

    assertEquals(false, n.equals(ncopy));
    assertEquals(1, x.size());
  }

  @Test
  public void colWithSubsetMethodsArith_simplified()
  {
    ExpressionNode n= colWithSubsetMethodsArit;
    ExpressionNode ncopy= n.copy();

    InvocationAnalyser.setUsageContextIdentifier(ncopy);
    StatementSimplifier s= new StatementSimplifier(ncopy);
    StatementListNode x= s.simplify();

    assertEquals(false, n.equals(ncopy));
    assertEquals(2, x.size());
  }

  @Test
  public void multiplication_notSimplified()
  {
    ExpressionNode n= multi;
    ExpressionNode ncopy= n.copy();

    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  @Test
  public void multiplicationInList_notSimplified()
  {
    ExpressionNode n= new ExprListNode(multi, new TimesNode(two, one));
    ExpressionNode ncopy= n.copy();

    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  @Test
  public void multiplicationInInvociation_notSimplified()
  {
    ExpressionNode n= new InvocationNode(new MethodNameNode("test"), new ExprListNode(multi, new TimesNode(two, one)));
    ExpressionNode ncopy= n.copy();

    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();

    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }

  // TODO: this test case fails because data[col]*data[col]*1 is simplified
  /*@Test
  public void directColumnComputation2_notSimplified()
  {
    ExpressionNode n= new TimesNode(new TimesNode(col, col), one);
    ExpressionNode ncopy= n.copy();
    
    StatementSimplifier s= new StatementSimplifier(n);
    StatementListNode x= s.simplify();
    
    assertEquals(true, n.equals(ncopy));
    assertEquals(0, x.size());
  }*/
}
