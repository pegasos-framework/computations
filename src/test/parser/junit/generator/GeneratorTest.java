package generator;

public abstract class GeneratorTest {

  public static String extractMainClassname(String code)
  {
    String[] parts= code.split("\n");
    for(String line : parts)
    {
      if( line.startsWith("public class") )
      {
        String p= line.substring(13);
        int idx= p.indexOf(" ");
        return p.substring(0, idx);
      }
    }

    return null;
  }

  public static String extractMethod(String code, String methodbegin)
  {
    String[] parts= code.split("\n");
    int i;
    int begin= -1, end= -1;
    for(i= 0; i < parts.length; i++)
    {
      if( parts[i].contains(methodbegin) )
      {
        begin= i;
        break;
      }
    }

    if( begin != -1 )
    {
      for(i= begin + 2; i < parts.length; i++)
      {
        if( parts[i].startsWith("  }") )
        {
          end= i;
          break;
        }
      }
    }

    if( end != -1 )
    {
      String ret= "";
      for(i= begin; i <= end; i++)
      {
        ret+= parts[i] + "\n";
      }
      return ret;
    }
    else
      return "";
  }
}
