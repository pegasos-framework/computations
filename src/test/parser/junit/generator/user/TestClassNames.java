package generator.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import at.pegasos.generator.formula.UserValue;
import generator.GeneratorTest;
import node.ExpressionNode;
import node.TimeEnum;
import node.TimespecNode;
import node.UserAssignmentNode;
import node.UserValueIdentifierNode;
import node.arithmetic.PlusNode;
import node.value.NumberNode;

public class TestClassNames {

  private NumberNode one= new NumberNode("1");
  private NumberNode two= new NumberNode("2");

  @Test
  public void test_notimespec() throws Exception
  {
    ExpressionNode expr= new PlusNode(one, two);
    UserValue gen= new UserValue(new UserAssignmentNode(new UserValueIdentifierNode("value"), expr), "at.test");

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, "UTF-8") )
    {
      gen.setOutput(ps);

      gen.generate();

      String code= new String(baos.toByteArray(), StandardCharsets.UTF_8);
      
      assertEquals("UserValuevalue", GeneratorTest.extractMainClassname(code));
    }
    catch(Exception e)
    {
      fail("Generation failed");
    }
  }

  @Test
  public void test_day() throws Exception
  {
    ExpressionNode expr= new PlusNode(one, two);
    UserValue gen= new UserValue(new UserAssignmentNode(new UserValueIdentifierNode("value", new TimespecNode(TimeEnum.Day)), expr), "at.test");

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, "UTF-8") )
    {
      gen.setOutput(ps);

      gen.generate();

      String code= new String(baos.toByteArray(), StandardCharsets.UTF_8);
      
      assertEquals("UserValuevalueDay", GeneratorTest.extractMainClassname(code));
    }
    catch(Exception e)
    {
      fail("Generation failed");
    }
  }
  

  @Test
  public void test_week() throws Exception
  {
    ExpressionNode expr= new PlusNode(one, two);
    UserValue gen= new UserValue(new UserAssignmentNode(new UserValueIdentifierNode("value", new TimespecNode(TimeEnum.Week)), expr), "at.test");

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, "UTF-8") )
    {
      gen.setOutput(ps);

      gen.generate();

      String code= new String(baos.toByteArray(), StandardCharsets.UTF_8);
      
      assertEquals("UserValuevalueWeek", GeneratorTest.extractMainClassname(code));
    }
    catch(Exception e)
    {
      fail("Generation failed");
    }
  }
  

  @Test
  public void test_month() throws Exception
  {
    ExpressionNode expr= new PlusNode(one, two);
    UserValue gen= new UserValue(new UserAssignmentNode(new UserValueIdentifierNode("value", new TimespecNode(TimeEnum.Month)), expr), "at.test");

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, "UTF-8") )
    {
      gen.setOutput(ps);

      gen.generate();

      String code= new String(baos.toByteArray(), StandardCharsets.UTF_8);
      
      assertEquals("UserValuevalueMonth", GeneratorTest.extractMainClassname(code));
    }
    catch(Exception e)
    {
      fail("Generation failed");
    }
  }

  @Test
  public void test_year() throws Exception
  {
    ExpressionNode expr= new PlusNode(one, two);
    UserValue gen= new UserValue(new UserAssignmentNode(new UserValueIdentifierNode("value", new TimespecNode(TimeEnum.Year)), expr), "at.test");

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, "UTF-8") )
    {
      gen.setOutput(ps);

      gen.generate();

      String code= new String(baos.toByteArray(), StandardCharsets.UTF_8);
      
      assertEquals("UserValuevalueYear", GeneratorTest.extractMainClassname(code));
    }
    catch(Exception e)
    {
      fail("Generation failed");
    }
  }
}
