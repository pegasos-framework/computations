package generator.user;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.generator.formula.UserValue;
import generator.GeneratorTest;
import node.ExpressionNode;
import node.UserAssignmentNode;
import node.UserValueIdentifierNode;
import node.arithmetic.PlusNode;
import node.value.NumberNode;

public class TestSimpleUserComputation {

  private NumberNode one= new NumberNode("1");
  private NumberNode two= new NumberNode("2");
  private UserValueIdentifierNode value= new UserValueIdentifierNode("value");
  private UserValue gen;
  private String code;

  @Before
  public void setUp() throws Exception
  {
    ExpressionNode expr= new PlusNode(one, two);
    gen= new UserValue(new UserAssignmentNode(value, expr), "at.test");

    final ByteArrayOutputStream baos= new ByteArrayOutputStream();
    try( PrintStream ps= new PrintStream(baos, true, "UTF-8") )
    {
      gen.setOutput(ps);

      gen.generate();

      code= new String(baos.toByteArray(), StandardCharsets.UTF_8);
    }
  }

  @Test
  public void test_classnameCorrect()
  {
    assertEquals("UserValuevalue", GeneratorTest.extractMainClassname(code));
  }

  @Test
  public void test_computationCorrect()
  {
    assertEquals("  public void compute(Computer computer)\n" +
        "  {\n" +
        "    setComputer(computer);\n" +
        "    value= 1 + 2;\n" +
        "  }\n", GeneratorTest.extractMethod(code, "public void compute"));
  }
}
