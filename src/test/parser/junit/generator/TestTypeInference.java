package generator;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.pegasos.formula.generator.InvocationAnalyser;
import at.pegasos.formula.parser.Type;
import at.pegasos.formula.parser.TypeInferer;
import node.DataSubsetSpec;
import node.DataValueIdentifierNode;
import node.ExpressionNode;
import node.ValueIdentifierNode;
import node.arithmetic.DivideNode;
import node.arithmetic.ExponentNode;
import node.arithmetic.MinusNode;
import node.arithmetic.PlusNode;
import node.arithmetic.TimesNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.expression.MethodNameNode;
import node.value.NumberNode;
import node.value.StringNode;

public class TestTypeInference {

  private NumberNode one= new NumberNode("1");
  private NumberNode two= new NumberNode("2");
  private ValueIdentifierNode value= new ValueIdentifierNode("value");
  private DataValueIdentifierNode col= new DataValueIdentifierNode("value");
  private DataValueIdentifierNode colWithSubset= new DataValueIdentifierNode("value", new DataSubsetSpec(one, two));

  @Before
  public void setUp() throws Exception
  {
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void testColumnIsColumn()
  {
    assertEquals(Type.Column, TypeInferer.inferType(col));
  }

  @Test
  public void testColumnWithSubsetIsColumn()
  {
    assertEquals(Type.Column, TypeInferer.inferType(colWithSubset));
  }

  @Test
  public void testArithmeticValueTimes()
  {
    ExpressionNode n= new TimesNode(one, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticValueExponent()
  {
    ExpressionNode n= new ExponentNode(one, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticValuePlus()
  {
    ExpressionNode n= new PlusNode(one, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticValueMinus()
  {
    ExpressionNode n= new MinusNode(one, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticValueDivde()
  {
    ExpressionNode n= new DivideNode(one, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticColumnTimes()
  {
    ExpressionNode n= new TimesNode(col, two);
    assertEquals(Type.Column, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticColumnExponent()
  {
    ExpressionNode n= new ExponentNode(col, two);
    assertEquals(Type.Column, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticColumnPlus()
  {
    ExpressionNode n= new PlusNode(col, two);
    assertEquals(Type.Column, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticColumnMinus()
  {
    ExpressionNode n= new MinusNode(col, two);
    assertEquals(Type.Column, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticColumnDivide()
  {
    ExpressionNode n= new DivideNode(col, two);
    assertEquals(Type.Column, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticUserValue()
  {
    ExpressionNode n= new TimesNode(value, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticUserValueExponent()
  {
    ExpressionNode n= new ExponentNode(value, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticUserValuePlus()
  {
    ExpressionNode n= new PlusNode(value, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticUserValueMinus()
  {
    ExpressionNode n= new MinusNode(value, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testArithmeticUserValueDivide()
  {
    ExpressionNode n= new DivideNode(value, two);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n));
  }

  @Test
  public void testJavaFunctionI()
  {
    ExpressionNode n1= new PlusNode(one, two);
    ExpressionNode n2= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(n1));
    InvocationAnalyser.setUsageContextIdentifier(n2);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n2));
  }

  @Test
  public void testJavaFunctionD1()
  {
    ExpressionNode n1= new PlusNode(one, two);
    ExpressionNode n2= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(n1));
    InvocationAnalyser.setUsageContextData(n2);
    assertEquals(Type.Column, TypeInferer.inferType(n2));
  }

  @Test
  public void testJavaFunctionD2()
  {
    ExpressionNode n1= new PlusNode(col, two);
    ExpressionNode n2= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(n1));
    InvocationAnalyser.setUsageContextData(n2);
    assertEquals(Type.Column, TypeInferer.inferType(n2));
  }

  @Test
  public void testJavaFunctionD3()
  {
    ExpressionNode n1= new PlusNode(col, two);
    ExpressionNode n2= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(n1));
    ExpressionNode n3= new PlusNode(n2, two);
    InvocationAnalyser.setUsageContextData(n3);
    assertEquals(Type.Column, TypeInferer.inferType(n3));
  }

  @Test
  public void testJavaFunctionD4()
  {
    ExpressionNode n1= new TimesNode(col, two);
    ExpressionNode n2= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(n1));
    ExpressionNode n3= new InvocationNode(new MethodNameNode("area"), new ExprListNode(col));
    ExpressionNode n4= new PlusNode(n2, n3);
    InvocationAnalyser.setUsageContextData(n4);
    // PrintTree.print(n4);
    assertEquals(Type.Column, TypeInferer.inferType(n4));
  }

  @Test
  public void testJavaFunctionUserSimple1()
  {
    ExpressionNode n2= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(one));
    InvocationAnalyser.setUsageContextUser(n2);
    assertEquals(Type.SingleValue, TypeInferer.inferType(n2));
  }

  @Test
  public void testJavaFunctionUserSimple2()
  {
    ExpressionNode n1= new InvocationNode(new MethodNameNode("sessionSumDay"),
        new ExprListNode(new StringNode("test"), one, one));
    InvocationAnalyser.setUsageContextUser(n1);
    assertEquals("sessionSumDay(\"test\",1,1) is a value", Type.SingleValue, TypeInferer.inferType(n1));
  }

  @Test
  public void testJavaFunctionUserNested()
  {
    ExpressionNode n1= new InvocationNode(new MethodNameNode("sessionSumDay"),
        new ExprListNode(new StringNode("test"), one, one));
    ExpressionNode n2= new InvocationNode(new MethodNameNode("sin"), new ExprListNode(n1));
    InvocationAnalyser.setUsageContextUser(n2);
    assertEquals("sin(sessionSumDay(\"test\",1,1)) is a value", Type.SingleValue, TypeInferer.inferType(n2));
  }
}
