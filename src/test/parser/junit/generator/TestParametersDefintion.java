package generator;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.pegasos.formula.parser.ParameterDefintion;
import at.pegasos.formula.parser.ParameterDefintion.Arity;
import at.pegasos.formula.parser.ParametersDefintion;
import at.pegasos.formula.parser.Type;
import node.DataValueIdentifierNode;
import node.ValueIdentifierNode;
import node.expression.ExprListNode;
import node.value.NumberNode;

public class TestParametersDefintion {

  private NumberNode one= new NumberNode("1");
  private NumberNode two= new NumberNode("2");
  private ValueIdentifierNode value= new ValueIdentifierNode("value");
  private ValueIdentifierNode value2= new ValueIdentifierNode("value2");
  private DataValueIdentifierNode dval1= new DataValueIdentifierNode("dval1");
  private DataValueIdentifierNode dval2= new DataValueIdentifierNode("dval2");

  private ExprListNode par1NVal= new ExprListNode(one);
  private ExprListNode par2NVal= new ExprListNode(one, two);
  private ExprListNode par1Val= new ExprListNode(value);
  private ExprListNode par2Val= new ExprListNode(value, value2);
  private ExprListNode par1DVal= new ExprListNode(dval1);
  private ExprListNode par2DVal= new ExprListNode(dval1, dval2);
  private ExprListNode par1DVal1Nval= new ExprListNode(dval1, one);
  private ExprListNode par1DVal2Nval= new ExprListNode(dval1, one, two);

  ParametersDefintion defOneValue= new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue));
  ParametersDefintion defTwoValues= new ParametersDefintion(new ParameterDefintion(Arity.One, Type.SingleValue),
      new ParameterDefintion(Arity.One, Type.SingleValue));
  ParametersDefintion defManyValues= new ParametersDefintion(new ParameterDefintion(Arity.Many, Type.SingleValue));
  ParametersDefintion defOneDataValue= new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column));
  ParametersDefintion defOneDataManyValues= new ParametersDefintion(new ParameterDefintion(Arity.One, Type.Column),
      new ParameterDefintion(Arity.Many, Type.SingleValue));

  @Before
  public void setUp() throws Exception
  {
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void testOneArgVal()
  {
    assertEquals(true, defOneValue.matches(par1NVal));
    assertEquals(false, defOneValue.matches(par2NVal));
    assertEquals(true, defOneValue.matches(par1Val));
    assertEquals(false, defOneValue.matches(par2Val));
    assertEquals(false, defOneValue.matches(par1DVal));
    assertEquals(false, defOneValue.matches(par2DVal));
    assertEquals(false, defOneValue.matches(par1DVal1Nval));
    assertEquals(false, defOneValue.matches(par1DVal2Nval));
  }

  @Test
  public void testTwoArgVal()
  {
    assertEquals(false, defTwoValues.matches(par1NVal));
    assertEquals(true, defTwoValues.matches(par2NVal));
    assertEquals(false, defTwoValues.matches(par1Val));
    assertEquals(true, defTwoValues.matches(par2Val));
    assertEquals(false, defTwoValues.matches(par1DVal));
    assertEquals(false, defTwoValues.matches(par2DVal));
    assertEquals(false, defTwoValues.matches(par1DVal1Nval));
    assertEquals(false, defTwoValues.matches(par1DVal2Nval));
  }

  @Test
  public void testManyArgVal()
  {
    assertEquals(true, defManyValues.matches(par1NVal));
    assertEquals(true, defManyValues.matches(par2NVal));
    assertEquals(true, defManyValues.matches(par1Val));
    assertEquals(true, defManyValues.matches(par2Val));
    assertEquals(false, defManyValues.matches(par1DVal));
    assertEquals(false, defManyValues.matches(par2DVal));
    assertEquals(false, defManyValues.matches(par1DVal1Nval));
    assertEquals(false, defManyValues.matches(par1DVal2Nval));
  }

  @Test
  public void testOneArgColumn()
  {
    assertEquals(false, defOneDataManyValues.matches(par1NVal));
    assertEquals(false, defOneDataManyValues.matches(par2NVal));
    assertEquals(false, defOneDataManyValues.matches(par1Val));
    assertEquals(false, defOneDataManyValues.matches(par2Val));
    assertEquals(false, defOneDataManyValues.matches(par1DVal));
    assertEquals(false, defOneDataManyValues.matches(par2DVal));
    assertEquals(true, defOneDataManyValues.matches(par1DVal1Nval));
    assertEquals(true, defOneDataManyValues.matches(par1DVal2Nval));
  }
  
  @Test
  public void testOneDataManyValues()
  {
    assertEquals(false, defOneDataValue.matches(par1NVal));
    assertEquals(false, defOneDataValue.matches(par2NVal));
    assertEquals(false, defOneDataValue.matches(par1Val));
    assertEquals(false, defOneDataValue.matches(par2Val));
    assertEquals(true, defOneDataValue.matches(par1DVal));
    assertEquals(false, defOneValue.matches(par2DVal));
    assertEquals(false, defOneValue.matches(par1DVal1Nval));
    assertEquals(false, defOneValue.matches(par1DVal2Nval));
  }
}
