package expr2code;

import static org.junit.Assert.*;

import org.junit.Test;

import at.pegasos.generator.formula.ExpressionToCode;
import node.ExpressionNode;
import node.arithmetic.DivideNode;
import node.arithmetic.ExponentNode;
import node.arithmetic.MinusNode;
import node.arithmetic.PlusNode;
import node.arithmetic.TimesNode;
import node.expression.BracketNode;
import node.value.EulerConst;
import node.value.NumberNode;

public class TestBinaryArith {
	
	private NumberNode one= new NumberNode("1");
	private NumberNode two= new NumberNode("2");
	private NumberNode three= new NumberNode("3");

	@Test
	public void addSimple() {
		ExpressionNode expr= new PlusNode(one, two);
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals("1 + 2", code.getCode());
	}
	
	@Test
	public void subSimple() {
		ExpressionNode expr= new MinusNode(one, two);
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals("1 - 2", code.getCode());
	}
	
	@Test
	public void divSimple() {
		ExpressionNode expr= new DivideNode(one, two);
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals("1 / 2", code.getCode());
	}
	
	@Test
	public void mulSimple() {
		ExpressionNode expr= new TimesNode(one, two);
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals("1 * 2", code.getCode());
	}
	
	@Test
	public void expSimple() {
		ExpressionNode expr= new ExponentNode(one, two);
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    // assertEquals("1 ^ 2", code.getCode());
		assertEquals("Math.pow(1, 2)", code.getCode());
	}
	
	@Test
	public void addThree() {
		ExpressionNode expr= new PlusNode(one, new PlusNode(two, three));
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals("1 + 2 + 3", code.getCode());
	}
	
	@Test
	public void brackets() {
		ExpressionNode expr= new BracketNode(new PlusNode(one, two));
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals("(1 + 2)", code.getCode());
	}
	
	@Test
	public void natExp() {
		ExpressionNode expr= new ExponentNode(new EulerConst(), two);
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals("Math.exp(2)", code.getCode());
	}
	
	@Test
	public void euler() {
		ExpressionNode expr= new EulerConst();
		
		ExpressionToCode code= new ExpressionToCode(expr);
		
	    assertEquals(Math.E + "", code.getCode());
	}
}
