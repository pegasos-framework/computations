package expr2code;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import at.pegasos.generator.formula.ExpressionToCode;
import grammar.AssignmentType;
import node.ExpressionNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.expression.MethodNameNode;
import node.value.NumberNode;
import node.value.StringNode;

public class TestMethods {

  private NumberNode zero= new NumberNode("0");
  private StringNode value= new StringNode("value");
  
  @Test
  public void testSessionSum()
  {
    // sessionSumDay("TSS", 0, 0)
    ExpressionNode expr= new InvocationNode(new MethodNameNode("sessionSumDay"), new ExprListNode(value, zero, zero), AssignmentType.User);
    
    ExpressionToCode code= new ExpressionToCode(expr);

    assertEquals("sessionSumDay(\"value\", 0, 0)", code.getCode());
  }
}
