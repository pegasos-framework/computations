package parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import grammar.AssignmentType;
import grammar.parser;
import java_cup.runtime.Symbol;
import node.DataValueIdentifierNode;
import node.IdentifierAssignmentNode;
import node.StatementListNode;
import node.StatementNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierNode;
import node.expression.ExprListNode;
import node.expression.InvocationNode;
import node.expression.MethodNameNode;

public class TestMethods {

  private ValueIdentifierNode value= new ValueIdentifierNode("value");
  private DataValueIdentifierNode dataa= new DataValueIdentifierNode("a");
  private UserValueIdentifierNode userb= new UserValueIdentifierNode("b");

  @Test
  public void testParse1() throws Exception
  {
    String input= "value= timeinzone(data[a], user[b])";
    parser p= new parser(input + "\n");
    Symbol s;
    s= p.parse();

    StatementNode ss= ((StatementListNode) s.value).elementAt(0);

    if( ss instanceof IdentifierAssignmentNode )
    {
      IdentifierAssignmentNode ass= (IdentifierAssignmentNode) ss;

      InvocationNode i= new InvocationNode(new MethodNameNode("timeinzone"), new ExprListNode(dataa, userb),
          AssignmentType.Identifier);
      InvocationNode i2= new InvocationNode(new MethodNameNode("timeinzone"), new ExprListNode(userb, dataa),
          AssignmentType.Identifier);

      assertEquals(value, ass.getIdent());

      assertEquals(i, ass.getExpr());
      assertNotEquals(i2, ass.getExpr());
    }
    else
    {
      fail(input + "--> is not parsed as an IdentifierAssignment");
    }
  }

  @Test
  public void testParse2() throws Exception
  {
    String input= "value= sin(sin(data[a]))";
    parser p= new parser(input + "\n");
    Symbol s;
    s= p.parse();

    StatementNode ss= ((StatementListNode) s.value).elementAt(0);

    if( ss instanceof IdentifierAssignmentNode )
    {
      IdentifierAssignmentNode ass= (IdentifierAssignmentNode) ss;

      InvocationNode i= new InvocationNode(new MethodNameNode("sin"),
          new ExprListNode(new InvocationNode(new MethodNameNode("sin"), new ExprListNode(dataa))),
          AssignmentType.Identifier);
      InvocationNode i2= new InvocationNode(new MethodNameNode("timeinzone"), new ExprListNode(userb, dataa),
          AssignmentType.Identifier);
      InvocationNode i3= new InvocationNode(new MethodNameNode("sin"),
          new ExprListNode(new InvocationNode(new MethodNameNode("sina"), new ExprListNode(dataa))),
          AssignmentType.Identifier);

      assertEquals(value, ass.getIdent());

      assertEquals(i, ass.getExpr());
      assertNotEquals(dataa, ass.getExpr());
      assertNotEquals(i2, ass.getExpr());
      assertNotEquals(i3, ass.getExpr());
    }
    else
    {
      fail(input + "--> is not parsed as an IdentifierAssignment");
    }
  }
}
