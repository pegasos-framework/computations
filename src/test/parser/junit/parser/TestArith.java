package parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import grammar.parser;
import java_cup.runtime.Symbol;
import node.ExpressionNode;
import node.IdentifierAssignmentNode;
import node.StatementListNode;
import node.StatementNode;
import node.ValueIdentifierNode;
import node.arithmetic.PlusNode;
import node.value.NumberNode;

public class TestArith {

  private NumberNode one= new NumberNode("1");
  private NumberNode two= new NumberNode("2");
  private NumberNode three= new NumberNode("3");
  private ValueIdentifierNode a= new ValueIdentifierNode("a");

  @Test
  public void addSimple() throws Exception
  {
    String input= "a= 1 + 2\n";
    parser p= new parser(input);
    Symbol s;
    s= p.parse();

    StatementNode ss= ((StatementListNode) s.value).elementAt(0);

    if( ss instanceof IdentifierAssignmentNode )
    {
      IdentifierAssignmentNode ass= (IdentifierAssignmentNode) ss;

      assertEquals(a, ass.getIdent());

      ExpressionNode expr= new PlusNode(one, two);
      ExpressionNode expr2= new PlusNode(one, one);
      ExpressionNode expr3= new PlusNode(two, one);

      assertEquals(expr, ass.getExpr());
      assertNotEquals(expr2, ass.getExpr());
      assertNotEquals(expr3, ass.getExpr());
    }
    else
    {
      fail(input + "--> is not parsed as an IdentifierAssignment");
    }
  }

  @Test
  public void addComplex() throws Exception
  {
    String input= "a= 1 + 2 + 3\n";
    parser p= new parser(input);
    Symbol s;
    s= p.parse();

    StatementNode ss= ((StatementListNode) s.value).elementAt(0);

    if( ss instanceof IdentifierAssignmentNode )
    {
      IdentifierAssignmentNode ass= (IdentifierAssignmentNode) ss;

      assertEquals(a, ass.getIdent());

      ExpressionNode expr= new PlusNode(new PlusNode(one, two), three);
      ExpressionNode expr2= new PlusNode(one, new PlusNode(two, three));

      assertEquals(expr, ass.getExpr());
      assertNotEquals(expr2, ass.getExpr());
    }
    else
    {
      fail(input + "--> is not parsed as an IdentifierAssignment");
    }
  }
}
