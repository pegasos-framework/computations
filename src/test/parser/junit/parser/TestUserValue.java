package parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import grammar.parser;
import java_cup.runtime.Symbol;
import node.ExpressionNode;
import node.IdentifierAssignmentNode;
import node.StatementListNode;
import node.StatementNode;
import node.TimeEnum;
import node.TimespecNode;
import node.UserAssignmentNode;
import node.UserValueIdentifierNode;
import node.ValueIdentifierNode;
import node.arithmetic.PlusNode;
import node.value.NumberNode;

public class TestUserValue {
	
	private NumberNode one= new NumberNode("1");
	private ValueIdentifierNode a= new ValueIdentifierNode("a");
	private UserValueIdentifierNode value= new UserValueIdentifierNode("value");
	private UserValueIdentifierNode valueD= new UserValueIdentifierNode("value", new TimespecNode(TimeEnum.Day));
	private UserValueIdentifierNode valueD1= new UserValueIdentifierNode("value", new TimespecNode(TimeEnum.Day, "1"));
	private UserValueIdentifierNode valueW= new UserValueIdentifierNode("value", new TimespecNode(TimeEnum.Week));

	@Test
	public void simple() throws Exception {
		parser p = new parser("a= user[value]\n");
		Symbol s;
		s = p.parse();
		
		StatementNode ss= ((StatementListNode) s.value).elementAt(0);
		
		if( ss instanceof IdentifierAssignmentNode ) {
			IdentifierAssignmentNode ass= (IdentifierAssignmentNode) ss;
			
			assertEquals(a, ass.getIdent());
			
		    assertEquals(value, ass.getExpr());
		} else {
			fail("a= user[value] --> is not parsed as an IdentifierAssignment");
		}
	}
	
	@Test
	public void add() throws Exception {
		String input= "a= user[value] + 1\n";
		parser p = new parser(input);
		Symbol s;
		s = p.parse();
		
		StatementNode ss= ((StatementListNode) s.value).elementAt(0);
		
		if( ss instanceof IdentifierAssignmentNode ) {
			IdentifierAssignmentNode ass= (IdentifierAssignmentNode) ss;
			
			assertEquals(a, ass.getIdent());
			
			ExpressionNode expr= new PlusNode(value, one);
			
		    assertEquals(expr, ass.getExpr());
		} else {
			fail(input + "--> is not parsed as an IdentifierAssignment");
		}
	}
	
	// user[kcal,d]= sessionSumDay("Energy", 0, 0)
	@Test
	public void timeSimpleDay() throws Exception {
		String input= "user[value,d]= 1\n";
		parser p = new parser(input);
		Symbol s;
		s = p.parse();
		
		StatementNode ss= ((StatementListNode) s.value).elementAt(0);
		
		if( ss instanceof UserAssignmentNode ) {
			UserAssignmentNode ass= (UserAssignmentNode) ss;
			
			assertEquals(valueD, ass.getIdent());
			
		    assertEquals(one, ass.getExpr());
		} else {
			fail(input + "--> is not parsed as an UserAssignment");
		}
	}
	
	@Test
	public void timeSimpleWeek() throws Exception {
		String input= "user[value,w]= 1\n";
		parser p = new parser(input);
		Symbol s;
		s = p.parse();
		
		StatementNode ss= ((StatementListNode) s.value).elementAt(0);
		
		if( ss instanceof UserAssignmentNode ) {
			UserAssignmentNode ass= (UserAssignmentNode) ss;
			
			assertEquals(valueW, ass.getIdent());
			
		    assertEquals(one, ass.getExpr());
		} else {
			fail(input + "--> is not parsed as an UserAssignment");
		}
	}
	
	@Test
	public void timeNextDay() throws Exception {
		String input= "user[value,d+1]= 1\n";
		parser p = new parser(input);
		Symbol s;
		s = p.parse();
		
		StatementNode ss= ((StatementListNode) s.value).elementAt(0);
		
		if( ss instanceof UserAssignmentNode ) {
			UserAssignmentNode ass= (UserAssignmentNode) ss;
			
			assertEquals(valueD1, ass.getIdent());
			
		    assertEquals(one, ass.getExpr());
		} else {
			fail(input + "--> is not parsed as an UserAssignment");
		}
	}
}
