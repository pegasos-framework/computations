package at.pegasos.data;

import static org.junit.Assert.*;

import java.sql.Types;
import java.util.*;

import org.junit.*;

import at.pegasos.data.Data.ColumnIterator;
import at.pegasos.data.loader.*;

public class TestBasicDataFunctions {
  
  static class CollEqualitiyChecker {
    /**
     * Check if two columns are equals ie. check if `colname` in data equals Col
     * @param expected
     * @param actual
     * @param colname
     */
    public static void assertEquals(Data expected, Data actual, String expectedColname, String actualColname)
    {
      Assert.assertEquals(expected.getRowCount(), actual.getRowCount());

      ColumnIterator it= expected.iterateTableColumn(expectedColname);
      ColumnIterator it2= actual.iterateTableColumn(actualColname);

      int i= 0;
      while( it.hasNext() )
      {
        assertTrue(it2.hasNext());
        Object o= it.next();
        Object o2= it2.next();
        // System.out.println("row: " + i);
        double d1= (Double) o;
        double d2= (Double) o2;
        if( Math.abs(d1 - d2) > 0.0001 )
        {
          System.out.println("Row " + i + " expected " + d1 + " got:" + d2);
        }
        assertTrue(Math.abs(d1 - d2) < 0.0001);
        i++;
      }
    }
  }
  
  String dataString=
      "001\t010\t100\n" + 
      "002\t020\t200\n" + 
      "003\t030\t300\n" + 
      "004\t040\t400\n" + 
      "005\t050\t500\n" + 
      "006\t060\t600\n" + 
      "007\t070\t700\n" + 
      "008\t080\t800\n" + 
      "009\t090\t900\n";
  Data rawData;
  Data data;

  @Before
  public void setUp() throws Exception
  {
    String[] lines= dataString.split("\n");
    rawData= CSVLoader.create()
        .setParams(new CSVLoader.Parameter("asdf.csv", "\t", "."))
        .setLines(Arrays.asList(lines))
        .Load(null, new NoHeader("c1", "c2", "c3"), null, new RowToTime(1)).getData();
    
    data= rawData.Col2Data();
  }

  @Test
  public void printTableNoDataNoException()
  {
    rawData.printTable();
    data.printTable();
    rawData.debug();
    data.debug();
  }

  @Test
  public void freq()
  {
    assertEquals(1, data.getFrequeny());
    assertEquals(0, rawData.getFrequeny());
  }
  
  @Test
  public void scale()
  {
    data.scaleColumn("c1", 10);
    CollEqualitiyChecker.assertEquals(data, data, "c2", "c1");
    data.scaleColumn("c3", 0.1);
    CollEqualitiyChecker.assertEquals(data, data, "c2", "c3");
  }
  
  @Test
  public void addValues()
  {
    Map<String,Object> values= new HashMap<String,Object>();
    Double[] exp= new Double[] {10d, 100d, 1000d};
    values.put("", exp);
    rawData.addColumnRow(9, values);
    data= rawData.Col2Data();

    assertEquals(10, data.getRowCount());

    Double[] act= (Double[]) data.getRow(9);
    assertEquals(exp.length, act.length);
    for(int i= 0; i < exp.length; i++)
      assertEquals(exp[i], act[i]);
  }
  
  @Test
  public void removeColumn()
  {
    String exp[]= new String[] {"c1", "c2"};
    data.removeColumn("c3");
    String act[]= new String[0];
    act= data.getColumnNames();
    assertEquals(exp.length, act.length);
    for(int i= 0; i < exp.length; i++)
      assertEquals(exp[i], act[i]);
  }
  
  @Test
  public void hasColumn()
  {
    assertEquals(true, data.hasColumn("c1"));
    assertEquals(true, data.hasColumn("c2"));
    assertEquals(true, data.hasColumn("c3"));
    assertEquals(false, data.hasColumn("c4"));
  }
  
  @Test
  public void addColumnDirect()
  {
    Data.Column c4= new Data.Column();
    c4.names= new String[] {"c4"};
    c4.timestamps= Arrays.asList(new Long[] {0l,1l,2l,3l,4l,5l,6l,7l,8l});
    c4.values= Arrays.asList(new Object[][] {new Object[] {1000d}, new Object[] {2000d}, new Object[] {3000d},
        new Object[] {4000d}, new Object[] {5000d}, new Object[] {6000d}, new Object[] {7000d}, new Object[] {8000d},
        new Object[] {9000d}});
    c4.setOriginal(true);
    c4.types= new int[] {Types.DOUBLE};

    data.addColumnDirect(c4);

    assertEquals(4, data.getColumnNames().length);
    Double[] exp= new Double[] {9d, 90d, 900d, 9000d};
    // for some reason it fails when we cast it to Double[]
    Object[] act= data.lastRow();
    assertEquals(exp.length, act.length);
    for(int i= 0; i < exp.length; i++)
      assertEquals(exp[i], act[i]);
  }
  
  @Test
  public void lastRow()
  {
    Double[] exp= new Double[] {9d, 90d, 900d};
    // for some reason it fails when we cast it to Double[]
    Object[] act= data.lastRow();
    assertEquals(exp.length, act.length);
    for(int i= 0; i < exp.length; i++)
      assertEquals(exp[i], act[i]);
  }
  
  @Test
  public void copyConstructor()
  {
    Double[] exp= new Double[] {9d, 90d, 900d};
    // for some reason it fails when we cast it to Double[]
    Object[] act= new Data(data).lastRow();
    assertEquals(exp.length, act.length);
    for(int i= 0; i < exp.length; i++)
      assertEquals(exp[i], act[i]);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void typeOfUnknownColumn_fails()
  {
    data.getType("donotexist");
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void iterateUnknownColumn_fails()
  {
    data.iterateTableColumnList("donotexist");
  }
  
  @Test(expected = IllegalStateException.class)
  public void iterateEmptyFrame_fails()
  {
    rawData.iterateTable();
  }
  
  @Test
  public void getColColNames()
  {
    String[] exp= new String[] {"c1", "c2", "c3"};
    // for some reason it fails when we cast it to Double[]
    Object[] act= data.getColumnNames();
    assertEquals(exp.length, act.length);
    for(int i= 0; i < exp.length; i++)
      assertEquals(exp[i], act[i]);
    
    data.renameColumn("c3", "c4");
    exp[2]= "c4";
    act= data.getColumnNames();
    assertEquals(exp.length, act.length);
    for(int i= 0; i < exp.length; i++)
      assertEquals(exp[i], act[i]);
  }
  
  @Test
  public void getTime()
  {
    assertEquals(0, data.getTime(0));
    assertEquals(1, data.getTime(1));
    assertEquals(2, data.getTime(2));
    assertEquals(3, data.getTime(3));
    assertEquals(4, data.getTime(4));
    assertEquals(5, data.getTime(5));
    assertEquals(6, data.getTime(6));
    assertEquals(7, data.getTime(7));
    assertEquals(8, data.getTime(8));
  }
}
