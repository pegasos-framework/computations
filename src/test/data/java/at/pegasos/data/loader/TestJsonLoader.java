package at.pegasos.data.loader;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import at.pegasos.data.Data;

public class TestJsonLoader {
  
  String data= "{\n" + 
      "    \"RIDE\":{\n" + 
      "        \"STARTTIME\":\"2017\\/01\\/09 19:54:27 UTC \",\n" + 
      "        \"RECINTSECS\":1,\n" + 
      "        \"SAMPLES\":[\n" + 
      "            { \"SECS\":0, \"KM\":0, \"WATTS\":115, \"CAD\":42, \"KPH\":22.554, \"HR\":98, \"TEMP\":0, \"SMO2\":61, \"THB\":0 },\n" + 
      "            { \"SECS\":1, \"KM\":0.00490811, \"WATTS\":116, \"CAD\":42, \"KPH\":10.342, \"HR\":98, \"TEMP\":0, \"SMO2\":61, \"THB\":0 },\n" + 
      "            { \"SECS\":2, \"KM\":0.00993518, \"WATTS\":128, \"CAD\":44, \"KPH\":23.688, \"HR\":98, \"TEMP\":0, \"SMO2\":59, \"THB\":0 },\n" + 
      "            { \"SECS\":3, \"KM\":0.0155879, \"WATTS\":138, \"CAD\":45, \"KPH\":24.192, \"HR\":98, \"TEMP\":0 },\n" + 
      "            { \"SECS\":4, \"KM\":0.0224129, \"WATTS\":139, \"CAD\":45, \"KPH\":24.066, \"HR\":98, \"TEMP\":0, \"SMO2\":59, \"THB\":0 },\n" + 
      "            { \"SECS\":5, \"KM\":0.0289719, \"WATTS\":202, \"CAD\":44, \"KPH\":22.932, \"HR\":99, \"TEMP\":0, \"SMO2\":59, \"THB\":0 },\n" + 
      "            { \"SECS\":6, \"KM\":0.0352579, \"WATTS\":202, \"CAD\":44, \"KPH\":22.428, \"HR\":100, \"TEMP\":0, \"SMO2\":58, \"THB\":0 },\n" + 
      "            { \"SECS\":7, \"KM\":0.0413269, \"WATTS\":138, \"CAD\":36, \"KPH\":19.53, \"HR\":102, \"TEMP\":0, \"SMO2\":58, \"THB\":0 },\n" + 
      "            { \"SECS\":8, \"KM\":0.0463879, \"WATTS\":131, \"CAD\":31, \"SOME\":\"String\" },\n" + 
      "            { \"SECS\":9, \"KM\":0.0509029, \"WATTS\":139, \"CAD\":43, \"KPH\":16.38, \"HR\":103, \"TEMP\":0, \"SMO2\":58, \"THB\":0 },\n" + 
      "            { \"SECS\":10, \"KM\":0.0555369, \"WATTS\":165, \"CAD\":67, \"KPH\":18.144, \"HR\":104, \"TEMP\":0, \"SMO2\":57, \"THB\":0 },\n" + 
      "            { \"SECS\":11, \"KM\":0.0601191, \"WATTS\":158, \"CAD\":67, \"KPH\":15.3971, \"HR\":105, \"TEMP\":0, \"SMO2\":57, \"THB\":0 },]}\n" +
      "}";
  
  @Rule
  public TemporaryFolder folder= new TemporaryFolder();
  
  @Test
  public void testDetermineParamsSep() throws IOException
  {
    File test= folder.newFile("2020_07_07_18_18_18.json");
    BufferedWriter writer= Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write(data);
    writer.close();
    
    JsonLoader loader= JsonLoader.create()
      .setParamsFromFile(test.getAbsolutePath())
      .setDataElement("RIDE", "SAMPLES")
      .setTimeExtractor(new JsonLoader.SecExtractor());

    loader.load();
    
    assertEquals(".", loader.params.dec);

    Data data= loader.getData().Col2Data();
    
    assertEquals(12, data.getRowCount());
    
    String[] expN= new String[] {"WATTS", "KM", "KPH", "SOME", "TEMP", "CAD", "HR", "SMO2", "THB"};
    String[] actN= data.getColumnNames();
    List<String> actNl= Arrays.asList(actN);
    assertEquals(expN.length, actN.length);
    for(int i= 0; i < expN.length; i++)
    {
      assertEquals(true, actNl.contains(expN[i]));
    }
  }
}