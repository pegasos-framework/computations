package at.pegasos.data.loader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.sql.Types;
import java.text.ParseException;

import org.junit.Test;

public class TestTimeExtractor {

  @Test
  public void testKinovea()
  {
    TimeExtractor te= new TimeFromKinovea("time");
    te.init(new String[] {"test", "test2", "time"}, new String[] {"time"},
        new int[] {Types.INTEGER, Types.INTEGER, Types.TIME}, new boolean[] {true, true});

    assertEquals(0, te.extractTime(new String[] {"a", "b", "0:00:00:00"}));
    assertEquals(0, te.extractTime(new String[] {"a", "b", "0"}));
  }

  @Test
  public void testTimeFromRecTime()
  {
    TimeExtractor te= new TimeFromRecTime("time");
    te.init(new String[] {"test", "test2", "time"}, new String[] {"time"},
        new int[] {Types.INTEGER, Types.INTEGER, Types.TIME}, new boolean[] {true, true});

    assertEquals(0, te.extractTime(new String[] {"a", "b", "0"}));
  }

  @Test
  public void testTimeFromRecTimeHMS()
  {
    TimeExtractor te= new TimeFromRecTimeHMS("time");
    te.init(new String[] {"test", "test2", "time"}, new String[] {"time"},
        new int[] {Types.INTEGER, Types.INTEGER, Types.TIME}, new boolean[] {true, true});

    assertEquals(0, te.extractTime(new String[] {"a", "b", "0:00:00:00"}));
  }

  @Test
  public void testTimeFromRecTimeHMShs()
  {
    TimeExtractor te= new TimeFromRecTimeHMShs("time");
    te.init(new String[] {"test", "test2", "time"}, new String[] {"time"},
        new int[] {Types.INTEGER, Types.INTEGER, Types.TIME}, new boolean[] {true, true});

    assertEquals(0, te.extractTime(new String[] {"a", "b", "0:00:00:00"}));
  }

  @Test
  public void testGuessingTime()
  {
    TimeExtractor te= new GuessingTimeExtractor();
    te.init(new String[] {"test", "test2", "time"}, new String[] {"time"},
        new int[] {Types.INTEGER, Types.INTEGER, Types.TIME}, new boolean[] {true, true});

    String timeString = "1970-01-01 01:00:00";
    try
    {
      long t = GuessingTimeExtractor.TimeFormats[0].parse(timeString).getTime();
      assertEquals(t, te.extractTime(new String[] {"a", "b", timeString}));
    }
    catch( ParseException e )
    {
      e.printStackTrace();
      fail();
    }
  }
}
