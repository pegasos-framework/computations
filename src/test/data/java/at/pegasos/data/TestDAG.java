package at.pegasos.data;

import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TestDAG {

  @Test public void can_addNode()
  {
    DAG<Object> dag = new DAG<>();
    Object o = new Object();
    DAG.Node<Object> n = dag.addNode(o);
    assertEquals(o, n.getID());
  }

  @Test public void addNode_unique()
  {
    DAG<Object> dag = new DAG<>();
    Object o = new Object();
    DAG.Node<Object> n = dag.addNode(o);
    assertEquals(o, n.getID());
    n = dag.addNode(o);
    assertEquals(o, n.getID());
  }

  @Test public void addEdgeNC()
  {
    DAG<Object> dag = new DAG<>();
    Object a = new Object();
    Object b = new Object();
    DAG.Node<Object> na = dag.addNode(a);
    dag.addNode(b);
    dag.addEdgeNC(a, b);
    assertEquals(1, na.successors.size());
    assertEquals(b, na.successors.get(0).getID());
  }

  @Test public void color_simple_works()
  {
    DAG<Integer> dag = new DAG<>();

    dag.addNode(0);
    dag.addNode(1);
    dag.addNode(2);
    dag.addNode(3);

    dag.addEdgeNC(2, 3);
    dag.addEdgeNC(0, 1);

    List<DAG.ColoredNode<Integer>> colored = dag.color(1, false);
    // System.out.println(colored);
    assertEquals(1, colored.get(0).getColor());
    assertEquals(Integer.valueOf(1), colored.get(0).getID());
    DAG.ColoredNode<Integer> n0 = colored.stream().filter(n -> n.getID() == 0 ).collect(
        Collectors.toList()).get(0);
    assertEquals(1, n0.getColor());
    assertEquals(Integer.valueOf(0), n0.getID());
    DAG.ColoredNode<Integer> n2 = colored.stream().filter(n -> n.getID() == 2 ).collect(
        Collectors.toList()).get(0);
    assertEquals(2, n2.getColor());
    assertEquals(Integer.valueOf(2), n2.getID());
    DAG.ColoredNode<Integer> n3 = colored.stream().filter(n -> n.getID() == 3 ).collect(
        Collectors.toList()).get(0);
    assertEquals(2, n3.getColor());
    assertEquals(Integer.valueOf(3), n3.getID());

    colored = dag.color(1, true);
    // System.out.println(colored);
    assertEquals(1, colored.get(0).getColor());
    assertEquals(Integer.valueOf(1), colored.get(0).getID());
    n0 = colored.stream().filter(n -> n.getID() == 0 ).collect(
        Collectors.toList()).get(0);
    assertEquals(2, n0.getColor());
    assertEquals(Integer.valueOf(0), n0.getID());
    n2 = colored.stream().filter(n -> n.getID() == 2 ).collect(
        Collectors.toList()).get(0);
    assertEquals(3, n2.getColor());
    assertEquals(Integer.valueOf(2), n2.getID());
    n3 = colored.stream().filter(n -> n.getID() == 3 ).collect(
        Collectors.toList()).get(0);
    assertEquals(3, n3.getColor());
    assertEquals(Integer.valueOf(3), n3.getID());
  }
}