package at.pegasos.data;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.CSVLoader.Parameter;


public class TestCSVLoader {
  
  String[] testData1= new String[] {
      "this;is;a;header",
      "1;2;3;4"
  };
  
  String testDataSep= ";";
  
  String seps[]= new String[] {";", ",", "\t", " "};

  @Before
  public void setUp() throws Exception
  {
    int idx= 0;
    for(String sep : seps)
    {
      File output= new File("/tmp/testData1" + idx + ".csv");
      BufferedWriter writer= new BufferedWriter(new FileWriter(output));
      for(String line : testData1)
      {
        writer.write(line.replaceAll(testDataSep, sep) + "\n");
      }
      writer.close();
      idx++;
    }
  }

  @After
  public void tearDown() throws Exception
  {
    int idx= 0;
    for(idx= 0; idx < seps.length; idx++)
    {
      File output= new File("/tmp/testData1" + idx + ".csv");
      output.delete();
    }
  }

  @Test
  public void testDetermineParamsSep()
  {
    int idx= 0;
    for(String sep : seps)
    {
      Parameter p= CSVLoader.determineParams("/tmp/testData1" + idx + ".csv");
      assertEquals(sep, p.sep);
      idx++;
    }
  }
}
