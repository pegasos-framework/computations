package at.pegasos.data.simplify;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.Data;
import at.pegasos.data.Data.ColumnIterator;
import at.pegasos.data.DataException;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.CSVLoader.Parameter;
import at.pegasos.data.loader.NoHeader;
import at.pegasos.data.loader.SecToRecTime;


public class TestFixedRateSpline {
  
  private Data data;
  private Data rawData;

  @Before
  public void setUp() throws DataException, IOException
  {
    CSVLoader loader= CSVLoader.create();
    // Parameter p= CSVLoader.determineParams("/home/martin/projects/Computer/src/test/resources/test.zones.csv");
    Parameter p= CSVLoader.determineParams(getClass().getResource("/test.csv").getFile());
    loader.setParams(p);
    loader.Load(null, new NoHeader("sec", "value"), null, new SecToRecTime("sec"));
    data= loader.getData();
    rawData= data.Col2Data();

    Map<String, Object> params= FixedRateSpline.newParams();
    FixedRateSpline.SingleCol(params);
    data.simplify(FixedRateSpline.class, params);

//    data.printTable();
//    rawData.printTable();
  }

  @Test
  public void testRowCount() throws DataException
  {
    assertEquals(rawData.getRowCount(), data.getRowCount());
  }

  @Test
  public void testValues() throws DataException
  {
    ColumnIterator it= data.iterateTableColumn("value");
    double exp= 1;
    while(it.hasNext())
    {
      assertEquals(exp, it.next());
      exp++;
    }
  }
  
  /*@Test
  public void testFirstRow() throws DataException
  {
    Object[] row= data.getRow(0);
    assertEquals(0d, row[0]);
    assertEquals(0d, row[1]);
    assertEquals(0d, row[2]);
  }

  @Test
  public void testLastRow() throws DataException
  {
    Object[] row= data.getRow(100);
    assertEquals(-7.255343, row[0]);
    assertEquals(-1.484967, row[1]);
    assertEquals(-1.555757, row[2]);
  }*/
}
