package at.pegasos.data.simplify;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;

import org.junit.*;

import at.pegasos.data.Data;
import at.pegasos.data.Data.ColumnIterator;
import at.pegasos.data.DataException;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.CSVLoader.Parameter;
import at.pegasos.data.loader.NoHeader;
import at.pegasos.data.loader.SecToRecTime;


public class TestSimpleSafe {
  
  @Test
  public void test() throws DataException, IOException
  {
    CSVLoader loader= CSVLoader.create();
    Parameter p= CSVLoader.determineParams(getClass().getResource("/test.csv").getFile());
    loader.setParams(p);
    loader.Load(null, new NoHeader("sec", "value"), null, new SecToRecTime("sec"));
    Data data= loader.getData();
    Data rawData= data.Col2Data().copy();

    Map<String, Object> params= FixedRateSpline.newParams();
    data.simplify(SimpleSafe.class, params);

    assertEquals(rawData.getRowCount(), data.getRowCount());
    ColumnIterator it= data.iterateTableColumn("_value");
    double exp= 1;
    while(it.hasNext())
    {
      assertEquals(exp, it.next());
      exp++;
    }
  }
}
