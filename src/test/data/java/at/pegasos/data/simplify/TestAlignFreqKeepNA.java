package at.pegasos.data.simplify;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;

import org.junit.*;

import at.pegasos.data.Data;
import at.pegasos.data.Data.Column;
import at.pegasos.data.Data.ColumnIterator;
import at.pegasos.data.DataException;
import at.pegasos.data.loader.CSVLoader;
import at.pegasos.data.loader.CSVLoader.Parameter;
import at.pegasos.data.loader.NoHeader;
import at.pegasos.data.loader.SecToRecTime;


public class TestAlignFreqKeepNA {
  
  @Test
  public void test1Col() throws DataException, IOException
  {
    CSVLoader loader= CSVLoader.create();
    Parameter p= CSVLoader.determineParams(getClass().getResource("/test.csv").getFile());
    loader.setParams(p);
    loader.Load(null, new NoHeader("sec", "value"), null, new SecToRecTime("sec"));
    Data data= loader.getData();
    Data rawData= data.Col2Data();
    
    Map<String, Object> params= FixedRateSpline.newParams();
    data.simplify(AlignFreqKeepNA.class, params);
    
    assertEquals(rawData.getRowCount(), data.getRowCount());
    ColumnIterator it= data.iterateTableColumn("value");
    double exp= 1;
    while(it.hasNext())
    {
      assertEquals(exp, it.next());
      exp++;
    }
  }

  @Test
  public void test2Col() throws DataException, IOException
  {
    CSVLoader loader= CSVLoader.create();
    Parameter p= CSVLoader.determineParams(getClass().getResource("/test.csv").getFile());
    loader.setParams(p);
    loader.Load(null, new NoHeader("sec1", "value1"), null, new SecToRecTime("sec1"));
    loader.Load(null, new NoHeader("sec2", "value2"), null, new SecToRecTime("sec2"));
    Data data= loader.getData();

    Column c= data.getColumn(1).copy();

    Column c1= data.getColumn(1);
    for(int i= 1; i <= c.values.size() / 2; i++)
    {
      // 0, X1, 2, X3, 4, X5
      // 1: 2->1, X3->2, 4->3, X5->4,
      // 2: 4->2, X5->3
      c1.values.remove(i);
      c1.timestamps.remove(i);
      c1.orig.remove(i);
    }
    c1.updateFrequency();

    Map<String, Object> params= FixedRateSpline.newParams();
    data.simplify(AlignFreqKeepNA.class, params);

    assertEquals(c.values.size(), data.getRowCount());
    ColumnIterator it= data.iterateTableColumn("_value1");
    ColumnIterator it2= data.iterateTableColumn("_value2");
    double exp= 1;
    while(it.hasNext())
    {
      assertEquals(exp, it.next());
      if( exp % 2 == 1 )
      {
        assertEquals(exp, it2.next());
      }
      else
      {
        assertEquals(exp - 1, it2.next());
      }
      exp++;
    }
  }
}
