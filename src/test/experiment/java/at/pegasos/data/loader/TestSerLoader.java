package at.pegasos.data.loader;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import at.pegasos.data.Data;
import at.pegasos.data.DataException;
import at.pegasos.data.simplify.MergeKeepNA;

public class TestSerLoader {
  
  @Test
  public void test() throws DataException
  {
    Data data= SerLoader.create(getClass().getResource("/2019-07-30_231803.ser").getFile())
      .useSensorNumbers(true)
      .loadFile()
      .getData();
    
    data.simplify(MergeKeepNA.class);
    
    assertEquals(3, data.getColumnNames().length);
  }
}