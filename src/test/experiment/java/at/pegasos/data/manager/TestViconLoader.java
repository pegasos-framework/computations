package at.pegasos.data.manager;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import at.pegasos.data.DataException;
import at.pegasos.data.manager.ViconDataManager;

public class TestViconLoader {
  
  private String fn;
  
  @Before
  public void setUp() throws Exception
  {
    // Somehow if we pass the string directly there is some substitution going on ...?
    fn= getClass().getResource("/vicontest.csv").getFile();
  }

  @Test
  public void accepts()
  {
    ViconDataManager mgr= new ViconDataManager();
    assertEquals(true, mgr.acceptFile(fn));
  }

  @Test
  public void process() throws DataException
  {
    ViconDataManager mgr= new ViconDataManager();
    assertEquals(true, mgr.acceptFile(fn));
    mgr.processFile();
    
    mgr.normalise();
    
    mgr.data.debug();
  }
}
