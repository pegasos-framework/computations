package at.pegasos.data.manager;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import at.pegasos.data.Data;
import at.pegasos.data.DataException;

public class TestGoldenCheetahManager {
  
  private String fn;
  
  @Before
  public void setUp() throws Exception
  {
    fn= getClass().getResource("/2018_07_07_18_18_38.csv").getFile();
  }

  @Test
  public void accepts()
  {
    GoldenCheetahDataManager mgr= new GoldenCheetahDataManager();
    assertEquals(true, mgr.acceptFile(fn));
  }

  @Test
  public void all() throws DataException
  {
    GoldenCheetahDataManager mgr= new GoldenCheetahDataManager();
    assertEquals(true, mgr.acceptFile(fn));
    mgr.processFile();
    mgr.normalise();

    Data data= mgr.getData();
//    data.printTable();

    assertEquals(true, mgr.hasStartTime());
    assertEquals(false, mgr.hasPersonInfo());

    assertEquals(true, data.hasColumn("distance_m"));
    assertEquals(false, data.hasColumn("alt"));
  }
}
