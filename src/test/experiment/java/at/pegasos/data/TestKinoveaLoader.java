package at.pegasos.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.pegasos.data.manager.KinoveaDataManager;

public class TestKinoveaLoader {
  
  private at.pegasos.data.manager.DataManager manager;
  private String fileName;

  @Before
  public void setUp() throws Exception
  {
    manager= new KinoveaDataManager();
    fileName= new File(getClass().getResource("CMJ.txt").getFile()).getAbsolutePath();
  }

  @After
  public void tearDown() throws Exception
  {
  }

  @Test
  public void testAcceptsFile()
  {
    try
    {
      assertEquals(true, manager.acceptFile(fileName));
    }
    catch( DataException e )
    {
      e.printStackTrace();
      fail("Unexpected Exception");
    }
  }
}
