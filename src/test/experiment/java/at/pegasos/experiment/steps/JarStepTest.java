package at.pegasos.experiment.steps;

import at.pegasos.computer.*;
import at.pegasos.computer.providers.*;
import at.pegasos.data.*;
import at.pegasos.experiment.*;
import at.pegasos.formula.*;
import org.junit.*;
import org.junit.rules.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import static org.junit.Assert.*;

public class JarStepTest {
  @Rule
  public TemporaryFolder folder= new TemporaryFolder();

  @Test
  public void generated_jar_computations_load() throws IOException, ExperimentException, ComputerBuilderException
  {
    File spec = folder.newFile("experiment.yml");
    BufferedWriter writer= Files.newBufferedWriter(Paths.get(spec.getAbsolutePath()));
    String h= "steps:\n" + "  - parse\n" + "  - jar\n";
    writer.write(h);
    writer.close();

    File formulas= folder.newFile("formulas.txt");
    writer= Files.newBufferedWriter(Paths.get(formulas.getAbsolutePath()));
    h= "test = area(data[a])\n";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(spec);
    assertTrue(parser.parse());

    Runner runner = new Runner();
    runner.setSteps(parser.getSteps());
    runner.setRunDirectory(folder.getRoot().toPath());

    runner.run();

    Path jarFile = folder.getRoot().toPath().resolve("formulas.jar");

    assertTrue(Files.exists(jarFile));

    ComputerBuilder builder = new ComputerBuilder(Data.createEmpty())
        .setUserValueProvider(new DummyUserValuesProvider())
        .addComputationPackage(jarFile.toString(), GetGenerator.DEFAULT_PACKAGENAME);
    builder.getComputer();
    Set<Computation> res = builder.getUnresolvedComputations();
    assertEquals(1, res.size());
  }
}