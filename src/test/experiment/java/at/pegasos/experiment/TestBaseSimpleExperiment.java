package at.pegasos.experiment;

import static org.junit.Assert.assertEquals;

import java.io.*;
import java.nio.file.*;
import java.text.ParseException;

import org.junit.*;
import org.junit.rules.TemporaryFolder;

import at.pegasos.data.*;
import at.pegasos.data.manager.DataManager.NoStarttimeException;
import at.pegasos.data.manager.GoldenCheetahDataManager;

public class TestBaseSimpleExperiment {
  @Rule
  public TemporaryFolder folder= new TemporaryFolder();

  @Test
  public void testLoadFolders() throws IOException, ParseException, NoStarttimeException, DataException
  {
    BaseSimpleExperiment exp= new SimpleExperiment(folder.getRoot().toPath());
    exp.addInputFolder(folder.getRoot().getAbsolutePath());

    // this file should not be accepted
    folder.newFile("test.csv");
    // this file should be accepted
    File test= folder.newFile("2020_07_07_18_18_18.csv");
    BufferedWriter writer= Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h= "secs,km,power,hr,cad,alt\n" + 
        "0,0,0,91,60,\n" + 
        "1,0.0027,110,91,60,\n" + 
        "3,0.0084,120,94,61,\n";
    writer.write(h);
    writer.close();
    // this file should also be accepted
    test= folder.newFile("2020_18_18_18_18_18.csv");
    writer= Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write(h);
    writer.close();

    exp.addManager(new GoldenCheetahDataManager());

    exp.preprocessData();
    
    assertEquals(2, exp.files.size());
    assertEquals(GoldenCheetahDataManager.GoldenCheetahCsvFormat.parse("2020_07_07_18_18_18").getTime(), 
        exp.files.get(0).getStartTime());
//    assertEquals()
  }
  
  @Test
  public void finishingEmptyExperimentDoesNotFail()
  {
    BaseSimpleExperiment exp= new SimpleExperiment(folder.getRoot().toPath());
    exp.output= new Data();
    exp.finishExperiment();
  }
}
