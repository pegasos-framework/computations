package at.pegasos.experiment;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import at.pegasos.computer.Computer;
import at.pegasos.computer.UserValueComputation;
import at.pegasos.computer.providers.UserValueProvider;
import at.pegasos.computer.ValueComputation;
import org.apache.log4j.BasicConfigurator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import at.pegasos.data.Data.ColumnIterator;
import at.pegasos.data.*;
import at.pegasos.data.manager.DataManager;
import at.pegasos.data.manager.DataManager.NoStarttimeException;

public class TestMultiUserExperiment {
  @Rule
  public TemporaryFolder folder= new TemporaryFolder();
  
  private static class MockManager extends DataManager {

    private static int pIdx= 0;
    private static int dIdx= 0;
    
    private int person;
    private long date;

    @Override
    public DataManager getInstance()
    {
      return new MockManager();
    }
    
    public MockManager()
    {
      person= pIdx++;
      date= (dIdx++) * 86_400_000;
    }

    @Override
    public boolean acceptFile(String filename) throws DataException
    {
      initFileName(filename);
      return true;
    }

    @Override
    public void processFile() throws DataException
    {
    }

    @Override
    public void normalise() throws DataException
    {
      // data= new Data();
    }

    @Override
    public long getStartTime() throws NoStarttimeException
    {
      return date;
    }

    @Override
    public boolean hasStartTime()
    {
      return true;
    }

    @Override
    public String getPerson() throws NoPersonInfoException
    {
      return "" + person;
    }

    @Override
    public boolean hasPersonInfo()
    {
      return true;
    }
  }

  @Test
  public void testLoadFolders() throws IOException, ParseException, NoStarttimeException, DataException
  {
    BasicConfigurator.configure();
    // System.out.println(Calendar.getInstance().getTimeInMillis());
    MultiUserExperiment exp= new MultiUserExperiment(folder.getRoot().toPath());
    exp.addInputFolder(folder.getRoot().getAbsolutePath());
    exp.addSessionComputation(new ValueComputation() {
      
      private Value result;
      @Override
      public Value getResult()
      {
        return result;
      }
      @Override
      public String getName()
      {
        return "Test";
      }
      @Override
      public Collection<String> getDependencies()
      {
        return null;
      }
      @Override
      public void compute(Computer computer)
      {
        result= new NumberValue("TestValue", Integer.parseInt(computer.getUserValueProvider().getUser()));
      }
    });
    exp.addUserComputation(new UserValueComputation() {
      
      private TimedValue result;
      @Override
      public TimedValue getResult()
      {
        return result;
      }
      @Override
      public String getName()
      {
        return "TestX";
      }
      @Override
      public Collection<String> getDependencies()
      {
        return null;
      }
      @Override
      public void compute(Computer computer)
      {
        System.out.println("compute x " + BaseSimpleExperiment.DateFormat.format(computer.getDate().getTime()));
        result= new TimedValue(
            computer.getDate().getTimeInMillis() + 1000 - 86_400_000,
            new NumberValue("TestValueX", Integer.parseInt(computer.getUserValueProvider().getUser())));
      }
    });

    // these files will be accepted by our manager
    folder.newFile("test1.csv");
    folder.newFile("test2.csv");

    exp.addManager(new MockManager());

    exp.run();
    
    // Two files processed
    assertEquals(2, exp.files.size());
    
    // this means two output rows
    assertEquals(2, exp.getResult().getRowCount());
    
    ColumnIterator it= exp.getResult().iterateTableColumn("TestValue");
    int expected= 1;
    while( it.hasNext())
    {
      assertEquals(expected++, Double.valueOf((String) it.next()).intValue());
    }
    
    UserValueProvider uvp= exp.getUserValueProvider();
    uvp.setUser("1");
    List<TimedValue> vals= uvp.fetchValues("TestValueX", 0, Long.MAX_VALUE);
    assertEquals(2, vals.size());
    assertEquals(0, vals.get(1).getEnd());
    assertEquals(86_400_000, vals.get(0).getStart());
    assertEquals(172_800_000, vals.get(1).getStart());

    Calendar date= Calendar.getInstance();
    uvp.setUser("1");
    TimedValue val= uvp.fetchValue("TestValueX", date);
    assertEquals(0, val.getEnd());
    assertEquals(172_800_000, val.getStart());
  }
}
