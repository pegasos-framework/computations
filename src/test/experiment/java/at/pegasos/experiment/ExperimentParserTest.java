package at.pegasos.experiment;

import at.pegasos.experiment.steps.*;
import org.apache.log4j.*;
import org.junit.*;
import org.junit.rules.*;

import java.io.*;
import java.nio.file.*;

import static org.junit.Assert.*;

public class ExperimentParserTest {

  @Rule public TemporaryFolder folder = new TemporaryFolder();
  private File test;

  @Before public void setUp() throws IOException
  {
    BasicConfigurator.configure();
    test = folder.newFile();
  }

  @Test public void parse_empty_fails() throws IOException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertThrows(ExperimentException.class, parser::parse);
  }

  @Test public void parse_empty_steps() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    parser.parse();
    assertEquals(0, parser.getSteps().size());
  }

  @Test public void parse_parse_without_params_works() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:\n  - parse\n";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof ParseAndCompileStep);
    ParseAndCompileStep s = (ParseAndCompileStep) parser.getSteps().get(0);
    assertEquals("formulas.txt", s.getParserParameters().inputfile);
  }

  @Test public void parse_parse_with_params() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:\n  - parse --input formulas.txt\n";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof ParseAndCompileStep);
  }

  @Test public void parse_jar_of_parse() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:\n  - parse --input formulas.txt\n  - jar output.jar";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(2, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof ParseAndCompileStep);
    assertTrue(parser.getSteps().get(1) instanceof JarStep);

    /*ParseAndCompileStep parseStep = (ParseAndCompileStep) parser.getSteps().get(0);
    JarStep jarStep = (JarStep) parser.getSteps().get(1);

    assertEquals(parseStep.getParserParameters().);*/
  }

  @Test public void parse_jar() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:\n  - jar output.jar --indir nonsense";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof JarStep);
  }

  @Test public void parse_simple() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:\n  - exp\n" + "exp:\n";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof BaseSimpleExperiment);
  }

  @Test public void parse_singleuser() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:\n  - exp\n" + "exp:\n" + "  type: singleuser";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof SingleUserExperiment);
  }

  @Test public void parse_multiuser() throws IOException, ExperimentException
  {
    File test = folder.newFile("experiment.yml");
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    String h = "steps:\n  - exp\n" + "exp:\n" + "  type: multiuser\n" + "  pattern: nonsense\n";
    writer.write(h);
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof MultiUserExperiment);
    assertNotNull(((MultiUserExperiment) parser.getSteps().get(0)).getPattern());
  }

  @Test public void noSteps() throws IOException, ExperimentException
  {
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write("steps:");
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertFalse(parser.parse());
    assertEquals(0, parser.getSteps().size());
  }

  @Test public void parseStep() throws IOException, ExperimentException
  {
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write("steps:\n");
    writer.write("  - parse test\n");
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof ParserStepInterface);
  }

  @Test public void parseUnknownStep() throws IOException, ExperimentException
  {
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write("steps:\n");
    writer.write("  - unknown test.txt\n");
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertFalse(parser.parse());
    assertEquals(0, parser.getSteps().size());
  }

  @Test public void parseParseJarStep() throws IOException, ExperimentException
  {
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write("steps:\n");
    writer.write("  - parse test.txt\n");
    writer.write("  - jar test.jar\n");
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(2, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof ParserStepInterface);
    assertTrue(parser.getSteps().get(1) instanceof JarStep);
  }

  @Test public void parseParseJarStepParams() throws IOException, ExperimentException
  {
    BufferedWriter writer = Files.newBufferedWriter(Paths.get(test.getAbsolutePath()));
    writer.write("steps:\n");
    writer.write("  - jar --out test.jar --indir baseDir --package pkg1\n");
    writer.close();

    ExperimentParser parser = new ExperimentParser(test);
    assertTrue(parser.parse());
    assertEquals(1, parser.getSteps().size());
    assertTrue(parser.getSteps().get(0) instanceof JarStep);
    JarStep j = (JarStep) parser.getSteps().get(0);
    assertEquals("test.jar", j.getOutputName());
  }
}