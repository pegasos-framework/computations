rmdir /Q /S testout

java -cp lib/parser-0.0.1.jar at.pegasos.formula.Formula2Class --input stunde.in > generator.txt

cd testout
dir /s /b *.java > ../tmpsources.txt
cd ..
javac @tmpsources.txt -cp lib/computer-0.0.1.jar >> compile.txt 2>> compile.txt
del tmpsources.txt

java -cp testout:lib/guava-28.1-jre.jar:lib/javassist-3.26.0-GA.jar:lib/reflections-0.9.10.jar:lib/jcommander-1.7.jar:lib/computer-0.0.1.jar at.pegasos.experiment.CSVExperiment --user test > experiment.txt

PAUSE

